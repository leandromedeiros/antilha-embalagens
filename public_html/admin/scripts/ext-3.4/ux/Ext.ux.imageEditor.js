/*!
 * Ext.ux.imageEditor | 10.09.2010
 *
 * @class		Ext.ux.imageEditor
 * @extends		Ext.Panel
 * @copyright	Copyright 2010, eyeworkers interactive GmbH
 * @author		tim <tim@eyeworkers.de>
 * @version		$Id: Ext.ux.imageEditor.js 779 2010-10-10 11:14:09Z tim $
 *
 * Ext.ux.imageEditor is licensed under the terms of the
 *                  GNU Open Source GPL 3.0
 * license.
 *
 * Commercial use is prohibited. Contact the author
 * if you need to obtain a commercial license.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
 *
 */

Ext.ux.imageEditor = Ext.extend(Ext.Panel, {

	image: '', 
	urlUpload: '',
	idImagem: '',
	tamanhos: {},
	thumb: '',
	initComponent: function() {
		
		var config = {
			layout: 'border',
		 	id: 'cmpPicturePanel',
		 	autoWidth: true,
		 	height: 500,
		 	disabled: true,
		 	items: [{	
				html: '<div style="position:relative;" id="pictureContainer"></div>',
				region: 'center',
				bodyStyle: 'background: url(img/bg_panel.png);',
				autoScroll: true,
				border: false
		 	}, {
				region: 'east',
				width: 300, 
				id: 'cmpPicturePanelEast',
				collapsible: true,
				collapsed: true,
				bodyStyle: 'padding: 10px;',
				layout: 'fit'		 					
			}],
		 	tbar: [{
				text: 'Iniciar corte da imagem',
				id: 'btnChooseCutArea',
				handler: this.addCutArea,
				scope: this,
				hidden: true,
				disabled: true,
				iconCls: 'cssChooseArea'
		 	}, 'Escolha o tamanho: ', {
				xtype: 'combo',
				id: 'comboTamanhos',
				store:  new Ext.data.SimpleStore({
					fields: ['valor', 'texto'],
					data: [[0,'Selecione um tamanho']]
				}),
				fieldLabel: 'Tamanho',
				hiddenName: 'comboTamanho',
				name: 'comboTamanho',
				valueField: 'valor',
				displayField: 'texto',
				value: '0',
				mode: 'local',
				allowBlank: false,
				typeAhead: true,
				triggerAction: 'all',
				editable: false,
				anchor: '100%',
				listeners: {
					select: function(combo, data) {
		 				if(combo.getValue() == 0) {
		 					Ext.Msg.show({
								title: 'Selecione o tamanho',
								msg: 'Selecione o tamanho para cortar a imagem',
								icon: Ext.Msg.ERROR,
								buttons: Ext.Msg.OK
		 					});
		 				} else {
		 					var arrayDados = combo.getValue().split('-')
  		 					if(Ext.ux.imageEditor.imageStatus.width < arrayDados[0] || Ext.ux.imageEditor.imageStatus.height < arrayDados[1]) {
		 						combo.reset();
		 						Ext.Msg.show({
									title: 'Tamanho da Imagem',
									msg: 'O tamanho da imagem � menor que o tamanho que voc� quer cortar, aumente o tamanhop da imagem.',
									icon: Ext.Msg.ERROR,
									buttons: Ext.Msg.OK
			 					});
		 					} else {
			 					this.thumb = data.get('texto');
			 					this.addCutArea(arrayDados[0], arrayDados[1]);
		 					}
		 				}
					},
				scope: this
				}
			}, {
				text: 'Cortar imagem',
				id: 'btnCut',
				handler: this.cutImage,
				scope: this,
				disabled: true,
				iconCls: 'cssCut'
		 	}, '-' , {
				text: 'Redimensionar',
				id: 'btnScale',
				handler: this.prepareScaling,
				disabled: false,
				iconCls: 'cssScale'
		 	}, '-' , {
				text: 'Girar',
				id: 'btnRotate',
				handler: this.prepareRotate,
				disabled: false,
				iconCls: 'cssRotate'
		 	}, '-' , {
				text: 'place text',
				id: 'btnText',
				hidden: true,
				handler: this.prepareText,
				disabled: false,
				conCls: 'cssAddText'
			}, {
				text: 'Salvar',
				id: 'btnSave',
				handler: this.prepareSave,
				scope: this,
				disabled: false,
				iconCls: 'cssSave'
			}, '->' , {	
				xtype: 'tbtext',
				text: '<span style="font-weight:bold;" id="tBarTextOriginalSize"></span>'
		 	}, '-' , {	
				xtype: 'tbtext',
				text: '<span style="font-weight:bold;" id="tBarTextCurrentSize"></span>'
			}]
		};

		// apply config
		Ext.apply(this, config);
		Ext.apply(this.initialConfig, config);

		// call parent
		Ext.ux.imageEditor.superclass.initComponent.apply(this);
	},
	
	afterRender: function() {
		Ext.ux.imageEditor.superclass.afterRender.apply(this);
		this.setupPictureContainer();
		
		var store = Ext.getCmp('comboTamanhos').getStore();
		if(!Ext.isEmpty(this.tamanhos)) {
			for (var tamanhoImagem in this.tamanhos) {
				var rData = Ext.data.Record.create(store.fields);
				var newRecord = new rData({
					valor: this.tamanhos[tamanhoImagem].w + '-' + this.tamanhos[tamanhoImagem].h ,
					texto: tamanhoImagem + '(' + this.tamanhos[tamanhoImagem].w + 'x' + this.tamanhos[tamanhoImagem].h + ')'
				});						
				store.addSorted(newRecord);
			}
		}
	},
	
	cleanUpUtils: function() {
		(Ext.getCmp('manipulationForm') != undefined) ? Ext.getCmp('manipulationForm').destroy() : null;
		Ext.getCmp('cmpPicturePanelEast').collapse();
	},
	
	setupPictureContainer: function () {
		this.cleanUpUtils();
		this.enable();
		
		Ext.ux.imageEditor.imageStatus = {};
		
		Ext.getCmp('btnChooseCutArea').enable();
		
		var img = new Image();
		img.onload = function(){
			 
			// put the canvas stuff in - not in IE! 
			if (!Ext.isIE) {			
				objImage = Ext.get(img);	
			  
				// update tBar with original size
				Ext.get('tBarTextOriginalSize').update('original: '+objImage.dom.width+'x'+objImage.dom.height);
				  
				// add a canvas and drwa the image w/o manipulation
				Ext.get('pictureContainer').update('<canvas id="canvas" width="'+objImage.dom.width+'" height="'+objImage.dom.height+'"></canvas>');
				var ctx = document.getElementById('canvas').getContext('2d');
			    ctx.drawImage(img,0,0);
			      
			    // store the current canvas and the initial size of the picture
			    Ext.ux.imageEditor.imageStatus.canvas = ctx.canvas;
			    // keep the "first" canvas - it has the best quality and is used in scaling as reference
			    Ext.ux.imageEditor.imageStatus.canvasOriginalQuality = ctx.canvas;
			    Ext.ux.imageEditor.imageStatus.width = objImage.dom.width;
			    Ext.ux.imageEditor.imageStatus.height = objImage.dom.height;
			    Ext.ux.imageEditor.imageStatus.initialWidth = objImage.dom.width;
			    Ext.ux.imageEditor.imageStatus.initialHeight = objImage.dom.height;
			    Ext.ux.imageEditor.imageStatus.ratio =  Ext.ux.imageEditor.imageStatus.width / Ext.ux.imageEditor.imageStatus.height;
			    
			    // enable buttons - they may have been disabled before 
			    Ext.getCmp('btnScale').enable();
				Ext.getCmp('btnSave').enable();
				Ext.getCmp('btnRotate').enable();
				Ext.getCmp('btnText').enable();
			    
			}
			// when IE, put an image tag in the displaying panel and disable all img editing stuff
			// pic can be tagged but not edited.
			else {
				Ext.getCmp('cmpPicturePanel').getTopToolbar().disable();
				Ext.getCmp('cmpPicturePanelEast').disable();
				Ext.get('pictureContainer').update('<img src="'+Ext.ux.imageEditor.imageStatus.path+'"/>');
			} 
		}
		img.src = this.image;
	},
	
	addCutArea	: function (width, height) {
		this.cleanUpUtils();
		
		Ext.getCmp('btnChooseCutArea').disable();
		Ext.getCmp('btnScale').disable();
		Ext.getCmp('btnSave').disable();
		Ext.getCmp('btnRotate').disable();
		Ext.getCmp('btnText').disable();
		
		Ext.getCmp('btnCut').enable();
		// put in the div for the resizable
		
		if(Ext.get('myResizable')) {
			Ext.get('pictureContainer').update('<img src="'+this.image+'"/>');
		} 
		
		// and add the resizable
		Ext.DomHelper.append(Ext.get('pictureContainer'), '<div id="myResizable" style="width: '+width+'px; height: '+height+'px; background-color: white; position: absolute; top:0; left:0; opacity:.5; filter: alpha(opacity=50); -ms-filter:alpha(opacity=50); -khtml-opacity:.5; -moz-opacity:.5;"></div>');
		Ext.ux.imageEditor.resizer = new Ext.Resizable('myResizable', 
		{
		     wrap:true,
		     width: width,
		     enabled: false,
		     height: height,
		     id: 'resizer',
		     constrainTo : Ext.get('canvas'),
		     minWidth:212,
		     maxWidth:440,
		     pinned: true,
		     draggable: true,
		     minHeight: 126,
		     maxHeight: 330,
		     handles: 'all',
		     dynamic:true
		});
		
	},

	cutImage: function () {
		
		Ext.getCmp('btnScale').enable();
		Ext.getCmp('btnSave').enable();
		Ext.getCmp('btnRotate').enable();
		Ext.getCmp('btnText').enable();
				
		var resizerEl = Ext.ux.imageEditor.resizer.getEl().dom;		
		Ext.ux.imageEditor.imageStatus.top = resizerEl.offsetTop;
		Ext.ux.imageEditor.imageStatus.left = resizerEl.offsetLeft;
		Ext.ux.imageEditor.imageStatus.width = resizerEl.scrollWidth;
		Ext.ux.imageEditor.imageStatus.height = resizerEl.scrollHeight;
		Ext.ux.imageEditor.imageStatus.ratio = resizerEl.scrollWidth / resizerEl.scrollHeight;
		
		// update tbar
		this.updateTbarWithCurrentSize();		
		
		// update the canvas element (it is overwritten here....)				
		Ext.get('pictureContainer').update('<canvas id="canvas" width="'+Ext.ux.imageEditor.imageStatus.width+'" height="'+Ext.ux.imageEditor.imageStatus.height+'"></canvas>');
		
		var ctx = document.getElementById('canvas').getContext('2d');
	    ctx.drawImage(	
			Ext.ux.imageEditor.imageStatus.canvas,
			Ext.ux.imageEditor.imageStatus.left,
			Ext.ux.imageEditor.imageStatus.top,
			Ext.ux.imageEditor.imageStatus.width,
			Ext.ux.imageEditor.imageStatus.height,
			0,
			0,
			Ext.ux.imageEditor.imageStatus.width,
			Ext.ux.imageEditor.imageStatus.height
		);
		
		// store the current canvas state
		Ext.ux.imageEditor.imageStatus.canvas = ctx.canvas;
		// overwrite the "original" here - we now have a new size
		Ext.ux.imageEditor.imageStatus.canvasOriginalQuality = ctx.canvas;
		
		Ext.getCmp('btnChooseCutArea').enable();
		Ext.getCmp('btnCut').disable();
		Ext.getCmp('comboTamanhos').disable();
		
	},
	
	updateTbarWithCurrentSize: function () {
		Ext.get('tBarTextCurrentSize').update('alterado para: '+ Math.round(Ext.ux.imageEditor.imageStatus.width) + '*' + Math.round(Ext.ux.imageEditor.imageStatus.height) );
	},
	
	prepareRotate: function() {
		
		Ext.ux.imageEditor.imageStatus.canvasOriginalQuality = Ext.ux.imageEditor.imageStatus.canvas;
		
		var tip = new Ext.slider.Tip({
        	getText: function(slider){
            	return String.format('<b>{0}�</b>', slider.value);
        	}
    	});
		
		Ext.getCmp('cmpPicturePanelEast').setTitle('Girar...');
		
		var rotateForm = new Ext.FormPanel({
	        id: 'manipulationForm',
	        frame: false,
	        autoScroll: true,
	        border: false,
	        labelWidth: 100,
	        labelAlign: 'left',
	        items: [{
		        xtype: 'slider',
		        id: 'cmpRotate',
		        increment: 90,
		        width: 160,
		        minValue: 0,
		        maxValue: 360,
		        value: 0,
		        isFormField: true,
	       		fieldLabel: 'Graus',
	       		plugins: tip,
	       		listeners: {
	       			change: function(slider, value) {
	       				Ext.ux.imageEditor.prototype.doRotate(value);
	       			}
	       		}
	    	}]
		});
		
		// destroy old content - all forms in the utils have the same id
		(Ext.get('manipulationForm') != null) ? Ext.get('manipulationForm').remove() : null;
		
		// setup east util area
		var configurePanel = Ext.getCmp('cmpPicturePanelEast');
		
		configurePanel.add(rotateForm);
		configurePanel.expand();
		configurePanel.doLayout();
		
	},
	
	doRotate: function(degree) {
		switch (degree) {		
			case 90:	
				Ext.get('pictureContainer').update('<canvas id="canvas" width="'+Ext.ux.imageEditor.imageStatus.height+'" height="'+Ext.ux.imageEditor.imageStatus.width+'"></canvas>');
				var ctx = document.getElementById('canvas').getContext('2d');		
				ctx.rotate(degree * Math.PI / 180);
			    ctx.drawImage(Ext.ux.imageEditor.imageStatus.canvasOriginalQuality, 0, -Ext.ux.imageEditor.imageStatus.height);
			break;
			
			case 180:	
				Ext.get('pictureContainer').update('<canvas id="canvas" width="'+Ext.ux.imageEditor.imageStatus.width+'" height="'+Ext.ux.imageEditor.imageStatus.height+'"></canvas>');
				var ctx = document.getElementById('canvas').getContext('2d');		
				ctx.rotate(degree * Math.PI / 180);
			    ctx.drawImage(Ext.ux.imageEditor.imageStatus.canvasOriginalQuality ,-Ext.ux.imageEditor.imageStatus.width, -Ext.ux.imageEditor.imageStatus.height);
			break;
			
			case 270:	
				Ext.get('pictureContainer').update('<canvas id="canvas" width="'+Ext.ux.imageEditor.imageStatus.height+'" height="'+Ext.ux.imageEditor.imageStatus.width+'"></canvas>');
				var ctx = document.getElementById('canvas').getContext('2d');		
				ctx.rotate(degree * Math.PI / 180);
			    ctx.drawImage(Ext.ux.imageEditor.imageStatus.canvasOriginalQuality ,-Ext.ux.imageEditor.imageStatus.width ,0);
			break;
			
			case 360:
			case 0:	
				Ext.get('pictureContainer').update('<canvas id="canvas" width="'+Ext.ux.imageEditor.imageStatus.width+'" height="'+Ext.ux.imageEditor.imageStatus.height+'"></canvas>');
				var ctx = document.getElementById('canvas').getContext('2d');		
			    ctx.drawImage(Ext.ux.imageEditor.imageStatus.canvasOriginalQuality ,0 ,0);
			break;
		}

		// store the current canvas state
		Ext.ux.imageEditor.imageStatus.canvas = ctx.canvas;
		
	},
	
	prepareSave: function() {
		if(Ext.getCmp('comboTamanhos').getValue() == 0) {
			Ext.Msg.show({
				title: 'Selecione o tamanho',
				msg: 'Selecione o tamanho para cortar a imagem',
				icon: Ext.Msg.ERROR,
				buttons: Ext.Msg.OK
			});
		} else {	
			var data = document.getElementById('canvas').toDataURL("image/png");
			data= data.replace(/^data:image\/(png|jpg);base64,/, "");
			
			Ext.Ajax.request({
			   url: this.urlUpload,
			   params: { data: data, idImagem: this.idImagem, tamanho: this.thumb},
			   success: function(){
				   Ext.getCmp('windowCrop').close();
			   }
			});
		}
	},
	
	prepareScaling: function () {
		
		// read out the width, height and thus ratio of the current canvas - it could have
		// been rotated before!
		var ctx = document.getElementById('canvas').getContext('2d');
		Ext.ux.imageEditor.imageStatus.height = ctx.canvas.height;
		Ext.ux.imageEditor.imageStatus.width = ctx.canvas.width;
		Ext.ux.imageEditor.imageStatus.ratio =  Ext.ux.imageEditor.imageStatus.width / Ext.ux.imageEditor.imageStatus.height;
				
		Ext.ux.imageEditor.imageStatus.canvasOriginalQuality = Ext.ux.imageEditor.imageStatus.canvas;
		
		var tip = new Ext.slider.Tip({
        	getText: function(slider){
            	return String.format('<b>{0} Pixel</b>', slider.value);
        	}
    	});
				
		Ext.getCmp('cmpPicturePanelEast').setTitle('Redimensionar...');
				
		var scaleForm = new Ext.FormPanel({
	        id: 'manipulationForm',
	        frame: false,
	        autoScroll: true,
	        border: false,
	        labelWidth: 100,
	        labelAlign: 'left',
	        items: [{
	    		xtype: 'checkbox',
	    		id: 'cmpProportional',
	    		fieldLabel: 'Proporcional?',
	    		checked: true
	    	}, {
		        xtype: 'slider',
		        id: 'cmpSliderWidth',
		        width: 160,
		        minValue: 0,
		        maxValue: Ext.ux.imageEditor.imageStatus.width * 1.5,
		        value: Ext.ux.imageEditor.imageStatus.width,
		        isFormField: true,
	       		fieldLabel: 'Largura',
	       		plugins: tip,
	       		listeners: {
	       			change: function(slider, value) {
	       				if (Ext.getCmp('cmpProportional').getValue() == true) {
	       					var newHeight = Math.round( value / Ext.ux.imageEditor.imageStatus.ratio );
	       					Ext.getCmp('cmpSliderHeight').setValue(newHeight);
	       					Ext.getCmp('cmpTextfieldHeight').setValue(newHeight);	
	       				}
	       				Ext.getCmp('cmpTextfieldWidth').setValue(value);
	       				Ext.ux.imageEditor.prototype.doScaling(value, Ext.getCmp('cmpSliderHeight').getValue());
	       			}
	       		}
	    	}, {
		        xtype: 'slider',
		        id: 'cmpSliderHeight',
		        width: 160,
		        minValue: 0,
		        maxValue: Ext.ux.imageEditor.imageStatus.height * 1.5,
		        value: Ext.ux.imageEditor.imageStatus.height,
		        isFormField: true,
		        fieldLabel: 'Altura',
		        plugins: tip,
		        listeners: {
	       			change: function(slider, value) {
	       				if (Ext.getCmp('cmpProportional').getValue() == true) {
	       					var newWidth = Math.round( value * Ext.ux.imageEditor.imageStatus.ratio );						    				
		    				Ext.getCmp('cmpSliderWidth').setValue(newWidth);
		    				Ext.getCmp('cmpTextfieldWidth').setValue( newWidth);	
	       				}
	       				Ext.getCmp('cmpTextfieldHeight').setValue(value);
	       				Ext.ux.imageEditor.prototype.doScaling( Ext.getCmp('cmpSliderWidth').getValue(), value);
	       			}
	       		}
	    	}, {
	    		xtype: 'textfield',
	    		fieldLabel: 'Largura',
	    		width: 160,
	    		id: 'cmpTextfieldWidth',
	    		value: Ext.ux.imageEditor.imageStatus.width,
	    		enableKeyEvents: true,
	    		listeners: {
	    			keyup: function(o,e){
	    				var currentHeight = Ext.getCmp('cmpTextfieldHeight').getValue();
	    				if (Ext.getCmp('cmpProportional').getValue() == true) {	
		    				var newHeight = o.getValue() / Ext.ux.imageEditor.imageStatus.ratio;						    				
		    				Ext.getCmp('cmpTextfieldHeight').setValue( Math.round(newHeight) );
		    			}
		    			else {
		    				newHeight = currentHeight;
		    			}
	    				Ext.ux.imageEditor.prototype.doScaling( o.getValue(), newHeight );
	    			}
	    		}
	    	}, {
	    		xtype: 'textfield',
	    		fieldLabel: 'Altura',
	    		width: 160,
	    		id: 'cmpTextfieldHeight',
	    		value: Ext.ux.imageEditor.imageStatus.height,
	    		enableKeyEvents: true,
	    		listeners: {
	    			keyup: function(o,e){
	    				var currentWidth = Ext.getCmp('cmpTextfieldWidth').getValue();
	    				if (Ext.getCmp('cmpProportional').getValue() == true) {
		    				var newWidth = o.getValue() * Ext.ux.imageEditor.imageStatus.ratio;						    				
		    				Ext.getCmp('cmpTextfieldWidth').setValue( Math.round(newWidth) );
		    			}
		    			else {
		    				newWidth = currentWidth;
		    			}
	    				Ext.ux.imageEditor.prototype.doScaling( newWidth, o.getValue() );
	    			}
	    		}
	    	}]
		});
		
		// destroy old content - all forms in the utils have the same id
		(Ext.get('manipulationForm') != null) ? Ext.get('manipulationForm').remove() : null;
		
		// setup east util area
		var configurePanel = Ext.getCmp('cmpPicturePanelEast');
		
		configurePanel.add(scaleForm);
		configurePanel.expand();
		configurePanel.doLayout();
			
	},
	
	doScaling: function (width,height) {
		
		Ext.ux.imageEditor.imageStatus.width = width;
		Ext.ux.imageEditor.imageStatus.height = height;
		
		// update tbar
		this.updateTbarWithCurrentSize();
		
		// update canvas				
		Ext.get('pictureContainer').update('<canvas id="canvas" width="'+Ext.ux.imageEditor.imageStatus.width+'" height="'+Ext.ux.imageEditor.imageStatus.height+'"></canvas>');
		
		var ctx = document.getElementById('canvas').getContext('2d');
	    // ! canvasOriginalQuality is used a scaling basis
	    ctx.drawImage	(	
			Ext.ux.imageEditor.imageStatus.canvasOriginalQuality, 
			0,
			0, 
			Ext.ux.imageEditor.imageStatus.width, 
			Ext.ux.imageEditor.imageStatus.height 
		);
		
		// store the current canvas state
		Ext.ux.imageEditor.imageStatus.canvas = ctx.canvas;
	},
	
	prepareText: function() {
		
		Ext.ux.imageEditor.imageStatus.canvasOriginalQuality = Ext.ux.imageEditor.imageStatus.canvas;
		
		var tip = new Ext.slider.Tip({
        	getText: function(slider){
            	return String.format('<b>{0} pixel</b>', slider.value);
        	}
    	});
		
		Ext.getCmp('cmpPicturePanelEast').setTitle('place text...');
		
		var fontStore = new Ext.data.JsonStore({
			data:{ 
				items: [ 
					{id:'Arial', val:'Arial'},
					{id:'Arial Black', val:'Arial Black'},
					{id:'Comic Sans MS', val:'Comic Sans MS'},
					{id:'Courier New', val:'Courier New'},
					{id:'Franklin Gothic Medium', val:'Franklin Gothic Medium'},
					{id:'Georgia', val:'Georgia'},
					{id:'Lucida Console', val:'Lucida Console'},
					{id:'Lucida Sans Unicode', val:'Lucida Sans Unicode'},
					{id:'Microsoft Sans Serif', val:'Microsoft Sans Serif'},
					{id:'Palatino Linotype', val:'Palatino Linotype'},
					{id:'Tahoma', val:'Tahoma'},
					{id:'Times New Roman', val:'Times New Roman'},
					{id:'Trebuchet MS', val:'Trebuchet MS'},
					{id:'Verdana', val:'Verdana'}
				]																
			},
		    root: 'items',
			fields: ['id','val']
		});
		
		var sizeStore = [];
		for( j = 3; j < 50; j++ ) {
			sizeStore.push([j, j]);
		}
		
		var fontStyleStore = new Ext.data.JsonStore({
			data:{ 
				items:[ 
					{id:'normal', val:'normal'},
					{id:'italic', val:'italic'},
					{id:'oblique', val:'oblique'}
				]																
			},
		    root: 'items',
			fields: ['id','val']
		});

		var fontWeightStore = new Ext.data.JsonStore({
			data:{ 
				items:[ 
					{id:'normal', val:'normal'},
					{id:'bold', val:'bold'},
					{id:'bolder', val:'bolder'},
					{id:'lighter', val:'lighter'}
				]																
			},
		    root: 'items',
			fields: ['id','val']
		});

		var fontVariantStore = new Ext.data.JsonStore({
			data:{ 
				items: [ 
					{id:'normal', val:'normal'},
					{id:'small-caps', val:'small-caps'}
				]																
			},
		    root: 'items',
			fields: ['id','val']
		});
		
		var textForm = new Ext.FormPanel({
	        id: 'manipulationForm',
	        frame: false,
	        autoScroll: true,
	        border: false,
	        labelWidth: 100,
	        labelAlign: 'left',
	        items: [{
		        xtype: 'slider',
		        id: 'cmpTextXPos',
		        width: 160,
		        minValue: 0,
		        maxValue: Ext.ux.imageEditor.imageStatus.width,
		        value: Ext.ux.imageEditor.imageStatus.width / 2,
		        isFormField: true,
	       		fieldLabel: 'X-position',
	       		plugins: tip,
	       		listeners: {
	       			change: function(slider, value) {
	       				Ext.ux.imageEditor.prototype.placeText( 
   							Ext.getCmp('cmpTextPlaceValue').getValue(), 
   							Ext.getCmp('cmpTextColor').value, 
   							value, 
   							Ext.getCmp('cmpTextYPos').getValue(), 
   							Ext.getCmp('cmpTextFont').getValue(),
							Ext.getCmp('cmpTextSize').getValue(),
							Ext.getCmp('cmpFontStyle').getValue(),
							Ext.getCmp('cmpFontVariant').getValue(),
							Ext.getCmp('cmpFontWeight').getValue()
   						 );
	       			}
	       		}
	    	}, {
		        xtype: 'slider',
		        id: 'cmpTextYPos',
		        width: 160,
		        minValue: 0,
		        maxValue: Ext.ux.imageEditor.imageStatus.height,
		        value: Ext.ux.imageEditor.imageStatus.height / 2,
		        isFormField: true,
		        fieldLabel: 'Y-position',
		        plugins: tip,
		        listeners: {
	       			change: function(slider, value) {
	       				Ext.ux.imageEditor.prototype.placeText( 
   							Ext.getCmp('cmpTextPlaceValue').getValue(), 
   							Ext.getCmp('cmpTextColor').value, 
   							Ext.getCmp('cmpTextXPos').getValue(), 
   							value, 
   							Ext.getCmp('cmpTextFont').getValue(),
							Ext.getCmp('cmpTextSize').getValue(),
							Ext.getCmp('cmpFontStyle').getValue(),
							Ext.getCmp('cmpFontVariant').getValue(),
							Ext.getCmp('cmpFontWeight').getValue()
   						 );
	       			}
	       		}
	    	}, {
	    		xtype: 'textfield',
	    		fieldLabel: 'text',
	    		width: 160,
	    		id: 'cmpTextPlaceValue',
	    		value: '',
	    		enableKeyEvents: true,
	    		listeners: {
	    			keyup: function(o,e){
	    				Ext.ux.imageEditor.prototype.placeText(
							o.getValue(), 
							Ext.getCmp('cmpTextColor').value, 
							Ext.getCmp('cmpTextXPos').getValue(), 
							Ext.getCmp('cmpTextYPos').getValue(), 
							Ext.getCmp('cmpTextFont').getValue(),
							Ext.getCmp('cmpTextSize').getValue(),
							Ext.getCmp('cmpFontStyle').getValue(),
							Ext.getCmp('cmpFontVariant').getValue(),
							Ext.getCmp('cmpFontWeight').getValue()
						 );
	    			}
	    		}
	    	}, {
	    		xtype: 'colorpalette',
	    		isFormField: true,
	    		id: 'cmpTextColor',
	    		fieldLabel: 'textcolor',
	    		value: '000000',
	    		listeners: {
	    			select: function(cmp, color) {
	    				Ext.ux.imageEditor.prototype.placeText(	
							Ext.getCmp('cmpTextPlaceValue').getValue(), 
							color, 
							Ext.getCmp('cmpTextXPos').getValue(), 
							Ext.getCmp('cmpTextYPos').getValue(), 
							Ext.getCmp('cmpTextFont').getValue(),
							Ext.getCmp('cmpTextSize').getValue(),
							Ext.getCmp('cmpFontStyle').getValue(),
							Ext.getCmp('cmpFontVariant').getValue(),
							Ext.getCmp('cmpFontWeight').getValue()
						 );
	    			}
	    		}
	    	}, {
		        xtype:'combo',
		        id: 'cmpTextFont',
				store: fontStore,
	    		typeAhead: false,
	    		isFormField: true,
	    		fieldLabel: 'font',
	    		emptyText:'',
			    forceSelection: true,
				triggerAction: 'all',
				selectOnFocus:true,
				valueField: 'id',
				value: 'Arial',
				displayField: 'val',
				mode:'local',
				allowBlank: false,
				tpl: new Ext.XTemplate('<tpl for=".">',
					                        '<div class="x-combo-list-item" style="font-family: {val};">',
					                            '{val}',
					                        '</div>',
					                    '</tpl>'),
				listeners: {
					select: function(combo,record,number){
						Ext.ux.imageEditor.prototype.placeText(	
							Ext.getCmp('cmpTextPlaceValue').getValue(), 
							Ext.getCmp('cmpTextColor').value, 
							Ext.getCmp('cmpTextXPos').getValue(), 
							Ext.getCmp('cmpTextYPos').getValue(), 
							record.data.val,
							Ext.getCmp('cmpTextSize').getValue(),
							Ext.getCmp('cmpFontStyle').getValue(),
							Ext.getCmp('cmpFontVariant').getValue(),
							Ext.getCmp('cmpFontWeight').getValue()
						 );
					}
				}
		    }, {
		        xtype:'combo',
		        id: 'cmpTextSize',
				store: 	sizeStore,
	    		typeAhead: false,
	    		isFormField: true,
	    		fieldLabel: 'font size (px)',
			    forceSelection: true,
				triggerAction: 'all',
				selectOnFocus:true,
				mode:'local',
				value: '20',
				allowBlank: false,
				listeners: {
					select: function(combo,record,number){										
						Ext.ux.imageEditor.prototype.placeText(	
							Ext.getCmp('cmpTextPlaceValue').getValue(), 
							Ext.getCmp('cmpTextColor').value, 
							Ext.getCmp('cmpTextXPos').getValue(), 
							Ext.getCmp('cmpTextYPos').getValue(), 
							Ext.getCmp('cmpTextFont').getValue(),
							record.data.field1,
							Ext.getCmp('cmpFontStyle').getValue(),
							Ext.getCmp('cmpFontVariant').getValue(),
							Ext.getCmp('cmpFontWeight').getValue()
						 );
					}
				}
		  }, {
		        xtype:'combo',
		        id: 'cmpFontStyle',
				store: 	fontStyleStore,
	    		typeAhead: false,
	    		isFormField: true,
	    		fieldLabel: 'style',
			    forceSelection: true,
				triggerAction: 'all',
				selectOnFocus:true,
				mode:'local',
				value: 'normal',
				allowBlank: false,
				displayField: 'val',
				valueField: 'id',
				listeners: {
					select: function(combo,record,number){
						Ext.ux.imageEditor.prototype.placeText(	
							Ext.getCmp('cmpTextPlaceValue').getValue(), 
							Ext.getCmp('cmpTextColor').value, 
							Ext.getCmp('cmpTextXPos').getValue(), 
							Ext.getCmp('cmpTextYPos').getValue(), 
							Ext.getCmp('cmpTextFont').getValue(),
							Ext.getCmp('cmpTextSize').getValue(),
							record.data.val,
							Ext.getCmp('cmpFontVariant').getValue(),
							Ext.getCmp('cmpFontWeight').getValue()
						 );
					}
				}
		  }, {
		        xtype:'combo',
		        id: 'cmpFontVariant',
				store: 	fontVariantStore,
	    		typeAhead: false,
	    		isFormField: true,
	    		fieldLabel: 'font variant',
			    forceSelection: true,
				triggerAction: 'all',
				selectOnFocus:true,
				mode:'local',
				value: 'normal',
				allowBlank: false,
				displayField: 'val',
				valueField: 'id',
				listeners: {
					select: function(combo,record,number){
						Ext.ux.imageEditor.prototype.placeText(	
							Ext.getCmp('cmpTextPlaceValue').getValue(), 
							Ext.getCmp('cmpTextColor').value, 
							Ext.getCmp('cmpTextXPos').getValue(), 
							Ext.getCmp('cmpTextYPos').getValue(), 
							Ext.getCmp('cmpTextFont').getValue(),
							Ext.getCmp('cmpTextSize').getValue(),
							Ext.getCmp('cmpFontStyle').getValue(),
							record.data.val,
							Ext.getCmp('cmpFontWeight').getValue()
						);
					}
				}
		  }, {
	        xtype:'combo',
	        id: 'cmpFontWeight',
			store: 	fontWeightStore,
    		typeAhead: false,
    		isFormField: true,
    		fieldLabel: 'font weight',
		    forceSelection: true,
			triggerAction: 'all',
			selectOnFocus:true,
			mode:'local',
			value: 'normal',
			allowBlank: false,
			displayField: 'val',
			valueField: 'id',
			listeners: {
				select: function(combo,record,number){
					Ext.ux.imageEditor.prototype.placeText(	
						Ext.getCmp('cmpTextPlaceValue').getValue(), 
						Ext.getCmp('cmpTextColor').value, 
						Ext.getCmp('cmpTextXPos').getValue(), 
						Ext.getCmp('cmpTextYPos').getValue(), 
						Ext.getCmp('cmpTextFont').getValue(),
						Ext.getCmp('cmpTextSize').getValue(),
						Ext.getCmp('cmpFontStyle').getValue(),
						Ext.getCmp('cmpFontVariant').getValue(),
						record.data.val
					);
				}
			}
		  }]
		});
		
		// destroy old content - all forms in the utils have the same id
		(Ext.get('manipulationForm') != null) ? Ext.get('manipulationForm').remove() : null;
		
		// setup east util area
		var configurePanel = Ext.getCmp('cmpPicturePanelEast');
		
		configurePanel.add(textForm);
		configurePanel.expand();
		configurePanel.doLayout();
		
		
	},
	
	placeText: function (strText, strColor, intXPos, intYPos, strFontFamily, strFontSize, strFontStyle, strFontVariant, strFontWeight) {
		
		Ext.get('pictureContainer').update('<canvas id="canvas" width="'+Ext.ux.imageEditor.imageStatus.width+'" height="'+Ext.ux.imageEditor.imageStatus.height+'"></canvas>');
		
		var ctx = document.getElementById('canvas').getContext('2d');
		ctx.drawImage(Ext.ux.imageEditor.imageStatus.canvasOriginalQuality ,0 ,0);

		ctx.font = strFontStyle + ' ' + strFontVariant + ' ' + strFontWeight + ' ' + strFontSize + 'px ' + strFontFamily;			    				
		ctx.fillStyle = '#'+strColor;
		ctx.fillText(strText, intXPos, intYPos);
		
		// store the current canvas state
		Ext.ux.imageEditor.imageStatus.canvas = ctx.canvas;
	}
	
});

Ext.reg('imageEditor', Ext.ux.imageEditor);
