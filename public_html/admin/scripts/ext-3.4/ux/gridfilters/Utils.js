Ext.override(Ext.grid.RowSelectionModel, {
	selectRow : function(index, keepExisting, preventViewNotify, fireEvent){
		fireEvent = typeof fireEvent !== 'undefined' ? fireEvent : true;
	    if(this.isLocked() || (index < 0 || index >= this.grid.store.getCount()) || (keepExisting && this.isSelected(index))){
	        return;
	    }
	    var r = this.grid.store.getAt(index);
	    if(r && this.fireEvent('beforerowselect', this, index, keepExisting, r) !== false){
	        if(!keepExisting || this.singleSelect){
	            this.clearSelections();
	        }
	        this.selections.add(r);
	        this.last = this.lastActive = index;
	        if(!preventViewNotify){
	            this.grid.getView().onRowSelect(index);
	        }
	        if(!this.silent){
	            this.fireEvent('rowselect', this, index, r);
	            if(fireEvent) this.fireEvent('selectionchange', this);
	        }
	    }
	},
	selectAll : function(){
        if(this.isLocked()){
            return;
        }
        this.selections.clear();
        for(var i = 0, len = this.grid.store.getCount(); i < len; i++){
            this.selectRow(i, true, false, false);
        }
    }
});


function filtrar(grid, retorno){
	grid.filters.clearValues();
	for (var vFiltro in retorno.filtros) {
		var campoFiltro = vFiltro;
		
		if(vFiltro.indexOf('Fim') != '-1') {
			campoFiltro = vFiltro.substr(0, vFiltro.length - 3);
		} else if(vFiltro.indexOf('Inicio') != '-1') {
			campoFiltro = vFiltro.substr(0, vFiltro.length - 6);
		}
		
		var configFilter = grid.filters.getFilter(campoFiltro);
		switch(configFilter.type){
			case 'numeric':
				if(retorno.filtros[vFiltro].length) {
					var valorAtual = configFilter.getValue();
					var numeroAtual = '';
					for (var vAtual in valorAtual) {
						numeroAtual = valorAtual[vAtual] + '';
					} 
					
					if(vFiltro.indexOf('Fim') != '-1') {
						if(numeroAtual.length) { 
							novoValor = {
								lt : retorno.filtros[vFiltro],
								gt: numeroAtual
							};
						} else {
							novoValor = {lt : retorno.filtros[vFiltro]};
						}
					} else {
						if(numeroAtual.length) { 
							novoValor = {
								gt : retorno.filtros[vFiltro],
								lt: numeroAtual
							};
						} else {
							novoValor = {gt : retorno.filtros[vFiltro]};
						}
					}
					
					configFilter.setValue(novoValor);
					configFilter.setActive(true);
				}
			break;
			
			case 'string':
			case 'set':
			case 'list':
				if(retorno.filtros[vFiltro].length) {
					configFilter.setValue(retorno.filtros[vFiltro]);
					configFilter.setActive(true);
				}
			break;
			
			case 'date':
				if(Ext.isDate(retorno.filtros[vFiltro])) {
					var valorAtual = configFilter.getValue();
					var numeroAtual = '';
					for (var vAtual in valorAtual) {
						numeroAtual = valorAtual[vAtual];
					} 
					
					if(vFiltro.indexOf('Fim') != '-1') {
						if(Ext.isDate(numeroAtual)) { 
							novoValor = {
								before: retorno.filtros[vFiltro],
								after: numeroAtual
							};
						} else {
							novoValor = {before : retorno.filtros[vFiltro]};
						}
					} else {
						if(Ext.isDate(numeroAtual)) { 
							novoValor = {
								after: retorno.filtros[vFiltro],
								before: numeroAtual
							};
						} else {
							novoValor = {after : retorno.filtros[vFiltro]};
						}
					}
					
					configFilter.setValue(novoValor);
					configFilter.setActive(true);
				}
			break;
		}
	}
}
