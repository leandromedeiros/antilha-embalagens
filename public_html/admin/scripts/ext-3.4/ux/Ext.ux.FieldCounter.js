Ext.namespace('Ext.ux.plugins');

Ext.ux.plugins.Counter = function (config) {
    Ext.apply(this, config);
};

Ext.extend(Ext.ux.plugins.Counter, Ext.util.Observable, {
	init: function (field) {
		field.on({
			scope: field,
            keyup: this.onKeyUp,
            valid: this.onValid,
            focus: this.onFocus
        });
        Ext.apply(field, {
            onRender: field.onRender.createSequence(function () {
				this.enableKeyEvents = true;
            }),

            afterRender: field.afterRender.createSequence(function () {
               this.counter = Ext.DomHelper.insertAfter(Ext.get(this.el),{
                    tag: 'span',
                    style: 'color:#C0C0C0; padding-left:5px; padding-top: 5px;',
                    html: field.getValue().length + '/' + this.maxLength
                });
            })
        });
	},
	onKeyUp: function(textField,eventObj){
		Ext.get(this.counter).update(textField.getValue().length + '/' + textField.maxLength);
	},
	onFocus:function(textField){
		Ext.get(this.counter).update(textField.getValue().length + '/' + textField.maxLength);
	},
	onValid: function(textField){
		Ext.get(this.counter).update(textField.getValue().length + '/' + textField.maxLength);
	}
});