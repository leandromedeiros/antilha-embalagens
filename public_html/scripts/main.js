$(document).ready(function(){$(".suport").mouseenter(function(){$(".header-default-sub-menu").css("display","block")}),$(".suport").mouseleave(function(){$(".header-default-sub-menu").css("display","none")}),$(".change-video li a").click(function(){var video=$(this).attr("video");$("#video-main").attr("src",video)});var eachLi,elementLI=$(".table .tbody"),elementsLI=$(elementLI).length;for(i=0;elementsLI>=i;i++)eachLi=$(elementLI[i]),i%2!=0&&eachLi.addClass("no-bg");var eachLi2,elementLI2=$(".complaints"),elementsLI2=$(elementLI2).length;for(i=0;elementsLI2>=i;i++)eachLi2=$(elementLI2[i]),i%2!=0&&eachLi2.addClass("no-bg");$(".list-questions-answers").slideUp(0);$(".click-pointer-event").click(function(){$(this).next(".list-questions-answers").slideToggle("fast"),$(this).find(".icon-control").toggleClass("icon-close")}),$(".openLevel").click(function(){$(".submenu").find("ul").slideToggle("fast")})});
$(function(){
	$("td,th").mouseenter(function( event ) {
		var tooltip = $(this).find(".tooltip");
		var tooltiparrow = $(tooltip).children(".tooltip-arrow");
		$(tooltip).fadeIn();
		$(tooltip).position({
			my: "center top",
			at: "center bottom",
			of: $(this),
			within: $(".scroll-tabela"),
			 collision: "fit"
		});
		$(tooltiparrow).position({
			my: "center top",
			at: "center bottom-18",
			of: $(this),
			within: $(tooltip),
			collision: "flip"
		});
	});
	$("td,th").mouseleave(function( event ) {
		var tooltip = $(this).find(".tooltip");
		$(tooltip).fadeOut();
	});
	$("td,th").click(function( event ) {
		var tooltip = $(this).find(".tooltip");
		$(tooltip).fadeIn();
		$(tooltip).position({
		        my: "center top",
		        at: "center bottom",
		        of: $(this),
		        within: $(".scroll-tabela"),
		        collision: "fit"
		});
	});
	
	$(".container :input").each(function() {
		if($(this).attr('type') != 'submit' && $(this).attr('type') != 'button') {
			if($(this).prop("tagName") == 'SELECT') {
				$(this).parent().css('background-color', $(this).val() ? '#CCC' : '#FFF');
			} else { 
				$(this).css('background-color', $(this).val() ? '#CCC' : '#FFF');
			}
		}
	});
});