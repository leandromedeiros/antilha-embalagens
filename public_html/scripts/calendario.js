//Funções para o calendário
var calendario = {
	natDays: [
      	
     ]
};

function AddBusinessDays(weekDaysToAdd, date) {
	var curdate = date ? new Date(date.getTime()) : new Date();
	var realDaysToAdd = 0;
	while (weekDaysToAdd > 0){
		curdate.setDate(curdate.getDate()+1);
		realDaysToAdd++;
		//check if current day is business day
		if (noWeekendsOrHolidays(curdate)[0]) {
			weekDaysToAdd--;
		}
	}
	return realDaysToAdd;
}

function SubBusinessDays(weekDaysToSub, date) {
	var curdate = date ? new Date(date.getTime()) : new Date();
	var realDaysToSub = 0;
	while (weekDaysToSub < 0) {
		curdate.setDate(curdate.getDate()-1);
		realDaysToSub--;
		//check if current day is business day
		if (noWeekendsOrHolidays(curdate)[0]) {
			weekDaysToSub++;
		}
	}
	return realDaysToSub;
}

function noWeekendsOrHolidays(date) {
	var noWeekend = $.datepicker.noWeekends(date);
	if (noWeekend[0]) {
		return nationalDays(date);
	} else {
		return noWeekend;
	}
}

function nationalDays(date) {
	for (i = 0; i < calendario.natDays.length; i++) {
		if (date.getMonth() == calendario.natDays[i][0] - 1 && date.getDate() == calendario.natDays[i][1]) {
			return [false, calendario.natDays[i][2] + '_day'];
		}
	}
	return [true, ''];
}