//placeholders for ie


	$(function(){
		
		$('input,textarea').placeholder();
		
		$('.sug,.rec').hide();
		$('#destiny').val('');
		
		$('#action').change(function(){
			
			var act = ($(this).val()=='1') ? 'sug' : 'rec';
			
			$('#destiny > option').each(function(){
				
				if(!$(this).hasClass(act)){
					$(this).hide();
				}else{
					$(this).show();
				}
			});
		});
	});