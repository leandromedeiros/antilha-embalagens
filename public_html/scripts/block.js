var block = function(mensagem) {
	$.blockUI({
		message: '<h3>'+mensagem+'</h3>',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#fff',  
            color: '#000' ,
            fontSize: '20px'
    	}
	});
}

var blockNotification = function(mensagem) {
	$.growlUI('Notificação', mensagem);	
}

var unblock = function() {
	$.unblockUI();
}

