<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<title>Antilhas Flexiveis</title>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/favicon.ico">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>

	<!--CSS-->
	<link rel="stylesheet" href="css/rwdgrid.css">
	<link rel="stylesheet" href="css/cbp-fwslider.css">
	<link rel="stylesheet" href="css/skew.css">
	<link rel="stylesheet" href="css/fonts.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/mediaqueries.css">
	<link rel="stylesheet" href="css/mail.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->





</head>

<body>
	<div id="cbp-fwslider" class="cbp-fwslider skew-bottom-ccw" style="background: url(images/bg-top-landing.jpg) no-repeat;background-size: cover;  @media (max-width:10%){background: url(images/bg-top-landing.jpg) no-repeat;background-size: cover;}">

	</div>


	<!--texto do container do topo-->
	<div class="container header-inner">
		<h1 class="headline">Stretch Hood</h1>
		<h2 class="headline2">filme para embalar e proteger cargas paletizadas</h2>
	</div>


	<section>
		<div class="container-12">

			<div class="grid-6">
				<video id="autoplay" class="responsive-vid" controls="controls" poster="images/back-video.PNG" width="820" height="500" autoplay="autoplay">
					<source src="video/Antilhas Flexíveis - Stretch Hood_Landing Page.mp4" type="video/mp4" />
					<source src="video/Antilhas-Flexíveis-Stretch-Hood_Landing-Page.webm" type="video/webm" />
					<source src="video/Antilhas-Flexíveis-Stretch-Hood_Landing-Page.ogg" type="video/ogg" />
					<source src="video/Antilhas-Flexíveis-Stretch-Hood_Landing-Page.ogv" type="video/ogv" />
					<source src="video/Antilhas-Flexíveis-Stretch-Hood_Landing-Page.3gp" type="video/3gp" />
					</object>
				</video>
			</div>
			<div class="grid-6">
				<div class="text-block-left">
					<h2 class="headline3">BENEFÍCIOS</h2>
					<p class="subheading">Embalagem eficaz, inteligente e segura para uma ampla gama de produtos</p>



					<p class="imgcss"> <img src="images/icon_cert.png" width="30" height="30" align="left">
						Ganho logístico e de produtividade, devido à agilidade na paletização (até 230 paletes por hora)<br />
					</p><br>
					<p class="imgcss"> <img src="images/icon_cert.png" width="30" height="30" align="left">
						Personalizável - possibilidade de impressão da marca &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
					</p><br>
					<p class="imgcss"> <img src="images/icon_cert.png" width="30" height="30" align="left">
						Eficaz contra variações climáticas no armazenamento ao ar livre - dispensa uso de lona no caminhão<br />
					</p><br>
					<p class="imgcss"> <img src="images/icon_cert.png" width="30" height="30" align="left">
						Mais estabilidade - Mais proteção para reduzir perdas.
						Segurança contra furtos<br />
					</p><br>
					<p class="imgcss"> <img src="images/icon_cert.png" width="30" height="30" align="left">
						Reduz a mão de obra em até 60% Aplicação 40% mais rápida que o strech convencional<br />
					</p>



				</div>
			</div>

			<div class="clear"></div>
		</div>
	</section>



	<section class="alt-background skew-top-cw">
		<h3 align="center" class="font-six">Preencha o formulário abaixo que a nossa equipe de<br> especialistas entrará em contato.</h3>
		<div class="container-fluid width-fix" id="stylized">




			<div class="row">
				<div class="col-md-8 media-form " style="margin:0 auto; float:none;">

					<br />
					<form method="post" action="send.php">
						<div class="form-group">
							<label>Nome e Sobrenome</label>
							<input type="text" name="nome" placeholder="Nome" class="form-control" value="" />
						</div>
						<div class="form-group">
							<label>Empresa</label>
							<input type="text" name="empresa" class="form-control" placeholder="Empresa" value="" />
						</div>

						<div class="form-group">
							<label>E-mail</label>
							<input type="text" name="email" class="form-control" placeholder="Email" value="" />
						</div>
						<div class="form-group">
							<label>Telefone</label>
							<input type="text" name="telefone" class="form-control" placeholder="Telefone" value="" />
						</div>
						<div class="form-group" align="center">
							<input type="submit" name="submit" value="Enviar" class="btn btn-secondary" />
						</div>
					</form>
				</div>
			</div>
		</div>
		<span class="imgcss2"> <img src="images/logo_footer.png" align="right"></span>
	</section>

	<?php
	if ($_POST) {
		$de       = $_POST['email'];
		$para     = "atendimentocliente@antilhas.com.br";
		$nome     = $_POST['nome'];
		$empresa  = $_POST['empresa'];
		$telefone = $_POST['telefone'];

		$retorno = mail(
			$para,
			$empresa,
			"De $nome \n $telefone",
			'From: ' . $de
		);

		if ($retorno) {
			echo "<div>E-mail enviado!</div>";
		}
	}



	?>

	</div>
	</div>

	</div>



	</section>



	</div>

	<script>
		document.getElementById('autoplay').autoplay();
	</script>


</body>

</html>