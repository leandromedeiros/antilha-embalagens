<?php

header(sprintf('Location: %s', 'http://homolog.antilhaspedidos.com.br/stretchhood/')); //http://homolog.antilhaspedidos.com.br/strechood/

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$name = $_POST['nome'];
$email = $_POST['email'];
$empresa = $_POST['empresa'];
$telefone = $_POST['telefone'];

// Cria uma nova instância do PHPMailer
$mail = new PHPMailer;

// Definição do idioma
$mail->setLanguage('pt_br', 'PHPMailer/language/');

// Informa ao PHPMailer para usar SMTP
$mail->isSMTP();

/**
 * Habilita debug SMTP
 * 
 * 0 = off (para uso em produção)
 * 1 = Mensagens do cliente
 * 2 = Mensagens do cliente e servidor
 */
//$mail->SMTPDebug = 2;

/**
 * Define o nome do servidor de e-mail
 * use $mail->Host = gethostbyname('smtp.gmail.com');
 * se sua rede não suporta SMTP over IPv6
 */
$mail->Host = 'smtplw.com.br';

// Define o número da porta SMTP - 587 para autenticação TLS,
// a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

// Define o sistema de criptografia a usar - ssl (depreciado)
// ou tls
$mail->SMTPSecure = 'tls';

// Se vai usar SMTP authentication
$mail->SMTPAuth = true;

// Usuário para usar SMTP authentication
// Use o endereço completo do e-mail do GMail
$mail->Username = 'antilhasflexiveis';

// Senha para SMTP authentication
$mail->Password = 'WUrpBMeu9193';

// Define o remetente
$mail->setFrom('atendimentoflexiveis@antilhasflexiveis.com.br', 'Antilhas Flexiveis');

// Define o endereço para resposta
$mail->addReplyTo('atendimentoflexiveis@antilhasflexiveis.com.br', 'Antilhas Flexiveis');

// Define o destinatário
// $mail->addAddress('<e-mail do destinatário>', 'Nome Dele :)');
$mail->addAddress('atendimentoflexiveis@antilhasflexiveis.com.br', 'Antilhas Flexiveis');

// Define o assunto
$mail->Subject = 'Contato Antilhas Flexiveis Stretch Hood';

// Define o formato da mensagem para HTML
$mail->isHTML(true);

// Corpo da mensagem
$mail->Body =
	<<<ENVIO

		<div>Nome:<strong> $name</strong></div>
		<div>Email:<strong> $email</strong></div>
		<div>Empresa:<strong> $empresa</strong></div>
		<div>Telefone:<strong> $telefone</strong></div>
ENVIO;

// Corpo alternativo caso o cliente de e-mail não suporte HTML
$mail->AltBody = 'Mensagem em texto simples';

// Envia a mensagem e verifica os erros
if (!$mail->send()) {
	echo "Erro no Mailer: " . $mail->ErrorInfo;
} else {
	echo 'Mensagem enviada!<br>';
}
