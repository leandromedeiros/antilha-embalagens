<?php

class LojasController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoGruposLoja = App_Model_DAO_GrupoLojas::getInstance();
				$daoLojas = App_Model_DAO_Lojas::getInstance();
				$filter = $daoLojas->getAdapter()->select()
					->from($daoLojas->info('name'))
					->joinInner($daoGruposLoja->info('name'), 'grp_loja_idGrupo = loja_idGrupo', 'grp_loja_nome')
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'loja_nomeCompleto')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('lojas' => array(), 'total' => 0);
					$rsLojas = $daoLojas->createRowset($daoLojas->getAdapter()->fetchAll($filter));
					foreach ($rsLojas as $loja) {
						$retorno['lojas'][] = array(
							'loja_idLoja' => $loja->getCodigo(),
							'grp_loja_nome' => $loja->getGrupo()->getNome(),
							'loja_cnpj' => $loja->getCnpj(),
							'loja_email' => $loja->getEmail(),
							'loja_nomeCompleto' => $loja->getNomeCompleto(),
							'loja_transportadora' => $loja->getTransportadora(),
							'loja_tipoBloqueio' => $loja->getTipoBloqueio(),
							'loja_classificacao' => $loja->getClassificacao()
						);
					}
					$retorno['total'] = $daoLojas->getCount($filter);
					unset($rsLojas);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoGruposLoja, $daoLojas, $filter);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Lojas.xls");
		header("Content-Disposition: attachment; filename=Lojas.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Lojas');

		$plan->writeString(0, 0, 'Listagem de Lojas', $format['titulo']);
		$plan->setMerge(0, 0, 0, 11);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 20);
		$plan->setColumn(1, 2, 20);
		$plan->setColumn(1, 3, 30);
		$plan->setColumn(1, 4, 20);
		$plan->setColumn(1, 5, 20);
		$plan->setColumn(1, 6, 20);
		$plan->setColumn(1, 7, 20);
		$plan->setColumn(1, 8, 20);
		$plan->setColumn(1, 9, 20);
		$plan->setColumn(1, 10, 20);
		$plan->setColumn(1, 11, 20);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'Grupo', $format['subtitulo']);
		$plan->writeString(2, 2, 'Tipo', $format['subtitulo']);
		$plan->writeString(2, 3, 'CNPJ', $format['subtitulo']);
		$plan->writeString(2, 4, 'E-mail', $format['subtitulo']);
		$plan->writeString(2, 5, 'ICMS', $format['subtitulo']);
		$plan->writeString(2, 6, 'IE', $format['subtitulo']);
		$plan->writeString(2, 7, 'Nome Completo', $format['subtitulo']);
		$plan->writeString(2, 8, 'Nome Reduzido', $format['subtitulo']);
		$plan->writeString(2, 9, 'Transportadora', $format['subtitulo']);
		$plan->writeString(2, 10, 'Tipo de Bloqueio', $format['subtitulo']);
		$plan->writeString(2, 11, 'Classifica��o', $format['subtitulo']);

		$daoLojas = App_Model_DAO_Lojas::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoLojas->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoLojas->createRowset($daoLojas->getAdapter()->fetchAll($filter));
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, $record->getGrupo()->getNome(), $format["normal-cen"]);
					$plan->writeString($linha, 2, $record->getTipo()->getNome(), $format["normal-cen"]);
					$plan->writeString($linha, 3, $record->getCnpj(), $format["normal-cen"]);
					$plan->writeString($linha, 4, $record->getEmail(), $format["normal-cen"]);
					$plan->writeString($linha, 5, $record->getIcms() ? App_Funcoes_Money::toCurrency($record->getIcms()) : '', $format["normal-cen"]);
					$plan->writeString($linha, 6, $record->getIe(), $format["normal-cen"]);
					$plan->writeString($linha, 7, $record->getNomeCompleto(), $format["normal"]);
					$plan->writeString($linha, 8, $record->getNomeReduzido(), $format["normal"]);
					$plan->writeString($linha, 9, $record->getTransportadora(), $format["normal-cen"]);
					$plan->writeString($linha, 10, $record->getTipoBloqueio(), $format["normal-cen"]);
					$plan->writeString($linha, 11, $record->getClassificacao(), $format["normal-cen"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 11; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 11);
		}
		unset($daoLojas, $filter);

		$xls->close();
	}
}