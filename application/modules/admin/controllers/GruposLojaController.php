<?php

class GruposLojaController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoGruposLoja = App_Model_DAO_GrupoLojas::getInstance();
				$daoRedes = App_Model_DAO_Redes::getInstance();
				$filter = $daoGruposLoja->getAdapter()->select()
					->from($daoGruposLoja->info('name'))
					->joinInner($daoRedes->info('name'), 'grp_loja_idRede = rede_idRede', 'rede_nome')
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'grp_loja_nome')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('grupos' => array(), 'total' => 0);
					$rsGrupos = $daoGruposLoja->createRowset($daoGruposLoja->getAdapter()->fetchAll($filter));
					foreach ($rsGrupos as $grupo) {
						$retorno['grupos'][] = array(
							'grp_loja_idGrupo' => $grupo->getCodigo(),
							'grp_loja_nome' => $grupo->getNome(),
							'rede_nome' => $grupo->getRede()->getNome(),
							'grp_loja_tipo_pedido' => $grupo->getTipoPedido(),
							'grp_loja_cliente_matriz' => $grupo->getClienteMatriz()
						);
					}
					$retorno['total'] = $daoGruposLoja->getCount($filter);
					unset($rsGrupos);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoGruposLoja, $daoRedes, $filter);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=GruposLoja.xls");
		header("Content-Disposition: attachment; filename=GruposLoja.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Grupos de Loja');

		$plan->writeString(0, 0, 'Listagem de Grupos de Loja', $format['titulo']);
		$plan->setMerge(0, 0, 0, 4);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 30);
		$plan->setColumn(1, 2, 30);
		$plan->setColumn(1, 3, 20);
		$plan->setColumn(1, 4, 20);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'Rede', $format['subtitulo']);
		$plan->writeString(2, 2, 'Grupo', $format['subtitulo']);
		$plan->writeString(2, 3, 'Tipo Pedido', $format['subtitulo']);
		$plan->writeString(2, 4, 'Cliente Matriz', $format['subtitulo']);

		$daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoGrupoLojas->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoGrupoLojas->createRowset($daoGrupoLojas->getAdapter()->fetchAll($filter));
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, $record->getRede()->getNome(), $format["normal"]);
					$plan->writeString($linha, 2, $record->getNome(), $format["normal"]);
					$plan->writeString($linha, 3, $record->getTipoPedido(), $format["normal"]);
					$plan->writeString($linha, 4, $record->getClienteMatriz(), $format["normal"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 3; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 2);
		}
		unset($daoGrupoLojas, $filter);

		$xls->close();
	}
}