<?php

class ProdutosController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'linhas':
				$daoLinhas = App_Model_DAO_Linhas::getInstance();
				$filter = $daoLinhas->select()
					->from($daoLinhas)
					->order("{$this->getRequest()->getParam('sort', 'lin_nome')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				$retorno = array('linhas' => array(), 'total' => 0);
				$rsLinhas = $daoLinhas->fetchAll($filter);
				foreach ($rsLinhas as $linha) {
					$retorno['linhas'][] = array(
						'lin_idLinha' => $linha->getCodigo(),
						'lin_nome' => $linha->getNome(),
						'lin_idRede' => $linha->getRede()->getCodigo()
					);
				}
				$retorno['total'] = $rsLinhas->count();
				unset($rsLinhas);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
				
				unset($daoLinhas, $filter);
				break;

			case 'produtos':
				$daoProdutos = App_Model_DAO_Produtos::getInstance();
				$filter = $daoProdutos->select()
					->from($daoProdutos)
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'prod_descricao')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));
				
				if($linha = $this->getRequest()->getParam('lin_idLinha')) {
					$filter->where('prod_idLinha = ?', $linha);
				}

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('produtos' => array(), 'total' => 0);
					$rsProdutos = $daoProdutos->fetchAll($filter);
					foreach ($rsProdutos as $produto) {
						$retorno['produtos'][] = array(
							'prod_idProduto' => $produto->getCodigo(),
							'prod_descricao' => $produto->getDescricao(),
							'prod_unidade' => $produto->getUnidade(),
							'prod_idClienteExterno' => $produto->getIdClienteExterno(),							
						);
					}
					$retorno['total'] = $daoProdutos->getCount($filter);
					unset($rsProdutos);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoProdutoss, $filter);
				break;	
			break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('prod_idProduto', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'produto' => array());
			$daoProdutos = App_Model_DAO_Produtos::getInstance();
			try {
				$produto = $daoProdutos->fetchRow(
					$daoProdutos->select()->where('prod_idProduto = ?', $idRegistro)
				);
				if (null == $produto) {
					throw new Exception('O produto solicitado n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					// carrega os dados
					$retorno['success'] = true;
					$arrProduto = $produto->toArray();
					$retorno['produto'] = array($arrProduto);					
					
				} else {
					// atualiza os dados
					$produto = $this->montaObjeto($produto);
					
					try {
						$produto->save();

						$retorno['success'] = true;
						$retorno['message'] = sprintf('Produto <b>%s</b> alterado com sucesso.', $produto->getDescricao());
					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor, verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o produto.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoProdutos, $produto);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {			
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('form');
		}
	}
	
	protected function montaObjeto(App_Model_Entity_Produto $produto) {
		$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();
		if($this->getRequest()->getParam('prod_idImagem')) {
			$produto->setImagem(
				$daoArquivos->fetchRow(
					$daoArquivos->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('prod_idImagem'))
				)
			);
		}
		
		return $produto;
	}
	
	public function detalheAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array();
			$daoProdutos = App_Model_DAO_Produtos::getInstance();

			$view = new Zend_View();
			$view->setBasePath(APPLICATION_PATH .DIRECTORY_SEPARATOR. 'views');

			try {
				$produto = $daoProdutos->fetchRow(
					$daoProdutos->select()->where('prod_idProduto = ?', $this->getRequest()->getParam('idProduto'))
				);
				
				$view->produto = $produto;
				$retorno = array('conteudo' => $view->render('produtos/detalhes-produto.phtml'));
			} catch (Exception $e) {
				$retorno['message'] = $e->getMessage();
			}

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getFrontController()->setParam('noViewRenderer', false);
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		}
	}

	public function linhaUpdateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('lin_idLinha', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'linha' => array());
			$daoLinhas = App_Model_DAO_Linhas::getInstance();
			try {
				$linha = $daoLinhas->fetchRow(
					$daoLinhas->select()->where('lin_idLinha = ?', $idRegistro)
				);
				if (null == $linha) {
					throw new Exception('A linha solicitado n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					// carrega os dados
					$retorno['success'] = true;
					$arrLinha = $linha->toArray();
					$retorno['linha'] = array($arrLinha);				

				} else {
					// atualiza os dados
					$linha = $this->montaObjetoLinha($linha);

					try {
						$linha->save();

						$retorno['success'] = true;
						$retorno['message'] = sprintf('Linha <b>%s</b> alterada com sucesso.', $linha->getNome());
					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor, verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar a linha.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoLinhas, $linha);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {			
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('form-linha');
		}
	}
	
	protected function montaObjetoLinha(App_Model_Entity_Linha $linha) {
		$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();
		$linha->setImagem(
				$daoArquivos->fetchRow(
					$daoArquivos->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('lin_idImagem'))
				)
			);			
		return $linha;
	}
	
	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Produtos.xls");
		header("Content-Disposition: attachment; filename=Produtos.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Produtos');

		$plan->writeString(0, 0, 'Listagem de Produtos', $format['titulo']);
		$plan->setMerge(0, 0, 0, 13);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 20);
		$plan->setColumn(1, 2, 35);
		$plan->setColumn(1, 3, 20);
		$plan->setColumn(1, 4, 20);
		$plan->setColumn(1, 5, 20);
		$plan->setColumn(1, 6, 20);
		$plan->setColumn(1, 7, 20);
		$plan->setColumn(1, 8, 20);
		$plan->setColumn(1, 9, 20);
		$plan->setColumn(1, 10, 20);
		$plan->setColumn(1, 11, 20);
		$plan->setColumn(1, 12, 20);
		$plan->setColumn(1, 13, 20);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'Linha', $format['subtitulo']);
		$plan->writeString(2, 2, 'Descri��o', $format['subtitulo']);
		$plan->writeString(2, 3, 'Unidade', $format['subtitulo']);
		$plan->writeString(2, 4, 'Id Cliente Externo', $format['subtitulo']);
		$plan->writeString(2, 5, 'CCF', $format['subtitulo']);
		$plan->writeString(2, 6, 'Medidas Net', $format['subtitulo']);
		$plan->writeString(2, 7, 'Unid. Desc', $format['subtitulo']);
		$plan->writeString(2, 8, 'PC. M�dulo', $format['subtitulo']);
		$plan->writeString(2, 9, 'Qtde Pacotes', $format['subtitulo']);
		$plan->writeString(2, 10, 'Peso Pacotes', $format['subtitulo']);
		$plan->writeString(2, 11, 'Aliquota IPI', $format['subtitulo']);
		$plan->writeString(2, 12, 'Cod. Resumo', $format['subtitulo']);
		$plan->writeString(2, 13, 'Sequ�ncial Itens', $format['subtitulo']);

		$daoProdutos = App_Model_DAO_Produtos::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoProdutos->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoProdutos->createRowset($daoProdutos->getAdapter()->fetchAll($filter));
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, $record->getLinha()->getNome(), $format["normal-cen"]);
					$plan->writeString($linha, 2, $record->getDescricao(), $format["normal-cen"]);
					$plan->writeString($linha, 3, $record->getUnidade(), $format["normal-cen"]);
					$plan->writeString($linha, 4, $record->getIdClienteExterno(), $format["normal-cen"]);
					$plan->writeString($linha, 5, $record->getCcf(), $format["normal-cen"]);
					$plan->writeString($linha, 6, $record->getMedidasNet(), $format["normal-cen"]);
					$plan->writeString($linha, 7, $record->getUnidDesc(), $format["normal-cen"]);
					$plan->writeString($linha, 8, App_Funcoes_Money::toCurrency($record->getPcModulo()), $format["normal-cen"]);
					$plan->writeString($linha, 9, App_Funcoes_Money::toCurrency($record->getQtdePacotes()), $format["normal-cen"]);
					$plan->writeString($linha, 10, App_Funcoes_Money::toCurrency($record->getPesoPacotes()), $format["normal-cen"]);
					$plan->writeString($linha, 11, App_Funcoes_Money::toCurrency($record->getAliquotaIpi()), $format["normal-cen"]);
					$plan->writeString($linha, 12, App_Funcoes_Money::toCurrency($record->getCodResumo()), $format["normal-cen"]);
					$plan->writeString($linha, 13, App_Funcoes_Money::toCurrency($record->getSequenciaItens()), $format["normal-cen"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 13; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 13);
		}
		unset($daoProdutos, $filter);

		$xls->close();
	}
}