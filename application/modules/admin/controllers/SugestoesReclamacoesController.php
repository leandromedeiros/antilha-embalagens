<?php

class SugestoesReclamacoesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
				$daoAssuntos = App_Model_DAO_SugestoesReclamacoes_Assuntos::getInstance();
				$daoMotivos = App_Model_DAO_SugestoesReclamacoes_Assuntos_Motivos::getInstance();
				$daoSugestoesReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();
				$daoConfiguracao = App_Model_DAO_Sistema_Configuracoes::getInstance();
				
				$filter = $daoSugestoesReclamacoes->getAdapter()->select()
					->from($daoSugestoesReclamacoes->info('name'))
					->joinInner($daoUsuarios->info('name'), 'usr_idUsuario = sug_rec_idUsuario', 'usr_nome')
					->joinLeft($daoAssuntos->info('name'), 'rec_ass_idAssunto = sug_rec_idAssunto', 'rec_ass_nome')
					->joinLeft($daoMotivos->info('name'), 'rec_ass_mot_idMotivo = sug_rec_idMotivo', 'rec_ass_mot_nome')					
					->order("{$this->getRequest()->getParam('sort', 'sug_rec_assunto')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));
				
				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('reclamacoes' => array(), 'total' => 0);
					$rsSugestoesReclamacoes = $daoSugestoesReclamacoes->createRowset(
						$daoSugestoesReclamacoes->getAdapter()->fetchAll($filter)
					);
										
					foreach ($rsSugestoesReclamacoes as $opcao) {
						$retorno['reclamacoes'][] = array(
							'sug_rec_idSugestaoReclamacao' => $opcao->getCodigo(),
							'sug_rec_dataCadastro' => $opcao->getDataCadastro(),
							'usr_idUsuario' => $opcao->getUsuario()->getCodigo(),
							'usr_login' => $opcao->getUsuario()->getLogin(),
							'sug_rec_tipo' => $opcao->getTipo(),
							'sug_rec_canal' => $opcao->getCanal(),
							'sug_rec_idPedido' => $opcao->getPedido() ? $opcao->getPedido()->getCodigo() : '',
							'rec_ass_nome' => $opcao->getAssunto() ? $opcao->getAssunto()->getNome() : '',
							'rec_ass_mot_nome' => $opcao->getMotivo() ? $opcao->getMotivo()->getNome() : '', 
							'sug_rec_usr_nome' => $opcao->getUsuario()->getNome(),
							'sug_rec_status' => (string) (int) $opcao->getStatus()
						);
					}
					$retorno ['total'] = $daoSugestoesReclamacoes->getCount($filter);
					unset($rsSugestoesReclamacoes);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoUsuarios, $daoSugestoesReclamacoes, $filter);
			break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}
	
	public function detalheAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array();
			$daoSugestoesReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();
			$daoSugestoesReclamacoes->getAdapter()->query('SET SESSION wait_timeout = 100');

			$view = new Zend_View();
			$view->setBasePath(APPLICATION_PATH .DIRECTORY_SEPARATOR. 'views');

			try {
				$opcao = $daoSugestoesReclamacoes->fetchRow(
					$daoSugestoesReclamacoes->select()
						->where('sug_rec_idSugestaoReclamacao = ?', $this->getRequest()->getParam('idOpcao'))
				);
				
				$view->opcao = $opcao;
				$retorno = array('conteudo' => $view->render('sugestoes-reclamacoes/detalhes-reclamacao.phtml'));
			} catch (Exception $e) {
				$retorno['message'] = $e->getMessage();
			}

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getFrontController()->setParam('noViewRenderer', false);
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		}
	}
	
	public function updatestatusAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => '', 'errors' => array());
			$daoReclamacoes = App_Model_DAO_Reclamacoes::getInstance();
			
			$reclamacao = $daoReclamacoes->fetchRow(
				$daoReclamacoes->select()
					->where('rec_idReclamacao = ?', $this->getRequest()->getParam('rec_idReclamacao'))
			);
			$status = App_Model_DAO_Reclamacoes_Status::getInstance()->createRow()
				->setCodigo($this->getRequest()->getParam('rec_status_codigo'))
				->setData(date('Y-m-d H:i:s'))
				->setDescricao(utf8_decode($this->getRequest()->getParam('rec_status_descricao')));

			$reclamacao->getHistoricoStatus()->offsetAdd($status);
			$reclamacao->setStatus($this->getRequest()->getParam('rec_status_codigo'));
			try {
				$reclamacao->save();
				//App_Model_DAO_Reclamacoes::getInstance()->sendEmailStatus($reclamacao);

				$retorno['success'] = true;
				$retorno['message'] = 'Status da reclama��o atualizado com sucesso.';
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoReclamacoes, $reclamacao);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getFrontController()->setParam('noViewRenderer', false);
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		}
	}
	
	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoReclamacoes = App_Model_DAO_Reclamacoes::getInstance();
		
		try {
			$reclamacao = $daoReclamacoes->fetchRow(
				$daoReclamacoes->select()->where('rec_idReclamacao = ?', $this->getRequest()->getParam('idReclamacao'))
			);
			if (null == $reclamacao) {
				throw new Exception('A reclama��o solicitada n�o foi encontrada.');
			}
			try {
				$nome = $reclamacao->getCodigo();
				$reclamacao->delete();

				$retorno['success'] = true;
				$retorno['message'] = sprintf('Reclama��o <b>%s</b> removida com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a reclama��o.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoReclamacoes, $reclamacao);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}
	
	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Reclamacoes.xls");
		header("Content-Disposition: attachment; filename=Reclamacoes.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Reclamacoes');

		$plan->writeString(0, 0, 'Listagem de Reclamacoes', $format['titulo']);
		$plan->setMerge(0, 0, 0, 18);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 15);
		$plan->setColumn(2, 2, 18);
		$plan->setColumn(3, 3, 15);
		$plan->setColumn(4, 4, 30);
		$plan->setColumn(5, 5, 15);
		$plan->setColumn(6, 6, 12);
		$plan->setColumn(7, 7, 30);
		$plan->setColumn(8, 8, 18);
		$plan->setColumn(9, 9, 18);
		$plan->setColumn(10, 10, 18);
		$plan->setColumn(11, 11, 15);
		$plan->setColumn(12, 12, 50);
		$plan->setColumn(13, 13, 10);
		$plan->setColumn(14, 14, 20);
		$plan->setColumn(15, 15, 20);
		$plan->setColumn(16, 16, 10);
		$plan->setColumn(17, 17, 35);
		$plan->setColumn(18, 18, 15);
		

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'Data Cadastro', $format['subtitulo']);
		$plan->writeString(2, 2, 'CPF', $format['subtitulo']);
		$plan->writeString(2, 3, 'Rg', $format['subtitulo']);
		$plan->writeString(2, 4, 'Nome', $format['subtitulo']);
		$plan->writeString(2, 5, 'Data Nascimento', $format['subtitulo']);
		$plan->writeString(2, 6, 'Sexo', $format['subtitulo']);
		$plan->writeString(2, 7, 'E-mail', $format['subtitulo']);
		$plan->writeString(2, 8, 'Telefone', $format['subtitulo']);
		$plan->writeString(2, 9, 'Celular', $format['subtitulo']);
		$plan->writeString(2, 10, 'Fax', $format['subtitulo']);
		$plan->writeString(2, 11, 'Tipo Endere�o', $format['subtitulo']);
		$plan->writeString(2, 12, 'Logradouro', $format['subtitulo']);
		$plan->writeString(2, 13, 'N�mero', $format['subtitulo']);
		$plan->writeString(2, 14, 'Bairro', $format['subtitulo']);
		$plan->writeString(2, 15, 'Cidade', $format['subtitulo']);
		$plan->writeString(2, 16, 'UF', $format['subtitulo']);
		$plan->writeString(2, 17, 'Referencia', $format['subtitulo']);
		$plan->writeString(2, 18, 'Status', $format['subtitulo']);

		$daoReclamacoes = App_Model_DAO_Reclamacoes::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoReclamacoes->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoReclamacoes->fetchAll($filter);
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, App_Funcoes_Date::conversion(substr($record->getDataCadastro(), 0, 10)), $format["normal-cen"]);
					$plan->writeString($linha, 2, $record->getCpf(), $format["normal-cen"]);
					$plan->writeString($linha, 3, $record->getRg(), $format["normal-cen"]);
					$plan->writeString($linha, 4, $record->getNome(), $format["normal"]);
					$plan->writeString($linha, 5, App_Funcoes_Date::conversion($record->getDataNascimento(), 0, 10), $format["normal-cen"]);
					$plan->writeString($linha, 6, App_Funcoes_Rotulos::$sexo[$record->getSexo()], $format["normal-cen"]);
					$plan->writeString($linha, 7, $record->getEmail(), $format["normal"]);
					$plan->writeString($linha, 8, $record->getTelefone(), $format["normal-cen"]);
					$plan->writeString($linha, 9, $record->getCelular(), $format["normal-cen"]);
					$plan->writeString($linha, 10, $record->getFax(), $format["normal-cen"]);
					$plan->writeString($linha, 11, App_Funcoes_Rotulos::$tipoEndereco[$record->getTipoEndereco()], $format["normal-cen"]);
					$plan->writeString($linha, 12, $record->getEndereco()->getLogradouro(), $format["normal"]);
					$plan->writeString($linha, 13, $record->getEndereco()->getNumero(), $format["normal-cen"]);
					$plan->writeString($linha, 14, $record->getEndereco()->getBairro(), $format["normal-cen"]);
					$plan->writeString($linha, 15, $record->getEndereco()->getCidade(), $format["normal-cen"]);
					$plan->writeString($linha, 16, $record->getEndereco()->getUF(), $format["normal-cen"]);
					$plan->writeString($linha, 17, $record->getReferencia(), $format["normal-cen"]);
					$plan->writeString($linha, 18, App_Funcoes_Rotulos::$status[$record->getStatus()], $format["normal-cen"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 9; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 9);
		}
		unset($daoReclamacoes, $filter);

		$xls->close();
	}
}