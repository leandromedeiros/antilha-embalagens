<?php

class RedesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoGruposRedes = App_Model_DAO_GrupoRedes::getInstance();
				$daoRedes = App_Model_DAO_Redes::getInstance();
				$filter = $daoRedes->getAdapter()->select()
					->from($daoRedes->info('name'))
					->joinInner($daoGruposRedes->info('name'), 'grp_rede_idGrupo = rede_idGrupo', 'grp_rede_nome')
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'rede_nome')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('redes' => array(), 'total' => 0);
					$rsRedes = $daoRedes->createRowset($daoRedes->getAdapter()->fetchAll($filter));
					foreach ($rsRedes as $rede) {
						$retorno['redes'][] = array(
							'rede_idRede' => $rede->getCodigo(),
							'grp_rede_nome' => $rede->getGrupo()->getNome(),
							'rede_nome' => $rede->getNome(),
							'rede_codigoMatriz' => $rede->getCodigoMatriz()
						);
					}
					$retorno['total'] = $daoRedes->getCount($filter);
					unset($rsRedes);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoRedes, $filter);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('rede_idRede', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'rede' => array());
			$daoRedes = App_Model_DAO_Redes::getInstance();
			try {
				$rede = $daoRedes->fetchRow(
					$daoRedes->select()->where('rede_idRede = ?', $idRegistro)
				);
				if (null == $rede) {
					throw new Exception('A rede solicitado n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					// carrega os dados
					$retorno['success'] = true;
					$arrRede = $rede->toArray();
					$retorno['rede'] = array($arrRede);					
					
				} else {
					// atualiza os dados
					$rede = $this->montaObjeto($rede);
					
					try {
						$rede->save();

						$retorno['success'] = true;
						$retorno['message'] = sprintf('Rede <b>%s</b> alterado com sucesso.', $rede->getNome());
					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor, verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o rede.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoRedes, $rede);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {			
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('form');
		}
	}
	
	protected function montaObjeto(App_Model_Entity_Rede $rede) {
		$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();
		$rede->setImagem(
			$daoArquivos->fetchRow(
				$daoArquivos->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('rede_idImagem'))
			)
		);
		return $rede;
	}
	
	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Redes.xls");
		header("Content-Disposition: attachment; filename=Redes.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Redes');

		$plan->writeString(0, 0, 'Listagem de Redes', $format['titulo']);
		$plan->setMerge(0, 0, 0, 3);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 30);
		$plan->setColumn(1, 2, 30);
		$plan->setColumn(1, 3, 20);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'Grupo', $format['subtitulo']);
		$plan->writeString(2, 2, 'Rede', $format['subtitulo']);
		$plan->writeString(2, 3, 'C�digo Matriz', $format['subtitulo']);

		$daoRedes = App_Model_DAO_Redes::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoRedes->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoRedes->createRowset($daoRedes->getAdapter()->fetchAll($filter));
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, $record->getGrupo()->getNome(), $format["normal"]);
					$plan->writeString($linha, 2, $record->getNome(), $format["normal"]);
					$plan->writeString($linha, 3, $record->getCodigoMatriz(), $format["normal"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 3; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 2);
		}
		unset($daoRedes, $filter);

		$xls->close();
	}
}