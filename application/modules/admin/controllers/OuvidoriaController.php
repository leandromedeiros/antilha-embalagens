<?php

class OuvidoriaController extends Zend_Controller_Action
{

    public function init()
    {
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->getResponse()->setHeader('Content-Type', 'text/json');
    }

    public function indexAction()
    {
        switch ($this->getRequest()->getParam('load')) {
            case 'grid':
                $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
                $daoOuvidoria = App_Model_DAO_Ouvidoria::getInstance();
                $filter = $daoOuvidoria->getAdapter()->select()
                    ->from($daoOuvidoria->info('name'))
                    ->joinInner($daoUsuarios->info('name'), 'usr_idUsuario = ouv_idUsuario', 'usr_nome')
                    ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
                    ->order("{$this->getRequest()->getParam('sort', 'usr_login')} {$this->getRequest()->getParam('dir', 'ASC')}");
                App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

                if ($this->getRequest()->getParam('excel', false) == true) {
                    $this->exportExcel($filter);
                } else {
                    $retorno = array('ouvidoria' => array(), 'total' => 0);
                    $rsOuvidoria = $daoOuvidoria->createRowset($daoOuvidoria->getAdapter()->fetchAll($filter));

                    foreach ($rsOuvidoria as $ouvidoria) {
                        $retorno['ouvidoria'][] = array(
                            'ouv_idContato' => $ouvidoria->getCodigo(),
                            'ouv_dataCadastro' => $ouvidoria->getDataCadastro(),
                            'ouv_protocolo' => $ouvidoria->getProtocolo(),
                            'usr_idUsuario' => $ouvidoria->getUsuario()->getCodigo(),
                            'usr_login' => $ouvidoria->getUsuario()->getLogin(),
                            'usr_nome' => $ouvidoria->getUsuario()->getNome(),
                            'ouv_nome' => $ouvidoria->getNome(),
                            'ouv_email' => $ouvidoria->getEmail(),
                            'ouv_mensagem' => $ouvidoria->getMensagem()
                        );
                    }
                    $retorno['total'] = $daoOuvidoria->getCount($filter);
                    unset($rsOuvidoria);

                    App_Funcoes_UTF8::encode($retorno);
                    echo Zend_Json::encode($retorno);
                }
                unset($daoUsuarios, $daoOuvidoria, $filter);
                break;

            default:
                $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
                $this->getFrontController()->setParam('noViewRenderer', false);
        }
    }

    public function deleteAction()
    {
        $retorno = array('success' => false, 'message' => null);
        $daoOuvidoria = App_Model_DAO_Ouvidoria::getInstance();
        try {
            $ouvidoria = $daoOuvidoria->fetchRow(
                $daoOuvidoria->select()->where('ouv_idContato = ?', $this->getRequest()->getParam('idContato'))
            );
            if (null == $ouvidoria) {
                throw new Exception('O contato solicitado n�o foi encontrado.');
            }
            try {
                $nome = $ouvidoria->getNome();
                $ouvidoria->delete();

                $retorno['success'] = true;
                $retorno['message'] = sprintf('Contato <b>%s</b> removido com sucesso.', $nome);
            } catch (Exception $e) {
                throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o contato.');
            }
        } catch (Exception $e) {
            $retorno['success'] = false;
            $retorno['message'] = $e->getMessage();
        }
        unset($daoOuvidoria, $ouvidoria);

        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    protected function exportExcel(Zend_Db_Select $filter)
    {
        require_once('Spreadsheet/Excel/Writer.php');
        header("Content-type: application/Octet-Stream");
        header("Content-Disposition: inline; filename=Ouvidoria.xls");
        header("Content-Disposition: attachment; filename=Ouvidoria.xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        $xls = new Spreadsheet_Excel_Writer();
        $xls->SetVersion(8);

        $format['titulo'] = & $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
        $format['subtitulo'] = & $xls->addFormat(array('bold' => 1, 'size' => 8, 'fontFamily' => 'arial', 'align' => 'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border' => 1));
        $format['subtitulo-esq'] = & $xls->addFormat(array('bold' => 1, 'size' => 8, 'fontFamily' => 'arial', 'align' => 'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border' => 1));
        $format['normal'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1));
        $format['normal-esq'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'left'));
        $format['normal-cen'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'center'));
        $format['normal-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'fgColor' => 'silver'));
        $format['normal-esq-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'left', 'fgColor' => 'silver'));
        $format['normal-cen-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'center', 'fgColor' => 'silver'));
        $plan = & $xls->addWorksheet('Ouvidoria');

        $plan->writeString(0, 0, 'Listagem de Ouvidoria', $format['titulo']);
        $plan->setMerge(0, 0, 0, 5);
        $plan->setColumn(0, 0, 15);
        $plan->setColumn(1, 1, 20);
        $plan->setColumn(1, 2, 20);
        $plan->setColumn(1, 3, 30);
        $plan->setColumn(1, 4, 20);
        $plan->setColumn(1, 5, 20);

        $plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
        $plan->writeString(2, 1, 'Data de Cadastro', $format['subtitulo']);
        $plan->writeString(2, 2, 'Usu�rio', $format['subtitulo']);
        $plan->writeString(2, 3, 'Nome', $format['subtitulo']);
        $plan->writeString(2, 4, 'Email', $format['subtitulo']);
        $plan->writeString(2, 5, 'Mensagem', $format['subtitulo']);

        $daoOuvidoria = App_Model_DAO_Ouvidoria::getInstance();
        $filter->limit(null, null);
        $totalRegistros = $daoOuvidoria->getCount(clone $filter);
        if ($totalRegistros > 0) {
            $linha = 3;
            for ($i = 0; $i < $totalRegistros; $i += 30) {
                $filter->limit(30, $i);
                $rsRegistros = $daoOuvidoria->createRowset($daoOuvidoria->getAdapter()->fetchAll($filter));
                foreach ($rsRegistros as $record) {
                    $plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
                    $plan->writeString($linha, 1, $record->getDataCadastro(), $format["normal-cen"]);
                    $plan->writeString($linha, 2, $record->getGrupo()->getNome(), $format["normal-cen"]);
                    $plan->writeString($linha, 3, $record->getNome(), $format["normal"]);
                    $plan->writeString($linha, 4, $record->getEmail(), $format["normal"]);
                    $plan->writeString($linha, 5, $record->getMensagem(), $format["normal-cen"]);
                    $linha++;
                }
                unset($rsRegistros);
            }
        } else {
            $plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
            for ($c = 1; $c <= 7; $c++) {
                $plan->writeString(3, $c, '', $format["normal-cen"]);
            }
            $plan->setMerge(3, 0, 3, 7);
        }
        unset($daoOuvidoria, $filter);

        $xls->close();
    }

}
