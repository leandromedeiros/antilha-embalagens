<?php

class DownloadsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoDownloads = App_Model_DAO_Downloads::getInstance();
				$filter = $daoDownloads->select()
					->from($daoDownloads)
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'down_nome')} {$this->getRequest()->getParam('dir', 'DESC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('downloads' => array(), 'total' => 0);
					$rsDownloads = $daoDownloads->fetchAll($filter);
					foreach ($rsDownloads as $download) {
						$retorno['downloads'][] = array(
							'down_idDownload' => $download->getCodigo(),
							'down_nome' => $download->getNome(),
							'down_data' => $download->getData(),
							'down_status' => (string) (int) $download->getStatus()
						);
					}
					$retorno['total'] = $daoDownloads->getCount($filter);
					unset($rsDownloads);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoDownloads, $filter);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function insertAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'download' => array());
			$daoDownloads = App_Model_DAO_Downloads::getInstance();
			$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();

			try {
				$download = $daoDownloads->createRow();
				$download = $this->montaObjeto($download);
				
				try {
					$download->save();
					$retorno['success'] = true;
					$retorno['message'] = sprintf('Arquivo <b>%s</b> cadastrado com sucesso.', $download->getNome());

				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					throw new Exception('Por favor verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o download.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoDownloads, $daoArquivos, $download);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('down_idDownload', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'download' => array());
			$daoDownloads = App_Model_DAO_Downloads::getInstance();
			try {
				$download = $daoDownloads->fetchRow(
					$daoDownloads->select()->where('down_idDownload = ?', $idRegistro)
				);
				if (null == $download) {
					throw new Exception('O arquivo solicitado n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					// carrega os dados
					$retorno['success'] = true;
					//$retorno ['download'] = array($download->toArray());
					$arrDownload = $download->toArray();
					
					foreach ($download->getPermissoes() as $permissao) {
						if($permissao->getGrupoRede()) {
							$arrDownload['down_permissoes'][] = array(
								'idControle' => $permissao->getGrupoRede()->getCodigo() . 'GR',
								'id' => $permissao->getGrupoRede()->getCodigo(),
								'nome' => $permissao->getGrupoRede()->getNome(),
								'grupo' => 'GR'
							);
						}	

						if($permissao->getRede()) {
							$arrDownload['down_permissoes'][] = array(
								'idControle' => $permissao->getRede()->getCodigo() . 'RE',
								'id' => $permissao->getRede()->getCodigo(),
								'nome' => $permissao->getRede()->getNome(),
								'grupo' => 'RE'
							);
						}

						if($permissao->getGrupoLoja()) {
							$arrDownload['down_permissoes'][] = array(
								'idControle' => $permissao->getGrupoLoja()->getCodigo() . 'GL',
								'id' => $permissao->getGrupoLoja()->getCodigo(),
								'nome' => $permissao->getGrupoLoja()->getNome(),
								'grupo' => 'GL'
							);
						}
						
						if($permissao->getLoja()) {
							$arrDownload['down_permissoes'][] = array(
								'idControle' => $permissao->getLoja()->getCodigo() . 'LO',
								'id' => $permissao->getLoja()->getCodigo(),
								'nome' => $permissao->getLoja()->getNomeCompleto(),
								'grupo' => 'LO'
							);
						}
						
						if($permissao->getLinha()) {
							$arrDownload['down_permissoes'][] = array(
								'idControle' => $permissao->getLinha()->getCodigo() . 'LI',
								'id' => $permissao->getLinha()->getCodigo(),
								'nome' => $permissao->getLinha()->getNome(),
								'grupo' => 'LI'
							);
						}
					}
					
					foreach ($download->getEstados() as $estado) {
						$arrDownload['down_estados'][] = $estado->getUf();
					}
					
					$retorno['download'] = array($arrDownload);
				} else {
					//atualiza os dados
					$download = $this->montaObjeto($download);
					
					try {
						$download->save();
						
						$retorno['success'] = true;
						$retorno['message'] = sprintf('Arquivo <b>%s</b> alterado com sucesso.', $download->getNome());

					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o download.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoDownloads);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);

		} else {		
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	protected function montaObjeto(App_Model_Entity_Download $download)
	{
		$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();
		// atualiza os dados
		$download->setNome(utf8_decode($this->getRequest()->getParam('down_nome')))
			->setData(App_Funcoes_Date::conversion($this->getRequest()->getParam('down_data')))
			->setImagem(
				$daoArquivos->fetchRow(
					$daoArquivos->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('down_idImagem'))
				)
			)
			->setStatus($this->getRequest()->getParam('down_status'));

		// Permiss�es
		$download->getPermissoes()->offsetRemoveAll();
		if ($this->getRequest()->getParam('down_permissoes')) {
			
			$daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
			$daoRedes = App_Model_DAO_Redes::getInstance();
			$daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
			$daoLojas = App_Model_DAO_Lojas::getInstance();
			$daoLinha = App_Model_DAO_Linhas::getInstance();
			
			$down_permissoes = Zend_Json::decode($this->getRequest()->getParam('down_permissoes', array()));
			foreach ($down_permissoes as $permissoes) {
				$permissao = App_Model_DAO_Downloads_Permissoes::getInstance()->createRow();
				switch ($permissoes['grupo']) {
					case 'GR':
						$permissao->setGrupoRede(
							$daoGrupoRedes->fetchRow(
								$daoGrupoRedes->select()->where('grp_rede_idGrupo = ?', $permissoes['id'])
							)
						)
						->setRede(null)
						->setGrupoLoja(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'RE':
						$permissao->setRede(
							$daoRedes->fetchRow(
								$daoRedes->select()->where('rede_idRede = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setGrupoLoja(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'GL':
						$permissao->setGrupoLoja(
							$daoGrupoLojas->fetchRow(
								$daoGrupoLojas->select()->where('grp_loja_idGrupo = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'LO':
						$permissao->setLoja(
							$daoLojas->fetchRow(
								$daoLojas->select()->where('loja_idLoja = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setGrupoLoja(null)
						->setLinha(null);
					break;
					case 'LI':
						$permissao->setLinha(
							$daoLinha->fetchRow(
								$daoLinha->select()->where('lin_idLinha = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setGrupoLoja(null)
						->setLoja(null);
					break;
				}
				$download->getPermissoes()->offsetAdd($permissao);
			}
		}
		
		// Estados
		$download->getEstados()->offsetRemoveAll();
		if (false != ($estados = $this->getRequest()->getParam('down_estados', false))) {
			$arrEstados = Zend_Json::decode($estados);
			foreach ($arrEstados as $key) {
				$estado = App_Model_DAO_Downloads_Estados::getInstance()->createRow();
				$estado->setUf($key);
				$download->getEstados()->offsetAdd($estado);
			}
		}
		return $download;
	}
	
	protected function buildForm()
	{
		$arrEstados = array();
		foreach (App_Funcoes_Rotulos::$UF as $key => $value) {
			$arrEstados[] = array(
				'uf' => $key,
				'nome' => $value
			);
		}
		App_Funcoes_UTF8::encode($arrEstados);
		$this->view->estados = Zend_Json::encode($arrEstados);

		$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		$this->render('form');
	}
	
	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoDownloads = App_Model_DAO_Downloads::getInstance();
		try {
			$download = $daoDownloads->fetchRow(
				$daoDownloads->select()->where('down_idDownload = ?', $this->getRequest()->getParam('idDownload'))
			);
			if (null == $download) {
				throw new Exception('O arquivo solicitado n�o foi encontrada.');
			}
			try {
				$nome = $download->getNome();
				$download->delete();
				$retorno['success'] = true;
				$retorno['message'] = sprintf('Arquivo <b>%s</b> removido com sucesso.', $nome);

			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o download.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoDownloads, $download);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}

	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=RelatoriosGerenciais.xls");
		header("Content-Disposition: attachment; filename=RelatoriosGerenciais.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Relat�rios Gerenciais');

		$plan->writeString(0, 0, 'Listagem de Relat�rios Gerenciais', $format['titulo']);
		$plan->setMerge(0, 0, 0, 2);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 30);
		$plan->setColumn(5, 2, 15);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'T�tulo', $format['subtitulo']);
		$plan->writeString(2, 2, 'Status', $format['subtitulo']);

		$daoDownloads = App_Model_DAO_Downloads::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoDownloads->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoDownloads->fetchAll($filter);
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, $record->getNome(), $format["normal"]);
					$plan->writeString($linha, 2, App_Funcoes_Rotulos::$status[$record->getStatus()], $format["normal-cen"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 2; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 2);
		}
		unset($daoDownloads, $filter);

		$xls->close();
	}
}