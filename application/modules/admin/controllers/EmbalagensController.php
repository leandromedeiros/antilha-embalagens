<?php

class EmbalagensController extends Zend_Controller_Action
{

    public function init()
    {
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->getResponse()->setHeader('Content-Type', 'text/json');
    }

    public function indexAction()
    {
        switch ($this->getRequest()->getParam('load')) {
            case 'grid':
                $daoEmbalagens = App_Model_DAO_Embalagens::getInstance();
                $filter = $daoEmbalagens->select()
                    ->from($daoEmbalagens)
                    ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
                    ->order("{$this->getRequest()->getParam('sort', 'emb_nome')} {$this->getRequest()->getParam('dir', 'ASC')}");
                App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

                if ($this->getRequest()->getParam('excel', false) == true) {
                    $this->exportExcel($filter);
                } else {
                    $retorno = array('embalagens' => array(), 'total' => 0);
                    $rsEmbalagem = $daoEmbalagens->fetchAll($filter);

                    foreach ($rsEmbalagem as $embalagem) {
                        $retorno['embalagens'][] = array(
                            'emb_idEmbalagem' => $embalagem->getCodigo(),
                            'emb_data' => App_Funcoes_Date::conversion($embalagem->getData()),
                            'emb_nome' => $embalagem->getNome(),
                            'emb_destaque' => (string) (int) $embalagem->getDestaque(),
                            'emb_status' => (string) (int) $embalagem->getStatus()
                        );
                    }
                    $retorno['total'] = $daoEmbalagens->getCount($filter);
                    unset($rsEmbalagem);

                    App_Funcoes_UTF8::encode($retorno);
                    echo Zend_Json::encode($retorno);
                }
                unset($daoEmbalagens, $filter);
                break;

            default:
                $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
                $this->getFrontController()->setParam('noViewRenderer', false);
        }
    }

    public function insertAction()
    {
        if ($this->getRequest()->isPost()) {
            $retorno = array('success' => false, 'message' => null, 'errors' => array(), 'embalagem' => array());
            $daoEmbalagens = App_Model_DAO_Embalagens::getInstance();
            try {
                $embalagem = $daoEmbalagens->createRow();
                $embalagem = $this->montaObjeto($embalagem);
                try {
                    $embalagem->save();
                    $retorno['success'] = true;
                    $retorno['message'] = sprintf('Embalagem <b>%s</b> cadastrado com sucesso.', $embalagem->getNome());
                } catch (App_Validate_Exception $e) {
                    $retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
                    throw new Exception('Por favor, verifique os campos marcados em vermelho.');
                } catch (Exception $e) {
                    throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o embalagem.');
                }
            } catch (Exception $e) {
                $retorno['success'] = false;
                $retorno['message'] = $e->getMessage();
            }
            unset($daoEmbalagens, $embalagem);

            App_Funcoes_UTF8::encode($retorno);
            echo Zend_Json::encode($retorno);
        } else {
            $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
            $this->buildForm();
        }
    }

    public function updateAction()
    {
        if (false != ($idRegistro = $this->getRequest()->getParam('emb_idEmbalagem', false))) {
            $retorno = array('success' => false, 'message' => null, 'errors' => array(), 'embalagem' => array());
            $daoEmbalagens = App_Model_DAO_Embalagens::getInstance();
            try {
                $embalagem = $daoEmbalagens->fetchRow(
                    $daoEmbalagens->select()->where('emb_idEmbalagem = ?', $idRegistro)
                );
                if (null == $embalagem) {
                    throw new Exception('A embalagem solicitada n�o foi encontrada.');
                }

                if ($this->getRequest()->getParam('load')) {
                    $retorno['success'] = true;
                    $arrEmbalagem = $embalagem->toArray();
                    $arrEmbalagem['emb_nome'] = html_entity_decode($arrEmbalagem['emb_nome'], null, 'ISO-8859-1');

                    foreach ($embalagem->getPermissoes() as $permissao) {
                        if ($permissao->getGrupoRede()) {
                            $arrEmbalagem['emb_permissoes'][] = array(
                                'idControle' => $permissao->getGrupoRede()->getCodigo() . 'GR',
                                'id' => $permissao->getGrupoRede()->getCodigo(),
                                'nome' => $permissao->getGrupoRede()->getNome(),
                                'grupo' => 'GR'
                            );
                        }

                        if ($permissao->getRede()) {
                            $arrEmbalagem['emb_permissoes'][] = array(
                                'idControle' => $permissao->getRede()->getCodigo() . 'RE',
                                'id' => $permissao->getRede()->getCodigo(),
                                'nome' => $permissao->getRede()->getNome(),
                                'grupo' => 'RE'
                            );
                        }

                        if ($permissao->getGrupoLoja()) {
                            $arrEmbalagem['emb_permissoes'][] = array(
                                'idControle' => $permissao->getGrupoLoja()->getCodigo() . 'GL',
                                'id' => $permissao->getGrupoLoja()->getCodigo(),
                                'nome' => $permissao->getGrupoLoja()->getNome(),
                                'grupo' => 'GL'
                            );
                        }

                        if ($permissao->getLoja()) {
                            $arrEmbalagem['emb_permissoes'][] = array(
                                'idControle' => $permissao->getLoja()->getCodigo() . 'LO',
                                'id' => $permissao->getLoja()->getCodigo(),
                                'nome' => $permissao->getLoja()->getNomeCompleto(),
                                'grupo' => 'LO'
                            );
                        }

                        if ($permissao->getLinha()) {
                            $arrEmbalagem['emb_permissoes'][] = array(
                                'idControle' => $permissao->getLinha()->getCodigo() . 'LI',
                                'id' => $permissao->getLinha()->getCodigo(),
                                'nome' => $permissao->getLinha()->getNome(),
                                'grupo' => 'LI'
                            );
                        }
                    }

                    foreach ($embalagem->getEstados() as $estado) {
                        $arrEmbalagem['emb_estados'][] = $estado->getUf();
                    }

                    $retorno['embalagem'] = array($arrEmbalagem);
                } else {
                    $embalagem = $this->montaObjeto($embalagem);

                    try {
                        $embalagem->save();

                        $retorno['success'] = true;
                        $retorno['message'] = sprintf('Embalagem <b>%s</b> alterado com sucesso.', $embalagem->getNome());
                    } catch (App_Validate_Exception $e) {
                        $retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
                        throw new Exception('Por favor, verifique os campos marcados em vermelho.');
                    } catch (Exception $e) {
                        throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar a embalagem.');
                    }
                }
            } catch (Exception $e) {
                $retorno['success'] = false;
                $retorno['message'] = $e->getMessage();
            }
            unset($daoEmbalagens);

            App_Funcoes_UTF8::encode($retorno);
            echo Zend_Json::encode($retorno);
        } else {
            $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
            $this->buildForm();
        }
    }

    public function deleteAction()
    {
        $retorno = array('success' => false, 'message' => null);
        $daoEmbalagens = App_Model_DAO_Embalagens::getInstance();
        try {
            $embalagem = $daoEmbalagens->fetchRow(
                $daoEmbalagens->select()->where('emb_idEmbalagem = ?', $this->getRequest()->getParam('idEmbalagem'))
            );
            if (null == $embalagem) {
                throw new Exception('A embalagem solicitada n�o foi encontrada.');
            }
            try {
                $nome = $embalagem->getNome();
                $embalagem->delete();

                $retorno['success'] = true;
                $retorno['message'] = sprintf('Embalagem <b>%s</b> removida com sucesso.', $nome);
            } catch (Exception $e) {
                throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a embalagem.');
            }
        } catch (Exception $e) {
            $retorno['success'] = false;
            $retorno['message'] = $e->getMessage();
        }
        unset($daoEmbalagens, $embalagem);

        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    protected function montaObjeto(App_Model_Entity_Embalagem $embalagem)
    {
        $daoImagens = App_Model_DAO_Galerias_Arquivos::getInstance();
        $embalagem->setMiniatura(
                $daoImagens->fetchRow(
                    $daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('emb_idMiniatura'))
                )
            )
            ->setManual(
                $daoImagens->fetchRow(
                    $daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('emb_idManual'))
                )
            )
            ->setNome(htmlentities($this->getRequest()->getParam('emb_nome')))
            ->setData(App_Funcoes_Date::conversion($this->getRequest()->getParam('emb_data')))
            ->setDestaque($this->getRequest()->getParam('emb_destaque', 0))
            ->setStatus($this->getRequest()->getParam('emb_status', 0));


        if ($this->getRequest()->getParam('emb_idImagem', false) != false) {
            $embalagem->setImagem(
                $daoImagens->fetchRow(
                    $daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('emb_idImagem'))
                )
            );
        } else {
            $embalagem->setImagem(null);
        }
        if ($this->getRequest()->getParam('emb_idImagemDestaque', false) != false) {
            $embalagem->setImagemDestaque(
                $daoImagens->fetchRow(
                    $daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('emb_idImagemDestaque'))
                )
            );
        } else {
            $embalagem->setImagemDestaque(null);
        }

        // Permiss�es
        $embalagem->getPermissoes()->offsetRemoveAll();
        if ($this->getRequest()->getParam('emb_permissoes')) {

            $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
            $daoRedes = App_Model_DAO_Redes::getInstance();
            $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
            $daoLojas = App_Model_DAO_Lojas::getInstance();
            $daoLinha = App_Model_DAO_Linhas::getInstance();

            $emb_permissoes = Zend_Json::decode($this->getRequest()->getParam('emb_permissoes', array()));
            foreach ($emb_permissoes as $permissoes) {
                $permissao = App_Model_DAO_Embalagens_Permissoes::getInstance()->createRow();
                switch ($permissoes['grupo']) {
                    case 'GR':
                        $permissao->setGrupoRede(
                                $daoGrupoRedes->fetchRow(
                                    $daoGrupoRedes->select()->where('grp_rede_idGrupo = ?', $permissoes['id'])
                                )
                            )
                            ->setRede(null)
                            ->setGrupoLoja(null)
                            ->setLoja(null)
                            ->setLinha(null);
                        break;
                    case 'RE':
                        $permissao->setRede(
                                $daoRedes->fetchRow(
                                    $daoRedes->select()->where('rede_idRede = ?', $permissoes['id'])
                                )
                            )
                            ->setGrupoRede(null)
                            ->setGrupoLoja(null)
                            ->setLoja(null)
                            ->setLinha(null);
                        break;
                    case 'GL':
                        $permissao->setGrupoLoja(
                                $daoGrupoLojas->fetchRow(
                                    $daoGrupoLojas->select()->where('grp_loja_idGrupo = ?', $permissoes['id'])
                                )
                            )
                            ->setGrupoRede(null)
                            ->setRede(null)
                            ->setLoja(null)
                            ->setLinha(null);
                        break;
                    case 'LO':
                        $permissao->setLoja(
                                $daoLojas->fetchRow(
                                    $daoLojas->select()->where('loja_idLoja = ?', $permissoes['id'])
                                )
                            )
                            ->setGrupoRede(null)
                            ->setRede(null)
                            ->setGrupoLoja(null)
                            ->setLinha(null);
                        break;
                    case 'LI':
                        $permissao->setLinha(
                                $daoLinha->fetchRow(
                                    $daoLinha->select()->where('lin_idLinha = ?', $permissoes['id'])
                                )
                            )
                            ->setGrupoRede(null)
                            ->setRede(null)
                            ->setGrupoLoja(null)
                            ->setLoja(null);
                        break;
                }
                $embalagem->getPermissoes()->offsetAdd($permissao);
            }
        }

        // Estados
        $embalagem->getEstados()->offsetRemoveAll();
        if (false != ($estados = $this->getRequest()->getParam('emb_estados', false))) {
            $arrEstados = Zend_Json::decode($estados);
            foreach ($arrEstados as $key) {
                $estado = App_Model_DAO_Embalagens_Estados::getInstance()->createRow();
                $estado->setUf($key);
                $embalagem->getEstados()->offsetAdd($estado);
            }
        }
        return $embalagem;
    }

    protected function buildForm()
    {
        $arrEstados = array();
        foreach (App_Funcoes_Rotulos::$UF as $key => $value) {
            $arrEstados[] = array(
                'uf' => $key,
                'nome' => $value
            );
        }
        App_Funcoes_UTF8::encode($arrEstados);
        $this->view->estados = Zend_Json::encode($arrEstados);

        $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
        $this->render('form');
    }

    protected function exportExcel(Zend_Db_Select $filter)
    {
        require_once('Spreadsheet/Excel/Writer.php');
        header("Content-type: application/Octet-Stream");
        header("Content-Disposition: inline; filename=Embalagens.xls");
        header("Content-Disposition: attachment; filename=Embalagens.xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        $xls = new Spreadsheet_Excel_Writer();
        $xls->SetVersion(8);

        $format['titulo'] = & $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
        $format['subtitulo'] = & $xls->addFormat(array('bold' => 1, 'size' => 8, 'fontFamily' => 'arial', 'align' => 'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border' => 1));
        $format['subtitulo-esq'] = & $xls->addFormat(array('bold' => 1, 'size' => 8, 'fontFamily' => 'arial', 'align' => 'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border' => 1));
        $format['normal'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1));
        $format['normal-esq'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'left'));
        $format['normal-cen'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'center'));
        $format['normal-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'fgColor' => 'silver'));
        $format['normal-esq-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'left', 'fgColor' => 'silver'));
        $format['normal-cen-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'center', 'fgColor' => 'silver'));
        $plan = & $xls->addWorksheet('Embalagens');

        $plan->writeString(0, 0, 'Listagem de Embalagens', $format['titulo']);
        $plan->setMerge(0, 0, 0, 4);
        $plan->setColumn(0, 0, 15);
        $plan->setColumn(1, 1, 20);
        $plan->setColumn(1, 2, 35);
        $plan->setColumn(1, 3, 20);
        $plan->setColumn(1, 4, 20);

        $plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
        $plan->writeString(2, 1, 'Data', $format['subtitulo']);
        $plan->writeString(2, 2, 'Nome', $format['subtitulo']);
        $plan->writeString(2, 3, 'Destaque?', $format['subtitulo']);
        $plan->writeString(2, 4, 'Status', $format['subtitulo']);

        $daoEmbalagens = App_Model_DAO_Embalagens::getInstance();
        $filter->limit(null, null);
        $totalRegistros = $daoEmbalagens->getCount(clone $filter);
        if ($totalRegistros > 0) {
            $linha = 3;
            for ($i = 0; $i < $totalRegistros; $i += 30) {
                $filter->limit(30, $i);
                $rsRegistros = $daoEmbalagens->createRowset($daoEmbalagens->getAdapter()->fetchAll($filter));
                foreach ($rsRegistros as $record) {
                    $plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
                    $plan->writeString($linha, 1, App_Funcoes_Date::conversion($record->getData()), $format["normal-cen"]);
                    $plan->writeString($linha, 2, $record->getNome(), $format["normal-cen"]);
                    $plan->writeString($linha, 3, App_Funcoes_Rotulos::$statusSimNao[$record->getDestaque()], $format["normal-cen"]);
                    $plan->writeString($linha, 4, App_Funcoes_Rotulos::$statusSimNao[$record->getStatus()], $format["normal-cen"]);
                    $linha++;
                }
                unset($rsRegistros);
            }
        } else {
            $plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
            for ($c = 1; $c <= 7; $c++) {
                $plan->writeString(3, $c, '', $format["normal-cen"]);
            }
            $plan->setMerge(3, 0, 3, 7);
        }
        unset($daoEmbalagens, $filter);

        $xls->close();
    }

}
