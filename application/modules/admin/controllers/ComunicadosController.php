<?php

class ComunicadosController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoComunicados = App_Model_DAO_Comunicados::getInstance();
				$filter = $daoComunicados->select()
					->from($daoComunicados)
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'com_titulo')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('comunicados' => array(), 'total' => 0);
					$rsComunicado = $daoComunicados->fetchAll($filter);
					
					foreach ($rsComunicado as $comunicado) {
						$retorno['comunicados'][] = array(
							'com_idComunicado' => $comunicado->getCodigo(),
							'com_dataCadastro' => $comunicado->getDataCadastro(),
							'com_dataInicial' => App_Funcoes_Date::conversion($comunicado->getDataInicial()),
							'com_dataFinal' =>  App_Funcoes_Date::conversion($comunicado->getDataFinal()),
							'com_titulo' => $comunicado->getTitulo(),
							'com_leituraObrigatoria' => (string) (int) $comunicado->getLeituraObrigatoria(),					
							'com_status' => (string) (int) $comunicado->getStatus()
						);
					}
					$retorno['total'] = $daoComunicados->getCount($filter);
					unset($rsComunicado);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoComunicados, $filter);
			break;
			
			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}
	
	public function insertAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'comunicado' => array());
			$daoComunicados = App_Model_DAO_Comunicados::getInstance();
			try {
				$comunicado = $daoComunicados->createRow();
				$comunicado = $this->montaObjeto($comunicado);
				try {
					$comunicado->save();
					$retorno['success'] = true;
					$retorno['message'] = sprintf('Comunicado <b>%s</b> cadastrado com sucesso.', $comunicado->getTitulo());
				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					throw new Exception('Por favor, verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o comunicado.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoComunicados, $comunicado);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('com_idComunicado', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'comunicado' => array());
			$daoComunicados = App_Model_DAO_Comunicados::getInstance();
			try {
				$comunicado = $daoComunicados->fetchRow(
					$daoComunicados->select()->where('com_idComunicado = ?', $idRegistro)
				);
				if (null == $comunicado) {
					throw new Exception('O comunicado solicitada n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					$retorno['success'] = true;
					$arrComunicado = $comunicado->toArray();
					
					foreach ($comunicado->getPermissoes() as $permissao) {
						if($permissao->getGrupoRede()) {
							$arrComunicado['com_permissoes'][] = array(
								'idControle' => $permissao->getGrupoRede()->getCodigo() . 'GR',
								'id' => $permissao->getGrupoRede()->getCodigo(),
								'nome' => $permissao->getGrupoRede()->getNome(),
								'grupo' => 'GR'
							);
						}	

						if($permissao->getRede()) {
							$arrComunicado['com_permissoes'][] = array(
								'idControle' => $permissao->getRede()->getCodigo() . 'RE',
								'id' => $permissao->getRede()->getCodigo(),
								'nome' => $permissao->getRede()->getNome(),
								'grupo' => 'RE'
							);
						}

						if($permissao->getGrupoLoja()) {
							$arrComunicado['com_permissoes'][] = array(
								'idControle' => $permissao->getGrupoLoja()->getCodigo() . 'GL',
								'id' => $permissao->getGrupoLoja()->getCodigo(),
								'nome' => $permissao->getGrupoLoja()->getNome(),
								'grupo' => 'GL'
							);
						}
						
						if($permissao->getLoja()) {
							$arrComunicado['com_permissoes'][] = array(
								'idControle' => $permissao->getLoja()->getCodigo() . 'LO',
								'id' => $permissao->getLoja()->getCodigo(),
								'nome' => $permissao->getLoja()->getNomeCompleto(),
								'grupo' => 'LO'
							);
						}
						
						if($permissao->getLinha()) {
							$arrComunicado['com_permissoes'][] = array(
								'idControle' => $permissao->getLinha()->getCodigo() . 'LI',
								'id' => $permissao->getLinha()->getCodigo(),
								'nome' => $permissao->getLinha()->getNome(),
								'grupo' => 'LI'
							);
						}
					}
					
					foreach ($comunicado->getEstados() as $estado) {
						$arrComunicado['com_estados'][] = $estado->getUf();
					}
					
					$retorno['comunicado'] = array($arrComunicado);
				} else {
					$comunicado = $this->montaObjeto($comunicado);
					
					try {
						$comunicado->save();

						$retorno['success'] = true;
						$retorno['message'] = sprintf('Comunicado <b>%s</b> alterado com sucesso.', $comunicado->getTitulo());
					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor, verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o comunicado.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoComunicados);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoComunicados = App_Model_DAO_Comunicados::getInstance();
		try {
			$comunicado = $daoComunicados->fetchRow(
				$daoComunicados->select()->where('com_idComunicado = ?', $this->getRequest()->getParam('idComunicado'))
			);
			if (null == $comunicado) {
				throw new Exception('O comunicado solicitado n�o foi encontrado.');
			}
			try {
				$nome = $comunicado->getTitulo();
				$comunicado->delete();

				$retorno['success'] = true;
				$retorno['message'] = sprintf('Comunicado <b>%s</b> removido com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o comunicado.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoComunicados, $comunicado);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}

	protected function montaObjeto(App_Model_Entity_Comunicado $comunicado) {
		$daoImagens = App_Model_DAO_Galerias_Arquivos::getInstance();
		$comunicado->setImagem(
				$daoImagens->fetchRow(
					$daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('com_idImagem'))
				)
			)
			->setMiniatura(
				$daoImagens->fetchRow(
					$daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('com_idMiniatura'))
				)
			)
			->setTitulo(utf8_decode($this->getRequest()->getParam('com_titulo')))
			->setDescricao(utf8_decode($this->getRequest()->getParam('com_descricao')))
			->setDataInicial(App_Funcoes_Date::conversion($this->getRequest()->getParam('com_dataInicial')))
			->setDataFinal(App_Funcoes_Date::conversion($this->getRequest()->getParam('com_dataFinal')))
			->setLeituraObrigatoria($this->getRequest()->getParam('com_leituraObrigatoria', 0))
			->setStatus($this->getRequest()->getParam('com_status', 0));
			
		// Permiss�es
		$comunicado->getPermissoes()->offsetRemoveAll();
		if ($this->getRequest()->getParam('com_permissoes')) {
			
			$daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
			$daoRedes = App_Model_DAO_Redes::getInstance();
			$daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
			$daoLojas = App_Model_DAO_Lojas::getInstance();
			$daoLinha = App_Model_DAO_Linhas::getInstance();
			
			$com_permissoes = Zend_Json::decode($this->getRequest()->getParam('com_permissoes', array()));
			foreach ($com_permissoes as $permissoes) {
				$permissao = App_Model_DAO_Comunicados_Permissoes::getInstance()->createRow();
				switch ($permissoes['grupo']) {
					case 'GR':
						$permissao->setGrupoRede(
							$daoGrupoRedes->fetchRow(
								$daoGrupoRedes->select()->where('grp_rede_idGrupo = ?', $permissoes['id'])
							)
						)
						->setRede(null)
						->setGrupoLoja(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'RE':
						$permissao->setRede(
							$daoRedes->fetchRow(
								$daoRedes->select()->where('rede_idRede = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setGrupoLoja(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'GL':
						$permissao->setGrupoLoja(
							$daoGrupoLojas->fetchRow(
								$daoGrupoLojas->select()->where('grp_loja_idGrupo = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'LO':
						$permissao->setLoja(
							$daoLojas->fetchRow(
								$daoLojas->select()->where('loja_idLoja = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setGrupoLoja(null)
						->setLinha(null);
					break;
					case 'LI':
						$permissao->setLinha(
							$daoLinha->fetchRow(
								$daoLinha->select()->where('lin_idLinha = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setGrupoLoja(null)
						->setLoja(null);
					break;
				}
				$comunicado->getPermissoes()->offsetAdd($permissao);
			}
		}
		
		// Estados
		$comunicado->getEstados()->offsetRemoveAll();
		if (false != ($estados = $this->getRequest()->getParam('com_estados', false))) {
			$arrEstados = Zend_Json::decode($estados);
			foreach ($arrEstados as $key) {
				$estado = App_Model_DAO_Comunicados_Estados::getInstance()->createRow();
				$estado->setUf($key);
				$comunicado->getEstados()->offsetAdd($estado);
			}
		}
		return $comunicado;
	}

	protected function buildForm()
	{
		$arrEstados = array();
		foreach (App_Funcoes_Rotulos::$UF as $key => $value) {
			$arrEstados[] = array(
				'uf' => $key,
				'nome' => $value
			);
		}
		App_Funcoes_UTF8::encode($arrEstados);
		$this->view->estados = Zend_Json::encode($arrEstados);

		$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		$this->render('form');
	}
	
	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Comunicados.xls");
		header("Content-Disposition: attachment; filename=Comunicados.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Comunicados');

		$plan->writeString(0, 0, 'Listagem de Comunicados', $format['titulo']);
		$plan->setMerge(0, 0, 0, 7);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 20);
		$plan->setColumn(1, 2, 20);
		$plan->setColumn(1, 3, 20);
		$plan->setColumn(1, 4, 35);
		$plan->setColumn(1, 5, 35);
		$plan->setColumn(1, 6, 20);
		$plan->setColumn(1, 7, 20);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'Data de Cadastro', $format['subtitulo']);
		$plan->writeString(2, 2, 'Data Inicial', $format['subtitulo']);
		$plan->writeString(2, 3, 'Data Final', $format['subtitulo']);
		$plan->writeString(2, 4, 'T�tulo', $format['subtitulo']);
		$plan->writeString(2, 5, 'Descri��o', $format['subtitulo']);
		$plan->writeString(2, 6, 'Leitura Obrigat�ria', $format['subtitulo']);
		$plan->writeString(2, 7, 'Status', $format['subtitulo']);

		$daoComunicados = App_Model_DAO_Comunicados::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoComunicados->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoComunicados->createRowset($daoComunicados->getAdapter()->fetchAll($filter));
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, App_Funcoes_Date::conversion($record->getDataCadastro()), $format["normal-cen"]);
					$plan->writeString($linha, 2, App_Funcoes_Date::conversion($record->getDataInicial()), $format["normal-cen"]);
					$plan->writeString($linha, 3, App_Funcoes_Date::conversion($record->getDataFinal()), $format["normal-cen"]);
					$plan->writeString($linha, 4, $record->getTitulo(), $format["normal-cen"]);
					$plan->writeString($linha, 5, $record->getDescricao() ? strip_tags(nl2br($record->getDescricao())) : '', $format["normal-cen"]);					
					$plan->writeString($linha, 6, App_Funcoes_Rotulos::$statusSimNao[$record->getLeituraObrigatoria()], $format["normal-cen"]);
					$plan->writeString($linha, 7, App_Funcoes_Rotulos::$statusSimNao[$record->getStatus()], $format["normal-cen"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 7; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 7);
		}
		unset($daoComunicados, $filter);

		$xls->close();
	}
}