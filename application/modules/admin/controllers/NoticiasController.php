<?php

class NoticiasController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $this->view->HeadLink()->appendStylesheet('styles/desktop/noticias.css', 'screen');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.scrollbars.css', 'screen');
        $this->view->HeadScript()->appendFile('scripts/lib/jquery.scrollbars.min.js');

        $daoNoticias = App_Model_DAO_Noticias::getInstance();

        $selectNoticias = $this->view->Permissoes()->selectNoticias();
        $selectNoticiasAux = clone $selectNoticias;
        $selectListaNoticias = clone $selectNoticias;

        if (!$idNoticia = $this->getRequest()->getParam('idNoticia', false)) {
            $selectNoticias->order('not_dataCadastro DESC')
                ->where('not_status = 1');
            $noticia = $daoNoticias->getAdapter()->fetchRow($selectNoticias);
        } else {
            $selectNoticias->where('not_idNoticia = ?', $idNoticia);
            $noticia = $daoNoticias->getAdapter()->fetchRow($selectNoticias);
        }

        if ($noticia) {
            $this->view->noticia = ($noticia) ? $daoNoticias->createRow($noticia) : null;

            // meses com noticias cadastradas
            $selectData = $selectNoticiasAux->reset(Zend_Db_Select::COLUMNS)
                ->reset(Zend_Db_Select::GROUP)
                ->columns('not_dataCadastro')
                ->group('MONTH(not_dataCadastro)')
                ->group('YEAR(not_dataCadastro)')
                ->where('not_status = 1')
                ->order('not_dataCadastro DESC');
            $noticiasDatas = $daoNoticias->getAdapter()->fetchAll($selectData);

            $this->view->noticiasDatas = ($noticiasDatas) ? $daoNoticias->createRowset($noticiasDatas) : null;

            // Lista noticias
            $mesAtual = explode('-', $noticia['not_dataCadastro']);
            $selectListaNoticias->where('MONTH(not_dataCadastro) = ?', $mesAtual[1]);
            $selectListaNoticias->where('YEAR(not_dataCadastro) = ?', $mesAtual[0]);
            $selectListaNoticias->where('not_status = 1');

            if (!$idNoticia = $this->getRequest()->getParam('idNoticia', false)) {
                $selectListaNoticias->where('not_idNoticia != ?', $noticia['not_idNoticia']);
            } else {
                $selectListaNoticias->where('not_idNoticia != ?', $this->getRequest()->getParam('idNoticia'));
            }
            $listaNoticias = $daoNoticias->getAdapter()->fetchAll($selectListaNoticias);
            $this->view->listaNoticias = ($listaNoticias) ? $daoNoticias->createRowset($listaNoticias) : null;
        } else {
            $this->view->noticia = null;
        }
    }

    public function listaNoticiasAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->getResponse()->setHeader('Content-Type', 'text/json');

        $daoNoticias = App_Model_DAO_Noticias::getInstance();
        $data = explode('-', $this->getRequest()->getParam('data'));
        $idNoticiaAtual = $this->getRequest()->getParam('idNoticia');

        $selectNoticias = $this->view->Permissoes()->selectNoticias();
        $selectNoticias->where('MONTH(not_dataCadastro) = ?', $data[0]);
        $selectNoticias->where('YEAR(not_dataCadastro) = ?', $data[1]);
        $selectNoticias->where('not_status = 1');
        $selectNoticias->where('not_idNoticia != ?', $idNoticiaAtual);

        //@todo retirar a noticia atual where != idAtual

        $listaNoticias = $daoNoticias->createRowset(
            $daoNoticias->getAdapter()->fetchAll($selectNoticias)
        );

        $view = new Zend_View();
        $view->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

        $retorno = array();
        $conteudo = '';
        foreach ($listaNoticias as $noticia) {
            $view->noticia = $noticia;
            $conteudo .= $view->render('noticias/bloco-noticia.phtml');
        }
        $retorno['conteudo'] = $conteudo;
        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

}
