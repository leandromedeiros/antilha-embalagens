<?php

class BannersController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoBanners = App_Model_DAO_Banners::getInstance();
				$filter = $daoBanners->select()
					->from($daoBanners)
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'ban_ordem')} {$this->getRequest()->getParam('dir', 'DESC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('banners' => array(), 'total' => 0);
					$rsBanners = $daoBanners->fetchAll($filter);
					foreach ($rsBanners as $banner) {
						$retorno['banners'][] = array(
							'ban_idBanner' => $banner->getCodigo(),
							'ban_ordem' => $banner->getOrdem(),
							'ban_titulo' => $banner->getTitulo(),
							'ban_link' => $banner->getLink(),
							'ban_status' => (string) (int) $banner->getStatus()
						);
					}
					$retorno['total'] = $daoBanners->getCount($filter);
					unset($rsBanners);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoBanners, $filter);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function insertAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'banner' => array());
			$daoBanners = App_Model_DAO_Banners::getInstance();
			$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();

			try {
				$banner = $daoBanners->createRow();
				$banner = $this->montaObjeto($banner);
				
				try {
					$banner->save();
					$retorno['success'] = true;
					$retorno['message'] = sprintf('Banner <b>%s</b> cadastrado com sucesso.', $banner->getTitulo());

				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					throw new Exception('Por favor verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o banner.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoBanners, $daoArquivos, $banner);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('ban_idBanner', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'banner' => array());
			$daoBanners = App_Model_DAO_Banners::getInstance();
			try {
				$banner = $daoBanners->fetchRow(
					$daoBanners->select()->where('ban_idBanner = ?', $idRegistro)
				);
				if (null == $banner) {
					throw new Exception('O banner solicitado n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					// carrega os dados
					$retorno['success'] = true;
					//$retorno ['banner'] = array($banner->toArray());
					$arrBanner = $banner->toArray();
					
					foreach ($banner->getPermissoes() as $permissao) {
						if($permissao->getGrupoRede()) {
							$arrBanner['ban_permissoes'][] = array(
								'idControle' => $permissao->getGrupoRede()->getCodigo() . 'GR',
								'id' => $permissao->getGrupoRede()->getCodigo(),
								'nome' => $permissao->getGrupoRede()->getNome(),
								'grupo' => 'GR'
							);
						}	

						if($permissao->getRede()) {
							$arrBanner['ban_permissoes'][] = array(
								'idControle' => $permissao->getRede()->getCodigo() . 'RE',
								'id' => $permissao->getRede()->getCodigo(),
								'nome' => $permissao->getRede()->getNome(),
								'grupo' => 'RE'
							);
						}

						if($permissao->getGrupoLoja()) {
							$arrBanner['ban_permissoes'][] = array(
								'idControle' => $permissao->getGrupoLoja()->getCodigo() . 'GL',
								'id' => $permissao->getGrupoLoja()->getCodigo(),
								'nome' => $permissao->getGrupoLoja()->getNome(),
								'grupo' => 'GL'
							);
						}
						
						if($permissao->getLoja()) {
							$arrBanner['ban_permissoes'][] = array(
								'idControle' => $permissao->getLoja()->getCodigo() . 'LO',
								'id' => $permissao->getLoja()->getCodigo(),
								'nome' => $permissao->getLoja()->getNomeCompleto(),
								'grupo' => 'LO'
							);
						}
						
						if($permissao->getLinha()) {
							$arrBanner['ban_permissoes'][] = array(
								'idControle' => $permissao->getLinha()->getCodigo() . 'LI',
								'id' => $permissao->getLinha()->getCodigo(),
								'nome' => $permissao->getLinha()->getNome(),
								'grupo' => 'LI'
							);
						}
					}
					
					foreach ($banner->getEstados() as $estado) {
						$arrBanner['ban_estados'][] = $estado->getUf();
					}
					
					$retorno['banner'] = array($arrBanner);
				} else {
					//atualiza os dados
					$banner = $this->montaObjeto($banner);
					
					try {
						$banner->save();
						
						$retorno['success'] = true;
						$retorno['message'] = sprintf('Banner <b>%s</b> alterado com sucesso.', $banner->getTitulo());

					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o banner.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoBanners);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);

		} else {		
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	protected function montaObjeto(App_Model_Entity_Banner $banner) {
		$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();
		// atualiza os dados
		$banner->setTitulo(utf8_decode($this->getRequest()->getParam('ban_titulo')))
			->setLink(utf8_decode($this->getRequest()->getParam('ban_link')))
			->setImagem(
				$daoArquivos->fetchRow(
					$daoArquivos->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('ban_idImagem'))
				)
			)
			->setOrdem($this->getRequest()->getParam('ban_ordem'))
			->setStatus($this->getRequest()->getParam('ban_status'));

		// Permiss�es
		$banner->getPermissoes()->offsetRemoveAll();
		if ($this->getRequest()->getParam('ban_permissoes')) {
			
			$daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
			$daoRedes = App_Model_DAO_Redes::getInstance();
			$daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
			$daoLojas = App_Model_DAO_Lojas::getInstance();
			$daoLinha = App_Model_DAO_Linhas::getInstance();
			
			$ban_permissoes = Zend_Json::decode($this->getRequest()->getParam('ban_permissoes', array()));
			foreach ($ban_permissoes as $permissoes) {
				$permissao = App_Model_DAO_Banners_Permissoes::getInstance()->createRow();
				switch ($permissoes['grupo']) {
					case 'GR':
						$permissao->setGrupoRede(
							$daoGrupoRedes->fetchRow(
								$daoGrupoRedes->select()->where('grp_rede_idGrupo = ?', $permissoes['id'])
							)
						)
						->setRede(null)
						->setGrupoLoja(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'RE':
						$permissao->setRede(
							$daoRedes->fetchRow(
								$daoRedes->select()->where('rede_idRede = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setGrupoLoja(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'GL':
						$permissao->setGrupoLoja(
							$daoGrupoLojas->fetchRow(
								$daoGrupoLojas->select()->where('grp_loja_idGrupo = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'LO':
						$permissao->setLoja(
							$daoLojas->fetchRow(
								$daoLojas->select()->where('loja_idLoja = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setGrupoLoja(null)
						->setLinha(null);
					break;
					case 'LI':
						$permissao->setLinha(
							$daoLinha->fetchRow(
								$daoLinha->select()->where('lin_idLinha = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setGrupoLoja(null)
						->setLoja(null);
					break;
				}
				$banner->getPermissoes()->offsetAdd($permissao);
			}
		}
		
		// Estados
		$banner->getEstados()->offsetRemoveAll();
		if (false != ($estados = $this->getRequest()->getParam('ban_estados', false))) {
			$arrEstados = Zend_Json::decode($estados);
			foreach ($arrEstados as $key) {
				$estado = App_Model_DAO_Banners_Estados::getInstance()->createRow();
				$estado->setUf($key);
				$banner->getEstados()->offsetAdd($estado);
			}
		}
		return $banner;
	}
	
	protected function buildForm()
	{
		$arrEstados = array();
		foreach (App_Funcoes_Rotulos::$UF as $key => $value) {
			$arrEstados[] = array(
				'uf' => $key,
				'nome' => $value
			);
		}
		App_Funcoes_UTF8::encode($arrEstados);
		$this->view->estados = Zend_Json::encode($arrEstados);

		$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		$this->render('form');
	}
	
	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoBanners = App_Model_DAO_Banners::getInstance();
		try {
			$banner = $daoBanners->fetchRow(
				$daoBanners->select()->where('ban_idBanner = ?', $this->getRequest()->getParam('idBanner'))
			);
			if (null == $banner) {
				throw new Exception('O banner solicitado n�o foi encontrada.');
			}
			try {
				$nome = $banner->getTitulo();
				$banner->delete();
				$retorno['success'] = true;
				$retorno['message'] = sprintf('Banner <b>%s</b> removido com sucesso.', $nome);

			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o banner.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoBanners, $banner);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}

	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Banners.xls");
		header("Content-Disposition: attachment; filename=Banners.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Banners');

		$plan->writeString(0, 0, 'Listagem de Banners', $format['titulo']);
		$plan->setMerge(0, 0, 0, 4);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 30);
		$plan->setColumn(2, 2, 35);
		$plan->setColumn(3, 3, 15);
		$plan->setColumn(4, 4, 15);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'T�tulo', $format['subtitulo']);
		$plan->writeString(2, 2, 'Link', $format['subtitulo']);
		$plan->writeString(2, 3, 'Ordem', $format['subtitulo']);
		$plan->writeString(2, 4, 'Status', $format['subtitulo']);

		$daoBanners = App_Model_DAO_Banners::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoBanners->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoBanners->fetchAll($filter);
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, $record->getTitulo(), $format["normal"]);
					$plan->writeString($linha, 2, $record->getLink(), $format["normal"]);
					$plan->writeString($linha, 3, $record->getOrdem(), $format["normal-cen"]);
					$plan->writeString($linha, 4, App_Funcoes_Rotulos::$status[$record->getStatus()], $format["normal-cen"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 5; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 5);
		}
		unset($daoBanners, $filter);

		$xls->close();
	}
}