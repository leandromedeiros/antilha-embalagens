<?php

class Sistema_ConfiguracoesController extends Zend_Controller_Action {

	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		$daoConfiguracao = App_Model_DAO_Sistema_Configuracoes::getInstance();
		$config = $daoConfiguracao->fetchRow($daoConfiguracao->select());
		switch ($this->getRequest()->getParam('acao')) {
			case 'load':
				$config = $config->toArray();
				$config['conf_emailsContato'] = Zend_Json::decode(utf8_encode($config['conf_emailsContato']));
				App_Funcoes_UTF8::decode($config['conf_emailsContato']);
				
				$config['conf_emailsReclamacao'] = Zend_Json::decode(utf8_encode($config['conf_emailsReclamacao']));
				App_Funcoes_UTF8::decode($config['conf_emailsReclamacao']);
				
				$config['conf_emailsOuvidoria'] = Zend_Json::decode(utf8_encode($config['conf_emailsOuvidoria']));
				App_Funcoes_UTF8::decode($config['conf_emailsOuvidoria']);

				$retorno = array(
					'configuracoes' => array($config),
					'success' => true
				);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
			break;
			
			case 'update':
				// Atualiza o XML com os dados postados
				try {
					$daoConfiguracao->getAdapter()->beginTransaction();
					$config
						->setEmailsContato(utf8_decode($this->getRequest()->getParam('conf_emailsContato')))
						->setEmailsReclamacao(utf8_decode($this->getRequest()->getParam('conf_emailsReclamacao')))
						->setEmailsOuvidoria(utf8_decode($this->getRequest()->getParam('conf_emailsOuvidoria')))
						->setPortaEmail($this->getRequest()->getParam('conf_portaEmail', 25))
						->setFormaEnvioEmail($this->getRequest()->getParam('conf_formaEnvioEmail', 'SMTP'))
						->setServidorEmail($this->getRequest()->getParam('conf_servidorEmail', ''))
                                                ->setUsuario($this->getRequest()->getParam('conf_usuario', ''))
						->setEmailRemetente($this->getRequest()->getParam('conf_emailRemetente', ''))
						->setNomeRemetente(utf8_decode($this->getRequest()->getParam('conf_nomeRemetente', '')))
						->setReplyTo($this->getRequest()->getParam('conf_replyTo', ''))
						->setEmailSugestao($this->getRequest()->getParam('conf_emailSugestao', ''))
						->setEmailLoja($this->getRequest()->getParam('conf_emailLoja', ''))
						->setTelAtendimento($this->getRequest()->getParam('conf_telAtendimento', ''))
						->setSenhaEmail($this->getRequest()->getParam('conf_senhaEmail', ''));
					
					//$config->getConfiguracaoEntrega()->offsetRemoveAll();
					
					$config->save();
					$this->geraConfig($config);
					
					$daoConfiguracao->getAdapter()->commit();
					$retorno['success'] = true;
					$retorno['message'] = 'Configurações alteradas com sucesso.';
					$retorno['errors'] = array();
				} catch (Exception $e) {
					$daoConfiguracao->getAdapter()->rollBack();
					$retorno['success'] = false;
					$retorno['message'] = $e->getMessage();
					$retorno['errors'] = array();
				}
	
				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
			break;
			
			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
			break;
		}
	}
	
	private function geraConfig($config) {
		$texto= '<?php
		
		return array(
			//configuração de e-mail
			"formaEnvio" => "'. $config->getFormaEnvioEmail() .'",
			"servidor" => "'. $config->getServidorEmail() .'",
			"porta" => "'. $config->getPortaEmail() .'",
                        "usuario" => "'. $config->getUsuario() .'",
			"emailRemetente" => "'. $config->getEmailRemetente() .'",
			"nomeRemetente" => "'. $config->getNomeRemetente() .'",
			"replyTo" => "'. $config->getReplyTo() .'",
			"emailSugestao" => "'. $config->getEmailSugestao() .'",
			"emailLoja" => "'. $config->getEmailLoja() .'",
			"telAtendimento" => "'. $config->getTelAtendimento() .'",
			"senhaEmail" => "'. $config->getSenhaEmail() .'",
			
			//destinatarios de formularios
			"emailsContato" => \''. $config->getEmailsContato() .'\',
			"emailsReclamacao" => \''. $config->getEmailsReclamacao() .'\',
			"emailsOuvidoria" => \''. $config->getEmailsOuvidoria() .'\'
		);';
		
		$fp = fopen(APPLICATION_ROOT .'/configs/configuracao.php', "w");
		fwrite($fp, $texto);
		fclose($fp);
	}
}