<?php

class Sistema_UsuariosController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'usuarios':
				$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
				$daoPerfis = App_Model_DAO_Sistema_Perfis::getInstance();
				
				$filter = $daoUsuarios->getAdapter()->select()
					->from($daoUsuarios->info('name'))
					->joinInner($daoPerfis->info('name'), 'usr_idPerfil = per_idPerfil')
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'per_nome')} {$this->getRequest()->getParam('dir', 'DESC')}");
				
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));
				
				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('usuarios' => array(), 'total' => 0);
					$rsUsuarios = $daoUsuarios->createRowset($daoUsuarios->getAdapter()->fetchAll($filter));
					foreach ($rsUsuarios as $usuario) {
						$retorno['usuarios'][] = array(
							'usr_idUsuario' => $usuario->getCodigo(),
							'usr_login' => $usuario->getLogin(),
							'usr_idGrupoRede' => $usuario->getGrupoRede() ? $usuario->getGrupoRede()->getCodigo() : null,
							'usr_idRede' => $usuario->getRede() ? $usuario->getRede()->getCodigo() : null,
							'usr_idGrupoLoja' => $usuario->getGrupoLoja() ? $usuario->getGrupoLoja()->getCodigo() : null,
							'usr_idLoja' => $usuario->getLoja() ? $usuario->getLoja()->getCodigo() : null,
							'per_idPerfil' => $usuario->getPerfil()->getCodigo(),
							'per_nome' => $usuario->getPerfil()->getNome(),
							'usr_nome' => $usuario->getNome(),
							'usr_email' => $usuario->getEmail(),
							'usr_status' => (string) (int) $usuario->getStatus()
						);
					}
					$retorno['total'] = $daoUsuarios->getCount($filter);
					unset($rsUsuarios);
				
					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoUsuarios, $filter);
				
			break;

			case 'permissoes':
				$retorno = array();
				$daoAcoes = App_Model_DAO_Sistema_Acoes::getInstance();
				$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
				$usuario = $daoUsuarios->fetchRow(
					$daoUsuarios->select()->where('usr_idUsuario = ?', $this->getRequest()->getParam('idUsuario'))
				);
				foreach ($daoAcoes->fetchAll() as $acao) {
					if($usuario->getPerfil()->getModulos()->find('mod_idModulo', $acao->getModulo()->getCodigo())) {
						$item = array(
							'codigo' => $acao->getCodigo(),
							'modulo' => $acao->getModulo()->getRotulo(),
							'acao' => $acao->getRotulo(),
							'permissao' => $usuario->hasPermission($acao)
						);
						$retorno['acoes'][] = $item;
					}
				}
				unset($daoAcoes, $daoUsuarios);
				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
			break;
			
			case 'combo':
				$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
				
				$filter = $daoUsuarios->getAdapter()->select()
					->from($daoUsuarios->info('name'))
					->order("{$this->getRequest()->getParam('sort', 'usr_login')} {$this->getRequest()->getParam('dir', 'ASC')}");

				$limit = $this->getRequest()->getParam('limit', 30);
				$start = $this->getRequest()->getParam('start', 0);

				$filter->limit($limit, $start);

				if($this->getRequest()->getParam('query')) {
					$filter->orWhere("usr_login LIKE '%{$this->getRequest()->getParam('query')}%'");
					$filter->orWhere("usr_email LIKE '%{$this->getRequest()->getParam('query')}%'");
				}
				
				$retorno = array('usuarios' => array(), 'total' => 0);
				$rsUsuarios = $daoUsuarios->createRowset($daoUsuarios->getAdapter()->fetchAll($filter));

				foreach ($rsUsuarios as $usuario) {
					$retorno['usuarios'][] = array(
						'usr_idUsuario' => $usuario->getCodigo(),
						'usr_login' => $usuario->getLogin(),
						'usr_email' => $usuario->getEmail()
					);
				}

				$retorno['total'] = $daoUsuarios->getCount($filter);
				unset($rsUsuarios);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);

				unset($daoUsuarios, $filter);
			break;
			
			case 'gruporedes':
				$daoGrupos = App_Model_DAO_GrupoRedes::getInstance();
				
				if($idGrupo = $this->getRequest()->getParam('idGrupo')) {
					$filter = $daoGrupos->select()
						->from($daoGrupos)						
						->where('grp_rede_idGrupo = ?', $idGrupo);	
				} else {
					$filter = $daoGrupos->select()
						->from($daoGrupos)
						->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
						->order("{$this->getRequest()->getParam('sort', 'grp_rede_idGrupo')} {$this->getRequest()->getParam('dir', 'ASC')}")
						->where('grp_rede_nome LIKE ?', "%{$this->getRequest()->getParam('query')}%");		
				}
				
				$retorno = array('grupos' => array(), 'total' => 0);
				$rsGrupos = $daoGrupos->fetchAll($filter);
				foreach ($rsGrupos as $grupo) {					
					$retorno['grupos'][] = array(
						'grp_rede_idGrupo' => $grupo->getCodigo(),
						'grp_rede_nome' => $grupo->getNome()						
					);
				}
				$retorno['total'] = $daoGrupos->getCount($filter);
				unset($rsGrupos);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
				
				unset($daoGrupos, $filter);
			break;
			
			case 'redes':
				$daoRedes = App_Model_DAO_Redes::getInstance();
				
				if($idRede = $this->getRequest()->getParam('idRede')) {
					$filter = $daoRedes->select()
						->from($daoRedes)						
						->where('rede_idRede = ?', $idRede);	
				} else {
					$filter = $daoRedes->select()
						->from($daoRedes)
						->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
						->order("{$this->getRequest()->getParam('sort', 'rede_idRede')} {$this->getRequest()->getParam('dir', 'ASC')}")
						->where('rede_nome LIKE ?', "%{$this->getRequest()->getParam('query')}%");		
				}
				
				$retorno = array('redes' => array(), 'total' => 0);
				$rsRedes = $daoRedes->fetchAll($filter);
				foreach ($rsRedes as $rede) {					
					$retorno['redes'][] = array(
						'rede_idRede' => $rede->getCodigo(),
						'rede_nome' => $rede->getNome()						
					);
				}
				$retorno['total'] = $daoRedes->getCount($filter);
				unset($rsRedes);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
				
				unset($daoRedes, $filter);
			break;
			
			case 'grupolojas':
				$daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
				
				if($idGrupo = $this->getRequest()->getParam('idGrupo')) {
					$filter = $daoGrupoLojas->select()
						->from($daoGrupoLojas)						
						->where('grp_loja_idGrupo = ?', $idGrupo);	
				} else {
					$filter = $daoGrupoLojas->select()
						->from($daoGrupoLojas)
						->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
						->order("{$this->getRequest()->getParam('sort', 'grp_loja_idGrupo')} {$this->getRequest()->getParam('dir', 'ASC')}")
						->where('grp_loja_nome LIKE ?', "%{$this->getRequest()->getParam('query')}%");		
				}
				
				$retorno = array('grupos' => array(), 'total' => 0);
				$rsGrupos = $daoGrupoLojas->fetchAll($filter);
				foreach ($rsGrupos as $grupo) {					
					$retorno['grupos'][] = array(
						'grp_loja_idGrupo' => $grupo->getCodigo(),
						'grp_loja_nome' => $grupo->getNome()						
					);
				}
				$retorno['total'] = $daoGrupoLojas->getCount($filter);
				unset($rsGrupos);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
				
				unset($daoGrupoLojas, $filter);
			break;
			
			case 'lojas':
				$daoLojas = App_Model_DAO_Lojas::getInstance();
				
				if($idLoja = $this->getRequest()->getParam('idLoja')) {
					$filter = $daoLojas->select()
						->from($daoLojas)						
						->where('loja_idLoja = ?', $idLoja);	
				} else {
					$filter = $daoLojas->select()
						->from($daoLojas)
						->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
						->order("{$this->getRequest()->getParam('sort', 'loja_idLoja')} {$this->getRequest()->getParam('dir', 'ASC')}")
						->where('loja_nomeCompleto LIKE ?', "%{$this->getRequest()->getParam('query')}%");		
				}
				
				$retorno = array('lojas' => array(), 'total' => 0);
				$rsLojas = $daoLojas->fetchAll($filter);
				foreach ($rsLojas as $loja) {					
					$retorno['lojas'][] = array(
						'loja_idLoja' => $loja->getCodigo(),
						'loja_nomeCompleto' => $loja->getNomeCompleto()						
					);
				}
				$retorno['total'] = $daoLojas->getCount($filter);
				unset($rsLojas);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
				
				unset($daoLojas, $filter);
			break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function usuarioInsertAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'usuario' => array());
			$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
			$daoPerfis = App_Model_DAO_Sistema_Perfis::getInstance();
			try {
				$usuario = $daoUsuarios->createRow();
				$usuario = $this->montaObjeto($usuario);
				try {
					$usuario->save();

					$retorno['success'] = true;
					$retorno['message'] = sprintf('Usu�rio <b>%s</b> cadastrado com sucesso.', $usuario->getNome());
				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					throw new Exception('Por favor verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o usu�rio.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoUsuarios, $daoPerfis, $usuario);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->buildForm();
		}
	}

	public function usuarioUpdateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('usr_idUsuario', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'usuario' => array());
			$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
			try {
				$usuario = $daoUsuarios->fetchRow(
					$daoUsuarios->select()->where('usr_idUsuario = ?', $idRegistro)
				);
				if (null == $usuario) {
					throw new Exception('O usu�rio solicitada n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					$retorno['success'] = true;
					$retorno['usuario'] = array($usuario->toArray());
					$retorno['usuario'][0]['usr_senha'] = '';
				} else {
					$usuario = $this->montaObjeto($usuario);
					try {
						$usuario->save();

						$retorno['success'] = true;
						$retorno['message'] = sprintf('Usu�rio <b>%s</b> alterado com sucesso.', $usuario->getNome());
					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o usu�rio.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoUsuarios);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->buildForm();
		}
	}

	public function usuarioDeleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
		try {
			$usuario = $daoUsuarios->fetchRow(
				$daoUsuarios->select()->where('usr_idUsuario = ?', $this->getRequest()->getParam('idUsuario'))
			);
			if (null == $usuario) {
				throw new Exception('O usu�rio solicitado n�o foi encontrada.');
			}
			try {
				$nome = $usuario->getNome();
				$usuario->delete();

				$retorno['success'] = true;
				$retorno['message'] = sprintf('Usu�rio <b>%s</b> removido com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o usu�rio.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoUsuarios, $usuario);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}

	public function perfilGrantPermsAction()
	{
		$retorno = array('success' => false, 'message' => null);

		$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
		$daoAcoes = App_Model_DAO_Sistema_Acoes::getInstance();

		$usuario = $daoUsuarios->fetchRow(
			$daoUsuarios->select()->where('usr_idUsuario = ?', $this->getRequest()->getParam('idUsuario'))
		);
		
		$perms = $this->getRequest()->getParam('perms', '');
		$perms = $perms != '' ? explode(',', $perms) : array();
		$usuario->getPermissoes()->offsetRemoveAll();
		if(count($perms)) {
			foreach ($perms as $idAcao) {
				$usuario->getPermissoes()->offsetAdd(
					$daoAcoes->fetchRow(
						$daoAcoes->select()->where('mod_acao_idAcao = ?', $idAcao)
					)
				);
			}
		}
		try {
			$usuario->save();

			$retorno['success'] = true;
			$retorno['message'] = sprintf('Permiss�es do usu�rio <b>%s</b> salvas com sucesso.', $usuario->getNome());
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = ('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o usu�rio.');
		}
		unset($daoUsuarios, $daoAcoes, $usuario);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}
	
	protected function buildForm()
	{
		// Perfis
		$daoPerfis = App_Model_DAO_Sistema_Perfis::getInstance();
		$rsPerfis = $daoPerfis->fetchAll($daoPerfis->select()->order('per_nome ASC'));
		$perfis = array();
		foreach ($rsPerfis as $perfil) {
			$perfis[] = array(
				$perfil->getCodigo(),
				$perfil->getNome(),
			);
		}
		unset($daoPerfis, $rsPerfis);
		App_Funcoes_UTF8::encode($perfis);
		$this->view->perfis = Zend_Json::encode($perfis);
		
		$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		$this->render('form-usuario');
	}
	
	protected function montaObjeto(App_Model_Entity_Sistema_Usuario $usuario) {
		
		$daoPerfis = App_Model_DAO_Sistema_Perfis::getInstance();
		
		if( ($idGrupoRede = $this->getRequest()->getParam('usr_idGrupoRede', ''))) {
			$daoGruposRede = App_Model_DAO_GrupoRedes::getInstance();
			$usuario->setGrupoRede(
				$daoGruposRede->fetchRow(
					$daoGruposRede->select()->where('grp_rede_idGrupo = ?', $this->getRequest()->getParam('usr_idGrupoRede'))
				)
			);
		} else {
			$usuario->setGrupoRede(null);
		}
		
		if( ($idRede = $this->getRequest()->getParam('usr_idRede', ''))) {
			$daoRedes = App_Model_DAO_Redes::getInstance();
			$usuario->setRede(
				$daoRedes->fetchRow(
					$daoRedes->select()->where('rede_idRede = ?', $this->getRequest()->getParam('usr_idRede'))
				)
			);
		} else {
			$usuario->setRede(null);
		}
		
		if( ($idGrupoLoja = $this->getRequest()->getParam('usr_idGrupoLoja', ''))) {
			$daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
			$usuario->setGrupoLoja(
				$daoGrupoLojas->fetchRow(
					$daoGrupoLojas->select()->where('grp_loja_idGrupo = ?', $this->getRequest()->getParam('usr_idGrupoLoja'))
				)
			);
		} else {
			$usuario->setGrupoLoja(null);
		}
		
		if( ($idLoja = $this->getRequest()->getParam('usr_idLoja', ''))) {
			$daoLojas = App_Model_DAO_Lojas::getInstance();
			$usuario->setLoja(
				$daoLojas->fetchRow(
					$daoLojas->select()->where('loja_idLoja = ?', $this->getRequest()->getParam('usr_idLoja'))
				)
			);
		} else {
			$usuario->setLoja(null);
		}
		
		$usuario->setPerfil(
				$daoPerfis->fetchRow(
					$daoPerfis->select()->where('per_idPerfil = ?', $this->getRequest()->getParam('usr_idPerfil'))
				)
			)
			->setLogin(utf8_decode($this->getRequest()->getParam('usr_login')))
			->setNome(utf8_decode($this->getRequest()->getParam('usr_nome')))
			->setEmail(utf8_decode($this->getRequest()->getParam('usr_email')))
			->setStatus($this->getRequest()->getParam('usr_status'));

		if($this->getRequest()->getParam('usr_senha', false) != false) {
			$usuario->setSenha(utf8_decode(md5($this->getRequest()->getParam('usr_senha'))));
		}	
			
		if(in_array($usuario->getGrupo()->getTipo(), array('GR', 'RE', 'GL', 'LO'))) {
			$usuario->getPermissoes()->offsetRemoveAll();
			$daoAcoes = App_Model_DAO_Sistema_Acoes::getInstance();
			$acoes = $daoAcoes->fetchAll(
				$daoAcoes->select()->from($daoAcoes)
					->where('FIND_IN_SET(?, mod_acao_acesso)', $usuario->getGrupo()->getTipo())						
			);
			foreach ($acoes as $acao) {
				$usuario->getPermissoes()->offsetAdd($acao);
			}
		}
			
		return $usuario;
	}
}