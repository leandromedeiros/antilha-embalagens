<?php

class Sistema_GruposController extends Zend_Controller_Action
{

    public function init()
    {
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->getResponse()->setHeader('Content-Type', 'text/json');
    }

    public function indexAction()
    {
        switch ($this->getRequest()->getParam('load', false)) {
            case 'galeria':
                $retorno = array();
                foreach (App_Funcoes_Rotulos::$grupos as $key => $grupo) {
                    $retorno[] = array(
                        'id' => $key,
                        'text' => $grupo,
                        'iconCls' => 'icon-categorias-small',
                        'leaf' => true
                    );
                }
                App_Funcoes_UTF8::encode($retorno);
                echo Zend_Json::encode($retorno);
                break;

            case 'grupos':
                $retorno = array('grupos' => array(), 'total' => 0);
                if (false != $this->getRequest()->getParam('galeria', false)) {

                    switch ($this->getRequest()->getParam('galeria')) {
                        case 'GR':

                            $dadosFiltro = array(
                                'id' => 'grp_rede_idGrupo',
                                'nome' => 'grp_rede_nome',
                                'idRede' => 'grp_rede_idGrupo'
                            );

                            $daoGrupos = App_Model_DAO_GrupoRedes::getInstance();
                            $filter = $daoGrupos->select()
                                ->from($daoGrupos)
                                ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0));

                            if ($this->getRequest()->getParam('grpredes')) {
                                $idsGrupos = Zend_Json::decode($this->getRequest()->getParam('grpredes', array()));
                                if (count($idsGrupos))
                                    $filter->where("grp_rede_idGrupo NOT IN ('" . implode("','", $idsGrupos) . "')");
                            }

                            $sort = $this->getRequest()->getParam('sort', 'grp_rede_nome');
                            switch ($sort) {
                                case 'nome': $sort = 'grp_rede_nome';
                                    break;
                                case 'id': $sort = 'grp_rede_idGrupo';
                                    break;
                            }
                            $filter->order("{$sort} {$this->getRequest()->getParam('dir', 'ASC')}");
                            App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'), $dadosFiltro);

                            $rsGrupo = $daoGrupos->fetchAll($filter);
                            foreach ($rsGrupo as $grupo) {
                                $retorno['grupos'][] = array(
                                    'idControle' => $grupo->getCodigo() . 'GR',
                                    'id' => $grupo->getCodigo(),
                                    'nome' => $grupo->getNome(),
                                    'grupo' => 'GR',
                                    'idRede' => '-',
                                );
                            }
                            $retorno['total'] = $daoGrupos->getCount($filter);
                            unset($rsGrupo, $daoGrupos, $filter);
                            break;
                        case 'RE':

                            $dadosFiltro = array(
                                'id' => 'rede_idRede',
                                'nome' => 'rede_nome',
                                'idRede' => 'rede_idRede'
                            );

                            $daoRedes = App_Model_DAO_Redes::getInstance();
                            $filter = $daoRedes->select()
                                ->from($daoRedes)
                                ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0));

                            if ($this->getRequest()->getParam('redes')) {
                                $idsGrupos = Zend_Json::decode($this->getRequest()->getParam('redes', array()));
                                if (count($idsGrupos))
                                    $filter->where("rede_idRede NOT IN ('" . implode("','", $idsGrupos) . "')");
                            }

                            $sort = $this->getRequest()->getParam('sort', 'rede_nome');
                            switch ($sort) {
                                case 'nome': $sort = 'rede_nome';
                                    break;
                                case 'id': $sort = 'rede_idRede';
                                    break;
                            }
                            $filter->order("{$sort} {$this->getRequest()->getParam('dir', 'ASC')}");
                            App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'), $dadosFiltro);

                            $rsRede = $daoRedes->fetchAll($filter);
                            foreach ($rsRede as $rede) {
                                $retorno['grupos'][] = array(
                                    'idControle' => $rede->getCodigo() . 'RE',
                                    'id' => $rede->getCodigo(),
                                    'nome' => $rede->getNome(),
                                    'grupo' => 'RE',
                                    'idRede' => '-',
                                );
                            }
                            $retorno['total'] = $daoRedes->getCount($filter);
                            unset($rsRede, $daoRedes, $filter);
                            break;
                        case 'GL':

                            $dadosFiltro = array(
                                'id' => 'grp_loja_idGrupo',
                                'nome' => 'grp_loja_nome',
                                'idRede' => 'grp_loja_idRede'
                            );

                            $daoGrupos = App_Model_DAO_GrupoLojas::getInstance();
                            $filter = $daoGrupos->select()
                                ->from($daoGrupos)
                                ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0));

                            if ($this->getRequest()->getParam('grplojas')) {
                                $idsGrupos = Zend_Json::decode($this->getRequest()->getParam('grplojas', array()));
                                if (count($idsGrupos))
                                    $filter->where("grp_loja_idGrupo NOT IN ('" . implode("','", $idsGrupos) . "')");
                            }

                            $sort = $this->getRequest()->getParam('sort', 'grp_loja_nome');
                            switch ($sort) {
                                case 'nome': $sort = 'grp_loja_nome';
                                    break;
                                case 'id': $sort = 'grp_loja_idGrupo';
                                    break;
                            }
                            $filter->order("{$sort} {$this->getRequest()->getParam('dir', 'ASC')}");
                            App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'), $dadosFiltro);

                            $rsGrupo = $daoGrupos->fetchAll($filter);
                            foreach ($rsGrupo as $grupo) {
                                $retorno['grupos'][] = array(
                                    'idControle' => $grupo->getCodigo() . 'GL',
                                    'id' => $grupo->getCodigo(),
                                    'nome' => $grupo->getNome(),
                                    'grupo' => 'GL',
                                    'idRede' => $grupo->getRede()->getCodigo()
                                );
                            }
                            $retorno['total'] = $daoGrupos->getCount($filter);
                            unset($rsGrupo, $daoGrupos, $filter);
                            break;
                        case 'LO':

                            $dadosFiltro = array(
                                'id' => 'loja_idLoja',
                                'nome' => 'loja_nomeCompleto',
                                'idRede' => 'grp_loja_idRede'
                            );

                            $daoLojas = App_Model_DAO_Lojas::getInstance();
                            $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
                            $filter = $daoLojas->getAdapter()->select()
                                ->from($daoLojas->info('name'))
                                ->joinInner($daoGrupoLojas->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
                                ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
                                ->group('loja_idLoja');

                            if ($this->getRequest()->getParam('lojas')) {
                                $idsGrupos = Zend_Json::decode($this->getRequest()->getParam('lojas', array()));
                                if (count($idsGrupos))
                                    $filter->where("loja_idLoja NOT IN ('" . implode("','", $idsGrupos) . "')");
                            }

                            $sort = $this->getRequest()->getParam('sort', 'loja_nomeCompleto');
                            switch ($sort) {
                                case 'nome': $sort = 'loja_nomeCompleto';
                                    break;
                                case 'id': $sort = 'loja_idLoja';
                                    break;
                            }
                            $filter->order("{$sort} {$this->getRequest()->getParam('dir', 'ASC')}");
                            App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'), $dadosFiltro);

                            $rsLoja = $daoLojas->createRowset($daoLojas->getAdapter()->fetchAll($filter));
                            foreach ($rsLoja as $loja) {
                                $retorno['grupos'][] = array(
                                    'idControle' => $loja->getCodigo() . 'LO',
                                    'id' => $loja->getCodigo(),
                                    'nome' => $loja->getNomeCompleto(),
                                    'grupo' => 'LO',
                                    'idRede' => $loja->getGrupo()->getRede()->getCodigo()
                                );
                            }
                            $retorno['total'] = $daoLojas->getCount($filter);
                            unset($rsLoja, $daoLojas, $filter);
                            break;

                        case 'LI':

                            $dadosFiltro = array(
                                'id' => 'lin_idLinha',
                                'nome' => 'lin_nome',
                                'idRede' => 'lin_idRede'
                            );

                            $daoLinhas = App_Model_DAO_Linhas::getInstance();
                            $filter = $daoLinhas->select()
                                ->from($daoLinhas)
                                ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0));

                            if ($this->getRequest()->getParam('linhas')) {
                                $idsGrupos = Zend_Json::decode($this->getRequest()->getParam('linhas', array()));
                                if (count($idsGrupos))
                                    $filter->where("lin_idLinha NOT IN ('" . implode("','", $idsGrupos) . "')");
                            }

                            $sort = $this->getRequest()->getParam('sort', 'lin_nome');
                            switch ($sort) {
                                case 'nome': $sort = 'lin_nome';
                                    break;
                                case 'id': $sort = 'lin_idLinha';
                                    break;
                            }
                            $filter->order("{$sort} {$this->getRequest()->getParam('dir', 'ASC')}");
                            App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'), $dadosFiltro);

                            $rsLinha = $daoLinhas->fetchAll($filter);
                            foreach ($rsLinha as $linha) {
                                $retorno['grupos'][] = array(
                                    'idControle' => $linha->getCodigo() . 'LI',
                                    'id' => $linha->getCodigo(),
                                    'nome' => $linha->getNome(),
                                    'grupo' => 'LI',
                                    'idRede' => $linha->getRede()->getCodigo(),
                                );
                            }
                            $retorno['total'] = $daoLinhas->getCount($filter);
                            unset($rsLinha, $daoLinhas, $filter);
                            break;
                    }

                    App_Funcoes_UTF8::encode($retorno);
                    echo Zend_Json::encode($retorno);
                }
                break;

            default:
                $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
                $this->getFrontController()->setParam('noViewRenderer', false);
        }
    }

    public function geralAction()
    {
        $this->render('geral');
        $this->_forward('index');
    }

}
