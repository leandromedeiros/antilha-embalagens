<?php

class StatusController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoStatus = App_Model_DAO_Status::getInstance();
				$filter = $daoStatus->select()
					->from($daoStatus)
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'sta_nome')} {$this->getRequest()->getParam('dir', 'DESC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('status' => array(), 'total' => 0);
					$rsStatus = $daoStatus->fetchAll($filter);
					foreach ($rsStatus as $status) {
						$retorno['status'][] = array(
							'sta_idStatus' => $status->getCodigo(),
							'sta_nome' => $status->getNome(),
							'sta_referenciaQuadro' => (string) (int) $status->getReferencia(),
							'sta_status' => (string) (int) $status->getStatus()
						);
					}
					$retorno['total'] = $daoStatus->getCount($filter);
					unset($rsStatus);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoStatus, $filter);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function insertAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'status' => array());
			$daoStatus = App_Model_DAO_Status::getInstance();

			try {
				$status = $daoStatus->createRow();
				$status = $this->montaObjeto($status);
				
				try {
					$status->save();
					$retorno['success'] = true;
					$retorno['message'] = sprintf('Status <b>%s</b> cadastrado com sucesso.', $status->getNome());

				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					throw new Exception('Por favor verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o status.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoStatus, $status);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('sta_idStatus', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'status' => array());
			$daoStatus = App_Model_DAO_Status::getInstance();
			try {
				$status = $daoStatus->fetchRow(
					$daoStatus->select()->where('sta_idStatus = ?', $idRegistro)
				);
				if (null == $status) {
					throw new Exception('O status solicitado n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					// carrega os dados
					$retorno['success'] = true;
					//$retorno ['status'] = array($status->toArray());
					$arrStatus = $status->toArray();
					
					foreach ($status->getRelacionados() as $relacionado) {
						$arrStatus['sta_relacionados'][] = $relacionado->getRelacionado();
					}
					
					$retorno['status'] = array($arrStatus);
				} else {
					//atualiza os dados
					$status = $this->montaObjeto($status);
					
					try {
						$status->save();
						
						$retorno['success'] = true;
						$retorno['message'] = sprintf('Status <b>%s</b> alterado com sucesso.', $status->getNome());

					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o status.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoStatus);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);

		} else {		
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	protected function montaObjeto(App_Model_Entity_Status $status) {
		
		// atualiza os dados
		$status->setNome(utf8_decode($this->getRequest()->getParam('sta_nome')))
			->setReferencia(utf8_decode($this->getRequest()->getParam('sta_referenciaQuadro')))
			->setStatus($this->getRequest()->getParam('sta_status'));

		// Relacionados
		$status->getRelacionados()->offsetRemoveAll();
		if (false != ($relacionados = $this->getRequest()->getParam('sta_relacionados', false))) {
			$arrRelacionados = Zend_Json::decode($relacionados);
			foreach ($arrRelacionados as $key) {
				$relacionado = App_Model_DAO_Status_Relacionados::getInstance()->createRow();
				$relacionado->setRelacionado($key);
				$status->getRelacionados()->offsetAdd($relacionado);
			}
		}
		return $status;
	}
	
	protected function buildForm()
	{
		$arrRelacionados = array();
		foreach (App_Funcoes_Rotulos::$statusPedidos as $key => $value) {
			$arrRelacionados[] = array(
				'relacionado' => $key,
				'nome' => $value
			);
		}
		App_Funcoes_UTF8::encode($arrRelacionados);
		$this->view->relacionados = Zend_Json::encode($arrRelacionados);

		$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		$this->render('form');
	}
	
	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoStatus = App_Model_DAO_Status::getInstance();
		try {
			$status = $daoStatus->fetchRow(
				$daoStatus->select()->where('sta_idStatus = ?', $this->getRequest()->getParam('idStatus'))
			);
			if (null == $status) {
				throw new Exception('O status solicitado n�o foi encontrado.');
			}
			try {
				$nome = $status->getNome();
				$status->delete();
				$retorno['success'] = true;
				$retorno['message'] = sprintf('Status <b>%s</b> removido com sucesso.', $nome);

			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o status.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoStatus, $status);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}

	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Status.xls");
		header("Content-Disposition: attachment; filename=Status.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Status');

		$plan->writeString(0, 0, 'Listagem de Status', $format['titulo']);
		$plan->setMerge(0, 0, 0, 3);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 35);
		$plan->setColumn(2, 2, 35);
		$plan->setColumn(3, 3, 15);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'Nome', $format['subtitulo']);
		$plan->writeString(2, 2, 'Status no Quadro de Acompanhamento', $format['subtitulo']);
		$plan->writeString(2, 3, 'Status', $format['subtitulo']);

		$daoStatus = App_Model_DAO_Status::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoStatus->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoStatus->fetchAll($filter);
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, $record->getNome(), $format["normal"]);
					$plan->writeString($linha, 2, App_Funcoes_Rotulos::$statusPedidosResumo[$record->getReferencia()], $format["normal"]);
					$plan->writeString($linha, 3, App_Funcoes_Rotulos::$status[$record->getStatus()], $format["normal-cen"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 5; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 5);
		}
		unset($daoStatus, $filter);

		$xls->close();
	}
}