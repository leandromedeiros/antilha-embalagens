<?php

class FaqsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoFaqs = App_Model_DAO_Faqs::getInstance();
				$daoTopicos = App_Model_DAO_Topicos::getInstance();
				$filter = $daoFaqs->getAdapter()->select()
					->from($daoFaqs->info('name'))
					->joinLeft($daoTopicos->info('name'), 'top_idTopico = faq_idTopico')
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'faq_pergunta')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('faqs' => array(), 'total' => 0);
					$rsFaqs = $daoFaqs->createRowset($daoFaqs->getAdapter()->fetchAll($filter));
					
					foreach ($rsFaqs as $faq) {
						$retorno['faqs'][] = array(
							'faq_idFaq' => $faq->getCodigo(),
							'top_nome' => $faq->getTopico()->getNome(),
							'faq_pergunta' => $faq->getPergunta(),
							'faq_resposta' => $faq->getResposta(),
							'faq_ordem' => $faq->getOrdem(),
							'faq_status' => (string) (int) $faq->getStatus()
						);
					}
					$retorno['total'] = $daoFaqs->getCount($filter);
					unset($rsFaqs);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoFaqs, $filter);
			break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function insertAction() {
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(),'faq' => array());
			$daoFaqs = App_Model_DAO_Faqs::getInstance();

			try {
				$faq = $daoFaqs->createRow();
				$faq = $this->montaObjeto($faq);

				try {
					$faq->save();

					$retorno ['success'] = true;
					$retorno ['message'] = sprintf ('FAQ %s cadastrada com sucesso.', $faq->getCodigo());
				} catch ( App_Validate_Exception $e ) {
					$retorno ['errors'] = App_Funcoes_Ext::fieldErrors ($e);
					throw new Exception ('Por favor verifique os campos marcados em vermelho.');
				} catch ( Exception $e ) {
					throw new Exception ('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar a FAQ.');
				}

			} catch ( Exception $e ) {
				//$daoFaqs->getAdapter()->rollBack();
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset ($daoFaqs, $faq);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->buildForm();
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('faq_idFaq', false))) {
			$retorno = array ('success' => false, 'message' => null, 'errors' => array(), 'faq' => array() );
			$daoFaqs = App_Model_DAO_Faqs::getInstance();
			try {
				$faq = $daoFaqs->fetchRow(
					$daoFaqs->select()->where('faq_idFaq = ?', $idRegistro)
				);
				if (null == $faq) {
					throw new Exception('A FAQ solicitada n�o foi encontrada.');
				}

				if ($this->getRequest()->getParam('load')) {
					// carrega os dados
					$retorno ['success'] = true;
					$retorno ['faq'] = array($faq->toArray());
					
				} else {
					// atualiza os dados
					$faq = $this->montaObjeto($faq);

					try {
						$faq->save();

						$retorno ['success'] = true;
						$retorno ['message'] = sprintf('Faq %s alterada com sucesso.', $faq->getCodigo());
					} catch ( App_Validate_Exception $e ) {
						$retorno ['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor verifique os campos marcados em vermelho.');
					} catch ( Exception $e ) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar a FAQ.' );
					}
				}
			} catch ( Exception $e ) {
				//$daoFaqs->getAdapter()->rollBack();
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoFaqs, $faq);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->buildForm();
		}
	}
	
	protected function buildForm()
	{
		$daoTopicos = App_Model_DAO_Topicos::getInstance();
		$topicos = $daoTopicos->fetchAll(
			$daoTopicos->select()->from($daoTopicos)
		);
		$arrTopicos = array();
		foreach ($topicos as $topico) {
			$arrTopicos[] = array(
				$topico->getCodigo(),
				$topico->getNome()
			);
		}
		App_Funcoes_UTF8::encode($arrTopicos);
		$this->view->topicos = Zend_Json::encode($arrTopicos);
	
		$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		$this->render('form');
	}

	protected function montaObjeto(App_Model_Entity_Faq $faq) {
		$daoTopicos = App_Model_DAO_Topicos::getInstance();
		// atualiza os dados
		$faq->setTopico(
			$daoTopicos->fetchRow(
				$daoTopicos->select()->from($daoTopicos)
					->where('top_idTopico = ?', $this->getRequest()->getParam('faq_idTopico'))
			))
			->setPergunta(utf8_decode($this->getRequest()->getParam('faq_pergunta')))
			->setResposta($this->getRequest()->getParam('faq_resposta'))
			->setOrdem($this->getRequest()->getParam('faq_ordem'))
			->setStatus($this->getRequest()->getParam('faq_status'));

		return $faq;
	}
	
	public function deleteAction() {
		$retorno = array (
			'success' => false,
			'message' => null
		);
		$daoFaqs = App_Model_DAO_Faqs::getInstance();
		try {
			$faq = $daoFaqs->fetchRow($daoFaqs->select()->where('faq_idFaq = ?', $this->getRequest()->getParam('idFaq')));
			if (null == $faq) {
				throw new Exception('A FAQ solicitada n�o foi encontrada.');
			}
			try {
				$nome = $faq->getPergunta();
				$faq->delete();
				$retorno['success'] = true;
				$retorno['message'] = sprintf('FAQ %s removida com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a FAQ.');
			}
		} catch (Exception $e) {
			$retorno ['success'] = false;
			$retorno ['message'] = $e->getMessage();
		}
		unset($daoFaqs, $faq);

		App_Funcoes_UTF8::encode ($retorno);
		echo Zend_Json::encode ($retorno);
	}
	
	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Faqs.xls");
		header("Content-Disposition: attachment; filename=Faqs.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Faqs');

		$plan->writeString(0, 0, 'Listagem de FAQs', $format['titulo']);
		$plan->setMerge(0, 0, 0, 4);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 55);
		$plan->setColumn(1, 2, 55);
		$plan->setColumn(2, 3, 15);
		$plan->setColumn(3, 4, 15);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'T�pico', $format['subtitulo']);
		$plan->writeString(2, 2, 'Pergunta', $format['subtitulo']);
		$plan->writeString(2, 3, 'Ordem', $format['subtitulo']);
		$plan->writeString(2, 4, 'Status', $format['subtitulo']);

		$daoFaqs = App_Model_DAO_Faqs::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoFaqs->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoFaqs->createRowset($daoFaqs->getAdapter()->fetchAll($filter));
				foreach ($rsRegistros as $record) {
					$plan->writeNumber($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, $record->getTopico()->getNome(), $format["normal"]);
					$plan->writeString($linha, 2, $record->getPergunta(), $format["normal"]);
					$plan->writeNumber($linha, 3, $record->getOrdem(), $format["normal-cen"]);
					$plan->writeString($linha, 4, App_Funcoes_Rotulos::$status[$record->getStatus()], $format["normal-cen"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 4; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 4);
		}
		unset($daoFaqs, $filter);

		$xls->close();
	}
}