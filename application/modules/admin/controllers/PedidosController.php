<?php

class PedidosController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {

			case 'grid':
				
				$daoPedidos = App_Model_DAO_Pedidos::getInstance();
				$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
				$daoPedidoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
				$daoLojas = App_Model_DAO_Lojas::getInstance();
				$daoStatus = App_Model_DAO_Pedidos_Status::getInstance();
				
				$selectStatus = $daoStatus->select()->from($daoStatus->info('name'), 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = ped_idPedido');
				
				$filter = $daoPedidos->getAdapter()->select()
					->from($daoPedidos->info('name'))
					->joinLeft($daoPedidoNotas->info('name'), 'ped_nota_idPedido = ped_idPedido')
					->joinInner($daoUsuarios->info('name'), 'ped_idUsuario = usr_idUsuario')
					->joinInner($daoLojas->info('name'), 'ped_idLoja = loja_idLoja')
					->joinInner($daoStatus->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->group('ped_idPedido')
					->order("{$this->getRequest()->getParam('sort', 'ped_idPedido')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));
				
				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('pedidos' => array(), 'total' => 0);
					$rsPedidos = $daoPedidos->getAdapter()->fetchAll($filter);
					foreach ($rsPedidos as $pedido) {
						$retorno['pedidos'][] = array(
							'ped_idPedido' => $pedido['ped_idPedido'],
							'ped_nota_numero' => $pedido['ped_nota_numero'],
							'loja_nomeReduzido' => $pedido['loja_nomeReduzido'],
							'ped_dataCadastro' => App_Funcoes_Date::conversion($pedido['ped_dataCadastro']),
							'ped_idUsuario' => $pedido['ped_idUsuario'],
							'usr_nome' => $pedido['usr_nome'],
							'ped_totalPedido' => $pedido['ped_totalPedido'],
							'ped_status_valor' => $pedido['ped_status_valor']							
						);
					}
					$retorno['total'] = $daoPedidos->getCount($filter);
					unset($rsPedidos);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($rsPedidos, $filter);
				break;	

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function detalheAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array();
			$daoPedidos = App_Model_DAO_Pedidos::getInstance();
			$daoPedidos->getAdapter()->query('SET SESSION wait_timeout = 100');

			$view = new Zend_View();
			$view->setBasePath(APPLICATION_PATH .DIRECTORY_SEPARATOR. 'views');

			try {
				$pedido = $daoPedidos->fetchRow(
					$daoPedidos->select()->where('ped_idPedido = ?', $this->getRequest()->getParam('idPedido'))
				);
				
				$view->pedido = $pedido;
				$retorno = array('conteudo' => $view->render('pedidos/pedido-detalhes.phtml'));
			} catch (Exception $e) {
				$retorno['message'] = $e->getMessage();
			}

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getFrontController()->setParam('noViewRenderer', false);
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		}
	}
	
	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Pedidos.xls");
		header("Content-Disposition: attachment; filename=Pedidos.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Pedidos');

		$plan->writeString(0, 0, 'Listagem de Pedidos', $format['titulo']);
		$plan->setMerge(0, 0, 0, 7);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 15);
		$plan->setColumn(2, 2, 30);
		$plan->setColumn(3, 3, 20);
		$plan->setColumn(4, 4, 20);
		$plan->setColumn(5, 5, 15);
		$plan->setColumn(6, 6, 15);
		$plan->setColumn(6, 7, 20);

		$plan->writeString(2, 0, 'Pedido', $format['subtitulo']);
		$plan->writeString(2, 1, 'Data', $format['subtitulo']);
		$plan->writeString(2, 2, 'Cod. Cliente', $format['subtitulo']);
		$plan->writeString(2, 3, 'Loja', $format['subtitulo']);
		$plan->writeString(2, 4, 'NF', $format['subtitulo']);
		$plan->writeString(2, 5, 'Usu�rio', $format['subtitulo']);
		$plan->writeString(2, 6, 'Valor', $format['subtitulo']);
		$plan->writeString(2, 7, 'Status', $format['subtitulo']);

		$daoPedidos = App_Model_DAO_Pedidos::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoPedidos->getAdapter()->fetchOne(
			$daoPedidos->getAdapter()->select()
				->from($filter, 'COUNT(1)')
		);

		//$formaPagamento = $this->view->FormaPagamento();

		if ($totalRegistros > 0) {
			$rsRegistros = $daoPedidos->getAdapter()->fetchAll($filter);
			$linha = 3;
			foreach ($rsRegistros as $record) {
				$plan->writeString($linha, 0, $record['ped_idPedido'], $format["normal-cen"]);
				$plan->writeString($linha, 1, App_Funcoes_Date::conversion($record['ped_dataCadastro']), $format["normal"]);
				$plan->writeString($linha, 2, $record['usr_idUsuario'], $format["normal-cen"]);
				$plan->writeString($linha, 3, $record['ped_nota_numero'], $format["normal-cen"]);
				$plan->writeString($linha, 4, $record['loja_nomeReduzido'], $format["normal-cen"]);
				$plan->writeString($linha, 5, $record['usr_nome'], $format["normal-cen"]);
				$plan->writeString($linha, 6, 'R$ '.App_Funcoes_Money::toCurrency($record['ped_totalPedido']), $format["normal-cen"]);
				$plan->writeString($linha, 7, App_Funcoes_Rotulos::$statusPed[$record['ped_nota_status']], $format["normal-cen"]);
				$linha++;

				unset($record);
			}
			unset($rsRegistros);

		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 7; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 7);
		}
		unset($daoPedidos, $filter);

		$xls->close();
	}
}