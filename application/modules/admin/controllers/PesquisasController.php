<?php

class PesquisasController extends Zend_Controller_Action
{

    public function init()
    {
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->getResponse()->setHeader('Content-Type', 'text/json');
    }

    public function indexAction()
    {
        switch ($this->getRequest()->getParam('load')) {
            case 'grid':
                $daoPesquisas = App_Model_DAO_Pesquisas::getInstance();
                $filter = $daoPesquisas->select()
                    ->from($daoPesquisas)
                    ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
                    ->order("{$this->getRequest()->getParam('sort', 'pes_nome')} {$this->getRequest()->getParam('dir', 'ASC')}");
                App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

                if ($this->getRequest()->getParam('excel', false) == true) {
                    $this->exportExcel($filter);
                } else {
                    $retorno = array('pesquisas' => array(), 'total' => 0);
                    $rsPesquisas = $daoPesquisas->fetchAll($filter);

                    foreach ($rsPesquisas as $pesquisa) {
                        $retorno['pesquisas'][] = array(
                            'pes_idPesquisa' => $pesquisa->getCodigo(),
                            'pes_dataInicial' => $pesquisa->getDataInicial(),
                            'pes_dataFinal' => $pesquisa->getDataFinal(),
                            'pes_nome' => $pesquisa->getNome(),
                            'pes_status' => (string) (int) $pesquisa->getStatus()
                        );
                    }
                    $retorno['total'] = $daoPesquisas->getCount($filter);
                    unset($rsPesquisas);

                    App_Funcoes_UTF8::encode($retorno);
                    echo Zend_Json::encode($retorno);
                }
                unset($daoPesquisas, $filter);
                break;
            case 'permissoes':
                $retorno = array('permissoes' => array(), 'total' => 0);
                if ($this->getRequest()->getParam('pesquisa', '')) {

                    $daoPermissoes = App_Model_DAO_Pesquisas_Permissoes::getInstance();
                    $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
                    $daoRedes = App_Model_DAO_Redes::getInstance();
                    $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
                    $daoLojas = App_Model_DAO_Lojas::getInstance();
                    $daoLinhas = App_Model_DAO_Linhas::getInstance();

                    $selectGR = $daoPermissoes->getAdapter()->select()
                        ->from($daoPermissoes->info('name'), array(
                            'pesquisa' => 'pes_perm_idPesquisa',
                            new Zend_Db_Expr('"GR" AS grupo')
                        ))
                        ->joinInner(
                        $daoGrupoRedes->info('name'), 'grp_rede_idGrupo = pes_perm_idGrupoRede', array(
                        'nome' => 'grp_rede_nome',
                        'id' => 'grp_rede_idGrupo'
                        )
                    );

                    $selectRE = $daoPermissoes->getAdapter()->select()
                        ->from($daoPermissoes->info('name'), array(
                            'pesquisa' => 'pes_perm_idPesquisa',
                            new Zend_Db_Expr('"RE" AS grupo')
                        ))
                        ->joinInner(
                        $daoRedes->info('name'), 'rede_idRede = pes_perm_idRede', array(
                        'nome' => 'rede_nome',
                        'id' => 'rede_idRede'
                        )
                    );

                    $selectGL = $daoPermissoes->getAdapter()->select()
                        ->from($daoPermissoes->info('name'), array(
                            'pesquisa' => 'pes_perm_idPesquisa',
                            new Zend_Db_Expr('"GL" AS grupo')
                        ))
                        ->joinInner(
                        $daoGrupoLojas->info('name'), 'grp_loja_idGrupo = pes_perm_idGrupoLoja', array(
                        'nome' => 'grp_loja_nome',
                        'id' => 'grp_loja_idGrupo'
                        )
                    );

                    $selectLO = $daoPermissoes->getAdapter()->select()
                        ->from($daoPermissoes->info('name'), array(
                            'pesquisa' => 'pes_perm_idPesquisa',
                            new Zend_Db_Expr('"LO" AS grupo')
                        ))
                        ->joinInner(
                        $daoLojas->info('name'), 'loja_idLoja = pes_perm_idLoja', array(
                        'nome' => 'loja_nomeCompleto',
                        'id' => 'loja_idLoja'
                        )
                    );

                    $selectLI = $daoPermissoes->getAdapter()->select()
                        ->from($daoPermissoes->info('name'), array(
                            'id' => 'pes_perm_idPesquisa',
                            new Zend_Db_Expr('"LI" AS grupo')
                        ))
                        ->joinInner(
                        $daoLinhas->info('name'), 'lin_idLinha = pes_perm_idLinha', array(
                        'nome' => 'lin_nome',
                        'id' => 'lin_idLinha'
                        )
                    );

                    $union = $daoPermissoes->getAdapter()->select()->union(array($selectGR, $selectRE, $selectGL, $selectLO, $selectLI));

                    $filter = $daoPermissoes->getAdapter()->select()->from(array('temp' => $union))
                        ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
                        ->order("{$this->getRequest()->getParam('sort', 'nome')} {$this->getRequest()->getParam('dir', 'ASC')}")
                        ->where('pesquisa = ?', $this->getRequest()->getParam('pesquisa'));

                    App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

                    $rsPermissoes = $daoPermissoes->getAdapter()->fetchAll($filter);
                    foreach ($rsPermissoes as $key => $permisso) {
                        $retorno['permissoes'][] = array(
                            'id' => $permisso['id'],
                            'nome' => $permisso['nome'],
                            'grupo' => $permisso['grupo']
                        );
                    }

                    $retorno['total'] = $daoPermissoes->getAdapter()->fetchOne(
                        $daoPermissoes->getAdapter()->select()
                            ->from(
                                array('temp' => $filter->reset(Zend_Db_Select::LIMIT_COUNT)->reset(Zend_Db_Select::LIMIT_OFFSET)), 'COUNT(1)'
                            )
                    );
                    unset($daoPermissoes, $daoGrupoLojas, $daoGrupoRedes, $daoLojas, $filter, $rsPermissoes);
                }

                App_Funcoes_UTF8::encode($retorno);
                echo Zend_Json::encode($retorno);

                break;
            case 'usuarios':

                $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
                $daoRespostas = App_Model_DAO_Pesquisas_Respostas::getInstance();
                $daoPerguntas = App_Model_DAO_Pesquisas_Perguntas::getInstance();
                $daoAlternativas = App_Model_DAO_Pesquisas_Alternativas::getInstance();

                $filter = $daoUsuarios->getAdapter()->select()
                    ->from($daoUsuarios->info('name'))
                    ->joinInner($daoRespostas->info('name'), 'pes_resp_idUsuario = usr_idUsuario', null)
                    ->joinInner($daoPerguntas->info('name'), 'pes_perg_idPergunta = pes_resp_idPergunta ', null)
                    ->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
                    ->order("{$this->getRequest()->getParam('sort', 'usr_nome')} {$this->getRequest()->getParam('dir', 'ASC')}")
                    ->group('usr_idUsuario');

                App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

                if ($pesquisa = $this->getRequest()->getParam('pes_idPesquisa')) {
                    $filter->where('pes_perg_idPesquisa = ?', $pesquisa);
                }

                if ($this->getRequest()->getParam('excel', false) == true) {
                    $this->exportExcel($filter, $pesquisa);
                } else {
                    $retorno = array('usuarios' => array(), 'total' => 0);
                    $rsUsuarios = $daoUsuarios->createRowset($daoUsuarios->getAdapter()->fetchAll($filter));
                    foreach ($rsUsuarios as $usuario) {
                        $retorno['usuarios'][] = array(
                            'usr_idUsuario' => $usuario->getCodigo(),
                            'usr_idLoja' => $usuario->getGrupo()->getCodigo(),
                            'usr_nome' => $usuario->getNome()
                        );
                    }
                    $retorno['total'] = $daoUsuarios->getCount($filter);
                    unset($rsUsuarios);

                    App_Funcoes_UTF8::encode($retorno);
                    echo Zend_Json::encode($retorno);
                }
                unset($daoUsuarios, $filter);
                break;
                break;

            default:
                $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
                $this->getFrontController()->setParam('noViewRenderer', false);
        }
    }

    public function detalheAction()
    {
        if (false != ($idPesquisa = $this->getRequest()->getParam('idPesquisa', false)) && false != ($idUsuario = $this->getRequest()->getParam('idUsuario', false))) {
            $retorno = array();
            $daoPesquisas = App_Model_DAO_Pesquisas::getInstance();
            $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
            $daoRespostas = App_Model_DAO_Pesquisas_Respostas::getInstance();
            $daoPerguntas = App_Model_DAO_Pesquisas_Perguntas::getInstance();
            $daoAlternativas = App_Model_DAO_Pesquisas_Alternativas::getInstance();

            $view = new Zend_View();
            $view->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

            try {

                $view->pesquisa = $daoPesquisas->fetchRow(
                    $daoPesquisas->select()->from($daoPesquisas)->where('pes_idPesquisa = ?', $idPesquisa)
                );

                $view->usuario = $daoUsuarios->fetchRow(
                    $daoUsuarios->select()->from($daoUsuarios)->where('usr_idUsuario = ?', $idUsuario)
                );

                $respostas = $daoRespostas->getAdapter()->fetchAll(
                    $daoRespostas->getAdapter()->select()->from(
                            $daoRespostas->info('name'), array('resposta' => new Zend_Db_Expr("IF(pes_perg_tipo = 'M', IF(pes_alt_tipo = 'O', CONCAT(pes_alt_alternativa,' - ', pes_resp_resposta),pes_alt_alternativa), pes_resp_resposta)"))
                        )
                        ->joinInner($daoPerguntas->info('name'), 'pes_resp_idPergunta = pes_perg_idPergunta', array('pes_perg_idPergunta', 'pes_perg_pergunta', 'pes_perg_obrigatorio'))
                        ->joinLeft($daoAlternativas->info('name'), 'pes_resp_idAlternativa = pes_alt_idAlternativa')
                        ->where('pes_perg_idPesquisa = ?', $idPesquisa)
                        ->where('pes_resp_idUsuario = ?', $idUsuario)
                );

                $view->respostas = $respostas;
                $retorno = array(
                    'conteudo' => $view->render('pesquisas/detalhes-respostas.phtml')
                );
            } catch (Exception $e) {
                $retorno['message'] = $e->getMessage();
            }

            App_Funcoes_UTF8::encode($retorno);
            echo Zend_Json::encode($retorno);
        } else {
            $this->getFrontController()->setParam('noViewRenderer', false);
            $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
        }
    }

    public function insertAction()
    {
        if ($this->getRequest()->isPost()) {
            $retorno = array('success' => false, 'message' => null, 'errors' => array(), 'pesquisa' => array());
            $daoPesquisas = App_Model_DAO_Pesquisas::getInstance();

            try {
                $pesquisa = $daoPesquisas->createRow();
                $pesquisa = $this->montaObjeto($pesquisa);

                try {
                    $pesquisa->save();

                    $retorno ['success'] = true;
                    $retorno ['message'] = sprintf('Pesquisa %s cadastrada com sucesso.', $pesquisa->getCodigo());
                } catch (App_Validate_Exception $e) {
                    $retorno ['errors'] = App_Funcoes_Ext::fieldErrors($e);
                    throw new Exception('Por favor, verifique os campos marcados em vermelho.');
                } catch (Exception $e) {
                    throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar a pesquisa.');
                }
            } catch (Exception $e) {
                //$daoPesquisas->getAdapter()->rollBack();
                $retorno['success'] = false;
                $retorno['message'] = $e->getMessage();
            }
            unset($daoPesquisas, $pesquisa);

            App_Funcoes_UTF8::encode($retorno);
            echo Zend_Json::encode($retorno);
        } else {
            $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
            $this->buildForm();
        }
    }

    public function insertPermissoesAction()
    {
        $retorno = array('success' => false, 'message' => null, 'errors' => array(), 'pesquisa' => array());
        if ($this->getRequest()->isPost()) {
            if ($idPesquisa = $this->getRequest()->getParam('idPesquisa', '')) {

                try {
                    $daoPesquisas = App_Model_DAO_Pesquisas::getInstance();
                    $pesquisa = $daoPesquisas->fetchRow(
                        $daoPesquisas->select()->from($daoPesquisas)->where('pes_idPesquisa = ?', $idPesquisa)
                    );
                    if (null == $pesquisa) {
                        throw new Exception('A pesquisa solicitada n�o foi encontrada.');
                    }
                    $pesquisa = $this->montaObjetoPermissoes($pesquisa);
                    try {
                        $pesquisa->save();

                        $retorno ['success'] = true;
                        $retorno ['message'] = sprintf('Pesquisa %s cadastrada com sucesso.', $pesquisa->getCodigo());
                    } catch (App_Validate_Exception $e) {
                        $retorno ['errors'] = App_Funcoes_Ext::fieldErrors($e);
                        throw new Exception('Por favor, verifique os campos marcados em vermelho.');
                    } catch (Exception $e) {
                        throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar a pesquisa.');
                    }
                } catch (Exception $e) {
                    //$daoPesquisas->getAdapter()->rollBack();
                    $retorno['success'] = false;
                    $retorno['message'] = $e->getMessage();
                }
                unset($daoPesquisas, $pesquisa);
            }
        }
        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function updateAction()
    {
        if (false != ($idRegistro = $this->getRequest()->getParam('pes_idPesquisa', false))) {
            $retorno = array('success' => false, 'message' => null, 'errors' => array(), 'pesquisa' => array());
            $daoPesquisas = App_Model_DAO_Pesquisas::getInstance();
            try {
                $pesquisa = $daoPesquisas->fetchRow(
                    $daoPesquisas->select()->where('pes_idPesquisa = ?', $idRegistro)
                );
                if (null == $pesquisa) {
                    throw new Exception('A pesquisa solicitada n�o foi encontrada.');
                }

                if ($this->getRequest()->getParam('load')) {
                    // carrega os dados
                    $retorno ['success'] = true;
                    $arrPesquisa = $pesquisa->toArray();

                    foreach ($pesquisa->getPerguntas() as $key => $pergunta) {
                        $arrPergunta = $pergunta->toArray();
                        foreach ($pergunta->getAlternativas() as $alternativa) {
                            $arrPergunta['pes_perg_alternativas'][] = $alternativa->toArray();
                        }
                        $arrPesquisa['pes_perguntas'][$key] = $arrPergunta;
                        $arrPesquisa['pes_perguntas'][$key]['pes_perg_ordem'] = str_pad($arrPergunta['pes_perg_ordem'], 2, "0", STR_PAD_LEFT);
                    }

                    foreach ($pesquisa->getEstados() as $estado) {
                        $arrPesquisa['pes_estados'][] = $estado->getUf();
                    }

                    $retorno['pesquisa'] = array($arrPesquisa);
                } else {
                    // atualiza os dados
                    $pesquisa = $this->montaObjeto($pesquisa);

                    try {
                        $pesquisa->save();

                        $retorno ['success'] = true;
                        $retorno ['message'] = sprintf('Pesquisa %s alterada com sucesso.', $pesquisa->getCodigo());
                    } catch (App_Validate_Exception $e) {
                        $retorno ['errors'] = App_Funcoes_Ext::fieldErrors($e);
                        throw new Exception('Por favor, verifique os campos marcados em vermelho.');
                    } catch (Exception $e) {
                        throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar a pesquisa.' );
                    }
                }
            } catch (Exception $e) {
                //$daoPesquisas->getAdapter()->rollBack();
                $retorno['success'] = false;
                $retorno['message'] = $e->getMessage();
            }
            unset($daoPesquisas, $pesquisa);

            App_Funcoes_UTF8::encode($retorno);
            echo Zend_Json::encode($retorno);
        } else {
            $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
            $this->buildForm();
        }
    }

    protected function montaObjeto(App_Model_Entity_Pesquisa $pesquisa)
    {
        // atualiza os dados
        $daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();
        $pesquisa->setNome(utf8_decode($this->getRequest()->getParam('pes_nome')))
            ->setDataInicial(App_Funcoes_Date::conversion($this->getRequest()->getParam('pes_dataInicial')))
            ->setDataFinal(App_Funcoes_Date::conversion($this->getRequest()->getParam('pes_dataFinal')))
            ->setResponderAntes($this->getRequest()->getParam('pes_responderAntes'))
            ->setExibirTodas($this->getRequest()->getParam('pes_exibirTodas'))
            ->setStatus($this->getRequest()->getParam('pes_status'));

        if ($this->getRequest()->getParam('pes_idImagem')) {
            $pesquisa->setImagem(
                $daoArquivos->fetchRow(
                    $daoArquivos->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('pes_idImagem'))
                )
            );
        } else {
            $pesquisa->setImagem(null);
        }

        // Perguntas
        $pes_perguntas = Zend_Json::decode($this->getRequest()->getParam('pes_perguntas', array()));
        $daoPerguntas = App_Model_DAO_Pesquisas_Perguntas::getInstance();
        $daoAlternativas = App_Model_DAO_Pesquisas_Alternativas::getInstance();
        $pesquisa->getPerguntas()->offsetRemoveAll();
        if (is_array($pes_perguntas)) {
            $daoPerguntas = App_Model_DAO_Pesquisas_Perguntas::getInstance();
            foreach ($pes_perguntas as $arrpergunta) {
                if (!$arrpergunta['pes_perg_idPergunta']) {
                    $pergunta = $daoPerguntas->createRow();
                } else {
                    $pergunta = $daoPerguntas->fetchRow(
                        $daoPerguntas->select()->where('pes_perg_idPergunta = ?', $arrpergunta['pes_perg_idPergunta'])
                    );
                }

                $pergunta->setPergunta(utf8_decode($arrpergunta['pes_perg_pergunta']))
                    ->setTipo($arrpergunta['pes_perg_tipo'])
                    ->setDataLimite($arrpergunta['pes_perg_dataLimite'])
                    ->setQtdeLimite($arrpergunta['pes_perg_qtdeLimite'])
                    ->setStatus($arrpergunta['pes_perg_status'])
                    ->setObrigatorio($arrpergunta['pes_perg_obrigatorio'])
                    ->setOrdem($arrpergunta['pes_perg_ordem']);

                if ($arrpergunta['pes_perg_idImagem']) {
                    $pergunta->setImagem(
                        $daoArquivos->fetchRow(
                            $daoArquivos->select()->where('gal_arq_idArquivo = ?', $arrpergunta['pes_perg_idImagem'])
                        )
                    );
                } else {
                    $pergunta->setImagem(null);
                }

                $pergunta->getAlternativas()->offsetRemoveAll();
                if ($pergunta->getTipo() == "M") {
                    foreach ($arrpergunta['pes_perg_alternativas'] as $arralternativa) {

                        if (!$arralternativa['pes_alt_idAlternativa']) {
                            $alternativa = $daoAlternativas->createRow();
                        } else {
                            $alternativa = $daoAlternativas->fetchRow(
                                $daoAlternativas->select()->where('pes_alt_idAlternativa = ?', $arralternativa['pes_alt_idAlternativa'])
                            );
                        }

                        $alternativa->setAlternativa(utf8_decode($arralternativa['pes_alt_alternativa']))
                            ->setTipo($arralternativa['pes_alt_tipo'] ? $arralternativa['pes_alt_tipo'] : 'N')
                            ->setOrdem($arralternativa['pes_alt_ordem'])
                            ->setStatus(1);

                        $pergunta->getAlternativas()->offsetAdd($alternativa);
                    }
                }
                $pesquisa->getPerguntas()->offsetAdd($pergunta);
            }
            unset($daoPerguntas);
        }

        // Estados
        $pesquisa->getEstados()->offsetRemoveAll();
        if (false != ($estados = $this->getRequest()->getParam('pes_estados', false))) {
            $arrEstados = Zend_Json::decode($estados);
            foreach ($arrEstados as $key) {
                $estado = App_Model_DAO_Pesquisas_Estados::getInstance()->createRow();
                $estado->setUf($key);
                $pesquisa->getEstados()->offsetAdd($estado);
            }
        }
        return $pesquisa;
    }

    protected function montaObjetoPermissoes(App_Model_Entity_Pesquisa $pesquisa)
    {
        // Permiss�es
        if ($this->getRequest()->getParam('permissoes')) {

            $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
            $daoRedes = App_Model_DAO_Redes::getInstance();
            $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
            $daoLojas = App_Model_DAO_Lojas::getInstance();
            $daoLinha = App_Model_DAO_Linhas::getInstance();
            $daoPermissoes = App_Model_DAO_Pesquisas_Permissoes::getInstance();

            $pes_permissoes = Zend_Json::decode($this->getRequest()->getParam('permissoes', array()));
            foreach ($pes_permissoes as $permissoes) {
                $permissao = $daoPermissoes->createRow();
                switch ($permissoes['grupo']) {
                    case 'GR':
                        $permissao->setGrupoRede(
                                $daoGrupoRedes->fetchRow(
                                    $daoGrupoRedes->select()->where('grp_rede_idGrupo = ?', $permissoes['id'])
                                )
                            )
                            ->setRede(null)
                            ->setGrupoLoja(null)
                            ->setLoja(null)
                            ->setLinha(null);
                        break;
                    case 'RE':
                        $permissao->setRede(
                                $daoRedes->fetchRow(
                                    $daoRedes->select()->where('rede_idRede = ?', $permissoes['id'])
                                )
                            )
                            ->setGrupoRede(null)
                            ->setGrupoLoja(null)
                            ->setLoja(null)
                            ->setLinha(null);
                        break;
                    case 'GL':
                        $permissao->setGrupoLoja(
                                $daoGrupoLojas->fetchRow(
                                    $daoGrupoLojas->select()->where('grp_loja_idGrupo = ?', $permissoes['id'])
                                )
                            )
                            ->setGrupoRede(null)
                            ->setRede(null)
                            ->setLoja(null)
                            ->setLinha(null);
                        break;
                    case 'LO':
                        $permissao->setLoja(
                                $daoLojas->fetchRow(
                                    $daoLojas->select()->where('loja_idLoja = ?', $permissoes['id'])
                                )
                            )
                            ->setGrupoRede(null)
                            ->setRede(null)
                            ->setGrupoLoja(null)
                            ->setLinha(null);
                        break;
                    case 'LI':
                        $permissao->setLinha(
                                $daoLinha->fetchRow(
                                    $daoLinha->select()->where('lin_idLinha = ?', $permissoes['id'])
                                )
                            )
                            ->setGrupoRede(null)
                            ->setRede(null)
                            ->setGrupoLoja(null)
                            ->setLoja(null);
                        break;
                }
                $pesquisa->getPermissoes()->offsetAdd($permissao);
            }
        }

        return $pesquisa;
    }

    protected function buildForm()
    {
        $arrEstados = array();
        foreach (App_Funcoes_Rotulos::$UF as $key => $value) {
            $arrEstados[] = array(
                'uf' => $key,
                'nome' => $value
            );
        }
        App_Funcoes_UTF8::encode($arrEstados);
        $this->view->estados = Zend_Json::encode($arrEstados);

        $this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
        $this->render('form');
    }

    public function deleteAction()
    {
        $retorno = array(
            'success' => false,
            'message' => null
        );
        $daoPesquisas = App_Model_DAO_Pesquisas::getInstance();
        try {
            $pesquisa = $daoPesquisas->fetchRow($daoPesquisas->select()->where('pes_idPesquisa = ?', $this->getRequest()->getParam('idPesquisa')));
            if (null == $pesquisa) {
                throw new Exception('A pesquisa solicitada n�o foi encontrada.');
            }
            try {
                $nome = $pesquisa->getNome();
                $pesquisa->delete();
                $retorno['success'] = true;
                $retorno['message'] = sprintf('Pesquisa %s removida com sucesso.', $nome);
            } catch (Exception $e) {
                throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a pesquisa.');
            }
        } catch (Exception $e) {
            $retorno ['success'] = false;
            $retorno ['message'] = $e->getMessage();
        }
        unset($daoPesquisas, $pesquisa);

        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function deletePermissaoAction()
    {
        $retorno = array(
            'success' => false,
            'message' => null
        );
        $daoPermissoes = App_Model_DAO_Pesquisas_Permissoes::getInstance();
        try {
            $where1 = $daoPermissoes->getAdapter()->quoteInto('pes_perm_idPesquisa = ?', $this->getRequest()->getParam('idPesquisa'));
            switch ($this->getRequest()->getParam('grupo')) {
                case 'GR':
                    $where2 = $daoPermissoes->getAdapter()->quoteInto('pes_perm_idGrupoRede = ?', $this->getRequest()->getParam('id'));
                    break;
                case 'RE':
                    $where2 = $daoPermissoes->getAdapter()->quoteInto('pes_perm_idRede = ?', $this->getRequest()->getParam('id'));
                    break;
                case 'GL':
                    $where2 = $daoPermissoes->getAdapter()->quoteInto('pes_perm_idGrupoLoja = ?', $this->getRequest()->getParam('id'));
                    break;
                case 'LO':
                    $where2 = $daoPermissoes->getAdapter()->quoteInto('pes_perm_idLoja = ?', $this->getRequest()->getParam('id'));
                    break;
                case 'LI':
                    $where2 = $daoPermissoes->getAdapter()->quoteInto('pes_perm_idLinha = ?', $this->getRequest()->getParam('id'));
                    break;
                default:
                    throw new Exception('Permiss�o solicitada n�o foi encontrada.');
            }

            try {
                $daoPermissoes->delete(array($where1, $where2));
                $retorno['success'] = true;
                $retorno['message'] = 'Permiss�o removida com sucesso.';
            } catch (Exception $e) {
                throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a permiss�o.');
            }
        } catch (Exception $e) {
            $retorno ['success'] = false;
            $retorno ['message'] = $e->getMessage();
        }
        unset($daoPermissoes, $permissao);

        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function deletePerguntaAction()
    {
        $retorno = array(
            'success' => false,
            'message' => null
        );
        $daoPerguntas = App_Model_DAO_Pesquisas_Perguntas::getInstance();
        try {
            $pergunta = $daoPerguntas->fetchRow($daoPerguntas->select()->where('pes_perg_idPergunta = ?', $this->getRequest()->getParam('idPergunta')));
            if (null == $pergunta) {
                throw new Exception('A pergunta solicitada n�o foi encontrada.');
            }
            try {
                $pergunta->delete();
                $retorno['success'] = true;
                $retorno['message'] = 'Pergunta foi removida com sucesso.';
            } catch (Exception $e) {
                throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a pergunta.');
            }
        } catch (Exception $e) {
            $retorno ['success'] = false;
            $retorno ['message'] = $e->getMessage();
        }
        unset($daoPerguntas, $pergunta);

        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function deleteAlternativaAction()
    {
        $retorno = array(
            'success' => false,
            'message' => null
        );
        $daoAlternativas = App_Model_DAO_Pesquisas_Alternativas::getInstance();
        try {
            $alternativa = $daoAlternativas->fetchRow($daoAlternativas->select()->where('pes_alt_idAlternativa = ?', $this->getRequest()->getParam('idAlternativa')));
            if (null == $alternativa) {
                throw new Exception('A alternativa solicitada n�o foi encontrada.');
            }
            try {
                $alternativa->delete();
                $retorno['success'] = true;
                $retorno['message'] = 'Alternativa foi removida com sucesso.';
            } catch (Exception $e) {
                throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a alternativa.');
            }
        } catch (Exception $e) {
            $retorno ['success'] = false;
            $retorno ['message'] = $e->getMessage();
        }
        unset($daoAlternativas, $alternativa);

        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    protected function exportExcel(Zend_Db_Select $filter, $idPesquisa)
    {
        ini_set('max_execution_time', 123456);
        ini_set('memory_limit', '-1');

        require_once('Spreadsheet/Excel/Writer.php');
        header("Content-type: application/Octet-Stream");
        header("Content-Disposition: inline; filename=Pesquisa.xls");
        header("Content-Disposition: attachment; filename=Pesquisa.xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        $xls = new Spreadsheet_Excel_Writer();
        $xls->SetVersion(8);

        $format['titulo'] = & $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
        $format['subtitulo'] = & $xls->addFormat(array('bold' => 1, 'size' => 8, 'fontFamily' => 'arial', 'align' => 'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border' => 1));
        $format['subtitulo-esq'] = & $xls->addFormat(array('bold' => 1, 'size' => 8, 'fontFamily' => 'arial', 'align' => 'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border' => 1));
        $format['normal'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1));
        $format['normal-esq'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'left'));
        $format['normal-cen'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'center'));
        $format['normal-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'fgColor' => 'silver'));
        $format['normal-esq-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'left', 'fgColor' => 'silver'));
        $format['normal-cen-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'center', 'fgColor' => 'silver'));
        $plan = & $xls->addWorksheet('Pesquisa');

        $plan->writeString(0, 0, 'Listagem das pesquisas', $format['titulo']);
        $plan->setColumn(0, 0, 12);
        $plan->setColumn(1, 1, 40);
        $plan->setColumn(1, 2, 20);
        $plan->setColumn(1, 3, 30);
        $plan->setColumn(1, 4, 15);

        $plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
        $plan->writeString(2, 1, 'Usu�rio', $format['subtitulo']);
        $plan->writeString(2, 2, 'Rede', $format['subtitulo']);
        $plan->writeString(2, 3, 'Cidade', $format['subtitulo']);
        $plan->writeString(2, 4, 'UF', $format['subtitulo']);

        $daoPesquisas = App_Model_DAO_Pesquisas::getInstance();
        $pesquisa = $daoPesquisas->fetchRow(
            $daoPesquisas->select()->from($daoPesquisas)->where('pes_idPesquisa = ?', $idPesquisa)
        );

        $plan->setMerge(0, 0, 0, $pesquisa->getPerguntas()->count() + 4);

        $linha = 2;
        $coluna = 5;
        $perguntas = $pesquisa->getPerguntas();
        foreach ($perguntas as $pergunta) {
            $plan->setColumn($linha, $coluna, 100);
            $plan->writeString($linha, $coluna, $pergunta->getPergunta(), $format['subtitulo-esq']);
            $coluna++;
            if ($pergunta->getTipo() != 'D') {
                $plan->writeString($linha, $coluna, 'Motivo', $format['subtitulo-esq']);
                $coluna++;
            }
        }
        $plan->setColumn($linha, $coluna, 100);
        $daoRespostas = App_Model_DAO_Pesquisas_Respostas::getInstance();
        $daoAlternativas = App_Model_DAO_Pesquisas_Alternativas::getInstance();
        $daoPerguntas = App_Model_DAO_Pesquisas_Perguntas::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();
        $daoLojasEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoGrupos = App_Model_DAO_GrupoLojas::getInstance();
        $daoRedes = App_Model_DAO_Redes::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();

        $totalRegistros = $daoPesquisas->getAdapter()->fetchOne(
            $daoPesquisas->getAdapter()->select()
                ->from(array('temp' => $filter->reset(Zend_Db_Select::LIMIT_COUNT)->reset(Zend_Db_Select::LIMIT_OFFSET)), 'COUNT(1)')
        );


        if ($totalRegistros > 0) {
            $linha = 3;
            for ($i = 0; $i < $totalRegistros; $i += 30) {
                $filter->limit(30, $i);
                $rsRegistros = $daoPesquisas->getAdapter()->fetchAll($filter);
                foreach ($rsRegistros as $record) {
                    $plan->writeString($linha, 0, ($record['usr_idLoja'] ? $record['usr_idLoja'] : $record['usr_idGrupoLoja']), $format["normal-cen"]);
                    $plan->writeString($linha, 1, $record['usr_nome'], $format["normal"]);

                    $select = $daoUsuarios->getAdapter()->select()
                        ->from($daoUsuarios->info('name'))
                        ->where('usr_idUsuario = ?', $record['usr_idUsuario']);

                    if ($record['usr_idLoja']) {
                        $select->joinInner($daoLojas->info('name'), 'usr_idLoja = loja_idLoja', null)
                            ->joinInner($daoLojasEnderecos->info('name'), 'loja_idLoja = loja_end_idLoja', array('loja_end_uf', 'loja_end_cidade'))
                            ->joinInner($daoGrupos->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
                            ->joinInner($daoRedes->info('name'), 'grp_loja_idRede = rede_idRede', 'rede_nome');
                    } else {
                        $select->joinInner($daoGrupos->info('name'), 'usr_idGrupoLoja = grp_loja_idGrupo', null)
                            ->joinInner($daoRedes->info('name'), 'grp_loja_idRede = rede_idRede', 'rede_nome');
                    }

                    $rede = $daoUsuarios->getAdapter()->fetchRow($select);
                    $plan->writeString($linha, 2, $rede['rede_nome'], $format["normal-cen"]);
                    $plan->writeString($linha, 3, isset($rede['loja_end_cidade']) ? $rede['loja_end_cidade'] : '', $format["normal-cen"]);
                    $plan->writeString($linha, 4, isset($rede['loja_end_uf']) ? $rede['loja_end_uf'] : '', $format["normal-cen"]);

                    $coluna = 5;
                    foreach ($perguntas as $pergunta) {

                        $resposta = $daoRespostas->getAdapter()->fetchRow(
                            $daoRespostas->getAdapter()->select()
                                ->from($daoRespostas->info('name'), 'pes_resp_resposta')
                                ->joinLeft($daoAlternativas->info('name'), 'pes_resp_idAlternativa = pes_alt_idAlternativa', array('pes_alt_alternativa'))
                                ->where('pes_resp_idUsuario = ?', $record['usr_idUsuario'])
                                ->where('pes_resp_idPergunta = ?', $pergunta->getCodigo())
                        );

                        if ($pergunta->getTipo() != 'D') {
                            $plan->writeString($linha, $coluna, $resposta['pes_alt_alternativa'], $format["normal"]);
                            $coluna++;
                            $plan->writeString($linha, $coluna, $resposta['pes_resp_resposta'], $format["normal"]);
                            $coluna++;
                        } else {
                            $plan->writeString($linha, $coluna, $resposta['pes_resp_resposta'], $format["normal"]);
                            $coluna++;
                        }
                    }

                    $linha++;
                }
                unset($rsRegistros);
            }
            unset($perguntas);
        } else {
            $plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
            for ($c = 1; $c <= $pesquisa->getPerguntas()->count() + 2; $c++) {
                $plan->writeString(3, $c, '', $format["normal-cen"]);
            }
            $plan->setMerge(3, 0, 3, $pesquisa->getPerguntas()->count() + 2);
        }
        unset($daoPesquisas, $filter);

        $xls->close();
    }

}
