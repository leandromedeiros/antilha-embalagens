<?php

class VideosController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoVideos = App_Model_DAO_Videos::getInstance();
				
				$filter = $daoVideos->select()
					->from($daoVideos->info('name'))
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'vid_dataCadastro')} {$this->getRequest()->getParam('dir', 'ASC')}")
					->group('vid_idVideo');

				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('videos' => array(), 'total' => 0);
					$rsVideos = $daoVideos->fetchAll($filter);
					foreach ($rsVideos as $video) {
						$retorno['videos'][] = array(
							'vid_idVideo' => $video->getCodigo(),
							'vid_dataCadastro' => $video->getDataCadastro(),
							'vid_titulo' => $video->getTitulo(),
							'vid_linkYoutube' => $video->getLinkYoutube(),
							'vid_destaque' => (int) (string) $video->getDestaque(),
							'vid_chamada' => $video->getChamada(),
							'vid_status' => (int) (string) $video->getStatus(),
						);
					}

					$retorno['total'] = $daoVideos->getCount($filter);
					unset($rsVideos);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoVideos, $filter);
				break;
			
			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function insertAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'video' => array());
			$daoVideos = App_Model_DAO_Videos::getInstance();
			
			try {
				$video = $daoVideos->createRow();
				$video = $this->montaObjeto($video);

				try {
					$video->save();
					$retorno['success'] = true;
					$retorno['message'] = sprintf('V�deo <b>%s</b> cadastrado com sucesso.', $video->getTitulo());

				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					throw new Exception('Por favor verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o v�deo.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoVideos, $video);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('vid_idVideo', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'video' => array());
			$daoVideos = App_Model_DAO_Videos::getInstance();
			try {
				$video = $daoVideos->fetchRow(
					$daoVideos->select()->where('vid_idVideo = ?', $idRegistro)
				);
				if (null == $video) {
					throw new Exception('O v�deo solicitado n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					//carrega os dados
					$retorno ['success'] = true;
					$arrVideo = $video->toArray();
					
					foreach ($video->getPermissoes() as $permissao) {
						if($permissao->getGrupoRede()) {
							$arrVideo['vid_permissoes'][] = array(
								'idControle' => $permissao->getGrupoRede()->getCodigo() . 'GR',
								'id' => $permissao->getGrupoRede()->getCodigo(),
								'nome' => $permissao->getGrupoRede()->getNome(),
								'grupo' => 'GR'
							);
						}	

						if($permissao->getRede()) {
							$arrVideo['vid_permissoes'][] = array(
								'idControle' => $permissao->getRede()->getCodigo() . 'RE',
								'id' => $permissao->getRede()->getCodigo(),
								'nome' => $permissao->getRede()->getNome(),
								'grupo' => 'RE'
							);
						}

						if($permissao->getGrupoLoja()) {
							$arrVideo['vid_permissoes'][] = array(
								'idControle' => $permissao->getGrupoLoja()->getCodigo() . 'GL',
								'id' => $permissao->getGrupoLoja()->getCodigo(),
								'nome' => $permissao->getGrupoLoja()->getNome(),
								'grupo' => 'GL'
							);
						}
						
						if($permissao->getLoja()) {
							$arrVideo['vid_permissoes'][] = array(
								'idControle' => $permissao->getLoja()->getCodigo() . 'LO',
								'id' => $permissao->getLoja()->getCodigo(),
								'nome' => $permissao->getLoja()->getNomeCompleto(),
								'grupo' => 'LO'
							);
						}
						
						if($permissao->getLinha()) {
							$arrVideo['vid_permissoes'][] = array(
								'idControle' => $permissao->getLinha()->getCodigo() . 'LI',
								'id' => $permissao->getLinha()->getCodigo(),
								'nome' => $permissao->getLinha()->getNome(),
								'grupo' => 'LI'
							);
						}
					}

					foreach ($video->getEstados() as $estado) {
						$arrVideo['vid_estados'][] = $estado->getUf();
					}
					
					$retorno['video'] = array($arrVideo);
				} else {
					//atualiza os dados
					$video = $this->montaObjeto($video);

					try {
						$video->save();
						
						$retorno['success'] = true;
						$retorno['message'] = sprintf('V�deo <b>%s</b> alterado com sucesso.', $video->getTitulo());

					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o v�deo.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoVideos, $video);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
		
	}

	protected function montaObjeto(App_Model_Entity_Video $video) {
		// atualiza os dados
		$video->setDataCadastro(App_Funcoes_Date::conversion($this->getRequest()->getParam('vid_dataCadastro')))
			->setTitulo(utf8_decode($this->getRequest()->getParam('vid_titulo')))
			->setLinkYoutube(utf8_decode($this->getRequest()->getParam('vid_linkYoutube')))
			->setDestaque($this->getRequest()->getParam('vid_destaque'))
			->setChamada(utf8_decode($this->getRequest()->getParam('vid_chamada')))
			->setStatus($this->getRequest()->getParam('vid_status'));

		// Permiss�es
		$video->getPermissoes()->offsetRemoveAll();
		if ($this->getRequest()->getParam('vid_permissoes')) {
			
			$daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
			$daoRedes = App_Model_DAO_Redes::getInstance();
			$daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
			$daoLojas = App_Model_DAO_Lojas::getInstance();
			$daoLinha = App_Model_DAO_Linhas::getInstance();
			
			$vid_permissoes = Zend_Json::decode($this->getRequest()->getParam('vid_permissoes', array()));
			foreach ($vid_permissoes as $permissoes) {
				$permissao = App_Model_DAO_Videos_Permissoes::getInstance()->createRow();
				switch ($permissoes['grupo']) {
					case 'GR':
						$permissao->setGrupoRede(
							$daoGrupoRedes->fetchRow(
								$daoGrupoRedes->select()->where('grp_rede_idGrupo = ?', $permissoes['id'])
							)
						)
						->setRede(null)
						->setGrupoLoja(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'RE':
						$permissao->setRede(
							$daoRedes->fetchRow(
								$daoRedes->select()->where('rede_idRede = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setGrupoLoja(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'GL':
						$permissao->setGrupoLoja(
							$daoGrupoLojas->fetchRow(
								$daoGrupoLojas->select()->where('grp_loja_idGrupo = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setLoja(null)
						->setLinha(null);
					break;
					case 'LO':
						$permissao->setLoja(
							$daoLojas->fetchRow(
								$daoLojas->select()->where('loja_idLoja = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setGrupoLoja(null)
						->setLinha(null);
					break;
					case 'LI':
						$permissao->setLinha(
							$daoLinha->fetchRow(
								$daoLinha->select()->where('lin_idLinha = ?', $permissoes['id'])
							)
						)
						->setGrupoRede(null)
						->setRede(null)
						->setGrupoLoja(null)
						->setLoja(null);
					break;
				}
				$video->getPermissoes()->offsetAdd($permissao);
			}
		}
		
		// Estados
		$video->getEstados()->offsetRemoveAll();
		if (false != ($estados = $this->getRequest()->getParam('vid_estados', false))) {
			$arrEstados = Zend_Json::decode($estados);
			foreach ($arrEstados as $key) {
				$estado = App_Model_DAO_Videos_Estados::getInstance()->createRow();
				$estado->setUf($key);
				$video->getEstados()->offsetAdd($estado);
			}
		}
		return $video;
	}
	
	protected function buildForm()
	{
		$arrEstados = array();
		foreach (App_Funcoes_Rotulos::$UF as $key => $value) {
			$arrEstados[] = array(
				'uf' => $key,
				'nome' => $value
			);
		}
		App_Funcoes_UTF8::encode($arrEstados);
		$this->view->estados = Zend_Json::encode($arrEstados);

		$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		$this->render('form');
	}
	
	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoVideos = App_Model_DAO_Videos::getInstance();
		try {
			$video = $daoVideos->fetchRow(
				$daoVideos->select()->where('vid_idVideo = ?', $this->getRequest()->getParam('idVideo'))
			);
			if (null == $video) {
				throw new Exception('O v�deo solicitado n�o foi encontrado.');
			}
			try {
				$nome = $video->getTitulo();
				$video->delete();

				$retorno['success'] = true;
				$retorno['message'] = sprintf('V�deo <b>%s</b> removido com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o v�deo.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoVideos, $video);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}

	protected function exportExcel(Zend_Db_Select $filter)
	{
		require_once('Spreadsheet/Excel/Writer.php');
		header("Content-type: application/Octet-Stream");
		header("Content-Disposition: inline; filename=Videos.xls");
		header("Content-Disposition: attachment; filename=Videos.xls");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		$xls = new Spreadsheet_Excel_Writer();
		$xls->SetVersion(8);

		$format['titulo'] =& $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
		$format['subtitulo'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['subtitulo-esq'] =& $xls->addFormat(array('bold'=>1, 'size'=>8, 'fontFamily'=>'arial', 'align'=>'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border'=>1));
		$format['normal'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1));
		$format['normal-esq'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left'));
		$format['normal-cen'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center'));
		$format['normal-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'fgColor' => 'silver'));
		$format['normal-esq-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'left', 'fgColor' => 'silver'));
		$format['normal-cen-gray'] =& $xls->addFormat(array('bold'=>0, 'size'=>8, 'fontFamily'=>'arial', 'borderColor' => 'black', 'border'=>1, 'align'=>'center', 'fgColor' => 'silver'));
		$plan =& $xls->addWorksheet('Videos');

		$plan->writeString(0, 0, 'Listagem de V�deos', $format['titulo']);
		$plan->setMerge(0, 0, 0, 5);
		$plan->setColumn(0, 0, 15);
		$plan->setColumn(1, 1, 20);
		$plan->setColumn(2, 2, 35);
		$plan->setColumn(3, 3, 40);
		$plan->setColumn(4, 4, 15);
		$plan->setColumn(4, 5, 15);

		$plan->writeString(2, 0, 'C�digo', $format['subtitulo']);
		$plan->writeString(2, 1, 'Data de Cadastro', $format['subtitulo']);
		$plan->writeString(2, 2, 'T�tulo', $format['subtitulo']);
		$plan->writeString(2, 3, 'C�digo do Youtube', $format['subtitulo']);
		$plan->writeString(2, 4, 'Destaque', $format['subtitulo']);
		$plan->writeString(2, 5, 'Status', $format['subtitulo']);

		$daoVideos = App_Model_DAO_Videos::getInstance();
		$filter->limit(null, null);
		$totalRegistros = $daoVideos->getCount(clone $filter);
		if ($totalRegistros > 0) {
			$linha = 3;
			for ($i = 0; $i < $totalRegistros; $i += 30) {
				$filter->limit(30, $i);
				$rsRegistros = $daoVideos->fetchAll($filter);
				foreach ($rsRegistros as $record) {
					$plan->writeString($linha, 0, $record->getCodigo(), $format["normal-cen"]);
					$plan->writeString($linha, 1, App_Funcoes_Date::conversion($record->getDataCadastro()), $format["normal-cen"]);
					$plan->writeString($linha, 2, $record->getTitulo(), $format["normal"]);
					$plan->writeString($linha, 3, $record->getCodigoYoutube(), $format["normal"]);
					$plan->writeString($linha, 4, App_Funcoes_Rotulos::$statusSimNao[$record->getDestaque()], $format["normal-cen"]);
					$plan->writeString($linha, 5, App_Funcoes_Rotulos::$status[$record->getStatus()], $format["normal-cen"]);
					$linha++;
				}
				unset($rsRegistros);
			}
		} else {
			$plan->writeString(3, 0, 'Sem registros para exibi��o', $format["normal-cen"]);
			for ($c = 1; $c <= 4; $c++) {
				$plan->writeString(3, $c, '', $format["normal-cen"]);
			}
			$plan->setMerge(3, 0, 3, 5);
		}
		unset($daoVideos, $filter);

		$xls->close();
	}
}