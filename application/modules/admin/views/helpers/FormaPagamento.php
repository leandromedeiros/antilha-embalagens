<?php

class Zend_View_Helper_FormaPagamento extends Zend_View_Helper_Abstract
{
	/**
	 * Recupera todos os c�digos e formas de pagamento
	 *
	 * @return array
	 */
	public function FormaPagamento()
	{
		$arrayFormas = array();
		//varre o diret�rio Pagamento/Forma procurando as classes dispon�veis
		$path = APPLICATION_ROOT. '/models/Entity/Pedido/Pagamento/Forma';
		$classes = new DirectoryIterator($path);
		foreach ($classes as $file) {
			if ($file->isDot() || $file->isDir()) continue;

			$forma = new Zend_Config_Xml(APPLICATION_ROOT."/configs/". $file->getBasename('.php') .".xml", APPLICATION_ENV);

			if($forma->services->service->code) {
				$arrayFormas[$forma->services->service->code] = $forma->services->service->name;
			} else {
				foreach ($forma->services->service as $servico) {
					$arrayFormas[$servico->code] = $servico->name;
				}
			}
		}
		
		return $arrayFormas;
	}
}
