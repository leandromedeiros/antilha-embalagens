<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initAutoload()
    {
        $loader = new Zend_Loader_Autoloader_Resource(array(
            'namespace' => 'App',
            'basePath' => APPLICATION_ROOT,
            'resourceTypes' => array(
                'models' => array(
                    'namespace' => 'Model',
                    'path' => 'models/'
                ),
                'plugins' => array(
                    'namespace' => 'Plugin',
                    'path' => 'modules/site/plugins/'
                ),
                'filters' => array(
                    'namespace' => 'Filter',
                    'path' => 'library/Filters/'
                ),
                'validators' => array(
                    'namespace' => 'Validate',
                    'path' => 'library/Validators/'
                ),
                'functions' => array(
                    'namespace' => 'Funcoes',
                    'path' => 'library/Funcoes/'
                ),
            )
        ));
        return $loader;
    }

    protected function _initRegistry()
    {
        $config = new Zend_Config(require_once APPLICATION_ROOT . '/configs/config.php');
        $configSistema = new Zend_Config(require_once APPLICATION_ROOT . '/configs/configuracao.php');
        Zend_Registry::set('config', $config);
        Zend_Registry::set('configInfo', $configSistema);
    }

    protected function _initFrontController()
    {
        $controller = Zend_Controller_Front::getInstance();
        $controller->setControllerDirectory(APPLICATION_PATH . '/controllers/')
            ->registerPlugin(new Zend_Controller_Plugin_ErrorHandler())
            ->registerPlugin(new App_Plugin_Auth())
            ->throwExceptions(true);

        return $controller;
    }

    protected function _initView()
    {
        $view = new Zend_View();
        $view->doctype('XHTML1_STRICT');
        $view->setEncoding('ISO-8859-1');
        $view->setEscape('htmlentities');
        $view->HeadTitle(Zend_Registry::get('config')->project)->setSeparator(' - ');

        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer($view);
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

        Zend_Layout::startMvc(array(
            'layout' => 'layout'
        ));

        return $view;
    }

    /**
     * Faz o cacheamento dos metadados de Zend_Db_Table_Abstract
     *
     * @return Zend_Cache
     */
    protected function _initMetadataCache()
    {
        /* if ('development' != APPLICATION_ENV) {
          $frontendOptions = array(
          'lifetime' => 86400, // 24 horas
          'automatic_serialization' => true
          );
          $backendOptions = array(
          'cache_dir' => APPLICATION_ROOT . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'metadata'
          );

          $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
          Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);

          return $cache;
          } */
    }

    protected function _initRoute()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();

        // -------------------------------
        // Autentica��o
        // -------------------------------
        $router->addRoute(
            'login', new Zend_Controller_Router_Route(
            'login/*', array('controller' => 'index', 'action' => 'login')
            )
        );

        $router->addRoute(
            'validar', new Zend_Controller_Router_Route(
            'validar/*', array('controller' => 'index', 'action' => 'validar')
            )
        );

        $router->addRoute(
            'logout', new Zend_Controller_Router_Route(
            'logout/*', array('controller' => 'index', 'action' => 'logout')
            )
        );

        $router->addRoute(
            'home', new Zend_Controller_Router_Route(
            'home/*', array('controller' => 'index', 'action' => 'home')
            )
        );

        $router->addRoute(
            'ajuda', new Zend_Controller_Router_Route(
            'ajuda/*', array('controller' => 'index', 'action' => 'ajuda')
            )
        );

        $router->addRoute(
            'acesso-administrador', new Zend_Controller_Router_Route(
            'acesso-administrador/*', array('controller' => 'index', 'action' => 'acesso-administrador')
            )
        );

        $router->addRoute(
            'politica', new Zend_Controller_Router_Route(
            'politica-de-devolucao/*', array('controller' => 'index', 'action' => 'politica-de-devolucao')
            )
        );

        $router->addRoute(
            'fale-conosco', new Zend_Controller_Router_Route(
            'fale-conosco/*', array('controller' => 'index', 'action' => 'fale-conosco')
            )
        );

        $router->addRoute(
            'reclamacoes-detalhe', new Zend_Controller_Router_Route(
            'relatorios/reclamacoes/detalhe/:id', array('controller' => 'relatorios', 'action' => 'reclamacoes-detalhe')
            )
        );

        $router->addRoute(
            'historico-produtos', new Zend_Controller_Router_Route(
            'produtos/historico', array('controller' => 'produtos', 'action' => 'produtos-historico')
            )
        );

        $router->addRoute(
            'produtos-catalogo', new Zend_Controller_Router_Route(
            'produtos/catalogo', array('controller' => 'produtos', 'action' => 'produtos-catalogo')
            )
        );

        $router->addRoute(
            'historico-financeiro', new Zend_Controller_Router_Route(
            'relatorios/financeiros', array('controller' => 'relatorios', 'action' => 'historico-financeiro')
            )
        );

        $router->addRoute(
            'download', new Zend_Controller_Router_Route(
            'relatorios/download/:id', array('controller' => 'relatorios', 'action' => 'download')
            )
        );

        $router->addRoute(
            'downloadXML', new Zend_Controller_Router_Route(
            'relatorios/download-xml/:id', array('controller' => 'relatorios', 'action' => 'download-xml')
            )
        );

        $router->addRoute(
            'visualizarPDF', new Zend_Controller_Router_Route(
            'relatorios/visualizar-pdf/:id', array('controller' => 'relatorios', 'action' => 'visualizar-pdf')
            )
        );

        $router->addRoute(
            'downloadPdfBoleto', new Zend_Controller_Router_Route(
            'relatorios/download-pdf-boleto/:id', array('controller' => 'relatorios', 'action' => 'download-pdf-boleto')
            )
        );

        $router->addRoute(
            'meus-pedidos-detalhe', new Zend_Controller_Router_Route(
            'faca-seu-pedido/meus-pedidos/detalhe/:id', array('controller' => 'faca-seu-pedido', 'action' => 'meus-pedidos-detalhe')
            )
        );

        // -------------------------------
        // Gerenciamento de usu�rios
        // -------------------------------
        $router->addRoute(
            'dados-loja', new Zend_Controller_Router_Route(
            'dados-da-loja/*', array('controller' => 'gerenciamento-de-usuarios', 'action' => 'dados-da-loja')
            )
        );

        $router->addRoute(
            'alterar-dados', new Zend_Controller_Router_Route(
            'alterar-dados/*', array('controller' => 'gerenciamento-de-usuarios', 'action' => 'alterar-dados')
            )
        );

        // -------------------------------
        // V�deos
        // -------------------------------
        $router->addRoute(
            'produtos-videos', new Zend_Controller_Router_Route(
            'produtos/videos/:idVideo/:nome/*', array('controller' => 'produtos', 'action' => 'videos')
            )
        );

        // -------------------------------
        // Comunicados
        // -------------------------------
        $router->addRoute(
            'comunicados-disponibilizados', new Zend_Controller_Router_Route(
            'comunicados-disponibilizados/:idComunicado/:nome/*', array('controller' => 'comunicados', 'action' => 'comunicados-disponibilizados')
            )
        );

        $router->addRoute(
            'comunicados-detalhe', new Zend_Controller_Router_Route(
            'comunicados/detalhe/:idComunicado/:nome/*', array('controller' => 'comunicados', 'action' => 'detalhe')
            )
        );

        // -------------------------------
        // Noticias
        // -------------------------------
        $router->addRoute(
            'detalhe-noticia', new Zend_Controller_Router_Route(
            'noticia/:idNoticia/:titulo', array('controller' => 'noticias', 'action' => 'index')
            )
        );
        $router->addRoute(
            'noticias', new Zend_Controller_Router_Route(
            'noticias', array('controller' => 'noticias', 'action' => 'index')
            )
        );
        $router->addRoute(
            'comunicados-noticias', new Zend_Controller_Router_Route(
            'comunicados/noticias', array('controller' => 'noticias', 'action' => 'index')
            )
        );

        $router->addRoute(
            'pesquisa', new Zend_Controller_Router_Route(
            'faca-seu-pedido/pesquisa/:codigo/:nome/*', array('controller' => 'faca-seu-pedido', 'action' => 'pesquisa')
            )
        );
        
        $router->addRoute(
            'prepesquisa', new Zend_Controller_Router_Route(
            'faca-seu-pedido/prepesquisa/:codigo/:nome', array('controller' => 'faca-seu-pedido', 'action' => 'prepesquisa')
            )
        );
    }

}
