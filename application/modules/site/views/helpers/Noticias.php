<?php

class Zend_View_Helper_Noticias extends Zend_View_Helper_Abstract
{
	public function Noticias()
	{
		return $this;
	}
	
	public function renderBlocoNoticia($noticia)
	{
		$view = Zend_Layout::getMvcInstance()->getView();
		$view->noticia = $noticia;
		echo $view->render('noticias/bloco-noticia.phtml');
	}
}