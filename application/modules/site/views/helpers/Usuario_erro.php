<?php

class Zend_View_Helper_Usuario extends Zend_View_Helper_Abstract
{

    /**
     * @var array
     */
    protected $usuarios = array();

    public function Usuario()
    {
        return $this;
    }

    /**
     * Recupera o controlador do usu�rio
     * @return @return App_Plugin_Login
     */
    public function getLogin()
    {
        return App_Plugin_Login::getInstance();
    }

    /**
     * Retorna um array de usu�rios que pertence a hierarquia do usu�rio logado
     * @return App_Model_Collection of App_Model_Entity_Sistema_Usuario
     */
    public function getUsuariosHierarquiasArray()
    {
        $usuarios = array();
        $login = App_Plugin_Login::getInstance()->getIdentity();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
        $daoRedes = App_Model_DAO_Redes::getInstance();
        $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();

        if ($login->getGrupoRede()) {

            $selectGrupoRedes = $daoUsuarios->getAdapter()->select()
                ->from($daoUsuarios->info('name'))
                ->where('usr_idGrupoRede = ?', $login->getGrupoRede()->getCodigo());

            $selectRedes = $daoUsuarios->getAdapter()->select()
                ->from($daoUsuarios->info('name'))
                ->joinInner($daoRedes->info('name'), 'usr_idRede = rede_idRede', null)
                ->where('rede_idGrupo = ?', $login->getGrupoRede()->getCodigo());

            $selectGrupoLojas = $daoUsuarios->getAdapter()->select()
                ->from($daoUsuarios->info('name'))
                ->joinInner($daoGrupoLojas->info('name'), 'usr_idGrupoLoja = grp_loja_idGrupo', null)
                ->joinInner($daoRedes->info('name'), 'rede_idRede = grp_loja_idRede', null)
                ->where('grp_rede_idGrupo = ?', $login->getGrupoRede()->getCodigo());

            $selectLojas = $daoUsuarios->getAdapter()->select()
                ->from($daoUsuarios->info('name'))
                ->joinInner($daoLojas->info('name'), 'loja_idLoja = usr_idLoja', null)
                ->joinInner($daoGrupoLojas->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
                ->joinInner($daoRedes->info('name'), 'rede_idRede = grp_loja_idRede', null)
                ->where('grp_rede_idGrupo = ?', $login->getGrupoRede()->getCodigo());

            $usuarios = $daoUsuarios->createRowset(
                $daoUsuarios->getAdapter()->fetchAll(
                    $daoUsuarios->getAdapter()->select()
                        ->from(array('temp' => $daoUsuarios->getAdapter()
                            ->select()
                            ->union($selectGrupoRedes, $selectRedes, $selectGrupoLojas, $selectLojas)))
                )
            );
        }

        if ($login->getRede()) {

            $selectRedes = $daoUsuarios->getAdapter()->select()
                ->from($daoUsuarios->info('name'))
                ->where('usr_idRede = ?', $login->getRede()->getCodigo());


            $selectGrupoLojas = $daoUsuarios->getAdapter()->select()
                ->from($daoUsuarios->info('name'))
                ->joinInner($daoGrupoLojas->info('name'), 'usr_idGrupoLoja = grp_loja_idGrupo', null)
                ->where('grp_loja_idRede = ?', $login->getRede()->getCodigo());

            $selectLojas = $daoUsuarios->getAdapter()->select()
                ->from($daoUsuarios->info('name'))
                ->joinInner($daoLojas->info('name'), 'loja_idLoja = usr_idLoja', null)
                ->joinInner($daoGrupoLojas->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
                ->where('grp_loja_idRede = ?', $login->getRede()->getCodigo());

            $usuarios = $daoUsuarios->createRowset(
                $daoUsuarios->getAdapter()->fetchAll(
                    $daoUsuarios->getAdapter()->select()
                        ->from(array('temp' => $daoUsuarios->getAdapter()
                            ->select()
                            ->union(array($selectRedes, $selectGrupoLojas, $selectLojas))))
                )
            );
        }

        if ($login->getGrupoLoja()) {

            $selectGrupoLojas = $daoUsuarios->getAdapter()->select()
                ->from($daoUsuarios->info('name'))
                ->where('usr_idGrupoLoja = ?', $login->getGrupoLoja()->getCodigo());

            $selectLojas = $daoUsuarios->getAdapter()->select()
                ->from($daoUsuarios->info('name'))
                ->joinInner($daoLojas->info('name'), 'loja_idLoja = usr_idLoja', null)
                ->where('loja_idGrupo = ?', $login->getGrupoLoja()->getCodigo());

            $usuarios = $daoUsuarios->createRowset(
                $daoUsuarios->getAdapter()->fetchAll(
                    $daoUsuarios->getAdapter()->select()
                        ->from(array('temp' => $daoUsuarios->getAdapter()
                            ->select()
                            ->union($selectGrupoLojas, $selectLojas)))
                )
            );
        }

        if ($login->getLoja()) {

            $usuarios = $daoUsuarios->createRowset(
                $daoUsuarios->getAdapter()->fetchAll(
                    $daoUsuarios->getAdapter()->select()
                        ->from($daoUsuarios->info('name'))
                        ->where('usr_idLoja = ?', $login->getLoja()->getCodigo())
            ));
        }

        return $usuarios;
    }

    /**
     * Retorna uma cole��o de usu�rios que pertence a hierarquia do usu�rio logado
     * @return App_Model_Collection of App_Model_Entity_Sistema_Usuario
     */
    public function getUsuariosHierarquias()
    {
        return $this->getUsuariosHierarquiasArray();
    }

    /**
     * Retorna o quoteInto de usu�rios
     * @param string
     */
    public function quoteIntoUsuarios($coluna)
    {
        $ids = array();
        $usuarios = $this->getUsuariosHierarquiasArray();
        foreach ($usuarios as $usuarios) {
            $ids[] = $usuarios->getCodigo();
        }
        $ids = implode("','", $ids);
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        return $daoUsuarios->getAdapter()->quoteInto("{$coluna} IN ('{$ids}')", '');
    }

}
