<?php

class Zend_View_Helper_Usuario extends Zend_View_Helper_Abstract
{	
	/**
	 * @var array
	 */
	protected $usuarios = array();
	
	public function Usuario()
	{
		return $this;
	}
	
	/**
	 * Recupera o controlador do usu�rio
	 * @return @return App_Plugin_Login
	 */
	public function getLogin() {
		return App_Plugin_Login::getInstance();
	}
	
	/**
	 * Retorna um array de usu�rios que pertence a hierarquia do usu�rio logado
	 * @return array
	 */
	public function getUsuariosHierarquiasArray() {
		//$login = App_Plugin_Login::getInstance()->getIdentity();
		$login = App_Model_DAO_Sistema_Usuarios::getInstance()->find(App_Plugin_Login::getInstance()->getIdentity()->getCodigo())->current();
		if($login->getGrupoRede()) 	$this->recursive($login->getGrupoRede());
		if($login->getRede()) 		$this->recursive($login->getRede());
		if($login->getGrupoLoja()) 	$this->recursive($login->getGrupoLoja());
		if($login->getLoja()) 		$this->recursive($login->getLoja());
				
		return $this->usuarios;
	}
	
	/**
	 * Retorna uma cole��o de usu�rios que pertence a hierarquia do usu�rio logado
	 * @return App_Model_Collection of App_Model_Entity_Sistema_Usuario
	 */
	public function getUsuariosHierarquias() {
		$ids = $this->getUsuariosHierarquiasArray();
		$retorno = array();
		if(count($ids)) {
			$ids = implode("','", $ids);
			$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();			
			$retorno = $daoUsuarios->fetchAll(
				$daoUsuarios->select()->from($daoUsuarios)
					->where("usr_idUsuario IN ('{$ids}')")
					->order('usr_nome')
			);
			return $retorno;
		}
		return $retorno;
	}
	
	/**
	 * Retorna o quoteInto de usu�rios
	 * @param string
	 */
	public function quoteIntoUsuarios($coluna) {
		$ids = $this->getUsuariosHierarquiasArray();
		$ids = implode("','", $ids);
		$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
		return $daoUsuarios->getAdapter()->quoteInto("{$coluna} IN ('{$ids}')", '');
	}
	
	public function recursive($inicio) {
		switch (get_class($inicio)) {
			case 'App_Model_Entity_GrupoRede':
				foreach ($inicio->getRedes() as $rede) {
					$this->recursive($rede);
				}
			break;
			case 'App_Model_Entity_Rede':	
				foreach ($inicio->getGrupos() as $grupo) {
					$this->recursive($grupo);
				}
			break;
			case 'App_Model_Entity_GrupoLoja':		
				foreach ($inicio->getLojas() as $loja) {
					$this->recursive($loja);
				}
			break;	
		}
		foreach ($inicio->getUsuarios() as $usuario) {
			$this->usuarios[] = $usuario->getCodigo();
		}
	}
}
