<?php

class Zend_View_Helper_IsEmail extends Zend_View_Helper_Abstract
{

    public function IsEmail($email)
    {
        $validator = array(
            'email' => array(new Zend_Validate_EmailAddress())
        );
        $filter = new Zend_Filter_Input(
            array('*' => new Zend_Filter_StringTrim()), $validator, array('email' => $email)
        );
        return ($filter->isValid() === false) ? false : true;
    }

}
