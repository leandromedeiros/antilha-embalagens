<?php

class Zend_View_Helper_Combo extends Zend_View_Helper_Abstract
{

    protected $retorno = array();

    /**
     * Lista de grupo de redes, redes, grupo de lojas, lojas e linhas conforme suas hierarquias
     *
     * @return array
     */
    public function Combo()
    {
        return $this;
    }

    /**
     * Retorna todas os grupos de rede que o usu�rio pode visualizar
     * @return array of App_Model_Entity_GrupoRede
     */
    public function getGrupoRedes()
    {
        $retorno = array();
        $login = App_Plugin_Login::getInstance()->getIdentity();
        $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();

        if ($login->getGrupoRede()) {
            $retorno = $daoGrupoRedes->fetchAll(
                $daoGrupoRedes->select()
                    ->from($daoGrupoRedes)
                    ->where('grp_rede_idGrupo = ?', $login->getGrupoRede()->getCodigo())
            );
        }

        return $retorno;
    }

    /**
     * Retorna todas as rede que o usu�rio pode visualizar
     * @return array of App_Model_Entity_Rede
     */
    public function getRedes()
    {
        $retorno = array();
        $login = App_Plugin_Login::getInstance()->getIdentity();
        $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
        $daoRedes = App_Model_DAO_Redes::getInstance();

        if ($login->getGrupoRede()) {
            $retorno = $daoRedes->createRowset(
                $daoRedes->getAdapter()->fetchAll(
                    $daoRedes->getAdapter()->select()
                        ->from($daoRedes->info('name'))
                        ->joinInner($daoGrupoRedes->info('name'), 'grp_rede_idGrupo = rede_idGrupo', null)
                        ->where('grp_rede_idGrupo = ?', $login->getGrupoRede()->getCodigo())
                )
            );
        }

        if ($login->getRede()) {
            $retorno = $daoRedes->createRowset(
                $daoRedes->getAdapter()->fetchAll(
                    $daoRedes->getAdapter()->select()
                        ->from($daoRedes->info('name'))
                        ->where('rede_idRede= ?', $login->getRede()->getCodigo())
                )
            );
        }

        return $retorno;
    }

    /**
     * Retorna todas os grupo de lojass que o usu�rio pode visualizar
     * @return array of App_Model_Entity_GrupoLojas
     */
    public function getGrupoLojas()
    {
        $retorno = array();
        $login = App_Plugin_Login::getInstance()->getIdentity();
        $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
        $daoRedes = App_Model_DAO_Redes::getInstance();
        $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();

        if ($login->getGrupoRede()) {
            $retorno = $daoGrupoLojas->createRowset(
                $daoGrupoLojas->getAdapter()->fetchAll(
                    $daoGrupoLojas->getAdapter()->select()
                        ->from($daoGrupoLojas->info('name'))
                        ->joinInner($daoRedes->info('name'), 'rede_idRede = grp_loja_idRede', null)
                        ->joinInner($daoGrupoRedes->info('name'), 'grp_rede_idGrupo = rede_idGrupo', null)
                        ->where('grp_rede_idGrupo = ?', $login->getGrupoRede()->getCodigo())
                )
            );
        }

        if ($login->getRede()) {
            $retorno = $daoGrupoLojas->createRowset(
                $daoGrupoLojas->getAdapter()->fetchAll(
                    $daoGrupoLojas->getAdapter()->select()
                        ->from($daoGrupoLojas->info('name'))
                        ->joinInner($daoRedes->info('name'), 'rede_idRede = grp_loja_idRede', null)
                        ->where('rede_idRede = ?', $login->getRede()->getCodigo())
                )
            );
        }

        if ($login->getGrupoLoja()) {
            $retorno = $daoGrupoLojas->createRowset(
                $daoGrupoLojas->getAdapter()->fetchAll(
                    $daoGrupoLojas->getAdapter()->select()
                        ->from($daoGrupoLojas->info('name'))
                        ->where('grp_loja_idGrupo = ?', $login->getGrupoLoja()->getCodigo())
                )
            );
        }

        return $retorno;
    }

    /**
     * Retorna todas as lojas que o usu�rio pode visualizar
     * @return array of App_Model_Entity_Lojas
     */
    public function getLojas()
    {
        $retorno = array();
        $login = App_Plugin_Login::getInstance()->getIdentity();
        $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
        $daoRedes = App_Model_DAO_Redes::getInstance();
        $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();

        if ($login->getGrupoRede()) {
            $retorno = $daoLojas->createRowset(
                $daoLojas->getAdapter()->fetchAll(
                    $daoLojas->getAdapter()->select()
                        ->from($daoLojas->info('name'))
                        ->joinInner($daoGrupoLojas->info('name'), 'grp_loja_idGrupo = loja_idGrupo', null)
                        ->joinInner($daoRedes->info('name'), 'rede_idRede = grp_loja_idRede', null)
                        ->joinInner($daoGrupoRedes->info('name'), 'grp_rede_idGrupo = rede_idGrupo', null)
                        ->where('grp_rede_idGrupo = ?', $login->getGrupoRede()->getCodigo())
                )
            );
        }

        if ($login->getRede()) {
            $retorno = $daoLojas->createRowset(
                $daoLojas->getAdapter()->fetchAll(
                    $daoLojas->getAdapter()->select()
                        ->from($daoLojas->info('name'))
                        ->joinInner($daoGrupoLojas->info('name'), 'grp_loja_idGrupo = loja_idGrupo', null)
                        ->joinInner($daoRedes->info('name'), 'rede_idRede = grp_loja_idRede', null)
                        ->where('rede_idRede = ?', $login->getRede()->getCodigo())
                )
            );
        }

        if ($login->getGrupoLoja()) {
            $retorno = $daoLojas->createRowset(
                $daoLojas->getAdapter()->fetchAll(
                    $daoLojas->getAdapter()->select()
                        ->from($daoLojas->info('name'))
                        ->joinInner($daoGrupoLojas->info('name'), 'grp_loja_idGrupo = loja_idGrupo', null)
                        ->where('grp_loja_idGrupo = ?', $login->getGrupoLoja()->getCodigo())
                )
            );
        }

        if ($login->getLoja()) {
            $retorno = $daoLojas->createRowset(
                $daoLojas->getAdapter()->fetchAll(
                    $daoLojas->getAdapter()->select()
                        ->from($daoLojas->info('name'))
                        ->where('loja_idLoja = ?', $login->getLoja()->getCodigo())
                )
            );
        }

        return $retorno;
    }

    /**
     * Retorna uma �rvore de todos os perfis que o usu�rio pode visualizar
     * @return array
     */
    public function Perfis()
    {
        $this->checarUsuarios();
        return $this->retorno;
    }

    /**
     * Retorna todas as linhas que o usu�rio pode visualizar
     * @return App_Model_Collection of App_Model_Entity_Linha
     */
    public function getLinhas($pedidoGrade = false, $idLinha = false, $verificarBloqueio = true)
    {
        $retorno = $ids = array();
        $objeto = $this->checarUsuarios(true);

        switch (get_class($objeto)) {
            case 'App_Model_Entity_GrupoRede':
                foreach ($objeto->getRedes() as $rede)
                    $ids[] = $rede->getCodigo();
                break;
            default:
                $ids[] = $objeto->getCodigo();
        }

        if (count($ids)) {
            $qtdLinhas = count($ids);
            $ids = implode("','", $ids);
            $daoLinhas = App_Model_DAO_Linhas::getInstance();
            $daoProdutos = App_Model_DAO_Produtos::getInstance();
            $daoPrecos = App_Model_DAO_Precos::getInstance();

            $query = $daoLinhas->getAdapter()->select()
                ->from($daoLinhas->info('name'), null)
                ->joinInner($daoProdutos->info('name'), 'prod_idLinha = lin_idLinha', null)
                ->where("lin_idRede IN ('{$ids}')")
                ->where("lin_status = ?", 1)
                ->where('prod_status = ?', 1);

            if (App_Plugin_Login::getInstance()->getIdentity()->getLoja()) {
                $query->joinLeft($daoPrecos->info('name'), 'prec_idProduto = prod_idProduto')
                    ->where('prec_idTipo = ?', App_Plugin_Login::getInstance()->getIdentity()->getLoja()->getTipo()->getCodigo());

                //verifica bloqueios
                $loja = App_Plugin_Login::getInstance()->getIdentity()->getLoja();
                $endereco = $loja->getEnderecos()->find('loja_end_padrao', 1);
                $UFLoja = $endereco->getEndereco()->getUf();

                $daoBloqueios = App_Model_DAO_Produtos_Bloqueios::getInstance();
                $selectBloqueio = $daoBloqueios->select()
                    ->from($daoBloqueios, 'count(1)')
                    ->where('NOW() BETWEEN prod_bloq_dataInicial AND prod_bloq_dataFinal OR (ISNULL(prod_bloq_dataInicial) AND ISNULL(prod_bloq_dataFinal))')
                    ->where('prod_bloq_uf = ? OR ISNULL(prod_bloq_uf)', $UFLoja)
                    ->where('prod_bloq_idGrupoLoja = ? OR ISNULL(prod_bloq_idGrupoLoja)', $loja->getGrupo()->getCodigo())
                    ->where('prod_bloq_idLoja = ? OR ISNULL(prod_bloq_idLoja)', $loja->getCodigo())
                    ->where('prod_bloq_idProduto = prod_idProduto');

                $query->columns(array(
                    '*',
                    'bloqueado' => "({$selectBloqueio})"
                ));
                $query->having('bloqueado = ?', 0);
            } else {
                $query->columns(array('*'));
                //$query->group('lin_idLinha');
            }

            if ($pedidoGrade) {
                if (!$idLinha && $qtdLinhas > 1)
                    $query->where('lin_compraUnica = ?', 0);
            }

            if ($idLinha)
                $query->where('lin_idLinha = ?', $idLinha);

            // verifica se o cliente est� bloqueado, se sim, s� ir� retornar linhas que n�o est�o bloqueadas.
            // pedido simples
            if (!$pedidoGrade && $verificarBloqueio) {
                $login = App_Plugin_Login::getInstance()->getIdentity();
                if ($login->getLoja() && $login->getLoja()->getTipoBloqueio() == 1)
                    $query->where('lin_bloqueioAtraso = ?', 0);
            }

            $queryFinal = $daoLinhas->getAdapter()->select()->from(array('temp' => $query))->group('lin_idLinha');
            $retorno = $daoLinhas->createRowset($daoLinhas->getAdapter()->fetchAll($queryFinal));
        }
        return $retorno;
    }

    public function getQuoteLinhas($coluna)
    {
        $ids = array();
        $objeto = $this->checarUsuarios(true);
        switch (get_class($objeto)) {
            case 'App_Model_Entity_GrupoRede':
                foreach ($objeto->getRedes() as $rede) {
                    foreach ($rede->getLinhas() as $linha) {
                        if ($linha->getStatus()) {
                            $ids[] = $linha->getCodigo();
                        }
                    }
                }
                break;
            default:
                foreach ($objeto->getLinhas() as $linha) {
                    if ($linha->getStatus()) {
                        $ids[] = $linha->getCodigo();
                    }
                }
        }
        $ids = implode("','", $ids);
        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        return $daoLinhas->getAdapter()->quoteInto("{$coluna} IN ('{$ids}')", '');
    }

    /**
     * Verifica o login do usuario e faz a verifica��o das suas hierarquias
     * @param string $linha Valor 'true' para verificar o grupo de rede e retornar as linhas
     * @return mixed
     */
    protected function checarUsuarios($linha = false)
    {
        $login = App_Plugin_Login::getInstance()->getIdentity();
        if ($linha) {
            $retorno = null;
            if ($login->getGrupoRede())
                $retorno = $login->getGrupoRede();
            if ($login->getRede())
                $retorno = $login->getRede();
            if ($login->getGrupoLoja())
                $retorno = $login->getGrupoLoja()->getRede();
            if ($login->getLoja())
                $retorno = $login->getLoja()->getGrupo()->getRede();
            return $retorno;
        } else {
            if ($login->getGrupoRede())
                $this->recursive($login->getGrupoRede(), 0);
            if ($login->getRede())
                $this->recursive($login->getRede(), 0);
            if ($login->getGrupoLoja())
                $this->recursive($login->getGrupoLoja(), 0);
            if ($login->getLoja())
                $this->recursive($login->getLoja(), 0);
        }
    }

    /**
     * Faz a verifica��o da �rvore de perfis do usu�rio logado
     *
     * @param mixed $inicio Objeto no qual ser� feita a verifica��o
     * @param string $nivel Indica o n�vel em que o n� se encontra na �rvore
     * @param string $pai Texto formatado com o nome de mostrar� do combo
     */
    public function recursive($inicio, $nivel, $pai = null)
    {
        switch (get_class($inicio)) {
            case 'App_Model_Entity_GrupoRede':
                $array = array(
                    'tipo' => 'GR',
                    'id' => $inicio->getCodigo(),
                    'nome' => $inicio->getNome(),
                    'nivel' => $nivel
                );

                $this->retorno[] = $array;
                $this->gruposredes[] = $inicio;

                foreach ($inicio->getRedes() as $rede) {
                    $this->recursive($rede, $nivel + 1, $inicio->getNome());
                }
                break;
            case 'App_Model_Entity_Rede':
                if ($pai != null)
                    $pai = "{$pai} > {$inicio->getNome()}";
                $array = array(
                    'tipo' => 'RE',
                    'id' => $inicio->getCodigo(),
                    'nome' => ($pai != null ? $pai : $inicio->getNome()),
                    'nivel' => $nivel
                );

                $this->retorno[] = $array;
                $this->redes[] = $inicio;

                foreach ($inicio->getGrupos() as $grupo) {
                    $this->recursive($grupo, $nivel + 1, $pai);
                }
                break;
            case 'App_Model_Entity_GrupoLoja':
                if ($pai != null)
                    $pai = "{$pai} > {$inicio->getNome()}";
                $array = array(
                    'tipo' => 'GL',
                    'id' => $inicio->getCodigo(),
                    'nome' => ($pai != null ? $pai : $inicio->getNome()),
                    'nivel' => $nivel
                );
                $this->retorno[] = $array;
                $this->grupolojas[] = $inicio;

                foreach ($inicio->getLojas() as $loja) {
                    $this->recursive($loja, $nivel + 1, $pai);
                }
                break;
            case 'App_Model_Entity_Loja':
                if ($pai != null)
                    $pai = "{$pai} > {$inicio->getNomeCompleto()}";
                $array = array(
                    'tipo' => 'LO',
                    'id' => $inicio->getCodigo(),
                    'nome' => ($pai != null ? $pai : $inicio->getNomeCompleto()),
                    'nivel' => $nivel
                );
                $this->retorno[] = $array;
                $this->lojas[] = $inicio;

                break;
        }
    }

}
