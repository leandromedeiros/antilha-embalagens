<?php

class Zend_View_Helper_MinhaSacola extends Zend_View_Helper_Abstract
{
	private $carrinho = null;
	
	/**
	 * @return App_Model_Entity_Pedido_Items
	 */
	public function MinhaSacola()
	{
		$this->carrinho = new Zend_Session_Namespace('carrinho');		
		if($this->carrinho->pedidos == null) {
			$daoCarrinho = App_Model_DAO_Carrinho::getInstance();
			$selectCarrinho = $daoCarrinho->select()->from($daoCarrinho)
				->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
				->where('ISNULL(car_idPedido)');		
				
			if(App_Plugin_Login::getInstance()->getIdentity()->getGrupo()->getTipo() == 'LO') {
				$selectCarrinho->where('car_idLoja = ?', App_Plugin_Login::getInstance()->getIdentity()->getLoja()->getCodigo());
			}
			
			$carrinho = new Zend_Session_Namespace('carrinho');
			$this->carrinho->pedidos = $daoCarrinho->fetchAll($selectCarrinho);
		}
		return $this;
	}
	
	public function getPagamento() {
		return $this->carrinho->pagamento;
	}
	
	public function qtde() {
		$count = 0;
		if ($this->carrinho->pedidos->count()) {
			foreach ($this->carrinhos->pedidos as $pedido) {
				foreach ($pedido->getItems() as $item) {
					$count += $item->getQuantidade();
				}
			}
			return $count;
		} else {
			return 0;
		}
	}
	
	public function getItens() {
		$lojas = array();	
		if ($this->carrinho->pedidos->count()) {
			foreach ($this->carrinho->pedidos as $pedido) {
				$lojas[$pedido->getLoja()->getCodigo()]['dados'] = $pedido->getLoja(); 
				if(!isset($lojas[$pedido->getLoja()->getCodigo()]['itens'])) $lojas[$pedido->getLoja()->getCodigo()]['itens'] = array();
				foreach ($pedido->getItens() as $item) {
					$lojas[$pedido->getLoja()->getCodigo()]['itens'][$item->getProduto()->getCodigo()] = $item;
				}
			}
		}		
		return $lojas;
	}	
	
	public function getValor($imposto = true) {
		$retorno = 0;	
		if ($this->carrinho->pedidos->count()) {
			foreach ($this->carrinho->pedidos as $pedido) {
				$retorno += $pedido->getValor($imposto);
			}
		}
		return $retorno;		
	}

	public function getValorLoja(App_Model_Entity_Loja $loja) {
		$retorno = 0;	
		if ($this->carrinho->pedidos->count()) {
			foreach ($this->carrinho->pedidos as $pedido) {
				if($loja->getCodigo() == $pedido->getLoja()->getCodigo()) $retorno += $pedido->getValor(true);
			}
		}
		return $retorno;	
	}
	
	public function getValoresPagamento() {
		$retorno = array('totalJuros' => 0, 'totalPedido' => 0, 'totalDesconto' => 0);
		if($this->carrinho->pagamento) {
			foreach ($this->carrinho->pedidos as $pedido) {
				if($pedido->getItens()->count()) {
					$taxa = $pedido->getValor() < $this->carrinho->pagamento->getFatMinimo() ? $this->carrinho->pagamento->getTaxaServico() : 0;
					$valorPedido = 0;
					foreach ($pedido->getItens() as $item) {
						$ipi = $icms = 0;
						if($item->getProduto()->getAliquotaIpi() > 0) {
							$ipi = ($item->getPrecoTotal()*$item->getProduto()->getAliquotaIpi())/100;
						}
					
						if($pedido->getLoja()->getIcms() > 0 && strtolower($item->getProduto()->getCcf()) == 'x') {
							$icms = ($item->getPrecoTotal()*$pedido->getLoja()->getIcms())/100;
						}
							
						$valorPedido += $item->getPrecoTotal()+$ipi+$icms;
					}
					
		            if($this->carrinho->pagamento->getJuros() > 0) {
		            	$retorno['totalJuros'] += ($valorPedido * ($this->carrinho->pagamento->getJuros() / 100));
		            	$retorno['totalPedido'] += $valorPedido + ($valorPedido * ($this->carrinho->pagamento->getJuros() / 100));
		            } else {
		            	$desconto = ($valorPedido * (($this->carrinho->pagamento->getDesconto() > 0 ? $this->carrinho->pagamento->getDesconto()/100 : 0)));
		            	$retorno['totalDesconto'] += $desconto;
		            	$retorno['totalPedido'] += ($valorPedido - $desconto);
		            }
		            $retorno['totalPedido'] += $taxa;
				}
			}
		}
		return $retorno;
	}
}