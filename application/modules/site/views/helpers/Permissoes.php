<?php

class Zend_View_Helper_Permissoes extends Zend_View_Helper_Abstract
{
	/**
	 * Retorna os dados conforme as suas permiss�es
	 * Pode retornar: banner, comunicados, not�cias, v�deos, pesquisas e downloads
	 *
	 * @return Zend_View_Helper_Permissoes
	 */
	public function Permissoes()
	{
		return $this;
	}
	
	/**
	 * @return Zend_Db_Select
	 */
	public function selectBanners() {
		return $this->select('b');
	}
	
	/**
	 * @return Zend_Db_Select
	 */
	public function selectComunicados() {
		return $this->select('c');
	}
	
	/**
	 * @return Zend_Db_Select
	 */
	public function selectEmbalagens() {
		return $this->select('e');
	}
	
	/**
	 * @return Zend_Db_Select
	 */
	public function selectNoticias() {
		return $this->select('n');
	}
	
	/**
	 * @return Zend_Db_Select
	 */
	public function selectVideos() {
		return $this->select('v');
	}
	
	/**
	 * @return Zend_Db_Select
	 */
	public function selectPesquisas() {
		return $this->select('p');
	}
	
	/**
	 * @return Zend_Db_Select
	 */
	public function selectDownloads() {
		return $this->select('d');
	}
	
	/**
	 * @param $tipo Pode ser: (b=>banner, c=>comunicados, n=>not�cias, v=>v�deos, p=>pesquisas e d=>downloads)
	 * @return Zend_Db_Select
	 */
	public function select($tipo) {
		switch (strtolower($tipo)) {
			case 'b':
				$daoPrincipal = App_Model_DAO_Banners::getInstance();
				$daoPrincipalPermissoes = App_Model_DAO_Banners_Permissoes::getInstance();
				$daoPrincipalEstados = App_Model_DAO_Banners_Estados::getInstance();
				$prefix1 = 'ban_';
				$prefix2 = 'ban_perm_';
				$prefix3 = 'ban_est_';
				$id = "{$prefix1}idBanner";
				$idPerm = "{$prefix2}idBanner";
				$idEst = "{$prefix3}idBanner";
				$vperm = array($idPerm, "{$prefix2}idGrupoRede", "{$prefix2}idRede", "{$prefix2}idGrupoLoja", "{$prefix2}idLoja", "{$prefix2}idLinha");
			break;
			case 'c':
				$daoPrincipal = App_Model_DAO_Comunicados::getInstance();
				$daoPrincipalPermissoes = App_Model_DAO_Comunicados_Permissoes::getInstance();
				$daoPrincipalEstados = App_Model_DAO_Comunicados_Estados::getInstance();
				$prefix1 = 'com_';
				$prefix2 = 'com_perm_';
				$prefix3 = 'com_est_';
				$id = "{$prefix1}idComunicado";
				$idPerm = "{$prefix2}idComunicado";
				$idEst = "{$prefix3}idComunicado";
				$vperm = array($idPerm, "{$prefix2}idGrupoRede", "{$prefix2}idRede", "{$prefix2}idGrupoLoja", "{$prefix2}idLoja", "{$prefix2}idLinha");
			break;
			case 'n':
				$daoPrincipal = App_Model_DAO_Noticias::getInstance();
				$daoPrincipalPermissoes = App_Model_DAO_Noticias_Permissoes::getInstance();
				$daoPrincipalEstados = App_Model_DAO_Noticias_Estados::getInstance();
				$prefix1 = 'not_';
				$prefix2 = 'not_perm_';
				$prefix3 = 'not_est_';
				$id = "{$prefix1}idNoticia";
				$idPerm = "{$prefix2}idNoticia";
				$idEst = "{$prefix3}idNoticia";
				$vperm = array($idPerm, "{$prefix2}idGrupoRede", "{$prefix2}idRede", "{$prefix2}idGrupoLoja", "{$prefix2}idLoja", "{$prefix2}idLinha");
			break;
			case 'v':
				$daoPrincipal = App_Model_DAO_Videos::getInstance();
				$daoPrincipalPermissoes = App_Model_DAO_Videos_Permissoes::getInstance();
				$daoPrincipalEstados = App_Model_DAO_Videos_Estados::getInstance();
				$prefix1 = 'vid_';
				$prefix2 = 'vid_perm_';
				$prefix3 = 'vid_est_';
				$id = "{$prefix1}idVideo";
				$idPerm = "{$prefix2}idVideo";
				$idEst = "{$prefix3}idVideo";
				$vperm = array($idPerm, "{$prefix2}idGrupoRede", "{$prefix2}idRede", "{$prefix2}idGrupoLoja", "{$prefix2}idLoja", "{$prefix2}idLinha");
			break;
			case 'p':
				$daoPrincipal = App_Model_DAO_Pesquisas::getInstance();
				$daoPrincipalPermissoes = App_Model_DAO_Pesquisas_Permissoes::getInstance();
				$daoPrincipalEstados = App_Model_DAO_Pesquisas_Estados::getInstance();
				$prefix1 = 'pes_';
				$prefix2 = 'pes_perm_';
				$prefix3 = 'pes_est_';
				$id = "{$prefix1}idPesquisa";
				$idPerm = "{$prefix2}idPesquisa";
				$idEst = "{$prefix3}idPesquisa";
				$vperm = array($idPerm, "{$prefix2}idGrupoRede", "{$prefix2}idRede", "{$prefix2}idGrupoLoja", "{$prefix2}idLoja", "{$prefix2}idLinha");
			break;
			case 'd':
				$daoPrincipal = App_Model_DAO_Downloads::getInstance();
				$daoPrincipalPermissoes = App_Model_DAO_Downloads_Permissoes::getInstance();
				$daoPrincipalEstados = App_Model_DAO_Downloads_Estados::getInstance();
				$prefix1 = 'down_';
				$prefix2 = 'down_perm_';
				$prefix3 = 'down_est_';
				$id = "{$prefix1}idDownload";
				$idPerm = "{$prefix2}idDownload";
				$idEst = "{$prefix3}idDownload";
				$vperm = array($idPerm, "{$prefix2}idGrupoRede", "{$prefix2}idRede", "{$prefix2}idGrupoLoja", "{$prefix2}idLoja", "{$prefix2}idLinha");
			break;
			
			case 'e':
				$daoPrincipal = App_Model_DAO_Embalagens::getInstance();
				$daoPrincipalPermissoes = App_Model_DAO_Embalagens_Permissoes::getInstance();
				$daoPrincipalEstados = App_Model_DAO_Embalagens_Estados::getInstance();
				$prefix1 = 'emb_';
				$prefix2 = 'emb_perm_';
				$prefix3 = 'emb_est_';
				$id = "{$prefix1}idEmbalagem";
				$idPerm = "{$prefix2}idEmbalagem";
				$idEst = "{$prefix3}idEmbalagem";
				$vperm = array($idPerm, "{$prefix2}idGrupoRede", "{$prefix2}idRede", "{$prefix2}idGrupoLoja", "{$prefix2}idLoja", "{$prefix2}idLinha");
			break;
		}		
		
		$login = App_Plugin_Login::getInstance()->getIdentity();
		$selects = array();
		$idlinhas = array();
		
		$teste = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
		foreach ($teste->view->Combo()->getLinhas() as $linha) {
			$idlinhas[] = $linha->getCodigo();
		}		
					
		$subqueryLinha = '';
		if(count($idlinhas)) {
			$ids = implode("','", $idlinhas);
			$subqueryLinha = ") OR (
		        {$prefix2}idLinha IN ('{$ids}') AND (
		        	ISNULL({$prefix2}idRede) AND 
		        	ISNULL({$prefix2}idGrupoLoja) AND 
		        	ISNULL({$prefix2}idLoja) AND 
		        	ISNULL({$prefix2}idGrupoRede)
		        )
		    ";
		}
		
		$selectLoja = $daoPrincipal->getAdapter()->select()->from($daoPrincipal->info('name'))
			->joinLeft($daoPrincipalPermissoes->info('name'), "{$id} = {$idPerm}")
			->joinLeft($daoPrincipalEstados->info('name'), "{$id} = {$idEst}", null)			
			->group($id);
			
		$selectGrupoLoja = $daoPrincipal->getAdapter()->select()->from($daoPrincipal->info('name'))
			->joinLeft($daoPrincipalPermissoes->info('name'), "{$id} = {$idPerm}")		
			->group($id);		

		$selectRede = $daoPrincipal->getAdapter()->select()->from($daoPrincipal->info('name'))
			->joinLeft($daoPrincipalPermissoes->info('name'), "{$id} = {$idPerm}")					
			->group($id);
			
		$selectGrupoRede = $daoPrincipal->getAdapter()->select()->from($daoPrincipal->info('name'))
			->joinLeft($daoPrincipalPermissoes->info('name'), "{$id} = {$idPerm}")					
			->group($id);
			
		$grupoRede = false;						
		switch ($login->getGrupo()->getTipo()) {
			case 'LO':
				$selects[] = $selectLoja->where("(
					ISNULL({$prefix2}idLoja) AND 
					ISNULL({$prefix2}idRede) AND 
					ISNULL({$prefix2}idGrupoLoja) AND 
					ISNULL({$prefix2}idGrupoRede) AND 
					ISNULL({$prefix2}idLinha)
				) OR (
					{$prefix2}idLoja = ? AND (
						ISNULL({$prefix2}idRede) AND 
						ISNULL({$prefix2}idGrupoLoja) AND 
						ISNULL({$prefix2}idGrupoRede) AND 
						ISNULL({$prefix2}idLinha)
					)
				{$subqueryLinha}
				)", $login->getLoja()->getCodigo())
				->where("ISNULL({$prefix3}uf) OR {$prefix3}uf = ?", $login->getLoja()->getEnderecoPadrao()->getEndereco()->getUf());					
					
				$selects[] = $selectGrupoLoja->joinLeft($daoPrincipalEstados->info('name'), "{$id} = {$idEst}", null)
					->where("{$prefix2}idGrupoLoja = ? AND (
						ISNULL({$prefix2}idGrupoRede) AND 
						ISNULL({$prefix2}idRede) AND 
						ISNULL({$prefix2}idLoja) AND 
						ISNULL({$prefix2}idLinha)
					)", $login->getLoja()->getGrupo()->getCodigo())	
					->where("ISNULL({$prefix3}uf) OR {$prefix3}uf = ?", $login->getLoja()->getEnderecoPadrao()->getEndereco()->getUf());

				$selects[] = $selectRede->joinLeft($daoPrincipalEstados->info('name'), "{$id} = {$idEst}", null)			
					->where("{$prefix2}idRede = ? AND (
						ISNULL({$prefix2}idGrupoRede) AND 
						ISNULL({$prefix2}idGrupoLoja) AND 
						ISNULL({$prefix2}idLoja) AND 
						ISNULL({$prefix2}idLinha)					
					)", $login->getLoja()->getGrupo()->getRede()->getCodigo())	
					->where("ISNULL({$prefix3}uf) OR {$prefix3}uf = ?", $login->getLoja()->getEnderecoPadrao()->getEndereco()->getUf());
					
				$selects[] = $selectGrupoRede->joinLeft($daoPrincipalEstados->info('name'), "{$id} = {$idEst}", null)			
					->where("{$prefix2}idGrupoRede = ? AND (
						ISNULL({$prefix2}idRede) AND 
						ISNULL({$prefix2}idGrupoLoja) AND 
						ISNULL({$prefix2}idLoja) AND 
						ISNULL({$prefix2}idLinha)
					)", $login->getLoja()->getGrupo()->getRede()->getGrupo()->getCodigo())						
					->where("ISNULL({$prefix3}uf) OR {$prefix3}uf = ?", $login->getLoja()->getEnderecoPadrao()->getEndereco()->getUf());					
			break;
			case 'GL':				
				$selects[] = $selectGrupoLoja->where("(
					ISNULL({$prefix2}idLoja) AND 
					ISNULL({$prefix2}idRede) AND 
					ISNULL({$prefix2}idGrupoLoja) AND 
					ISNULL({$prefix2}idGrupoRede) AND 
					ISNULL({$prefix2}idLinha)
				) OR (
					{$prefix2}idGrupoLoja = ? AND (
						ISNULL({$prefix2}idGrupoRede) AND 
						ISNULL({$prefix2}idRede) AND 
						ISNULL({$prefix2}idLoja) AND 
						ISNULL({$prefix2}idLinha)
					)
				{$subqueryLinha}
				)", $login->getGrupoLoja()->getCodigo());

				$selects[] = $selectRede->where("{$prefix2}idRede = ? AND (
					ISNULL({$prefix2}idGrupoRede) AND 
					ISNULL({$prefix2}idGrupoLoja) AND 
					ISNULL({$prefix2}idLoja) AND 
					ISNULL({$prefix2}idLinha)
				)", $login->getGrupoLoja()->getRede()->getCodigo());
					
				$selects[] = $selectGrupoRede->where("{$prefix2}idGrupoRede = ? AND (
					ISNULL({$prefix2}idRede) AND 
					ISNULL({$prefix2}idGrupoLoja) AND 
					ISNULL({$prefix2}idLoja) AND 
					ISNULL({$prefix2}idLinha)
				)", $login->getGrupoLoja()->getRede()->getGrupo()->getCodigo());					
			break;
			case 'RE':	
				$selects[] = $selectRede->where("(
					ISNULL({$prefix2}idLoja) AND 
					ISNULL({$prefix2}idRede) AND 
					ISNULL({$prefix2}idGrupoLoja) AND 
					ISNULL({$prefix2}idGrupoRede) AND 
					ISNULL({$prefix2}idLinha)
				) OR (
					{$prefix2}idRede = ? AND (
						ISNULL({$prefix2}idGrupoRede) AND 
						ISNULL({$prefix2}idGrupoLoja) AND 
						ISNULL({$prefix2}idLoja) AND 
						ISNULL({$prefix2}idLinha)
					)
				{$subqueryLinha}
				)", $login->getRede()->getCodigo());					
					
				$selects[] = $selectGrupoRede->where("{$prefix2}idGrupoRede = ? AND (
					ISNULL({$prefix2}idRede) AND 
					ISNULL({$prefix2}idGrupoLoja) AND 
					ISNULL({$prefix2}idLoja) AND 
					ISNULL({$prefix2}idLinha)
				)", $login->getRede()->getGrupo()->getCodigo());
			break;
			case 'GR':	
				$grupoRede = true;							
				$selectGr = $selectGrupoRede->where("(
					ISNULL({$prefix2}idLoja) AND 
					ISNULL({$prefix2}idRede) AND 
					ISNULL({$prefix2}idGrupoLoja) AND 
					ISNULL({$prefix2}idGrupoRede) AND 
					ISNULL({$prefix2}idLinha)
				) OR (
					{$prefix2}idGrupoRede = ? AND (
						ISNULL({$prefix2}idRede) AND 
						ISNULL({$prefix2}idGrupoLoja) AND 
						ISNULL({$prefix2}idLoja) AND 
						ISNULL({$prefix2}idLinha)
					)
				{$subqueryLinha}
				)", $login->getGrupoRede()->getCodigo());				
			break;			
		}
		$select = $grupoRede ? $selectGr : $daoPrincipal->getAdapter()->select()->from(array('temp' => $daoPrincipal->getAdapter()->select()->union($selects)));
		return $select;
	}
}