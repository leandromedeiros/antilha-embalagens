<?php

class GerenciamentoDeUsuariosController extends Zend_Controller_Action
{	
	public function indexAction() {
		
		$this->view->HeadTitle('Gerenciamento de Usu�rios');		
		$this->view->HeadLink()->appendStylesheet('styles/desktop/usuario-gerenciamento.css', 'screen');
		
		$login = App_Plugin_Login::getInstance()->getIdentity();
		$login->podeCriarPedidosSemAprovacao();
		
		$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
		$daoGruposRede = App_Model_DAO_GrupoRedes::getInstance();
		$daoRedes = App_Model_DAO_Redes::getInstance();
		$daoGruposLoja = App_Model_DAO_GrupoLojas::getInstance();
		$daoLojas = App_Model_DAO_Lojas::getInstance();
		
		$select = $daoUsuarios->select()
			->from($daoUsuarios)
			->where('usr_idUsuario <> ?', $login->getCodigo())
			->where($this->view->Usuario()->quoteIntoUsuarios('usr_idUsuario'));
		
		// Pagina��o
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
		$paginator = Zend_Paginator::factory($select);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
		$this->view->paginator = $paginator;
	}
	
	public function cadastroAction($alterarPermissao = true) {
		
		$this->view->HeadTitle('Gerenciamento de Usu�rios')->HeadTitle('Cadastro de Usu�rios');
		$this->view->HeadLink()->appendStylesheet('styles/desktop/usuario-cadastro.css', 'screen');
		
		$this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
		$this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');
		$this->view->HeadScript()->appendFile('scripts/messages_ptbr.js');
		$this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');
		
		$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
		$insert = true;
		if($idUsuario = $this->getRequest()->getParam('usuario', false)) {
			$insert = false;
			$usuario = $daoUsuarios->fetchRow(
				$daoUsuarios->select()
					->from($daoUsuarios)
					->where('usr_idUsuario = ?', $idUsuario)
			);
			if($usuario == null) {
				$this->view->success = false;
				$this->view->message = "Usuario n�o encontrado.";	
			}
		} else {			
			$usuario = App_Model_DAO_Sistema_Usuarios::getInstance()->createRow();
		}
		
		if ($this->getRequest()->isPost()) {
			
			$daoGrupos = App_Model_DAO_GrupoRedes::getInstance();
			$daoPerfis = App_Model_DAO_Sistema_Perfis::getInstance();
			
			$usuario->setPerfil(
					$daoPerfis->fetchRow(
						$daoPerfis->select()->from($daoPerfis)
							->where('per_idPerfil = ?', 2)
					)
				)					
				->setNome($this->getRequest()->getParam('usr_nome'))
				->setLogin($this->getRequest()->getParam('usr_login'))
				->setEmail($this->getRequest()->getParam('usr_email'))
				->setStatus(true);				

			try {
				
				if($insert) {
				
					$perfil = null;
					$usr_perfil = $this->getRequest()->getParam('usr_perfil', '');
					if($usr_perfil != '') {
						$vpefil = explode('|', $usr_perfil);
						switch ($vpefil[0]) {
							case 'GR':
								$daoGrupos = App_Model_DAO_GrupoRedes::getInstance();
								$perfil = $daoGrupos->fetchRow(
									$daoGrupos->select()->from($daoGrupos)->where('grp_rede_idGrupo = ?', $vpefil[1])
								);
								$usuario->setGrupoRede($perfil);
							break;
							case 'RE':
								$daoRedes = App_Model_DAO_Redes::getInstance();
								$perfil = $daoRedes->fetchRow(
									$daoRedes->select()->from($daoRedes)->where('rede_idRede = ?', $vpefil[1])
								);
								$usuario->setRede($perfil);
							break;
							case 'GL':
								$daoGrupos = App_Model_DAO_GrupoLojas::getInstance();
								$perfil = $daoGrupos->fetchRow(
									$daoGrupos->select()->from($daoGrupos)->where('grp_loja_idGrupo = ?', $vpefil[1])
								);
								$usuario->setGrupoLoja($perfil);
							break;
							case 'LO':
								$daoLojas = App_Model_DAO_Lojas::getInstance();
								$perfil = $daoLojas->fetchRow(
									$daoLojas->select()->from($daoLojas)->where('loja_idLoja = ?', $vpefil[1])
								);
								$usuario->setLoja($perfil);
							break;
						}
					}
					
					if(!$perfil) throw new App_Validate_Exception('Por favor, verifique os dados de acesso', null, array('usr_perfil' => 'Perfil n�o encontrado'));
				
					$usuario->setSenha($this->getRequest()->getParam('usr_senha'));
					if ($usuario->getSenha() != $this->getRequest()->getParam('usr_confirmacao')) {
						throw new App_Validate_Exception('Por favor, verifique os dados de acesso', null, array('usr_confirmacao' => 'Confirma��o de senha inv�lida'));
					}
					$usuario->setSenha(md5($usuario->getSenha()));
				} else {
					if ($this->getRequest()->getParam('usr_senha', false) && $this->getRequest()->getParam('usr_confirmacao', false)) {						
						if ($this->getRequest()->getParam('usr_senha') != $this->getRequest()->getParam('usr_confirmacao')) {
							throw new App_Validate_Exception('Por favor, verifique os dados de acesso', null, array('usr_confirmacao' => 'Confirma��o de senha inv�lida'));
						}
						$usuario->setSenha(md5($this->getRequest()->getParam('usr_senha')));
					}
					if($alterarPermissao) $usuario->getPermissoes()->offsetRemoveAll();
				}
				
				if($alterarPermissao) {
					$daoAcoes = App_Model_DAO_Sistema_Acoes::getInstance();
					$acoes = $daoAcoes->fetchAll(
						$daoAcoes->select()->from($daoAcoes)
							->where('FIND_IN_SET(?, mod_acao_acesso)', $usuario->getGrupo()->getTipo())						
					);
					foreach ($acoes as $acao) {
						$usuario->getPermissoes()->offsetAdd($acao);
					}
					
					//salvando permiss�o especial de pedidos
					if(($usuario->getGrupo()->getTipo() == 'GL' || $usuario->getGrupo()->getTipo() == 'LO') && $this->getRequest()->getParam('permissao', false) != false) {
						$acaoEspecial = $daoAcoes->find($this->getRequest()->getParam('permissao', false))->current();
						if($acaoEspecial) $usuario->getPermissoes()->offsetAdd($acaoEspecial);
					}
				}
				
				$usuario->save();
				$this->view->success = true;
				
				// Autentica novamente para atualizar os dados da sess�o se caso for os dados do usu�rio logado 
				if($usuario->getCodigo() == App_Plugin_Login::getInstance()->getIdentity()->getCodigo()) {
					$this->view->success = false;
					App_Plugin_Login::getInstance()->authenticate($usuario->getLogin(), $usuario->getSenha());
				}
				
				$this->view->message = "Cadastro ".($insert ? 'inserido' : 'atualizado')." com sucesso.";				
			} catch (App_Validate_Exception $e) {
				$fieldErrors = array();
				foreach ($e->getFields() as $field => $message) {
					$fieldErrors[$field] = $message;
				}
				App_Funcoes_UTF8::encode($fieldErrors);
				$this->view->success = false;
				$this->view->fieldErrors = Zend_Json::encode($fieldErrors);
				$this->view->message = count($fieldErrors) ? 'Por favor, verifique o(s) campo(s) marcado(s) em vermelho.' : $e->getMessage();
			} 
		}
		$this->view->alterarPermissao = $alterarPermissao;
		$this->view->insert = $insert;
		$this->view->usuario = $usuario;
	}

	public function alterarDadosAction()
	{
		$this->view->alterar = true;	
		$this->cadastroAction(false);
		$this->render('cadastro');
	}

	public function excluirAction()
	{
		Zend_Layout::getMvcInstance()->disableLayout();
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
		
		$retorno = array('success' => false, 'message' => null);
		$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
		$daoPedidos = App_Model_DAO_Pedidos::getInstance();
		try {
			$pedidos = $daoPedidos->fetchAll(
				$daoPedidos->select()->from($daoPedidos)->where('ped_idUsuario = ?', $this->getRequest()->getParam('usuario'))
			);
			
			if ($pedidos->count()) throw new Exception('N�o � poss�vel excluir o usu�rio, pois existem pedidos vinculados.');
			
			$usuario = $daoUsuarios->fetchRow(
				$daoUsuarios->select()->where('usr_idUsuario = ?', $this->getRequest()->getParam('usuario'))
			);
			if (null == $usuario) {
				throw new Exception('O usu�rio solicitado n�o foi encontrado.');
			}
			try {
				$nome = $usuario->getNome();
				$usuario->delete();

				$retorno['success'] = true;
				$retorno['message'] = sprintf('Usu�rio <b>%s</b> removido com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o usu�rio. Verifique se exitem dados vinculados a ele.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoUsuarios, $usuario);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}
	
	public function dadosDeAcessoAction()
	{	
		$this->view->HeadTitle('Gerenciamento de Usu�rios')->HeadTitle('Dados de Acesso');
		$validacao = new Zend_Session_Namespace('validacao');
		if (!isset($validacao->loja) && !($validacao->loja instanceof App_Model_Entity_Loja)) {
			$this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base)->sendResponse();
		}
		
		$this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
		$this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');
		$this->view->HeadScript()->appendFile('scripts/messages_ptbr.js');
		$this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');
		$this->view->HeadLink()->appendStylesheet('styles/desktop/usuario-cadastro.css', 'screen');
						
		$usuario = App_Model_DAO_Sistema_Usuarios::getInstance()->createRow();
		if ($this->getRequest()->isPost()) {
			
			$daoGrupos = App_Model_DAO_GrupoLojas::getInstance();
			$daoPerfis = App_Model_DAO_Sistema_Perfis::getInstance();
			
			$usuario->setPerfil(
					$daoPerfis->fetchRow(
						$daoPerfis->select()->from($daoPerfis)
							->where('per_idPerfil = ?', 2)
					)
				)
				->setNome($this->getRequest()->getParam('usr_nome'))
				->setLogin($this->getRequest()->getParam('usr_login'))
				->setSenha($this->getRequest()->getParam('usr_senha'))
				->setEmail($this->getRequest()->getParam('usr_email'))
				->setStatus(true);			

			if($validacao->loja->getGrupo()->getTipoPedido() == 'G') {
				$usuario->setGrupoLoja(
					$daoGrupos->fetchRow(
						$daoGrupos->select()->from($daoGrupos)
							->where('grp_loja_idGrupo = ?', $validacao->loja->getGrupo()->getCodigo())
						)
					);
			} else {
				$usuario->setLoja($validacao->loja);
			}		

			try {
				
				if ($usuario->getSenha() != $this->getRequest()->getParam('usr_confirmacao')) {
					throw new App_Validate_Exception('Por favor, verifique os dados de acesso', null, array('usr_confirmacao' => 'Confirma��o de senha inv�lida'));
				}
				$usuario->setSenha(md5($usuario->getSenha()));
				
				$daoAcoes = App_Model_DAO_Sistema_Acoes::getInstance();
				$acoes = $daoAcoes->fetchAll(
					$daoAcoes->select()->from($daoAcoes)
						->where('FIND_IN_SET(?, mod_acao_acesso)', $usuario->getGrupo()->getTipo())						
				);
				foreach ($acoes as $acao) {
					$usuario->getPermissoes()->offsetAdd($acao);
				}
				
				// Se caso for Grupo loja, permiss�o para aprovar pedidos c/ qtde
				if($usuario->getGrupo()->getTipo() == 'GL') {
					$usuario->getPermissoes()->offsetAdd(
						$daoAcoes->fetchRow(
							$daoAcoes->select()->from($daoAcoes)->where('mod_acao_idAcao = ?', 128)
						)
					);
				}
				$usuario->save();
				
				// Efetua o login do usu�rio
				App_Plugin_Login::getInstance()->authenticate($usuario->getLogin(), $usuario->getSenha());
				Zend_Session::namespaceUnset('validacao');
				
				$this->view->success = true;
				$this->view->message = "Cadastro efetuado com sucesso.";				
			} catch (App_Validate_Exception $e) {
				$fieldErrors = array();
				foreach ($e->getFields() as $field => $message) {
					$fieldErrors[$field] = $message;
				}
				App_Funcoes_UTF8::encode($fieldErrors);
				$this->view->success = false;
				$this->view->fieldErrors = Zend_Json::encode($fieldErrors);
				$this->view->message = count($fieldErrors) ? 'Por favor, verifique o(s) campo(s) marcado(s) em vermelho.' : $e->getMessage();
			} 
		}
		$this->view->validacao = $validacao->loja;
		$this->view->usuario = $usuario;
	}

	public function dadosDaLojaAction()
	{
		$this->view->HeadTitle('Gerenciamento de Usu�rios')->HeadTitle('Dados da Loja');
		$this->view->HeadLink()->appendStylesheet('styles/desktop/dados-cadastrais.css', 'screen');
		
		$this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
		$this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');
		$this->view->HeadScript()->appendFile('scripts/messages_ptbr.js');
		$this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');
		$this->view->HeadLink()->appendStylesheet('styles/desktop/usuario-cadastro.css', 'screen');

		$this->view->form = true;
		$login = App_Plugin_Login::getInstance();
		if($login->getIdentity()->getGrupo()->getTipo() == 'LO') {
			$this->view->combo = true;
			$loja = $login->getIdentity()->getLoja();			
		} else {
			$this->view->combo = false;
			$loja = App_Model_DAO_Lojas::getInstance()->createRow();
			if( ($idLoja = $this->getRequest()->getParam('loja', false)) != false) {
				$daoLojas = App_Model_DAO_Lojas::getInstance();
				$loja = $daoLojas->fetchRow(
					$daoLojas->select()->from($daoLojas)
						->where('loja_idLoja = ?', $idLoja)
				);
			} else {
				$this->view->form = false;
			}
		}
		
		if ($this->getRequest()->isPost()) {
			try {
				$loja->setNomeCompleto($this->getRequest()->getParam('loja_nomeCompleto'))
					->setNomeReduzido($this->getRequest()->getParam('loja_nomeReduzido'))
					->setCnpj($this->getRequest()->getParam('loja_cnpj'))
					->setIe($this->getRequest()->getParam('loja_ie'))
					->setEmail($this->getRequest()->getParam('loja_email'));	

				$endereco =& $loja->getEnderecos()->find('loja_end_idEndereco', $this->getRequest()->getParam('loja_end_idEndereco'));
				if($endereco != null) {
					$endereco->setPadrao(1)						
						->getEndereco()
							->setCep($this->getRequest()->getParam('loja_end_cep'))
							->setLogradouro($this->getRequest()->getParam('loja_end_logradouro'))
							->setNumero($this->getRequest()->getParam('loja_end_numero'))
							->setComplemento($this->getRequest()->getParam('loja_end_complemento'))
							->setCidade($this->getRequest()->getParam('loja_end_cidade'))
							->setUF($this->getRequest()->getParam('loja_end_uf'))
							->setBairro($this->getRequest()->getParam('loja_end_bairro'));	
				}	
				
				//$loja->save();	
				//$loja->refresh();

				//Envia o email com os dados
				$template = new Zend_View();
				$template->setBasePath(APPLICATION_PATH.DIRECTORY_SEPARATOR.'views');
				$template->loja = $loja;
				$template->endereco = $endereco;
				
				try {
					// Email
					$daoConfiguracao = App_Model_DAO_Sistema_Configuracoes::getInstance();
					$config = $daoConfiguracao->fetchRow($daoConfiguracao->select());
					$config = $config->toArray();
					unset($daoConfiguracao);
					
					$mailTransport = new App_Model_Email();
					$mail = new Zend_Mail('ISO-8859-1');
					$mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('configInfo')->nomeRemetente);
					$mail->addTo($config['conf_emailLoja']);
					
					$mail->setSubject(Zend_Registry::get('config')->project . ' - Altera��o de Dados da Loja');
					$mail->setBodyHtml($template->render('emails/dados-loja.phtml'));
					$mail->send($mailTransport->getFormaEnvio());

					$this->view->success = true;
					$this->view->message = "Os dados foram enviados com sucesso. As informa��es da loja ser�o atualizadas assim que os dados forem validados.";

				} catch (Exception $e) {
					throw new Exception("N�o foi poss�vel enviar sua mensagem.");
				}

			} catch (App_Validate_Exception $e) {
				$fieldErrors = array();
				foreach ($e->getFields() as $field => $message) {
					$fieldErrors[$field] = $message;
				}
				App_Funcoes_UTF8::encode($fieldErrors);
				$this->view->success = false;
				$this->view->fieldErrors = Zend_Json::encode($fieldErrors);
				$this->view->message = count($fieldErrors) ? 'Por favor, verifique o(s) campo(s) marcado(s) em vermelho.' : $e->getMessage();
			} catch (Exception $e) {
				$this->view->success = false;
				$this->view->message = $e->getMessage();
			}
		}
		$this->view->loja = $loja;
	}
}