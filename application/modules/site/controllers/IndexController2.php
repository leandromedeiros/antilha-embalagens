<?php

class IndexController extends Zend_Controller_Action
{

    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->view->HeadTitle('�rea Restrita');

        $this->view->HeadScript()->appendFile('scripts/jquery.blockUI.js');
        $this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
        $this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');
        $this->view->HeadScript()->appendFile('scripts/messages_ptbr.js');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');

        $validacao = new Zend_Session_Namespace('validacao');
        if (isset($validacao->loja) && ($validacao->loja instanceof App_Model_Entity_Loja)) {
            Zend_Session::namespaceUnset('validacao');
        }
        $login = App_Plugin_Login::getInstance();
        if ($login->hasIdentity()) {
            $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . '/home')->sendResponse();
        }

        // Esqueci minha senha
        if (false !== ($usrLogin = $this->getRequest()->getParam('lost-password', false))) {
            $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
            $usuario = $daoUsuarios->fetchRow(
                $daoUsuarios->select()->where('usr_login = ?', $usrLogin)
            );
            if (null != $usuario) {
                // Envia o email
                $template = new Zend_View();
                $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

                try {
                    $daoUsuarios->getAdapter()->beginTransaction();
                    $novaSenha = rand(1000, 100000);
                    $usuario->setSenha(md5($novaSenha));
                    $usuario->save();

                    $template->nome = $usuario->getNome();
                    $template->email = $usuario->getEmail();
                    $template->login = $usuario->getLogin();
                    $template->senha = $novaSenha;

                    $mailTransport = new App_Model_Email();
                    $mail = new Zend_Mail('ISO-8859-1');
                    $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('configInfo')->nomeRemetente);
                    $mail->setReplyTo($template->email, $template->nome);
                    $mail->addTo($template->email);
                    $mail->setSubject(Zend_Registry::get('config')->project . ' - Esqueci minha senha');
                    $mail->setBodyHtml($template->render('emails/esqueci-senha.phtml'));
                    $mail->send($mailTransport->getFormaEnvio());

                    $this->view->message = "Um e-mail foi enviado para <b>{$template->email}</b> contendo os dados de acesso.";
                    $daoUsuarios->getAdapter()->commit();
                } catch (Exception $e) {
                    $daoUsuarios->getAdapter()->rollBack();
                    $this->view->message = "N�o foi poss�vel enviar o e-mail para <b>{$template->email}</b>. " . $e->getMessage();
                }
            } else {
                $this->view->message = "Nenhum usu�rio encontrado com o login <b>{$usrLogin}</b>.";
            }
            unset($daoUsuarios, $usuario);
        }
    }

    public function loginAction()
    {
        // Efetuar login
        $login = App_Plugin_Login::getInstance();
        if ($this->getRequest()->isPost()) {
            if ($login->authenticate($this->getRequest()->getParam('login'), md5($this->getRequest()->getParam('senha')))) {
                $url = Zend_Registry::get('config')->paths->site->base;
                if ($this->getRequest()->getParam('redirect')) {
                    $url = urldecode($this->getRequest()->getParam('redirect'));
                }
                $this->getResponse()->setRedirect($url)->sendResponse();
                exit();
            } else {
                $this->view->message = "Login ou senha inv�lidos";
            }
        }
        $this->view->redirect = $this->getRequest()->getParam('redirect');

        $this->_forward('index');
    }

    public function acessoAdministradorAction()
    {
        $login = App_Plugin_Login::getInstance();
        $this->view->HeadLink()->appendStylesheet('styles/desktop/meus-pedidos.css', 'screen');

        if (!$login->hasMasterUser()) {
            $this->getResponse()->setHttpResponseCode(301)
                ->setRedirect(Zend_Registry::get('config')->paths->site->base)
                ->sendResponse();
        }

        if ($this->getRequest()->isPost() && $this->getRequest()->getParam('idUser', false) != false) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $this->getFrontController()->setParam('noViewRenderer', true);

            $retorno = array(
                'success' => true,
                'message' => ''
            );

            try {
                $login->setIdentity(App_Model_DAO_Sistema_Usuarios::getInstance()->find($this->getRequest()->getParam('idUser', false))->current());
            } catch (Exception $e) {
                $retorno = array(
                    'success' => false,
                    'message' => 'N�o foi poss�vel realizar a opera��o'
                );
            }

            App_Funcoes_UTF8::encode($retorno);
            echo Zend_Json::encode($retorno);
            exit();
        }

        $this->view->HeadTitle('Relatorios')->HeadTitle('Acesso do Administrador');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/acesso-administrador.css', 'screen');

        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
        $daoRedes = App_Model_DAO_Redes::getInstance();
        $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();

        // Loja
        $selectLoja = $daoUsuarios->getAdapter()->select()->from(
                $daoUsuarios->info('name'), array(
                'usr_idUsuario',
                'usr_login',
                'usr_nome',
                'usr_idGrupoRede',
                'usr_idRede',
                'usr_idGrupoLoja',
                'usr_idLoja',
                'usr_status',
                'usr_idPerfil'
                )
            )
            ->joinInner($daoLojas->info('name'), 'loja_idLoja = usr_idLoja', array('loja_idLoja', 'loja_nomeReduzido'))
            ->joinInner($daoGrupoLojas->info('name'), 'loja_idGrupo = grp_loja_idGrupo', 'grp_loja_nome')
            ->joinInner($daoRedes->info('name'), 'grp_loja_idRede = rede_idRede', 'rede_nome')
            ->joinInner($daoGrupoRedes->info('name'), 'rede_idGrupo = grp_rede_idGrupo', 'grp_rede_nome')
            ->where('NOT ISNULL(usr_idLoja)');

        // Grupo Loja
        $selectGrLoja = $daoUsuarios->getAdapter()->select()->from(
                $daoUsuarios->info('name'), array(
                'usr_idUsuario',
                'usr_login',
                'usr_nome',
                'usr_idGrupoRede',
                'usr_idRede',
                'usr_idGrupoLoja',
                'usr_idLoja',
                'usr_status',
                'usr_idPerfil',
                new Zend_Db_Expr("'' AS loja_idLoja"),
                new Zend_Db_Expr("'' AS loja_nomeReduzido")
                )
            )
            ->joinInner($daoGrupoLojas->info('name'), 'usr_idGrupoLoja = grp_loja_idGrupo', 'grp_loja_nome')
            ->joinInner($daoRedes->info('name'), 'grp_loja_idRede = rede_idRede', 'rede_nome')
            ->joinInner($daoGrupoRedes->info('name'), 'rede_idGrupo = grp_rede_idGrupo', 'grp_rede_nome')
            ->where('NOT ISNULL(usr_idGrupoLoja)');

        // Rede
        $selectRede = $daoUsuarios->getAdapter()->select()->from(
                $daoUsuarios->info('name'), array(
                'usr_idUsuario',
                'usr_login',
                'usr_nome',
                'usr_idGrupoRede',
                'usr_idRede',
                'usr_idGrupoLoja',
                'usr_idLoja',
                'usr_status',
                'usr_idPerfil',
                new Zend_Db_Expr("'' AS loja_idLoja"),
                new Zend_Db_Expr("'' AS loja_nomeReduzido"),
                new Zend_Db_Expr("'' AS grp_loja_nome")
                )
            )
            ->joinInner($daoRedes->info('name'), 'usr_idRede = rede_idRede ', 'rede_nome')
            ->joinInner($daoGrupoRedes->info('name'), 'rede_idGrupo = grp_rede_idGrupo', 'grp_rede_nome')
            ->where('NOT ISNULL(usr_idRede)');

        // Grupo de Rede
        $selectGrRede = $daoUsuarios->getAdapter()->select()->from(
                $daoUsuarios->info('name'), array(
                'usr_idUsuario',
                'usr_login',
                'usr_nome',
                'usr_idGrupoRede',
                'usr_idRede',
                'usr_idGrupoLoja',
                'usr_idLoja',
                'usr_status',
                'usr_idPerfil',
                new Zend_Db_Expr("'' AS loja_idLoja"),
                new Zend_Db_Expr("'' AS loja_nomeReduzido"),
                new Zend_Db_Expr("'' AS grp_loja_nome"),
                new Zend_Db_Expr("'' AS rede_nome")
                )
            )
            ->joinInner($daoGrupoRedes->info('name'), 'usr_idGrupoRede = grp_rede_idGrupo', 'grp_rede_nome')
            ->where('NOT ISNULL(usr_idGrupoRede)');

        $union = $daoUsuarios->getAdapter()->select()->union(array($selectLoja, $selectGrLoja, $selectRede, $selectGrRede));
        $consulta = $daoUsuarios->getAdapter()->select()->from(array('temp' => $union))
            ->where('usr_status = ?', 1)
            ->where('usr_idPerfil != ?', 1);

        //filtro por numrero de protocolo
        if ($this->getRequest()->getParam('perfil', false) != false) {
            $this->view->grupo = $this->getRequest()->getParam('perfil');
            switch ($this->getRequest()->getParam('perfil')) {
                case 'GR':
                    $consulta->where('NOT ISNULL(usr_idGrupoRede)');
                    break;

                case 'RE':
                    $consulta->where('NOT ISNULL(usr_idRede)');
                    break;

                case 'GL':
                    $consulta->where('NOT ISNULL(usr_idGrupoLoja)');
                    break;

                case 'LO':
                    $consulta->where('NOT ISNULL(usr_idLoja)');
                    break;
            }
        }

        //filtro por tipo
        if ($this->getRequest()->getParam('nome', false) != false) {
            $palavra = $this->getRequest()->getParam('nome');
            $consulta->where("usr_nome LIKE ?", "%{$palavra}%");
            $this->view->palavra = $palavra;
        }

        //filtro por login
        if ($this->getRequest()->getParam('login', false) != false) {
            $palavra = $this->getRequest()->getParam('login');
            $consulta->where("usr_login LIKE ?", "%{$palavra}%");
            $this->view->login = $palavra;
        }

        //filtro por grupo de rede
        if ($this->getRequest()->getParam('gprede', false) != false) {
            $palavra = $this->getRequest()->getParam('gprede');
            $consulta->where("grp_rede_nome LIKE ?", "%{$palavra}%");
            $this->view->gprede = $palavra;
        }

        //filtro por rede
        if ($this->getRequest()->getParam('rede', false) != false) {
            $palavra = $this->getRequest()->getParam('rede');
            $consulta->where("rede_nome LIKE ?", "%{$palavra}%");
            $this->view->rede = $palavra;
        }

        //filtro por grupo de loja
        if ($this->getRequest()->getParam('gploja', false) != false) {
            $palavra = $this->getRequest()->getParam('gploja');
            $consulta->where("grp_loja_nome LIKE ?", "%{$palavra}%");
            $this->view->gploja = $palavra;
        }

        //filtro por loja
        if ($this->getRequest()->getParam('loja', false) != false) {
            $palavra = $this->getRequest()->getParam('loja');
            $consulta->where("loja_nomeReduzido LIKE ?", "%{$palavra}%");
            $this->view->loja = $palavra;
        }

        //filtro por cod loja
        if ($this->getRequest()->getParam('codloja', false) != false) {
            $id = $this->getRequest()->getParam('codloja');
            $consulta->where("loja_idLoja = ?", $id);
            $this->view->codloja = $id;
        }

        // paginacao
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($consulta);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));

        $this->view->paginator = $paginator;
    }

    public function validarAction()
    {
        // Validar
        if ($this->getRequest()->isPost()) {

            $filters = array(
                '*' => new Zend_Filter_StringTrim()
            );

            $validators = array(
                'codigo' => array(Zend_Filter_Input::ALLOW_EMPTY => false),
                'email' => array(
                    Zend_Filter_Input::ALLOW_EMPTY => false,
                    new Zend_Validate_EmailAddress()
                )
            );

            $dados = $this->getRequest()->getParams();
            $this->view->dados = $dados;
            $filter = new Zend_Filter_Input($filters, $validators, $dados, array('escapeFilter' => 'StringTrim'));
            if (!$filter->isValid()) {
                foreach ($filter->getMessages() as $field => $messages) {
                    $msg = array();
                    foreach ($messages as $value)
                        $msg[] = $value;
                    $fieldErrors[$field] = implode(', ', $msg);
                }
                App_Funcoes_UTF8::encode($fieldErrors);
                $this->view->fieldErrors = Zend_Json::encode($fieldErrors);
                $this->view->message = "Por favor, verifique o(s) campo(s) marcado(s) em vermelho.";
                $this->view->dados = $this->getRequest()->getParams();
            } else {

                $daoLojas = App_Model_DAO_Lojas::getInstance();
                $loja = $daoLojas->fetchRow(
                    $daoLojas->select()->from($daoLojas)
                        ->where('loja_idLoja = ?', $this->getRequest()->getParam('codigo'))
                        ->where('loja_email = ?', $this->getRequest()->getParam('email'))
                );

                if ($loja != null) {
                    $validacao = new Zend_Session_Namespace('validacao');
                    $validacao->loja = $loja;
                    $url = Zend_Registry::get('config')->paths->site->base . '/gerenciamento-de-usuarios/dados-de-acesso';
                    $this->getResponse()->setRedirect($url)->sendResponse();
                    exit();
                } else {
                    $this->view->message = "C�digo n�o encontrado.";
                    $this->view->dados = $this->getRequest()->getParams();
                }
            }
        }
        $this->_forward('index');
    }

    public function logoutAction()
    {
        $login = App_Plugin_Login::getInstance();
        if ($login->hasIdentity()) {
            $login->clearIdentity();
            Zend_Session::namespaceUnset('validacao');
            Zend_Session::namespaceUnset('carrinho');
            Zend_Session::namespaceUnset('historico');
            Zend_Session::namespaceUnset('pesquisa');
        }
        $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base)->sendResponse();
    }

    public function homeAction()
    {
        $login = App_Plugin_Login::getInstance();
        $usuarioLogado = $login->getIdentity()->getCodigo();

        //faz confirma��o de leitura de comunicados
        if ($this->getRequest()->isPost() && $this->getRequest()->getParam('idComunicado', false) != false) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $this->getFrontController()->setParam('noViewRenderer', true);

            $retorno = array(
                'success' => true,
                'message' => ''
            );

            try {
                $daoLeituras = App_Model_DAO_Comunicados_Leituras::getInstance();
                $daoComunicados = App_Model_DAO_Comunicados::getInstance();

                $idComunidado = $this->getRequest()->getParam('idComunicado', false);
                $filtroComunicadosAtivos = $this->view->Permissoes()->selectComunicados();
                $filtroComunicadosAtivos
                    ->joinLeft($daoLeituras->info('name'), "com_idComunicado = com_leit_idComunicado AND com_leit_idUsuario = {$usuarioLogado}")
                    ->where('com_status = ?', 1)
                    ->where('? BETWEEN com_dataInicial AND com_dataFinal', date('Y-m-d'))
                    ->where('ISNULL(com_leit_idComunicado)')
                    ->where('com_idComunicado = ?', $idComunidado)
                    ->where('com_leituraObrigatoria = ?', 1)
                    ->order('com_dataCadastro ASC')
                    ->limit(1);

                $temComunicado = $daoComunicados->getAdapter()->fetchRow($filtroComunicadosAtivos);
                if ($temComunicado) {
                    $daoLeituras->insert(array(
                        'com_leit_idComunicado' => $idComunidado,
                        'com_leit_idUsuario' => $usuarioLogado,
                        'com_leit_data' => date('Y-m-d H:i:s')
                    ));
                }
            } catch (Exception $e) {
                $retorno = array(
                    'success' => false,
                    'message' => 'N�o foi poss�vel realizar a opera��o'
                );
            }

            App_Funcoes_UTF8::encode($retorno);
            echo Zend_Json::encode($retorno);
            exit();
        }

        // Verifica se tem comunicado ativo para exibir
        $this->view->comunicado = false;
        if (!$login->hasMasterUser()) {
            $daoLeituras = App_Model_DAO_Comunicados_Leituras::getInstance();
            $daoComunicados = App_Model_DAO_Comunicados::getInstance();
            $filtroComunicadosAtivos = $this->view->Permissoes()->selectComunicados();
            $filtroComunicadosAtivos
                ->joinLeft($daoLeituras->info('name'), "com_idComunicado = com_leit_idComunicado AND com_leit_idUsuario = {$usuarioLogado}")
                ->where('com_status = ?', 1)
                ->where('? BETWEEN com_dataInicial AND com_dataFinal', date('Y-m-d'))
                ->where('ISNULL(com_leit_idComunicado)')
                ->where('com_leituraObrigatoria = ?', 1)
                ->order('com_dataCadastro ASC')
                ->limit(1);

            $arrComunicado = $daoComunicados->getAdapter()->fetchRow($filtroComunicadosAtivos);
            if (is_array($arrComunicado)) {
                $this->view->comunicado = $daoComunicados->createRow(
                    $daoComunicados->getAdapter()->fetchRow($filtroComunicadosAtivos)
                );
            }
        }
        
        // Verifica se tem embalagens
        $this->view->embalagens = false;
        if (!$login->hasMasterUser()) {
            $daoEmbalagens = App_Model_DAO_Embalagens::getInstance();
            $filtroEmbalagensAtivos = $this->view->Permissoes()->selectEmbalagens();
            $filtroEmbalagensAtivos                
                ->where('emb_status = ?', 1)
                ->where('emb_destaque = ?', 1)
                ->where('!ISNULL(emb_idImagemDestaque)')
                    ->group('emb_idEmbalagem');


            $arrEmbalagens = $daoEmbalagens->getAdapter()->fetchAll($filtroEmbalagensAtivos);
            if (is_array($arrEmbalagens)) {
                $this->view->embalagens = $daoEmbalagens->createRowset(
                    $daoEmbalagens->getAdapter()->fetchAll($filtroEmbalagensAtivos)
                );
            }
        }

        $this->view->HeadTitle('Home');
        $this->view->HeadScript()->appendFile('scripts/lib/jquery.bxslider.min.js');
        $this->view->HeadScript()->appendFile('scripts/lib/jquery.cycle.all.js');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.bxslider.css', 'screen');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/home.css', 'screen');

        // Banners
        $selectBanner = $this->view->Permissoes()->selectBanners();
        $selectBanner->where('ban_status = ?', 1)->order('ban_ordem ASC')->limit(10);
        $daoBannes = App_Model_DAO_Banners::getInstance();
        $banners = $daoBannes->getAdapter()->fetchAll($selectBanner);
        $this->view->banners = ($banners != null) ? $daoBannes->createRowset($banners) : null;

        // Linhas
        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        $this->view->linhas = $daoLinhas->fetchAll(
            $daoLinhas->select()->order('RAND()')
                ->limit(10)
        );

        $daoNoticias = App_Model_DAO_Noticias::getInstance();

        // Not�cia de Destaque
        $selectNoticias = $this->view->Permissoes()->selectNoticias();
        $selectNoticias->where('not_status = ?', 1);
        $selectNoticias->where('not_destaque = ?', 1)->limit(1)->order('not_idNoticia DESC');
        $destaque = $daoNoticias->getAdapter()->fetchRow($selectNoticias);
        $this->view->noticiaDestaque = ($destaque != null) ? $daoNoticias->createRow($destaque) : null;

        // �ltimas Not�cias
        $selectNoticias = $this->view->Permissoes()->selectNoticias();
        $selectNoticias->where('not_status = ?', 1);
        $selectNoticias->limit(3)->order('not_dataCadastro DESC');
        if ($destaque != null)
            $selectNoticias->where('not_idNoticia <> ?', $destaque['not_idNoticia']);
        $ultNoticias = $daoNoticias->getAdapter()->fetchAll($selectNoticias);
        $this->view->noticias = ($ultNoticias != null) ? $daoNoticias->createRowset($ultNoticias) : null;

        $daoVideos = App_Model_DAO_Videos::getInstance();

        // V�deo de Destaque
        $selectVideos = $this->view->Permissoes()->selectVideos();
        $selectVideos->where('vid_destaque = ?', 1)->where('vid_status = ?', 1)->limit(1)->order('RAND()');
        $videoDestaque = $daoVideos->getAdapter()->fetchRow($selectVideos);
        $this->view->videoDestaque = ($videoDestaque != null) ? $daoVideos->createRow($videoDestaque) : null;

        // �ltimos Videos
        $selectVideos = $this->view->Permissoes()->selectVideos();
        $selectVideos->limit(2)->order('vid_dataCadastro DESC')->where('vid_status = ?', 1);
        if ($videoDestaque != null)
            $selectVideos->where('vid_idVideo <> ?', $videoDestaque['vid_idVideo']);
        $videos = $daoVideos->getAdapter()->fetchAll($selectVideos);
        $this->view->videos = ($videos != null) ? $daoVideos->createRowset($videos) : null;
    }

    public function accessDeniedAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $this->getFrontController()->setParam('noViewRenderer', true);
            $this->getResponse()->setHeader('Content-Type', 'text/json');
            $retorno = array('success' => false, 'message' => 'Acesso Negado');
            App_Funcoes_UTF8::encode($retorno);
            echo Zend_Json::encode($retorno);
        } else {
            $this->view->HeadTitle('Home')->HeadTitle('Acesso Negado');
        }
    }

    public function ajudaAction()
    {
        $this->view->HeadTitle('Ajuda');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/ajuda.css', 'screen');
    }

    public function politicaDeDevolucaoAction()
    {
        $this->view->HeadTitle('Home')->HeadTitle('Pol�tica de Devolu��o');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/ajuda.css', 'screen');
    }

    public function faleConoscoAction()
    {
        $this->view->HeadTitle('Fale Conosco');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/fale-conosco.css', 'screen');

        $this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
        $this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');
        $this->view->HeadScript()->appendFile('scripts/messages_ptbr.js');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');

        // Assuntos do Fale Conosco
        $daoConfiguracao = App_Model_DAO_Sistema_Configuracoes::getInstance();
        $config = $daoConfiguracao->fetchRow($daoConfiguracao->select());
        $config = $config->toArray();
        $config['conf_emailsContato'] = Zend_Json::decode(utf8_encode($config['conf_emailsContato']));
        unset($daoConfiguracao);
        $this->view->assuntos = $config['conf_emailsContato'];

        // Formul�rio foi enviado
        if ($this->getRequest()->isPost()) {

            $daoFaleConosco = App_Model_DAO_FaleConosco::getInstance();

            try {
                $faleconosco = $daoFaleConosco->createRow();
                $faleconosco->setUsuario(App_Plugin_Login::getInstance()->getIdentity())
                    ->setNome($this->getRequest()->getParam('fale_nome'))
                    ->setEmail($this->getRequest()->getParam('fale_email'))
                    ->setTelefone($this->getRequest()->getParam('fale_telefone'))
                    ->setEmpresa($this->getRequest()->getParam('fale_empresa'))
                    ->setCargo($this->getRequest()->getParam('fale_cargo'))
                    ->setMensagem($this->getRequest()->getParam('fale_mensagem'));

                // Verifica qual o assunto postado
                $txtAssunto = '';
                $emailDestino = '';
                foreach ($config['conf_emailsContato'] as $assunto) {
                    if ($assunto['contato_id'] == $this->getRequest()->getParam('fale_assunto')) {
                        $emailDestino = trim($assunto['contato_email']);
                        $txtAssunto = utf8_decode($assunto['contato_assunto']);
                        break;
                    }
                }

                $faleconosco->setAssunto($txtAssunto);
                $faleconosco->save();

                // Envia o email do contato
                $template = new Zend_View();
                $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

                // Verifica qual o assunto postado
                $txtAssunto = '';
                $emailDestino = '';
                foreach ($config['conf_emailsContato'] as $assunto) {
                    if ($assunto['contato_id'] == $this->getRequest()->getParam('fale_assunto')) {
                        $emailDestino = trim($assunto['contato_email']);
                        $txtAssunto = utf8_decode($assunto['contato_assunto']);
                        break;
                    }
                }

                $template->fale_nome = $faleconosco->getNome();
                $template->fale_email = $faleconosco->getEmail();
                $template->fale_telefone = $faleconosco->getTelefone();
                $template->fale_empresa = $faleconosco->getEmpresa();
                $template->fale_cargo = $faleconosco->getCargo();
                $template->fale_assunto = $txtAssunto;
                $template->fale_mensagem = nl2br(strip_tags($faleconosco->getMensagem()));
                $template->ip = $_SERVER['REMOTE_ADDR'];

                try {
                    $mailTransport = new App_Model_Email();
                    $mail = new Zend_Mail('ISO-8859-1');
                    $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('configInfo')->nomeRemetente);
                    $mail->setReplyTo($template->email, $template->nome);
                    $mail->addTo($emailDestino);

                    $mail->setSubject(Zend_Registry::get('config')->project . ' - Fale Conosco - ' . $txtAssunto);
                    $mail->setBodyHtml($template->render('emails/contato.phtml'));
                    $mail->send($mailTransport->getFormaEnvio());

                    $this->view->message = "Mensagem enviada com sucesso.";
                } catch (Exception $e) {
                    throw new Exception("N�o foi poss�vel enviar sua mensagem.");
                }
            } catch (App_Validate_Exception $e) {
                $fieldErrors = array();
                foreach ($e->getFields() as $field => $message) {
                    $fieldErrors[$field] = $message;
                }
                App_Funcoes_UTF8::encode($fieldErrors);
                $this->view->fieldErrors = Zend_Json::encode($fieldErrors);
                $this->view->message = count($fieldErrors) ? 'Por favor, verifique os campos marcados em vermelho.' : $e->getMessage();
                $this->view->dados = $this->getRequest()->getParams();
            } catch (Exception $e) {
                $this->view->message = $e->getMessage();
                $this->view->dados = $this->getRequest()->getParams();
            }
        }
    }

}
