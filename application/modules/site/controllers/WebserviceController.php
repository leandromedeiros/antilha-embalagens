<?php

class WebserviceController extends Zend_Controller_Action
{

    public function indexAction()
    {
        ini_set("soap.wsdl_cache_enabled", 0);

        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        try {
              $options = array('login' => 'antilhas', 'password' => 'xantilhasvm2', 'encoding' => 'ISO-8859-1', 'trace' => 1);
			        //$soap = new SoapClient('http://projetosphp/Antilhas/B2B/public/site/webservice/soap?wsdl', $options);
              //$soap = new SoapClient('http://www.antilhaspedidos.com.br/webservice/soap?wsdl', $options);
			          $soap = new SoapClient('http://homolog.antilhaspedidos.com.br/webservice/soap?wsdl', $options);
			


               /*$permissao1 = new App_Model_Entity_ProdutoPermissao();
                $permissao1->codigo = "50";
                $permissao1->codigoGrupo = "C02";
                $permissao1->codigoRede = "C02";
                $permissao1->codigoProduto = "000000005100000580";

               
              $permissoes[] = $permissao1; */
              

             /*$retorno = $soap->insereAtualizaProdutoPermissao($permissoes);*/

            $permissao1 = new App_Model_Entity_ProdutoPermissao();
             $permissao1->codigo = "50";
            $permissao1->codigoGrupo = "C02";
            $permissao1->codigoRede = "C02";
            $permissao1->codigoProduto = "000000000013000335";
            $permissoes[] = $permissao1;

           /* $delete[] = "000000005100000754";*/
           
            $retorno = $soap->removeProdutoPermissao($permissoes);
            print_r($retorno);
            $xml = simplexml_load_string($retorno);
            print_r($xml); 
            print_r($permissoes);

        } catch (App_Model_WebServiceException $e) {
            print_r('deu ruim');
            print_r($e->getMessage());
            die($e->getMessage());
        }
    }

    /**
     *
     * Inicializa o webservice
     */
    public function soapAction()
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        Zend_Layout::getMvcInstance()->disableLayout();

        $this->getFrontController()->setParam('noViewRenderer', true);

        if (isset($_GET['wsdl'])) {
            /*
             * Usar o Soap AutoDiscover para criacao do WSDL de forma dinamica
             */

             try {
                $autodiscover = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');
                $autodiscover->setClass('App_Model_Webservice');
                $autodiscover->handle();
             }catch(Exception $e){
                //print_r($e);
             }

            
        } else {
            //Disponibilizar o webservice atraves do canal:
            $soap = new Zend_Soap_Server(Zend_Registry::get('config')->webservice);
            $soap->setClass('App_Model_Webservice');
            $soap->handle();
        }
    }

}
