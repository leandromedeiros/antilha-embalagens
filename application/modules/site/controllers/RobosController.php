<?php

class RobosController extends Zend_Controller_Action
{
	public function init(){
		Zend_Layout::getMvcInstance()->disableLayout();
		$this->getFrontController()->setParam('noViewRenderer', true);
	}
	
	
	public function testeAction() {
		$dataFaturamento = App_Funcoes_Date::conversion(App_Funcoes_Date::SubtraiDiasUteis('20/02/2015', 5));
		$dataAtual = date('Y-m-d');
		
		echo $dataFaturamento . ' - ' . $dataAtual;
		die(($dataAtual < $dataFaturamento) . '====');
		/*$pastaDestino = Zend_Registry::get('config')->documentos->notasFiscais;
		
		$arquivos = glob("$pastaDestino{*.jpg,*.png,*.gif,*.bmp}", GLOB_BRACE);
		foreach($arquivos as $img){
			var_dump($img);
		}*/
		
		/*		
		// Envia o email
		$daoPedidos = App_Model_DAO_Pedidos::getInstance();
		$pedido = $daoPedidos->find(140)->current();
		
		$template = new Zend_View();
		$template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');
		
		$template->pedido = $pedido;		
		$template->usuario = $pedido->getUsuario();	
		
		$mailTransport = new App_Model_Email();
		$mail = new Zend_Mail('ISO-8859-1');
		$mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
		$mail->addTo($pedido->getUsuario()->getEmail(), $pedido->getUsuario()->getNome());			
		
		$mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido Reprovado');
		$mail->setBodyHtml($template->render('emails/pedido-aprovado.phtml'));
		
		die($template->render('emails/pedido-aprovado.phtml'));
		$mail->send($mailTransport->getFormaEnvio());
		*/
		
		/*
		$status = $this->getRequest()->getParam('status', 1);
		$daoStatus = App_Model_DAO_Pedidos_Status::getInstance();
		$objStatus = $daoStatus->find(28)->current();
		
		$objStatus->setValor($status);
		$daoStatus->enviaEmail($objStatus);
		*/
	}
	
	/*
	 * Rotina que procura reclama�oes com status pendente e envia e-mail 
	 *
	 */
	public function verificaAtendimentosAction()
	{
		$daoReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();
		
		$select = $daoReclamacoes->select()
			->from($daoReclamacoes)
			->where('sug_rec_status <> ?', 2)
			->where('NOW() >= ADDDATE(sug_rec_dataCadastro, 1)');
		
		$listaPendencias = $daoReclamacoes->fetchAll($select);
		if($listaPendencias->count()) {
			//Envia o email
			$template = new Zend_View();
			$template->setBasePath(Zend_Registry::get('config')->paths->site->root);
			
			$template->pendencias = $listaPendencias;
			
			$mailTransport = new App_Model_Email();
			$mail = new Zend_Mail('ISO-8859-1');
			$mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
			$mail->addTo(Zend_Registry::get('config')->mail->pendencias);			
			
			$mail->setSubject(Zend_Registry::get('config')->project . ' - Atendimentos em aberto');
			$mail->setBodyHtml($template->render('emails/atendimentos-pendentes.phtml'));
			$mail->send($mailTransport->getFormaEnvio());						
		}
		die('fim');
	}
	
	/*
	 * Rotina que procura por carrinhos abandonados e envia e-mail de aviso para o cliente 
	 *
	 */
	public function verificaCarrinhoAction()
	{
		$daoCarrinhos = App_Model_DAO_Carrinho::getInstance();
		
		//aviso de 1 dia
		$select = $daoCarrinhos->select()
			->where('car_aviso1Dia = ?', 0)
			->where('NOW() >= ADDDATE(car_dataCadastro, 1)')
			->where('ISNULL(car_idPedido)');
		
		$listaCarrinhos = $daoCarrinhos->fetchAll($select);
		foreach($listaCarrinhos as $carrinho) {
			try {
				$carrinho->setAviso1Dia(1);
				$carrinho->save();
				
				$daoCarrinhos->emailAviso($carrinho, 1);
			} catch (Exception $e) { }
		}
		
		//aviso de 7 dias
		$select = $daoCarrinhos->select()
			->where('car_aviso1Dia = ?', 0)
			->where('NOW() >= ADDDATE(car_dataCadastro, 7)')
			->where('ISNULL(car_idPedido)');
		
		$listaCarrinhos = $daoCarrinhos->fetchAll($select);
		foreach($listaCarrinhos as $carrinho) {
			try {
				$carrinho->setAviso7Dias(1);
				$carrinho->save();
				
				$daoCarrinhos->emailAviso($carrinho, 7);
			} catch (Exception $e) { }
		}
		die('fim');
	}
	
	/*
	 * Rotina para importar imagens baseadas no c�digo do produto 
	 *
	 */
	public function importarImagensAction() {
		$daoProdutos = App_Model_DAO_Produtos::getInstance();
		$pastaLeitura = Zend_Registry::get('config')->paths->site->file . '/upload-imagens/';
		$pastaGravacao = Zend_Registry::get('config')->paths->site->file . '/images/produtos/';
		$idGaleriaProduto = 'p';
		$log = array();
		
		$arquivos = glob("$pastaLeitura{*.jpg,*.png,*.gif,*.bmp}", GLOB_BRACE);
		try {
			foreach($arquivos as $img){
				try {
					$daoProdutos->getAdapter()->beginTransaction();
					$info = pathinfo($img);
					$fileName = $info['filename'];
					
					$selectProduto = "SELECT prod_idProduto FROM antilhas_produtos WHERE prod_idProduto = '{$fileName}'"; 
					$query = $daoProdutos->getAdapter()->query($selectProduto);
					$temProduto = $query->rowCount();
					
					//se n�o achar o produto dispara erro
					if(!$temProduto) {
						throw new Exception('Produto n�o encontrado.');
					}
					
					//verifica se o arquivo existe na pasta de destino, se sim, apaga
					$arquivoDestino = "$pastaGravacao{$info['basename']}";
					if(file_exists($arquivoDestino)) {
						//remove o arquivo da pasta
						unlink($arquivoDestino);
						
						//remove o registro do banco
						$updateProdutoImagem = "UPDATE antilhas_produtos SET prod_idImagem = (NULL) WHERE prod_idProduto = '{$fileName}'";
						$daoProdutos->getAdapter()->query($updateProdutoImagem);
						
						$sqlDelete = "DELETE FROM antilhas_galerias_arquivos WHERE gal_arq_idGaleria = 'p' AND gal_arq_nome = '{$info['basename']}'";
						$daoProdutos->getAdapter()->query($sqlDelete);
					}
					
					//move o arquivo de lugar, se der erro, aborta opera��o, se der certo apaga o arquivo da pasta de leitura
					if(!copy($img, $arquivoDestino)) {
						throw new Exception('Problema ao mover arquivo.');
					}
					
					//apagando arquivo da pasta de leitura
					unlink($img);
					
					//inserindo registro na tabela de arquivos
					$dataAtual = date('Y-m-d');
					$selectInsertGaleria = "INSERT INTO antilhas_galerias_arquivos (gal_arq_idGaleria, gal_arq_nome, gal_arq_data) VALUES ('p', '{$info['basename']}', '{$dataAtual}')";
					$daoProdutos->getAdapter()->query($selectInsertGaleria);
					
					$idArquivo = $daoProdutos->getAdapter()->lastInsertId();
					
					//atualizando a tabela de produtos
					$updateProduto = "UPDATE antilhas_produtos SET prod_idImagem = '{$idArquivo}' WHERE prod_idProduto = '{$fileName}'";
					$daoProdutos->getAdapter()->query($updateProduto);
					
					$log[] = "<span>A imagem do produto <b>{$fileName}</b> foi salva com sucesso</span>";
					$daoProdutos->getAdapter()->commit();
				} catch (Exception $e) {
					$daoProdutos->getAdapter()->rollBack();
					$log[] = "<span style='color: red'>Houve um problema ao importar a imagem do produto <b>{$fileName}</b>. Motivo: {$e->getMessage()}</span>";
				}
			}
		} catch (Exception $e) {
			
		}
		
		echo '<pre>';
		print_r($log);
	}
	
	/**
	 * Pega a fila de e-mails que est�o pendentes de envio depois da altera��o de status do pedido
	 */
	public function emailsPendentesAction() {
	
		Zend_Layout::getMvcInstance()->disableLayout();
		$this->getFrontController()->setParam('noViewRenderer', true);
	
		$daoStatus = App_Model_DAO_Pedidos_Status::getInstance();
		$allStatus = $daoStatus->fetchAll(
			$daoStatus->select()->from($daoStatus)->where('ped_status_envioPendente = ?', 1)->limit('10')
		);
	
		foreach ($allStatus as $key => $status) {
			$daoStatus->enviaEmail($status);
		}
		die('Finalizado');
	}
	
}