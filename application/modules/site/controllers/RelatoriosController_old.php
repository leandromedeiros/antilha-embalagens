<?php

class RelatoriosController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $this->view->HeadTitle('Relat�rios');
    }

    public function registrosDeReclamacoesAction()
    {
        $this->view->HeadTitle('Relatorios')->HeadTitle('Registros de Reclama��es');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/historico-reclamacoes.css', 'screen');

        $daoSugestoesReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();

        // seleciona todas as reclamacoes e sugestoes da tabela sem filtros
        $consulta = $daoSugestoesReclamacoes->select();

        //filtro por numrero de protocolo
        if ($this->getRequest()->getParam('protocol')) {
            $consulta->where('sug_rec_idSugestaoReclamacao = ?', $this->getRequest()->getParam('protocol'));
            $this->view->protocol = $this->getRequest()->getParam('protocol');
        }
        //filtro por tipo
        if ($this->getRequest()->getParam('tipo')) {
            $consulta->where('sug_rec_tipo = ?', $this->getRequest()->getParam('tipo'));
            $this->view->tipo = $this->getRequest()->getParam('tipo');
        }
        // filtro por usuario
        if ($this->getRequest()->getParam('user')) {
            $consulta->where('sug_rec_idUsuario = ?', $this->getRequest()->getParam('user'));
            $this->view->usr = $this->getRequest()->getParam('user');
        }
        // filtro por status
        if ($this->getRequest()->getParam('status') != '') {
            $consulta->where('sug_rec_status = ?', $this->getRequest()->getParam('status'));
            $this->view->status = $this->getRequest()->getParam('status');
        }

        //filtro de hierarquias
        $consulta->where($this->view->Usuario()->quoteIntoUsuarios('sug_rec_idUsuario'));

        $consulta->order('sug_rec_dataCadastro DESC');

        // paginacao
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($consulta);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));

        $this->view->paginator = $paginator;
    }

    public function reclamacoesDetalheAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Reclama��es Detalhe');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/historico-reclamacoes-detalhe.css', 'screen');

        $daoSugestoesReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();

        $registro = $daoSugestoesReclamacoes->fetchRow(
            $daoSugestoesReclamacoes->select()->from($daoSugestoesReclamacoes)
                ->where('sug_rec_idSugestaoReclamacao = ?', $this->getRequest()->getParam('id'))
                ->where($this->view->Usuario()->quoteIntoUsuarios('sug_rec_idUsuario')));

        if ($registro == null) {
            $this->getResponse()
                ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/relatorios/registros-de-reclamacoes')
                ->sendResponse();
        }

        $daoSugestoesReclamacoesStatus = App_Model_DAO_SugestoesReclamacoes_Status::getInstance();
        $interList = $daoSugestoesReclamacoesStatus->fetchAll($daoSugestoesReclamacoesStatus->select()
                ->from($daoSugestoesReclamacoesStatus)
                ->where('sug_rec_status_idSugestaoReclamacao = ?', $this->getRequest()->getParam('id')));

        $this->view->reclamacao = $registro;
        $this->view->interacoes = $interList;
    }

    public function historicoFinanceiroAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('2� Via de Boleto');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/historico-financeiro.css', 'screen');

        $daoLojas = App_Model_DAO_Lojas::getInstance();
        $daoLojasEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoPedidosNotasBoletos = App_Model_DAO_Pedidos_Notas_Boletos::getInstance();

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }

        $stringLojas = implode(',', $arrayLojas);
        $selectBase = $daoPedidosNotasBoletos->getAdapter()->select()->from($daoPedidosNotasBoletos->info('name'))
            ->joinInner($daoLojas->info('name'), 'ped_nota_bol_cliente = loja_idLoja')
            ->joinInner($daoLojasEnderecos->info('name'), 'loja_end_idLoja = loja_idLoja')
            ->where('ped_nota_bol_tipoDoc = ?', 'A')
            ->where('loja_end_padrao = 1')
            ->where("loja_idLoja IN ({$stringLojas})");

        $selectMeses = clone $selectBase;
        $selectMeses->reset(Zend_Db_Select::COLUMNS);
        $selectMeses->columns(array('meses' => new Zend_Db_Expr("DISTINCT(CONCAT(YEAR(ped_nota_bol_dataEmissao), '-', LPAD(MONTH(ped_nota_bol_dataEmissao) , 2, '0')))")))
            ->order('meses DESC');

        $this->view->filtroMeses = $this->view->filtroLojas = $this->view->filtroRegiao = array();

        $listaMeses = $daoPedidosNotasBoletos->getAdapter()->fetchAll($selectMeses);
        foreach ($listaMeses as $k => $v) {
            $this->view->filtroMeses[] = $v['meses'];
        }

        $listaLojas = $daoPedidosNotasBoletos->getAdapter()->fetchAll($selectBase);
        foreach ($listaLojas as $k => $v) {
            $this->view->filtroLojas[] = $v['loja_nomeReduzido'];
        }

        $listaRegiao = $daoPedidosNotasBoletos->getAdapter()->fetchAll($selectBase);
        foreach ($listaRegiao as $k => $v) {
            $this->view->filtroRegiao[] = $v['loja_end_uf'];
        }

        $filtro = clone $selectBase;
        $filtro->order('ped_nota_bol_dataEmissao DESC');

        //filtro de data
        $this->view->data = $this->getRequest()->getParam('data', false);
        if ($this->view->data) {
            $filtro->where('LEFT(ped_nota_bol_dataEmissao, 7) = ?', $this->view->data);
            $this->view->data = $this->getRequest()->getParam('data');
        }

        //filtro por nota fiscal
        if ($this->getRequest()->getParam('nota', false)) {
            $filtro->where('ped_nota_bol_notaFiscal LIKE ?', "%{$this->getRequest()->getParam('nota')}%");
            $this->view->nota = $this->getRequest()->getParam('nota');
        }

        //filtro por numero de pedido
        if ($this->getRequest()->getParam('boleto', false)) {
            $filtro->where('ped_nota_bol_idBoleto LIKE ?', "%{$this->getRequest()->getParam('boleto')}%");
            $this->view->boleto = $this->getRequest()->getParam('boleto');
        }

        //filtro por loja
        if ($this->getRequest()->getParam('loja', false)) {
            $filtro->where('loja_nomeReduzido LIKE ?', "%{$this->getRequest()->getParam('loja')}%");
            $this->view->loja = $this->getRequest()->getParam('loja');
        }

        //filtro de codigo de loja
        if ($this->getRequest()->getParam('codigoLoja', false)) {
            $filtro->where('loja_idLoja = ?', $this->getRequest()->getParam('codigoLoja'));
            $this->view->codigoLoja = $this->getRequest()->getParam('codigoLoja');
        }

        //filtro por regiao
        if ($this->getRequest()->getParam('regiao', false)) {
            $filtro->where('loja_end_uf = ?', $this->getRequest()->getParam('regiao'));
            $this->view->regiao = $this->getRequest()->getParam('regiao');
        }

        $this->view->filtro = $filtro;
        // Pagina��o
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($filtro);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

    public function historicoFinanceiroEmailAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'text/json');
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);

        $retorno['success'] = false;
        $retorno['message'] = "Problemas ao enviar a solicita��o.";

        $daoPedidosNotasBoletos = App_Model_DAO_Pedidos_Notas_Boletos::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();

        $boleto = $daoPedidosNotasBoletos->getAdapter()->fetchRow(
            $daoPedidosNotasBoletos->getAdapter()->select()->from($daoPedidosNotasBoletos->info('name'))
                ->joinInner($daoLojas->info('name'), 'ped_nota_bol_cliente = loja_idLoja')
                ->where('ped_nota_bol_idBoleto = ?', $this->getRequest()->getParam('id'))
        );

        try {
            // Envia o email
            $template = new Zend_View();
            $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

            $template->ped_nota_bol_docto = $boleto['ped_nota_bol_docto'];
            $template->ped_nota_bol_notaFiscal = $boleto['ped_nota_bol_notaFiscal'];
            $template->loja_idLoja = $boleto['loja_idLoja'];
            $template->loja = $boleto['loja_nomeCompleto'];
            $template->ped_nota_bol_dataEmissao = $boleto['ped_nota_bol_dataEmissao'];
            $template->ped_nota_bol_dataVencimento = $boleto['ped_nota_bol_dataVencimento'];
            $template->email = App_Plugin_Login::getInstance()->getIdentity()->getEmail();
            $template->usuario = App_Plugin_Login::getInstance()->getIdentity()->getNome();

            $mailTransport = new App_Model_Email();
            $mail = new Zend_Mail('ISO-8859-1');
            $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
            $mail->addTo($boleto['ped_nota_bol_link_boleto_2v']);
            $mail->addTo(App_Plugin_Login::getInstance()->getIdentity()->getEmail(), App_Plugin_Login::getInstance()->getIdentity()->getNome());
            $mail->addTo("alexandre@vm2.com.br", "Alexandre");

            $mail->setSubject(Zend_Registry::get('config')->project . ' - 2� Via do Boleto');
            $mail->setBodyHtml($template->render('emails/segunda-via.phtml'));

            $mail->send($mailTransport->getFormaEnvio());

            $retorno['success'] = true;
            $retorno['message'] = "Solicita��o enviado com sucesso. Sua segunda via ser� enviada para o email '" . App_Plugin_Login::getInstance()->getIdentity()->getEmail() . "' em at� 48 hs";
        } catch (\Exception $e) {

        }
        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function danfesAction()
    {

        $this->view->HeadTitle('Relat�rios')->HeadTitle('Danfes');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/danfe.css', 'screen');

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoPedidosNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();
        $daoLojasEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance();

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }

        $stringLojas = implode(',', $arrayLojas);
        $selectBase = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoPedidosNotas->info('name'), 'ped_idPedido = ped_nota_idPedido')
            ->joinInner($daoLojas->info('name'), 'ped_idLoja = loja_idLoja')
            ->joinInner($daoLojasEnderecos->info('name'), 'loja_end_idLoja = loja_idLoja')
            ->where('loja_end_padrao = 1')
            ->where("loja_idLoja IN({$stringLojas})");

        $selectMeses = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoPedidosNotas->info('name'), 'ped_idPedido = ped_nota_idPedido')
            ->joinInner($daoLojas->info('name'), 'ped_idLoja = loja_idLoja')
            ->joinInner($daoLojasEnderecos->info('name'), 'loja_end_idLoja = loja_idLoja')
            ->where('loja_end_padrao = 1')
            ->where("loja_idLoja IN({$stringLojas})");

        $selectMeses->reset(Zend_Db_Select::COLUMNS);
        $selectMeses->columns(array('meses' => new Zend_Db_Expr("DISTINCT(CONCAT(YEAR(ped_dataCadastro), '-', LPAD(MONTH(ped_dataCadastro) , 2, '0')))")))
            ->order('meses DESC');

        $this->view->filtroMeses = $this->view->filtroLojas = $this->view->filtroRegiao = array();

        $listaMeses = $daoPedidos->getAdapter()->fetchAll($selectMeses);
        foreach ($listaMeses as $k => $v) {
            $this->view->filtroMeses[] = $v['meses'];
        }
        unset($listaMeses);

        $selectBase->group('loja_idLoja');
        $listaLojas = $daoPedidos->getAdapter()->fetchAll($selectBase);
        foreach ($listaLojas as $k => $v) {
            $this->view->filtroLojas[] = $v['loja_nomeCompleto'];
        }
        unset($listaLojas);

        $listaRegiao = $daoPedidos->getAdapter()->fetchAll($selectBase);
        foreach ($listaRegiao as $k => $v) {
            $this->view->filtroRegiao[] = $v['loja_end_uf'];
        }
        unset($listaRegiao);

        $filtro = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoPedidosNotas->info('name'), 'ped_idPedido = ped_nota_idPedido')
            ->joinInner($daoLojas->info('name'), 'ped_idLoja = loja_idLoja')
            ->joinInner($daoLojasEnderecos->info('name'), 'loja_end_idLoja = loja_idLoja')
            ->where('loja_end_padrao = 1')
            ->where("loja_idLoja IN({$stringLojas})");

        $filtro->order('ped_nota_dataEmissao DESC');

        //filtro por numero de protocolo
        if ($this->getRequest()->getParam('protocolo', false)) {
            $filtro->where('ped_nota_numeroPedidoCliente = ?', $this->getRequest()->getParam('protocolo'));
            $this->view->protocolo = $this->getRequest()->getParam('protocolo');
        }

        //filtro por nota fiscal
        if ($this->getRequest()->getParam('nota', false)) {
            $filtro->where('ped_nota_numero = ?', $this->getRequest()->getParam('nota'));
            $this->view->nota = $this->getRequest()->getParam('nota');
        }

        //filtro por numero de pedido
        if ($this->getRequest()->getParam('pedido', false)) {
            $filtro->where('ped_nota_idPedido = ?', $this->getRequest()->getParam('pedido'));
            $this->view->pedido = $this->getRequest()->getParam('pedido');
        }

        //filtro por loja
        if ($this->getRequest()->getParam('loja', false)) {
            $filtro->where('loja_nomeCompleto LIKE ?', "%{$this->getRequest()->getParam('loja')}%");
            $this->view->loja = $this->getRequest()->getParam('loja');
        }
        //filtro por regiao
        if ($this->getRequest()->getParam('regiao', false)) {
            $filtro->where('loja_end_uf = ?', $this->getRequest()->getParam('regiao'));
            $this->view->regiao = $this->getRequest()->getParam('regiao');
        }

        //filtro de codigo de loja
        if ($this->getRequest()->getParam('codigoLoja', false)) {
            $filtro->where('loja_idLoja = ?', $this->getRequest()->getParam('codigoLoja'));
            $this->view->codigoLoja = $this->getRequest()->getParam('codigoLoja');
        }

        // Pagina��o
        unset($listaLojas, $arrayLojas, $stringLojas, $daoPedidos, $daoPedidosNotas, $daoLojas, $daoLojasEnderecos);
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($filtro);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

    public function downloadXmlAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Download Danfe');

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoPedidosNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();
        $daoLojasEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance();

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }

        $stringLojas = implode(',', $arrayLojas);

        $pedidos = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinLeft($daoPedidosNotas->info('name'), 'ped_idPedido = ped_nota_idPedido')
            ->joinInner($daoLojas->info('name'), 'ped_idLoja = loja_idLoja')
            ->where("loja_idLoja IN({$stringLojas})")
            ->where('ped_nota_caminhoXML = ?', $this->getRequest()->getParam('id'));

        if (empty($pedidos)) {
            $this->getResponse()
                ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/relatorios/danfes')
                ->sendResponse();
        }

        $pedidos = $daoPedidos->getAdapter()->fetchRow($pedidos);
        $filePath = Zend_Registry::get('config')->documentos->xmls . $pedidos['ped_nota_caminhoXML'];

        if (file_exists($filePath) && $pedidos) {
            $this->getResponse()->setHeader('Content-Type', 'application/octet-stream')
                ->setHeader('Content-Description', 'File Transfer')
                ->setHeader('Content-Disposition', 'attachment;filename="' . $pedidos['ped_nota_caminhoXML'] . '";')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Pragma', 'public')
                ->sendHeaders();
            @readfile($filePath);
            exit;
        } else {
            $flashMessenger = $this->_helper->getHelper('FlashMessenger');
            $flashMessenger->addMessage('Erro ao tentar baixar o arquivo');
            $this->_redirect('/relatorios/danfes');
        }
    }

    public function visualizarPdfAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Visualizar Danfe');

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoPedidosNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();
        $daoLojasEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance();

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }

        $stringLojas = implode(',', $arrayLojas);

        $pedidos = $daoPedidos->getAdapter()->select()
            ->from($daoPedidos->info('name'))
            ->joinLeft($daoPedidosNotas->info('name'), 'ped_idPedido = ped_nota_idPedido')
            ->joinInner($daoLojas->info('name'), 'ped_idLoja = loja_idLoja')
            ->where("loja_idLoja IN({$stringLojas})")
            ->where('ped_nota_caminhoPDF = ?', $this->getRequest()->getParam('id'));

        if (empty($pedidos)) {
            $this->getResponse()
                ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/relatorios/danfes')
                ->sendResponse();
        }

        $pedidos = $daoPedidos->getAdapter()->fetchRow($pedidos);
        $filePath = Zend_Registry::get('config')->documentos->notasFiscais . $pedidos['ped_nota_caminhoPDF'];

        if (file_exists($filePath) && $pedidos) {
            $this->getResponse()->setHeader('Content-Type', 'application/pdf')
                ->setHeader('Content-Description', 'File Transfer')
                ->setHeader('Content-Disposition', 'inline;filename="' . $pedidos['ped_nota_caminhoPDF'] . '";')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Pragma', 'public')
                ->sendHeaders();
            @readfile($filePath);
            exit;
        } else {
            $flashMessenger = $this->_helper->getHelper('FlashMessenger');
            $flashMessenger->addMessage('Erro ao tentar abrir o arquivo');
            $this->_redirect('/relatorios/danfes');
        }
    }

    public function relatoriosGerenciaisAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Gerenciais');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/relatorios-gerenciais.css', 'screen');

        $daoDownloads = App_Model_DAO_Downloads::getInstance();
        $downloads = $this->view->Permissoes()->selectDownloads()
            ->where('down_status = 1')
            ->group('down_idDownload');

        // Pagina��o
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($downloads);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

    public function downloadAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);

        $this->view->HeadTitle('Relat�rios')->HeadTitle('Download');

        $daoDownloads = App_Model_DAO_Downloads::getInstance();

        $downloads = $daoDownloads->getAdapter()->fetchRow(
            $this->view->Permissoes()
                ->selectDownloads()
                ->where('down_idDownload = ?', $this->getRequest()->getParam('id'))
        );

        if (empty($downloads)) {
            $this->getResponse()
                ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/relatorios/relatorios-gerenciais')
                ->sendResponse();
        }

        $downloads = $daoDownloads->createRow($downloads);
        $filePath = $downloads->getImagem()->getFilePath();

        if (file_exists($filePath)) {
            $this->getResponse()->setHeader('Content-Type', 'application/octet-stream')
                ->setHeader('Content-Description', 'File Transfer')
                ->setHeader('Content-Disposition', 'attachment;filename="' . $downloads->getImagem()->getNome() . '";')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Pragma', 'public')
                ->sendHeaders();
            @readfile($filePath);
            exit;
        } else {
            $flashMessenger = $this->_helper->getHelper('FlashMessenger');
            $flashMessenger->addMessage('Erro ao tentar baixar o arquivo');
            $this->_redirect('/relatorios/relatorios-gerenciais');
        }
    }

    public function pedidosSinteticosAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Sint�ticos');

        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoLoja = App_Model_DAO_Lojas::getInstance();
        $daoGrupoLoja = App_Model_DAO_GrupoLojas::getInstance();
        $daoRede = App_Model_DAO_Redes::getInstance();
        $daoGrupoRede = App_Model_DAO_GrupoRedes::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoRegiao = App_Model_DAO_Regioes::getInstance();
        $daoPedidoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoPedidosItens = App_Model_DAO_Pedidos_Itens::getInstance();
        $daoStatusPedidos = App_Model_DAO_Pedidos_Status::getInstance();
        $daoStatus = App_Model_DAO_Status::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoStatusRelacionados = App_Model_DAO_Status_Relacionados::getInstance();

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);

        $this->view->listaStatus = $daoStatus->fetchAll(
            $daoStatus->select()
                ->where('sta_status = ?', 1)
                ->order('sta_nome')
        );

        $selectStatus = $daoStatusPedidos->select()->from($daoStatusPedidos->info('name'), 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = ped_idPedido');

        $select = $daoPedidos->getAdapter()->select()
            ->from($daoPedidos->info('name'), array('mes' => new Zend_Db_Expr('CONCAT(LPAD(MONTH(ped_dataCadastro), 2, 0))'), 'ped_dataCadastro'))
            ->joinInner($daoLoja->info('name'), 'ped_idLoja = loja_idLoja', null)
            ->joinInner($daoGrupoLoja->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
            ->joinInner($daoRede->info('name'), 'rede_idRede = grp_loja_idRede', null)
            ->joinInner($daoGrupoRede->info('name'), 'rede_idGrupo = grp_rede_idGrupo', null)
            ->joinInner($daoUsuarios->info('name'), 'ped_idUsuario = usr_idUsuario', null)
            ->joinInner($daoLojaEndereco->info('name'), 'loja_end_idLoja = loja_idLoja', null)
            ->joinLeft($daoRegiao->info('name'), 'loja_end_uf = reg_uf', null)
            ->joinInner($daoPedidosItens->info('name'), 'ped_idPedido = ped_item_idPedido', null)
            ->joinInner($daoProdutos->info('name'), 'ped_item_produto = prod_idProduto', array('totalItem' => new Zend_Db_Expr('SUM(ped_item_qtdTotal)')))
            ->joinInner($daoLinhas->info('name'), 'prod_idLinha = lin_idLinha', array('lin_idLinha', 'lin_nome'))
            ->joinLeft($daoStatusPedidos->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})", null)
            ->joinLeft($daoPedidoNotas->info('name'), 'ped_nota_idPedido = ped_idPedido', 'ped_nota_numero', null)
            ->group('lin_idLinha')
            ->where($this->view->Usuario()->quoteIntoUsuarios('ped_idUsuario'))
            ->where($this->view->Combo()->getQuoteLinhas('lin_idLinha'))
            ->group(new Zend_Db_Expr('MONTH(ped_dataCadastro)'))
            ->where('ped_tipo = ?', 0)
            ->where("loja_idLoja IN({$stringLojas})");

        $anos = range(date('Y') - 5, date('Y'));
        $this->view->filtroAnos = array_reverse($anos);

        //filtro de grupo de rede
        if ($this->getRequest()->getParam('grupoderede', false)) {
            $select->where('grp_rede_idGrupo = ?', $this->getRequest()->getParam('grupoderede'));
            $this->view->grupoderede = $this->getRequest()->getParam('grupoderede');
        }
        //filtro de rede
        if ($this->getRequest()->getParam('rede', false)) {
            $select->where('rede_idRede = ?', $this->getRequest()->getParam('rede'));
            $this->view->rede = $this->getRequest()->getParam('rede');
        }
        //filtro de grupo de loja
        if ($this->getRequest()->getParam('grupodeloja', false)) {
            $select->where('grp_loja_idGrupo = ?', $this->getRequest()->getParam('grupodeloja'));
            $this->view->grupodeloja = $this->getRequest()->getParam('grupodeloja');
        }
        //filtro por regiao
        if ($this->getRequest()->getParam('local', false)) {
            $select->where('reg_nome = ?', $this->getRequest()->getParam('local'));
            $this->view->local = $this->getRequest()->getParam('local');
        }
        //filtro por estado
        if ($this->getRequest()->getParam('estado', false)) {
            $select->where('loja_end_uf = ?', $this->getRequest()->getParam('estado'));
            $this->view->estado = $this->getRequest()->getParam('estado');
        }
        //filtro por cidade
        if ($this->getRequest()->getParam('municipio', false)) {
            $select->where('loja_end_cidade = ?', $this->getRequest()->getParam('municipio'));
            $this->view->municipio = $this->getRequest()->getParam('municipio');
        }
        //filtro por linha
        if ($this->getRequest()->getParam('linha', false)) {
            $select->where('lin_idLinha = ?', $this->getRequest()->getParam('linha'));
            $this->view->linha = $this->getRequest()->getParam('linha');
        }
        //filtro por codigo do produto
        if ($this->getRequest()->getParam('produto', false)) {
            $select->where('prod_idProduto = ?', $this->getRequest()->getParam('produto'));
            $this->view->produto = $this->getRequest()->getParam('produto');
        }
        //filtro de numero do Pedido
        if ($this->getRequest()->getParam('pedido', false)) {
            $select->where('ped_idPedido = ?', $this->getRequest()->getParam('pedido'));
            $this->view->pedido = $this->getRequest()->getParam('pedido');
        }
        //filtro por nota fiscal
        if ($this->getRequest()->getParam('nf', false)) {
            $select->where('ped_nota_numero = ?', $this->getRequest()->getParam('nf'));
            $this->view->nf = $this->getRequest()->getParam('nf');
        }
        //filtro de data ou ano
        $ano = $this->getRequest()->getParam('ano', date('Y'));
        $select->where('YEAR(ped_dataCadastro) = ?', $ano);
        $this->view->ano = $ano;


        //filtro por status
        if ($this->getRequest()->getParam('status', '') != '') {
            $listaStatus = $daoStatus->getAdapter()->fetchOne(
                $daoStatus->getAdapter()->select()
                    ->from($daoStatus->info('name'), null)
                    ->joinInner($daoStatusRelacionados->info('name'), 'sta_status_idStatus = sta_idStatus', 'GROUP_CONCAT(sta_status_idStatusRelacionado)')
                    ->where('sta_idStatus = ?', $this->getRequest()->getParam('status'))
                    ->where('sta_status = ?', 1)
            );

            $select->where("ped_status_valor IN(" . ($listaStatus ? $listaStatus : 0) . ")");
            $this->view->status = $this->getRequest()->getParam('status', '');
        }

        $linhas = $daoPedidos->getAdapter()->fetchAll($select);

        $listLinhas = array();
        $totaisPorMes = array();
        $totalGeral = 0;
        foreach ($linhas as $linha) {
            $totalPcModuloLinha = 0;
            $listLinhas[$linha['lin_idLinha']]['id'] = $linha['lin_idLinha'];
            $listLinhas[$linha['lin_idLinha']]['nome'] = $linha['lin_nome'];
            if (isset($listLinhas[$linha['lin_idLinha']]['totalItem'])) {
                $listLinhas[$linha['lin_idLinha']]['totalItem'] += $linha['totalItem'];
            } else {
                $listLinhas[$linha['lin_idLinha']]['totalItem'] = $linha['totalItem'];
            }
            $listLinhas[$linha['lin_idLinha']]['meses'][$linha['mes']]['data'] = "{$this->getRequest()->getParam('ano', date('Y'))}-{$linha['mes']}";
            if (isset($listLinhas[$linha['lin_idLinha']]['meses'][$linha['mes']]['totalItem'])) {
                $listLinhas[$linha['lin_idLinha']]['meses'][$linha['mes']]['totalItem'] += $linha['totalItem'];
            } else {
                $listLinhas[$linha['lin_idLinha']]['meses'][$linha['mes']]['totalItem'] = $linha['totalItem'];
            }
            $totaisPorMes[$linha['mes']] = isset($totaisPorMes[$linha['mes']]) ? $totaisPorMes[$linha['mes']] + $linha['totalItem'] : $linha['totalItem'];
        }
        $this->view->totalGeral = count($totaisPorMes) ? array_sum($totaisPorMes) : 0;
        $this->view->listTotMeses = $totaisPorMes;
        $this->view->listLinhas = $listLinhas;
    }

    public function pedidosAnaliticosAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Anal�ticos');
        $this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');

        $this->view->headLink()->appendStylesheet('scripts/datepicker/css/ui-lightness/jquery-ui-1.10.3.custom.css', 'screen');
        $this->view->HeadScript()->appendFile('scripts/datepicker/js/jquery-ui-1.10.3.custom.js');
        $this->view->HeadScript()->appendFile('scripts/datepicker/js/jquery.ui.datepicker-pt-BR.js');

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);

        $anos = range(date('Y') - 5, date('Y'));
        $this->view->filtroAnos = array_reverse($anos);
    }

    public function pedidosAnaliticosAjaxAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'text/json');
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);

        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoLoja = App_Model_DAO_Lojas::getInstance();
        $daoGrupoLoja = App_Model_DAO_GrupoLojas::getInstance();
        $daoRede = App_Model_DAO_Redes::getInstance();
        $daoGrupoRede = App_Model_DAO_GrupoRedes::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoRegiao = App_Model_DAO_Regioes::getInstance();
        $daoPedidoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoPedidosItens = App_Model_DAO_Pedidos_Itens::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoStatusPedidos = App_Model_DAO_Pedidos_Status::getInstance();
        $daoStatus = App_Model_DAO_Status::getInstance();
        $daoStatusRelacionados = App_Model_DAO_Status_Relacionados::getInstance();
        $daoReclamacoesStatus = App_Model_DAO_SugestoesReclamacoes_Status::getInstance();
        $daoReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();

        $selectStatus = $daoStatusPedidos->select()
            ->from($daoStatusPedidos->info('name'), 'MAX(ped_status_idStatus)')
            ->where('ped_status_idPedido = ped_idPedido');

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);

        $select = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoLoja->info('name'), 'ped_idLoja = loja_idLoja')
            ->joinInner($daoGrupoLoja->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
            ->joinInner($daoRede->info('name'), 'rede_idRede = grp_loja_idRede', null)
            ->joinInner($daoGrupoRede->info('name'), 'rede_idGrupo = grp_rede_idGrupo', null)
            ->joinInner($daoUsuarios->info('name'), 'ped_idUsuario = usr_idUsuario', null)
            ->joinInner($daoLojaEndereco->info('name'), 'loja_end_idLoja = loja_idLoja', null)
            ->joinInner($daoRegiao->info('name'), 'loja_end_uf = reg_uf', null)
            ->joinLeft($daoPedidoNotas->info('name'), 'ped_nota_idPedido = ped_idPedido', 'ped_nota_numero')
            ->joinInner($daoPedidosItens->info('name'), 'ped_item_idPedido = ped_idPedido')
            ->joinLeft($daoProdutos->info('name'), 'prod_idProduto = ped_item_produto')
            ->joinInner($daoLinhas->info('name'), 'prod_idLinha = lin_idLinha', null)
            ->joinLeft($daoStatusPedidos->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
            //->where($this->view->Usuario()->quoteIntoUsuarios('ped_idUsuario'))
            ->where("loja_idLoja IN({$stringLojas})")
            ->group('ped_idPedido')
            ->order('ped_dataCadastro DESC');

        $filter = false;
        //filtro de grupo de rede
        if ($this->getRequest()->getParam('grupoderede', false)) {
            $select->where('grp_rede_idGrupo = ?', $this->getRequest()->getParam('grupoderede'));
            $filter = true;
        }
        //filtro de rede
        if ($this->getRequest()->getParam('rede', false)) {
            $select->where('rede_idRede = ?', $this->getRequest()->getParam('rede'));
            $filter = true;
        }
        //filtro de grupo de loja
        if ($this->getRequest()->getParam('grupodeloja', false)) {
            $select->where('grp_loja_idGrupo = ?', $this->getRequest()->getParam('grupodeloja'));
            $filter = true;
        }
        //filtro de loja
        if ($this->getRequest()->getParam('loja', false)) {
            $select->where('loja_idLoja = ?', $this->getRequest()->getParam('loja'));
            $filter = true;
        }
        //filtro de cnpj
        if ($this->getRequest()->getParam('cnpj', false)) {
            $select->where('loja_cnpj = ?', $this->getRequest()->getParam('cnpj'));
            $filter = true;
        }
        //filtro por regiao
        if ($this->getRequest()->getParam('local', false)) {
            $select->where('reg_nome = ?', $this->getRequest()->getParam('local'));
            $filter = true;
        }
        //filtro por estado
        if ($this->getRequest()->getParam('estado', false)) {
            $select->where('loja_end_uf = ?', $this->getRequest()->getParam('estado'));
            $filter = true;
        }
        //filtro por cidade
        if ($this->getRequest()->getParam('municipio', false)) {
            $select->where('loja_end_cidade = ?', $this->getRequest()->getParam('municipio'));
            $filter = true;
        }
        //filtro por linha
        if ($this->getRequest()->getParam('linha', false)) {
            $select->where('lin_idLinha = ?', $this->getRequest()->getParam('linha'));
            $filter = true;
        }
        //filtro por codigo do produto
        if ($this->getRequest()->getParam('produto', false)) {
            $select->where('prod_idProduto = ?', $this->getRequest()->getParam('produto'));
            $filter = true;
        }
        //filtro de numero do Pedido
        if ($this->getRequest()->getParam('pedido', false)) {
            $select->where('ped_idPedido = ?', $this->getRequest()->getParam('pedido'));
            $filter = true;
        }
        //filtro por nota fiscal
        if ($this->getRequest()->getParam('nf', false)) {
            $select->where('ped_nota_numero = ?', $this->getRequest()->getParam('nf'));
            $filter = true;
        }

        //filtro por status
        if ($this->getRequest()->getParam('status') == 'abertos') {
            $listaStatus = $daoStatus->getAdapter()->fetchOne(
                $daoStatus->getAdapter()->select()
                    ->from($daoStatus->info('name'), null)
                    ->joinInner($daoStatusRelacionados->info('name'), 'sta_status_idStatus = sta_idStatus', 'GROUP_CONCAT(sta_status_idStatusRelacionado)')
                    ->where('sta_idStatus NOT IN (17,15)')
                    ->where('sta_status = ?', 1)
            );

            $select->where("ped_status_valor IN({$listaStatus})");
        } else {
            $listaStatus = $daoStatus->getAdapter()->fetchOne(
                $daoStatus->getAdapter()->select()
                    ->from($daoStatus->info('name'), null)
                    ->joinInner($daoStatusRelacionados->info('name'), 'sta_status_idStatus = sta_idStatus', 'GROUP_CONCAT(sta_status_idStatusRelacionado)')
                    ->where('sta_idStatus IN (17,15)')
                    ->where('sta_status = ?', 1)
            );

            $select->where("ped_status_valor IN({$listaStatus})");
        }

        //filtro por sub status
        if ($this->getRequest()->getParam('substatus') !== false && $this->getRequest()->getParam('substatus') !== null && $this->getRequest()->getParam('substatus') != '') {
            $select->where('ped_status_valor = ?', $this->getRequest()->getParam('substatus'));
            $filter = true;
        }

        if ($de = $this->getRequest()->getParam('de')) {
            $select->where('DATE(ped_dataCadastro) >= ?', App_Funcoes_Date::conversion($de));
            $filter = true;
        }

        if ($ate = $this->getRequest()->getParam('ate')) {
            $select->where('DATE(ped_dataCadastro) <= ?', App_Funcoes_Date::conversion($ate));
            $filter = true;
        }

        if ($this->getRequest()->getParam('periodo')) {
            $select->where('DATEDIFF(NOW(), ped_dataCadastro) <= ?', $this->getRequest()->getParam('periodo'));
            $filter = true;
        }

        // Pagina��o
        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $retorno['last'] = $paginator->getPages()->last;
        $retorno['pedidos'] = array();
        foreach ($paginator as $arrPedido) {
            $pedido = App_Model_DAO_Pedidos::getInstance()->createRow($arrPedido);
            if (!$pedido->getUltimoStatus()) {
                continue;
            }
            $statusOriginal = App_Funcoes_Rotulos::$statusPedidos[$pedido->getUltimoStatus()->getValor()];
            $statusExibicao = $pedido->getStatusExibicao();
            $uf = $pedido->getLoja()->getEnderecoPadrao();
            if ($statusExibicao) {
                $statusOriginal = $statusExibicao['sta_nome'];
            }

            $dadosRetorno = array();
            $dadosRetorno['style'] = ($pedido->getUltimoStatus()->getValor() == 9) ? "green" : '';
            $dadosRetorno['url'] = "relatorios/produtos?pedido={$pedido->getCodigo()}";
            $dadosRetorno['codigo'] = $pedido->getCodigo();
            $dadosRetorno['ped_nota_numero'] = $arrPedido['ped_nota_numero'] ? $arrPedido['ped_nota_numero'] : '-';
            $dadosRetorno['data_cadastro'] = App_Funcoes_Date::conversion(substr($pedido->getDataCadastro(), 0, 10));
            $dadosRetorno['data_previsao'] = App_Funcoes_Date::conversion(substr($pedido->getPrevisaoEntrega(), 0, 10));
            $dadosRetorno['data_entrega'] = ($pedido->getUltimoStatus()->getValor() == 9) ? App_Funcoes_Date::conversion(substr($pedido->getUltimoStatus()->getData(), 0, 10)) : '-';
            $dadosRetorno['loja'] = $pedido->getLoja()->getCodigo();
            $dadosRetorno['grupo_loja'] = $pedido->getLoja()->getGrupo()->getCodigo();
            $dadosRetorno['rede'] = $pedido->getLoja()->getGrupo()->getRede()->getNome();
            $dadosRetorno['uf'] = $uf['loja_end_uf'];
            $dadosRetorno['status'] = $statusOriginal ? $statusOriginal : 'N�o informado';
            $dadosRetorno['ocorrencia'] = '';

            $contatos = $daoReclamacoes->fetchAll(
                $daoReclamacoes->select()->from($daoReclamacoes)
                    ->where('sug_rec_idPedido = ?', $pedido->getCodigo())
                    ->order('sug_rec_dataCadastro DESC')
            );
            $dadosRetorno['contatos'] = array();
            foreach ($contatos as $key => $contato) {

                $statusRetorno = $daoReclamacoesStatus->getAdapter()->fetchAll(
                    $daoReclamacoesStatus->select()->from($daoReclamacoesStatus)
                        ->where('sug_rec_status_idSugestaoReclamacao = ?', $contato->getCodigo())
                        ->order('sug_rec_status_data DESC')
                );

                $historico = array();
                foreach ($statusRetorno as $statusDetalhe) {
                    $historico[] = array(
                        'codigo' => $statusDetalhe['sug_rec_status_idStatus'],
                        'data' => App_Funcoes_Date::conversion($statusDetalhe['sug_rec_status_data']),
                        'atendente' => $statusDetalhe['sug_rec_nomeAtendente'],
                        'descricao' => $statusDetalhe['sug_rec_status_descricao'],
                        'status' => App_Funcoes_Rotulos::$statusSugestoesReclamacoes[$statusDetalhe['sug_rec_status_codigo']]
                    );
                }

                $dadosRetorno['contatos'][] = array(
                    'codigo' => $contato->getCodigo(),
                    'protocolo' => $contato->getProtocolo(),
                    'nome' => $contato->getNome(),
                    'email' => $contato->getEmail(),
                    'descricao' => $contato->getMensagem(),
                    'assunto' => $contato->getAssunto()->getNome(),
                    'motivo' => $contato->getMotivo() ? $contato->getMotivo()->getNome() : '',
                    'data' => App_Funcoes_Date::conversion($contato->getDataCadastro()),
                    'status' => App_Funcoes_Rotulos::$statusSugestoesReclamacoes[$contato->getStatus()],
                    'historico' => $historico
                );

                if (!$key) {
                    $dadosRetorno['ocorrencia'] = App_Funcoes_Date::conversion(substr($contato->getDataCadastro(), 0, 10)) . '<br>' . $contato->getMensagem();
                }
            }

            $retorno['pedidos'][] = $dadosRetorno;
        }

        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function produtosAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Produtos');

        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoLoja = App_Model_DAO_Lojas::getInstance();
        $daoGrupoLoja = App_Model_DAO_GrupoLojas::getInstance();
        $daoRede = App_Model_DAO_Redes::getInstance();
        $daoGrupoRede = App_Model_DAO_GrupoRedes::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoRegiao = App_Model_DAO_Regioes::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoPedidosItens = App_Model_DAO_Pedidos_Itens::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoStatusPedidos = App_Model_DAO_Pedidos_Status::getInstance();
        $daoStatus = App_Model_DAO_Status::getInstance();
        $daoStatusRelacionados = App_Model_DAO_Status_Relacionados::getInstance();

        $this->view->listaStatus = $daoStatus->fetchAll(
            $daoStatus->select()
                ->where('sta_status = ?', 1)
                ->order('sta_nome')
        );

        $selectStatus = $daoStatusPedidos->select()->from($daoStatusPedidos->info('name'), 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = P1.ped_idPedido');

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);

        $select = $daoPedidosItens->getAdapter()->select()->from(
                array(
                'I1' => $daoPedidosItens->info('name')), array(
                'I1.ped_item_qtdTotal',
                'qtdeEntregue' => new Zend_Db_Expr('IF(ped_status_valor = 9, `I1`.`ped_item_qtdTotal`, 0)'),
                'idReposicao' => 'I1.ped_item_idReposicao'
                )
            )
            ->joinInner(array('P1' => $daoPedidos->info('name')), 'I1.ped_item_idPedido = P1.ped_idPedido', array('P1.ped_idPedido', 'P1.ped_dataCadastro'))
            ->joinLeft($daoUsuarios->info('name'), 'P1.ped_idUsuario = usr_idUsuario', null)
            ->joinInner($daoLoja->info('name'), 'P1.ped_idLoja = loja_idLoja', null)
            ->joinInner($daoGrupoLoja->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
            ->joinInner($daoRede->info('name'), 'rede_idRede = grp_loja_idRede', null)
            ->joinInner($daoGrupoRede->info('name'), 'rede_idGrupo = grp_rede_idGrupo', null)
            ->joinInner($daoLojaEndereco->info('name'), 'loja_end_idLoja = loja_idLoja', null)
            ->joinInner($daoRegiao->info('name'), 'loja_end_uf = reg_uf', null)
            ->joinLeft($daoProdutos->info('name'), 'prod_idProduto = I1.ped_item_produto', array('prod_idProduto', 'prod_descricao', 'prod_qtdePacotes', 'prod_pcModulo'))
            ->joinInner($daoLinhas->info('name'), 'prod_idLinha = lin_idLinha', 'lin_nome')
            ->joinLeft($daoStatusPedidos->info('name'), "P1.ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
            //->where($this->view->Usuario()->quoteIntoUsuarios('P1.ped_idUsuario'))
            ->where("loja_idLoja IN({$stringLojas})")
            ->group('I1.ped_item_idItem');

        $anos = range(date('Y') - 5, date('Y'));
        $this->view->filtroAnos = array_reverse($anos);

        //filtro de grupo de rede
        if ($this->getRequest()->getParam('grupoderede', false)) {
            $select->where('grp_rede_idGrupo = ?', $this->getRequest()->getParam('grupoderede'));
            $this->view->grupoderede = $this->getRequest()->getParam('grupoderede');
        }
        //filtro de rede
        if ($this->getRequest()->getParam('rede', false)) {
            $select->where('rede_idRede = ?', $this->getRequest()->getParam('rede'));
            $this->view->rede = $this->getRequest()->getParam('rede');
        }
        //filtro de grupo de loja
        if ($this->getRequest()->getParam('grupodeloja', false)) {
            $select->where('grp_loja_idGrupo = ?', $this->getRequest()->getParam('grupodeloja'));
            $this->view->grupodeloja = $this->getRequest()->getParam('grupodeloja');
        }
        //filtro por regiao
        if ($this->getRequest()->getParam('local', false)) {
            $select->where('reg_nome = ?', $this->getRequest()->getParam('local'));
            $this->view->local = $this->getRequest()->getParam('local');
        }
        //filtro por estado
        if ($this->getRequest()->getParam('estado', false)) {
            $select->where('loja_end_uf = ?', $this->getRequest()->getParam('estado'));
            $this->view->estado = $this->getRequest()->getParam('estado');
        }
        //filtro por cidade
        if ($this->getRequest()->getParam('municipio', false)) {
            $select->where('loja_end_cidade = ?', $this->getRequest()->getParam('municipio'));
            $this->view->municipio = $this->getRequest()->getParam('municipio');
        }
        //filtro por linha
        if ($this->getRequest()->getParam('linha', false)) {
            $select->where('lin_idLinha = ?', $this->getRequest()->getParam('linha'));
            $this->view->linha = $this->getRequest()->getParam('linha');
        }
        //filtro por codigo do produto
        if ($this->getRequest()->getParam('produto', false)) {
            $select->where('P1.prod_idProduto = ?', $this->getRequest()->getParam('produto'));
            $this->view->produto = $this->getRequest()->getParam('produto');
        }
        //filtro de numero do Pedido
        if ($this->getRequest()->getParam('pedido', false)) {
            $select->where('P1.ped_idPedido = ?', $this->getRequest()->getParam('pedido'));
            $this->view->pedido = $this->getRequest()->getParam('pedido');
        }
        //filtro por nota fiscal
        if ($this->getRequest()->getParam('nf', false)) {
            $select->where('P1.ped_nota_numero = ?', $this->getRequest()->getParam('nf'));
            $this->view->nf = $this->getRequest()->getParam('nf');
        }
        //filtro de ano
        if ($this->getRequest()->getParam('ano')) {
            $select->where('YEAR(P1.ped_dataCadastro) = ?', $this->getRequest()->getParam('ano'));
            $this->view->ano = $this->getRequest()->getParam('ano');
        }

        //filtro por status
        if ($this->getRequest()->getParam('status', '') != '') {
            $listaStatus = $daoStatus->getAdapter()->fetchOne(
                $daoStatus->getAdapter()->select()
                    ->from($daoStatus->info('name'), null)
                    ->joinInner($daoStatusRelacionados->info('name'), 'sta_status_idStatus = sta_idStatus', 'GROUP_CONCAT(sta_status_idStatusRelacionado)')
                    ->where('sta_idStatus = ?', $this->getRequest()->getParam('status'))
                    ->where('sta_status = ?', 1)
            );

            $select->where("ped_status_valor IN({$listaStatus})");
            $this->view->status = $this->getRequest()->getParam('status', '');
        }

        // Pagina��o
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

    public function royaltiesAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Royalties');

        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        $daoLoja = App_Model_DAO_Lojas::getInstance();
        $daoGrupoLoja = App_Model_DAO_GrupoLojas::getInstance();
        $daoRede = App_Model_DAO_Redes::getInstance();
        $daoGrupoRede = App_Model_DAO_GrupoRedes::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoRegiao = App_Model_DAO_Regioes::getInstance();
        $daoNotasBoletos = App_Model_DAO_Pedidos_Notas_Boletos::getInstance();
        $daoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoPedidosItens = App_Model_DAO_Pedidos_Itens::getInstance();
        $daoStatusPedidos = App_Model_DAO_Pedidos_Status::getInstance();
        $daoStatus = App_Model_DAO_Status::getInstance();
        $daoStatusRelacionados = App_Model_DAO_Status_Relacionados::getInstance();

        $this->view->listaStatus = $daoStatus->fetchAll(
            $daoStatus->select()
                ->where('sta_status = ?', 1)
                ->order('sta_nome')
        );

        $selectStatus = $daoStatusPedidos->select()->from($daoStatusPedidos->info('name'), 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = ped_idPedido');

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);

        $subSelect = $daoNotasBoletos->getAdapter()->select()
            ->from($daoNotasBoletos->info('name'), array(
                'mes' => new Zend_Db_Expr('MONTH(ped_nota_bol_dataEmissao)'),
                'ano' => new Zend_Db_Expr('YEAR(ped_nota_bol_dataEmissao)'),
                'ped_nota_bol_dataEmissao',
                'ped_nota_bol_valor'
            ))
            ->joinInner($daoNotas->info('name'), 'ped_nota_bol_notaFiscal = ped_nota_numero', null)
            ->joinLeft($daoPedidos->info('name'), 'ped_nota_idPedido = ped_idPedido', 'ped_dataCadastro')
            ->joinLeft($daoPedidosItens->info('name'), 'ped_idPedido = ped_item_idPedido', null)
            ->joinInner($daoLoja->info('name'), 'ped_idLoja = loja_idLoja', null)
            ->joinInner($daoGrupoLoja->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
            ->joinInner($daoRede->info('name'), 'rede_idRede = grp_loja_idRede', null)
            ->joinInner($daoGrupoRede->info('name'), 'rede_idGrupo = grp_rede_idGrupo', null)
            ->joinInner($daoLojaEndereco->info('name'), 'loja_end_idLoja = loja_idLoja', null)
            ->joinInner($daoRegiao->info('name'), 'loja_end_uf = reg_uf', array('reg_nome', 'reg_idRegiao'))
            ->joinLeft($daoProdutos->info('name'), 'prod_idProduto = ped_item_produto', array('prod_idProduto', 'prod_descricao', 'prod_qtdePacotes', 'prod_pcModulo'))
            ->joinLeft($daoLinhas->info('name'), 'prod_idLinha = lin_idLinha', 'lin_nome')
            ->joinLeft($daoStatusPedidos->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
            ->group('ped_nota_numero')
            ->where("loja_idLoja IN({$stringLojas})");
        //->where($this->view->Usuario()->quoteIntoUsuarios('ped_idUsuario'))
        //->group('reg_nome');

        $select = $daoNotasBoletos->getAdapter()->select()
            ->from(array('dt' => new Zend_Db_Expr('(' . $subSelect . ')')), array(
                'total' => new Zend_Db_Expr('SUM(ped_nota_bol_valor)'),
                '*'
            ))
            ->group('mes')
            ->group('reg_nome');

        $anos = range(date('Y') - 5, date('Y'));
        $this->view->filtroAnos = array_reverse($anos);

        //filtro de grupo de rede
        if ($this->getRequest()->getParam('grupoderede', false)) {
            $select->where('grp_rede_idGrupo = ?', $this->getRequest()->getParam('grupoderede'));
            $this->view->grupoderede = $this->getRequest()->getParam('grupoderede');
        }

        //filtro de rede
        if ($this->getRequest()->getParam('rede', false)) {
            $select->where('rede_idRede = ?', $this->getRequest()->getParam('rede'));
            $this->view->rede = $this->getRequest()->getParam('rede');
        }

        //filtro de grupo de loja
        if ($this->getRequest()->getParam('grupodeloja', false)) {
            $select->where('grp_loja_idGrupo = ?', $this->getRequest()->getParam('grupodeloja'));
            $this->view->grupodeloja = $this->getRequest()->getParam('grupodeloja');
        }

        //filtro por regiao
        if ($this->getRequest()->getParam('local', false)) {
            $select->where('reg_nome = ?', $this->getRequest()->getParam('local'));
            $this->view->local = $this->getRequest()->getParam('local');
        }

        //filtro por estado
        if ($this->getRequest()->getParam('estado', false)) {
            $select->where('loja_end_uf = ?', $this->getRequest()->getParam('estado'));
            $this->view->estado = $this->getRequest()->getParam('estado');
        }

        //filtro por cidade
        if ($this->getRequest()->getParam('municipio', false)) {
            $select->where('loja_end_cidade = ?', $this->getRequest()->getParam('municipio'));
            $this->view->municipio = $this->getRequest()->getParam('municipio');
        }

        //filtro por linha
        if ($this->getRequest()->getParam('linha', false)) {
            $select->where('lin_idLinha = ?', $this->getRequest()->getParam('linha'));
            $this->view->linha = $this->getRequest()->getParam('linha');
        }

        //filtro por codigo do produto
        if ($this->getRequest()->getParam('produto', false)) {
            $select->where('prod_idProduto = ?', $this->getRequest()->getParam('produto'));
            $this->view->produto = $this->getRequest()->getParam('produto');
        }

        //filtro de numero do Pedido
        if ($this->getRequest()->getParam('pedido', false)) {
            $select->where('ped_idPedido = ?', $this->getRequest()->getParam('pedido'));
            $this->view->pedido = $this->getRequest()->getParam('pedido');
        }

        //filtro por nota fiscal
        if ($this->getRequest()->getParam('nf', false)) {
            $select->where('ped_nota_numero = ?', $this->getRequest()->getParam('nf'));
            $this->view->nf = $this->getRequest()->getParam('nf');
        }

        //filtro por status
        if ($this->getRequest()->getParam('status', '') != '') {
            $listaStatus = $daoStatus->getAdapter()->fetchOne(
                $daoStatus->getAdapter()->select()
                    ->from($daoStatus->info('name'), null)
                    ->joinInner($daoStatusRelacionados->info('name'), 'sta_status_idStatus = sta_idStatus', 'GROUP_CONCAT(sta_status_idStatusRelacionado)')
                    ->where('sta_idStatus = ?', $this->getRequest()->getParam('status'))
                    ->where('sta_status = ?', 1)
            );

            $select->where("ped_status_valor IN({$listaStatus})");
            $this->view->status = $this->getRequest()->getParam('status', '');
        }

        //filtro de data ou ano
        if ($this->getRequest()->getParam('data', false)) {
            $select->where('LEFT(ped_nota_bol_dataEmissao,7) = ?', $this->getRequest()->getParam('data'));
            $this->view->data = $this->getRequest()->getParam('data');
        } else {
            $ano = $this->getRequest()->getParam('ano', date('Y'));
            $select->where('YEAR(ped_nota_bol_dataEmissao) = ?', $ano);
            $this->view->ano = $ano;
        }

        //echo "<pre>";
        //$sqle = $select->__toString();
        //print_r($sqle);
        //echo "<br>============================<br>";
        //print_r($select);
        //die();

        $royalties = $daoNotasBoletos->getAdapter()->fetchAll($select);

        $listRoyalties = array();
        $totaisPorMes = array();

        //echo "<pre>";
        //print_r($royalties);
        //die;

        foreach ($royalties as $royaltie) {
            if ($royaltie['reg_idRegiao'] == "") {
                $royaltie['reg_nome'] = "NUL";
            }

            $listRoyalties[$royaltie['reg_nome']]['regiao'] = $royaltie['reg_nome'];
            $listRoyalties[$royaltie['reg_nome']]['meses'][$royaltie['mes']]['total'] = $royaltie['total'];

            if (isset($listRoyalties[$royaltie['reg_nome']]['total'])) {
                $listRoyalties[$royaltie['reg_nome']]['total'] += $royaltie['total'];
            } else {
                $listRoyalties[$royaltie['reg_nome']]['total'] = $royaltie['total'];
            }

            /* $listRoyalties[$royaltie['reg_idRegiao']]['regiao'] = $royaltie['reg_nome'];
              $listRoyalties[$royaltie['reg_idRegiao']]['meses'][$royaltie['mes']]['total'] = $royaltie['total'];

              if (isset($listRoyalties[$royaltie['reg_idRegiao']]['total'])) {
              $listRoyalties[$royaltie['reg_idRegiao']]['total'] += $royaltie['total'];
              } else {
              $listRoyalties[$royaltie['reg_idRegiao']]['total'] = $royaltie['total'];
              } */

            $totaisPorMes[$royaltie['mes']] = isset($totaisPorMes[$royaltie['mes']]) ? $totaisPorMes[$royaltie['mes']] + $royaltie['total'] : $royaltie['total'];
        }

        $this->view->totalGeral = count($totaisPorMes) ? array_sum($totaisPorMes) : 0;
        $this->view->listTotMeses = $totaisPorMes;
        $this->view->listRoyalties = $listRoyalties;

        //echo "<pre>";
        //print_r($listRoyalties);
        //die;
    }

    public function royaltiesAnaliticosAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Royalties Anal�ticos');

        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();
        $daoGrupoLoja = App_Model_DAO_GrupoLojas::getInstance();
        $daoRede = App_Model_DAO_Redes::getInstance();
        $daoGrupoRede = App_Model_DAO_GrupoRedes::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoRegiao = App_Model_DAO_Regioes::getInstance();
        $daoLojasEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoRegioes = App_Model_DAO_Regioes::getInstance();
        $daoNotasBoletos = App_Model_DAO_Pedidos_Notas_Boletos::getInstance();
        $daoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoPedidosItens = App_Model_DAO_Pedidos_Itens::getInstance();
        $daoStatusPedidos = App_Model_DAO_Pedidos_Status::getInstance();
        $daoStatus = App_Model_DAO_Status::getInstance();
        $daoStatusRelacionados = App_Model_DAO_Status_Relacionados::getInstance();

        $this->view->listaStatus = $daoStatus->fetchAll(
            $daoStatus->select()
                ->where('sta_status = ?', 1)
                ->order('sta_nome')
        );

        $selectStatus = $daoStatusPedidos->select()->from($daoStatusPedidos->info('name'), 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = ped_idPedido');

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);

        $select = $daoNotas->getAdapter()->select()
            ->from($daoNotas->info('name'), null)
            ->joinLeft($daoNotasBoletos->info('name'), 'ped_nota_bol_notaFiscal = ped_nota_numero', array(
                'ped_nota_bol_notaFiscal',
                'ped_nota_bol_dataEmissao',
                'ped_nota_bol_cliente',
                'ped_nota_bol_valor',
                'ped_nota_bol_dataVencimento'
            ))
            ->joinInner($daoLojas->info('name'), 'loja_idLoja = ped_nota_bol_cliente', null)
            ->joinInner($daoLojasEnderecos->info('name'), 'loja_idLoja = loja_end_idLoja', null)
            ->joinInner($daoRegioes->info('name'), 'reg_uf = loja_end_uf', array('reg_idRegiao', 'reg_nome'))
            ->joinLeft($daoPedidos->info('name'), 'ped_nota_idPedido = ped_idPedido', 'ped_dataCadastro')
            ->joinLeft($daoPedidosItens->info('name'), 'ped_idPedido = ped_item_idPedido', null)
            ->joinInner($daoGrupoLoja->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
            ->joinInner($daoRede->info('name'), 'rede_idRede = grp_loja_idRede', null)
            ->joinInner($daoGrupoRede->info('name'), 'rede_idGrupo = grp_rede_idGrupo', null)
            ->joinLeft($daoProdutos->info('name'), 'prod_idProduto = ped_item_produto', array('prod_idProduto', 'prod_descricao', 'prod_qtdePacotes', 'prod_pcModulo'))
            ->joinLeft($daoLinhas->info('name'), 'prod_idLinha = lin_idLinha', 'lin_nome')
            ->joinLeft($daoStatusPedidos->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
            ->group('ped_nota_numero')
            ->where("ped_nota_cliente IN({$stringLojas})");
        //->where($this->view->Usuario()->quoteIntoUsuarios('ped_idUsuario'))

        $anos = range(date('Y') - 5, date('Y'));
        $this->view->filtroAnos = array_reverse($anos);

        //filtro de grupo de rede
        if ($this->getRequest()->getParam('grupoderede', false)) {
            $select->where('grp_rede_idGrupo = ?', $this->getRequest()->getParam('grupoderede'));
            $this->view->grupoderede = $this->getRequest()->getParam('grupoderede');
        }
        //filtro de rede
        if ($this->getRequest()->getParam('rede', false)) {
            $select->where('rede_idRede = ?', $this->getRequest()->getParam('rede'));
            $this->view->rede = $this->getRequest()->getParam('rede');
        }
        //filtro de grupo de loja
        if ($this->getRequest()->getParam('grupodeloja', false)) {
            $select->where('grp_loja_idGrupo = ?', $this->getRequest()->getParam('grupodeloja'));
            $this->view->grupodeloja = $this->getRequest()->getParam('grupodeloja');
        }
        //filtro por regiao
        if ($this->getRequest()->getParam('local', false)) {
            $select->where('reg_nome = ?', $this->getRequest()->getParam('local'));
            $this->view->local = $this->getRequest()->getParam('local');
        }
        //filtro por estado
        if ($this->getRequest()->getParam('estado', false)) {
            $select->where('loja_end_uf = ?', $this->getRequest()->getParam('estado'));
            $this->view->estado = $this->getRequest()->getParam('estado');
        }
        //filtro por cidade
        if ($this->getRequest()->getParam('municipio', false)) {
            $select->where('loja_end_cidade = ?', $this->getRequest()->getParam('municipio'));
            $this->view->municipio = $this->getRequest()->getParam('municipio');
        }
        //filtro por linha
        if ($this->getRequest()->getParam('linha', false)) {
            $select->where('lin_idLinha = ?', $this->getRequest()->getParam('linha'));
            $this->view->linha = $this->getRequest()->getParam('linha');
        }
        //filtro por codigo do produto
        if ($this->getRequest()->getParam('produto', false)) {
            $select->where('prod_idProduto = ?', $this->getRequest()->getParam('produto'));
            $this->view->produto = $this->getRequest()->getParam('produto');
        }
        //filtro de numero do Pedido
        if ($this->getRequest()->getParam('pedido', false)) {
            $select->where('ped_idPedido = ?', $this->getRequest()->getParam('pedido'));
            $this->view->pedido = $this->getRequest()->getParam('pedido');
        }
        //filtro por nota fiscal
        if ($this->getRequest()->getParam('nf', false)) {
            $select->where('ped_nota_bol_notaFiscal = ?', $this->getRequest()->getParam('nf'));
            $this->view->nf = $this->getRequest()->getParam('nf');
        }

        //filtro por status
        if ($this->getRequest()->getParam('status', '') != '') {
            $listaStatus = $daoStatus->getAdapter()->fetchOne(
                $daoStatus->getAdapter()->select()
                    ->from($daoStatus->info('name'), null)
                    ->joinInner($daoStatusRelacionados->info('name'), 'sta_status_idStatus = sta_idStatus', 'GROUP_CONCAT(sta_status_idStatusRelacionado)')
                    ->where('sta_idStatus = ?', $this->getRequest()->getParam('status'))
                    ->where('sta_status = ?', 1)
            );

            $select->where("ped_status_valor IN({$listaStatus})");
            $this->view->status = $this->getRequest()->getParam('status', '');
        }

        //filtro de data ou ano
        if ($this->getRequest()->getParam('data', false)) {
            $select->where('LEFT(ped_nota_bol_dataEmissao,7) = ?', $this->getRequest()->getParam('data'));
            $this->view->data = $this->getRequest()->getParam('data');
        }
        if ($this->getRequest()->getParam('ano')) {
            $ano = $this->getRequest()->getParam('ano', date('Y'));
            $select->where('YEAR(ped_nota_bol_dataEmissao) = ?', $ano);
            $this->view->ano = $ano;
        }


        // Pagina��o
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

    public function reclamacoesAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Reclama��es');

        $daoReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();

        $daoReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();
        $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
        $daoRedes = App_Model_DAO_Redes::getInstance();
        $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
        $daoAssuntos = App_Model_DAO_SugestoesReclamacoes_Assuntos::getInstance();
        $daoMotivos = App_Model_DAO_SugestoesReclamacoes_Assuntos_Motivos::getInstance();

        $this->view->assuntos = $daoAssuntos->fetchAll(
            $daoAssuntos->select()->from($daoAssuntos)
        );

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);


        $selectLojas = $daoReclamacoes->getAdapter()
            ->select()->from($daoReclamacoes->info('name'), array('*', 'data' => new Zend_Db_Expr('LEFT(sug_rec_dataCadastro, 7)')))
            ->joinInner($daoAssuntos->info('name'), 'sug_rec_idAssunto = rec_ass_idAssunto', array('rec_ass_nome', 'rec_ass_idAssunto'))
            ->joinLeft($daoMotivos->info('name'), 'rec_ass_mot_idAssunto = rec_ass_idAssunto', array('rec_ass_mot_nome', 'rec_ass_mot_idMotivo'))
            ->joinInner($daoUsuarios->info('name'), 'sug_rec_idUsuario = usr_idUsuario', 'usr_idLoja')
            ->joinInner($daoLojas->info('name'), 'usr_idLoja = loja_idLoja', null)
            ->joinInner($daoGrupoLojas->info('name'), 'loja_idGrupo = grp_loja_idGrupo', 'grp_loja_idGrupo')
            ->joinInner($daoRedes->info('name'), 'grp_loja_idRede = rede_idRede', 'rede_idRede')
            ->joinInner($daoGrupoRedes->info('name'), 'grp_rede_idGrupo = rede_idRede', 'grp_rede_idGrupo')
            //->where($this->view->Usuario()->quoteIntoUsuarios('sug_rec_idUsuario'))
            ->where("loja_idLoja IN({$stringLojas})")
            ->group('sug_rec_idSugestaoReclamacao');

        $selectGrupoLojas = $daoReclamacoes->getAdapter()
            ->select()->from($daoReclamacoes->info('name'), array('*', 'data' => new Zend_Db_Expr('LEFT(sug_rec_dataCadastro, 7)')))
            ->joinInner($daoAssuntos->info('name'), 'sug_rec_idAssunto = rec_ass_idAssunto', array('rec_ass_nome', 'rec_ass_idAssunto'))
            ->joinLeft($daoMotivos->info('name'), 'rec_ass_mot_idAssunto = rec_ass_idAssunto', array('rec_ass_mot_nome', 'rec_ass_mot_idMotivo'))
            ->joinInner($daoUsuarios->info('name'), 'sug_rec_idUsuario = usr_idUsuario', 'usr_idLoja')
            ->joinInner($daoGrupoLojas->info('name'), 'usr_idGrupoLoja = grp_loja_idGrupo', 'grp_loja_idGrupo')
            ->joinInner($daoRedes->info('name'), 'grp_loja_idRede = rede_idRede', 'rede_idRede')
            ->joinInner($daoGrupoRedes->info('name'), 'grp_rede_idGrupo = rede_idRede', 'grp_rede_idGrupo')
            //->where($this->view->Usuario()->quoteIntoUsuarios('sug_rec_idUsuario'))
            ->group('sug_rec_idSugestaoReclamacao');

        $select = $daoReclamacoes->getAdapter()->select()->from(array(
                'temp' => $daoReclamacoes->getAdapter()->select()->union(array($selectLojas, $selectGrupoLojas))->order('sug_rec_dataCadastro DESC')), array(
                'mes' => new Zend_Db_Expr('LPAD(MONTH(sug_rec_dataCadastro), 2, 0)'),
                'ano' => new Zend_Db_Expr('YEAR(sug_rec_dataCadastro)'),
                'total' => new Zend_Db_Expr('COUNT(*)'),
                '*'
                )
            )
            ->where('YEAR(sug_rec_dataCadastro) = ?', date('Y'))
            ->order('data ASC')
            ->group('sug_rec_idAssunto')
            ->group('sug_rec_status')
            ->group('data');

        if ($this->getRequest()->getParam('assunto', '')) {
            $select->where('sug_rec_idAssunto = ?', $this->getRequest()->getParam('assunto'));
            $this->view->assunto = $this->getRequest()->getParam('assunto');
        }

        if ($this->getRequest()->getParam('motivo', '')) {
            $select->where('rec_ass_mot_idMotivo = ?', $this->getRequest()->getParam('motivo'));
            $this->view->motivo = $this->getRequest()->getParam('motivo');
        }

        if ($this->getRequest()->getParam('protocolo', '')) {
            $select->where('sug_rec_protocolo = ?', $this->getRequest()->getParam('protocolo'));
            $this->view->protocolo = $this->getRequest()->getParam('protocolo');
        }

        if ($this->getRequest()->getParam('loja', '')) {
            $select->where('usr_idLoja = ?', $this->getRequest()->getParam('loja'));
            $this->view->loja = $this->getRequest()->getParam('loja');
        }

        if ($this->getRequest()->getParam('grupodeloja', '')) {
            $select->where('grp_loja_idGrupo = ?', $this->getRequest()->getParam('grupodeloja'));
            $this->view->grupodeloja = $this->getRequest()->getParam('grupodeloja');
        }

        if ($this->getRequest()->getParam('rede', '')) {
            $select->where('rede_idRede = ?', $this->getRequest()->getParam('rede'));
            $this->view->rede = $this->getRequest()->getParam('rede');
        }

        if ($this->getRequest()->getParam('grupoderede', '')) {
            $select->where('grp_rede_idGrupo = ?', $this->getRequest()->getParam('grupoderede'));
            $this->view->grupoderede = $this->getRequest()->getParam('grupoderede');
        }
        $reclamacoes = $daoReclamacoes->getAdapter()->fetchAll($select);

        $arrayReclamacoes = array();
        foreach (App_Funcoes_Rotulos::$statusSugestoesReclamacoes as $codeStatus => $status) {
            $arrayReclamacoes[$codeStatus]['status'] = $status;
            foreach ($reclamacoes as $reclamacao) {
                if ($codeStatus == $reclamacao['sug_rec_status']) {
                    $arrayReclamacoes[$codeStatus]['assuntos'][$reclamacao['sug_rec_idAssunto']]['assunto'] = $reclamacao['rec_ass_nome'];
                    $arrayReclamacoes[$codeStatus]['assuntos'][$reclamacao['sug_rec_idAssunto']]['idAssunto'] = $reclamacao['rec_ass_idAssunto'];
                    $arrayReclamacoes[$codeStatus]['assuntos'][$reclamacao['sug_rec_idAssunto']]['meses'][$reclamacao['mes']]['data'] = "{$reclamacao['mes']}/{$reclamacao['ano']}";
                    $arrayReclamacoes[$codeStatus]['assuntos'][$reclamacao['sug_rec_idAssunto']]['meses'][$reclamacao['mes']]['total'] = $reclamacao['total'];
                    $arrayReclamacoes[$codeStatus]['assuntos'][$reclamacao['sug_rec_idAssunto']]['total'] = isset($arrayReclamacoes[$codeStatus]['assuntos'][$reclamacao['sug_rec_idAssunto']]['total']) ? $arrayReclamacoes[$codeStatus]['assuntos'][$reclamacao['sug_rec_idAssunto']]['total'] + $reclamacao['total'] : $reclamacao['total'];
                }
            }
        }

        $anos = range(date('Y') - 5, date('Y'));
        $this->view->filtroAnos = array_reverse($anos);
        $this->view->reclamacoes = $arrayReclamacoes;
    }

    public function reclamacoesListagemAction()
    {
        $this->view->HeadTitle('Relat�rios')->HeadTitle('Reclama��es');

        $daoReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();
        $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
        $daoRedes = App_Model_DAO_Redes::getInstance();
        $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();
        $daoAssuntos = App_Model_DAO_SugestoesReclamacoes_Assuntos::getInstance();

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);

        $selectLojas = $daoReclamacoes->getAdapter()
            ->select()->from($daoReclamacoes->info('name'))
            //->where($this->view->Usuario()->quoteIntoUsuarios('sug_rec_idUsuario'))
            ->joinInner($daoAssuntos->info('name'), 'sug_rec_idAssunto = rec_ass_idAssunto', array('rec_ass_nome', 'rec_ass_idAssunto'))
            ->joinInner($daoUsuarios->info('name'), 'sug_rec_idUsuario = usr_idUsuario', null)
            ->joinInner($daoLojas->info('name'), 'usr_idLoja = loja_idLoja', null)
            ->joinInner($daoGrupoLojas->info('name'), 'loja_idGrupo = grp_loja_idGrupo', null)
            ->joinInner($daoRedes->info('name'), 'grp_loja_idRede = rede_idRede', null)
            ->joinInner($daoGrupoRedes->info('name'), 'grp_loja_idRede = rede_idRede', null)
            ->where("loja_idLoja IN({$stringLojas})");

        $selectGrupoLojas = $daoReclamacoes->getAdapter()
            ->select()->from($daoReclamacoes->info('name'))
            //->where($this->view->Usuario()->quoteIntoUsuarios('sug_rec_idUsuario'))
            ->joinInner($daoAssuntos->info('name'), 'sug_rec_idAssunto = rec_ass_idAssunto', array('rec_ass_nome', 'rec_ass_idAssunto'))
            ->joinInner($daoUsuarios->info('name'), 'sug_rec_idUsuario = usr_idUsuario', null)
            ->joinInner($daoGrupoLojas->info('name'), 'usr_idGrupoLoja = grp_loja_idGrupo', null)
            ->joinInner($daoRedes->info('name'), 'grp_loja_idRede = rede_idRede', null)
            ->joinInner($daoGrupoRedes->info('name'), 'grp_loja_idRede = rede_idRede', null);
        //->where("loja_idLoja IN({$stringLojas})");


        $select = $daoReclamacoes->getAdapter()->select()->from(array(
            'temp' => $daoReclamacoes->getAdapter()->select()->union(array($selectLojas, $selectGrupoLojas))->order('sug_rec_dataCadastro DESC'))
        );
        $selectMeses = clone $select;
        $selectMeses->reset(Zend_Db_Select::COLUMNS);
        $selectMeses->columns(array('meses' => new Zend_Db_Expr("DISTINCT(CONCAT(LPAD(MONTH(sug_rec_dataCadastro) , 2, '0'), '/', YEAR(sug_rec_dataCadastro)))")))
            ->order('meses DESC');

        $this->view->filtroMeses = array();

        $listaMeses = $daoReclamacoes->getAdapter()->fetchAll($selectMeses);
        foreach ($listaMeses as $k => $v) {
            $this->view->filtroMeses[] = $v['meses'];
        }
        unset($listaMeses);

        if ($this->getRequest()->getParam('data', false)) {
            $select->where("CONCAT(LPAD(MONTH(sug_rec_dataCadastro), 2, 0),'/', YEAR(sug_rec_dataCadastro)) = ?", $this->getRequest()->getParam('data'));
            $this->view->data = $this->getRequest()->getParam('data');
        }

        if ($this->getRequest()->getParam('ano', false)) {
            $select->where("YEAR(sug_rec_dataCadastro) = ?", $this->getRequest()->getParam('ano'));
        }

        if ($this->getRequest()->getParam('assunto', false)) {
            $select->where('sug_rec_idAssunto = ?', $this->getRequest()->getParam('assunto'));
            $this->view->assunto = $this->getRequest()->getParam('assunto');
        }

        $this->view->status = null;
        if ($this->getRequest()->getParam('status', '') != '') {
            $select->where('sug_rec_status = ?', $this->getRequest()->getParam('status'));
            $this->view->status = (int) $this->getRequest()->getParam('status');
        }

        if ($this->getRequest()->getParam('tipo', false)) {
            $select->where('sug_rec_tipo = ?', $this->getRequest()->getParam('tipo'));
            $this->view->tipo = $this->getRequest()->getParam('tipo');
        }
        // Pagina��o
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($daoReclamacoes->getAdapter()->fetchAll($select));
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

}
