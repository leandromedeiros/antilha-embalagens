<?php

class FacaSeuPedidoController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'));
    }

    public function uploadAction()
    {
        $this->view->HeadTitle('Upload')->HeadTitle('Planilha');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/upload.css', 'screen');

        if ($this->getRequest()->getParam('g') == true) {
            echo 'teste ddadasdadsa';
        }
    }

    public function fazerPedidosAction()
    {
        $pesquisa = $this->getPesquisa();
        $sessionpesquisa = new Zend_Session_Namespace('pesquisa');
        $sessionpesquisa->pular = false;
        if ($this->getRequest()->getParam('pular') == true) {
            if ($pesquisa != null) {
                $daoAcessos = App_Model_DAO_Pesquisas_Acessos::getInstance();
                $daoAcessos->insert(array(
                    'pes_ace_idPesquisa' => $pesquisa->getCodigo(),
                    'pes_ace_idUsuario' => App_Plugin_Login::getInstance()->getIdentity()->getCodigo()
                ));
                $sessionpesquisa->pular = true;
            }
       }

        if ($pesquisa != null && $sessionpesquisa->pular != true) {
            if ($pesquisa->getResponderAntes()) {
                $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/prepesquisa/{$pesquisa->getCodigo()}/{$pesquisa->getNome()}?inicio=true")->sendResponse();
                exit();
            }
        }

        $this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');

        switch (App_Plugin_Login::getInstance()->getIdentity()->getGrupo()->getTipo()) {
            case 'LO':
                $this->fazerPedidos();
                break;
            case 'GL':
                if ($this->getRequest()->getParam('detalhado') == 'true') {
                    $this->fazerPedidosDetalhe();
                } else {
                    $this->fazerPedidosGrade();
                }
                break;
            default:
                $this->getResponse()->setHttpResponseCode(401)
                    ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/index/access-denied')
                    ->sendResponse();
                exit();
        }
    }

    public function imprimirPedidoAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();

        $listaLojas = $this->view->Combo()->getlojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);

        $select = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoUsuarios->info('name'), 'ped_idUsuario = usr_idUsuario')
            ->where('ped_idPedido = ?', $this->getRequest()->getParam('id'))
            //->where($this->view->Usuario()->quoteIntoUsuarios('ped_idUsuario'))
            ->where("ped_idLoja IN({$stringLojas})");

        $registro = $daoPedidos->getAdapter()->fetchRow($select);

        if ($registro == NULL) {
            $this->getResponse()
                ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/faca-seu-pedido/meus-pedidos')
                ->sendResponse();
        }

        $this->view->pedido = $daoPedidos->createRow($registro);

        $statusOriginal = App_Funcoes_Rotulos::$statusPedidos[$this->view->pedido->getUltimoStatus()->getValor()];
        $statusExibicao = $this->view->pedido->getStatusExibicao();
        if ($statusExibicao) {
            $statusOriginal = $statusExibicao['sta_nome'];
        }

        $this->view->status = $statusOriginal;
    }

    public function fazerPedidos()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Fazer Pedido');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/fazer-pedido.css', 'screen');

        $daoCarrinho = App_Model_DAO_Carrinho::getInstance();
        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoPrecos = App_Model_DAO_Precos::getInstance();
        $daoCarrinhoItens = App_Model_DAO_Carrinho_Itens::getInstance();
        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoRede = App_Model_DAO_Redes::getInstance();
        $daoProdutoPermissao = App_Model_DAO_ProdutoPermissao::getInstance();
        
        $selectCarrinhos = $daoCarrinho->select()->from($daoCarrinho)
            ->where('car_idLoja = ?', App_Plugin_Login::getInstance()->getIdentity()->getLoja()->getCodigo())
            ->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
            ->where('ISNULL(car_idPedido)');

        $linha = $this->getRequest()->getParam('linha');
        $termo = $this->getRequest()->getParam('termo');
        $tipo  = $this->getRequest()->getParam('tipo');

        if ($linha != null) {
            $objLinha = $daoLinhas->fetchRow(
                $daoLinhas->select()->from($daoLinhas)->where('lin_idLinha = ?', $linha)
            );
        } else {
            $subSelLinha = $daoLinhas->select()->from($daoLinhas, new Zend_Db_Expr('MIN(lin_idLinha)'))
                ->where($this->view->Combo()->getQuoteLinhas('lin_idLinha'));

            $objLinha = $daoLinhas->fetchRow(
                $daoLinhas->select()->from($daoLinhas)->where('lin_idLinha = ?', $subSelLinha)
            );

            if ($objLinha != null) {
                $pedidos = $daoCarrinho->fetchAll(
                    $daoCarrinho->select()->from($daoCarrinho)
                        ->where('car_idLoja = ?', App_Plugin_Login::getInstance()->getIdentity()->getLoja()->getCodigo())
                        ->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
                        ->where('ISNULL(car_idPedido)')
                        ->where('car_idLinha <> ?', $objLinha->getCodigo())
                );
                if ($pedidos->count()) {
                    $objLinha = $pedidos->offsetGet(0)->getLinha();
                }
            }
        }

        if ($objLinha == null) {
            $this->message = 'Linha não encontrada';
        } else {

            //verifica bloqueio de linha
            $loja = App_Plugin_Login::getInstance()->getIdentity()->getLoja();

            if ($loja->getTipoBloqueio() == 1 && $objLinha->getBloqueioAtraso() == 1) {
                $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . '/faca-seu-pedido/access-denied')->sendResponse();
                exit;
            }

            $selectCarrinhos->where('car_idLinha = ?', $objLinha->getCodigo());
            $pedidos = $daoCarrinho->fetchAll($selectCarrinhos);
            if ($pedidos->count() == 0) {

                // Deleta outros pedidos no carrinho com outra linha
                $where[] = $daoCarrinho->getAdapter()->quoteInto('car_idLinha <> ?', $objLinha->getCodigo());
                $where[] = $daoCarrinho->getAdapter()->quoteInto('car_idLoja = ?', App_Plugin_Login::getInstance()->getIdentity()->getLoja()->getCodigo());
                $where[] = $daoCarrinho->getAdapter()->quoteInto('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo());
                $daoCarrinho->delete($where);

                // Cria um novo pedido
                $novoCarrinho = $daoCarrinho->getInstance()->createRow();
                $novoCarrinho->setLinha($objLinha)
                    ->setLoja($loja)
                    ->setUsuario(App_Plugin_Login::getInstance()->getIdentity())
                    ->setStatus(0);

                if (App_Plugin_Login::getInstance()->hasMasterUser() == true) {
                    $novoCarrinho->setUsuarioAntilhas(App_Plugin_Login::getInstance()->getMasterUser());
                } else {
                    $novoCarrinho->setUsuarioAntilhas(null);
                }
                $novoCarrinho->save();
            }

            // Guarda no Carrinho
            $carrinho = new Zend_Session_Namespace('carrinho');
            $carrinho->pedidos = $daoCarrinho->fetchAll($selectCarrinhos);

            // Seleciona os produtos com suas quantidades feitas no carrinho
            $selectCar = $daoCarrinho->getAdapter()->select()->from($daoCarrinho->info('name'), 'car_idCarrinho')
                ->where('car_idLoja = ?', App_Plugin_Login::getInstance()->getIdentity()->getLoja()->getCodigo())
                ->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
                ->where('car_idLinha = ?', $objLinha->getCodigo())
                ->where('ISNULL(car_idPedido)');

            //verifica bloqueios
            $loja = App_Plugin_Login::getInstance()->getIdentity()->getLoja();
            $endereco = $loja->getEnderecos()->find('loja_end_padrao', 1);
            $UFLoja = $endereco->getEndereco()->getUf();

            $daoBloqueios = App_Model_DAO_Produtos_Bloqueios::getInstance();
            $selectBloqueio = $daoBloqueios->select()
                ->from($daoBloqueios, 'count(1)')
                ->where('NOW() BETWEEN prod_bloq_dataInicial AND prod_bloq_dataFinal OR (ISNULL(prod_bloq_dataInicial) AND ISNULL(prod_bloq_dataFinal))')
                ->where('prod_bloq_uf = ? OR ISNULL(prod_bloq_uf)', $UFLoja)
                ->where('prod_bloq_idGrupoLoja = ? OR ISNULL(prod_bloq_idGrupoLoja)', $loja->getGrupo()->getCodigo())
                ->where('prod_bloq_idLoja = ? OR ISNULL(prod_bloq_idLoja)', $loja->getCodigo())
                ->where('prod_bloq_idProduto = prod_idProduto');


            //INICIO PERMISSÕES PARA VISUALIZAÇÃO DE PRODUTOS ADICIONAIS
            
            //Definir objeto para pesquisa e os campos que serão retornados
            $selectPermissao = $daoProdutoPermissao->select()->from($daoProdutoPermissao->info('name'), array('*'));

            $idRede = $objLinha->getCodigoRede();
            $idLinha = $objLinha->getCodigo();

            //Obter rede
            $rede = $daoRede->fetchRow(
                $daoRede->select()
                ->from($daoRede->info('name'), '*')
                ->where('rede_idRede = ?', $idRede)
            );

            $idGrupo = $rede->getCodigoGrupo();

            //FIM PERMISSÃO PARA VISUALIZAÇÃO DE PRODUTOS ADICIONAIS

            //Código do grupo    
            //$loja->getGrupo()->getCodigo()
            $select = $daoProdutos->getAdapter()->select()
                ->from($daoProdutos->info('name'), array(
                    '*',
                    'bloqueado' => "({$selectBloqueio})"
                ))
                ->joinLeft($daoPrecos->info('name'), 'prec_idProduto = prod_idProduto')
                ->joinLeft($daoCarrinhoItens->info('name')   , "prod_idProduto = car_item_idProduto AND car_item_idCarrinho = ({$selectCar})")
                ->joinLeft($daoProdutoPermissao->info('name'), "prod_idProduto = per_idProduto AND (per_idGrupo = '{$idGrupo}' OR per_idGrupo = '') AND (per_idRede = '{$idRede}' OR per_idRede = '') AND (per_idLinha = '{$idLinha}' OR per_idLinha = '')")
                ->where('prod_status = ?', 1)
                ->where('prod_idLinha = ? OR per_IdProdutoPermissao is not null', $objLinha->getCodigo())
                ->where('prod_idProduto LIKE ? OR prod_descricao LIKE ?', '%'.$termo.'%')
                ->where('prec_idTipo = ?', App_Plugin_Login::getInstance()->getIdentity()->getLoja()->getTipo()->getCodigo());

            if($tipo != null) {
                if($tipo == 0){
                    $select->where('per_IdProdutoPermissao IS NOT NULL', $tipo);
                }
                else {
                    $select->where('per_IdProdutoPermissao IS NULL', $tipo);
                }
            }

            $subquery = $daoProdutos
                ->select()
                ->from($daoProdutos->info('name'), array('prod_idProduto'))
                ->where("prod_tipocontrole = 'C' and prod_Qtde-prod_Empenho <= 0");
                
            $select
                ->where('prod_idProduto not in (?)', new Zend_Db_Expr('('.$subquery.')'))
                ->having('bloqueado = ?', 0)
                ->group('prod_idProduto')
                ->order('prod_sequenciaItens ASC');
            
            // Verifica qtde de pedidos para avisar o usuário que já existem pedidos
            $dataInicial = date('Y-m-d', strtotime('-7 day'));
            $dataFinal = date('Y-m-d', strtotime('+1 day'));
            $pedidos = $daoPedidos->fetchAll(
                $daoPedidos->select()->from($daoPedidos)
                    ->where('ped_idLoja = ?', App_Plugin_Login::getInstance()->getIdentity()->getLoja()->getCodigo())
                    ->where("ped_dataCadastro BETWEEN '{$dataInicial}' AND '{$dataFinal}'")
            );

            $hist = new Zend_Session_Namespace('historico');
            if (!$hist->verificado) {
                if ($pedidos->count())
                    $hist->verificado = true;
                $this->view->aviso = $pedidos->count() ? true : false;
            } else {
                $this->view->aviso = false;
            }

            // var_dump($daoProdutos->getAdapter()->fetchAll($select)); die();

            $this->view->loja = $loja;
            $this->view->presicaAprovar = (App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosIntermediario() || App_Plugin_Login::getInstance()->getIdentity()->podeCriarPedidosComAprovacao()) ? true : false;
            $this->view->alterarQtde = true;
            $this->view->produtos = $daoProdutos->getAdapter()->fetchAll($select);
            $this->view->linha = $objLinha;
        }

        unset($daoProdutoPermissao, $daoRede, $daoCarrinho, $daoLinhas, $daoProdutos, $daoPrecos, $daoCarrinhoItens, $daoPedidos, $objLinha);
    }

    public function accessDeniedAction()
    {
        $this->view->HeadTitle('Operação não permitida');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/ajuda.css', 'screen');
    }

    public function fazerPedidosGrade()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Fazer Pedido');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/fazer-pedido-grade.css', 'screen');

        $daoCarrinho = App_Model_DAO_Carrinho::getInstance();
        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoPrecos = App_Model_DAO_Precos::getInstance();
        $daoCarrinhoItens = App_Model_DAO_Carrinho_Itens::getInstance();
        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();
        $daoLojasEnderedos = App_Model_DAO_Lojas_Enderecos::getInstance();

        $objLinha = null;
        if ($this->getRequest()->getParam('linha', false) != false) {
            $objLinha = $daoLinhas->find($this->getRequest()->getParam('linha'))->current();
        }

        $selectCarrinhos = $daoCarrinho->select()->from($daoCarrinho)
            ->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
            ->where('ISNULL(car_idPedido)');

        $pedidos = $daoCarrinho->fetchAll($selectCarrinhos);

        // Cria carrinhos temporários de cada loja do grupo
        //codigo corrigido
        foreach (App_Plugin_Login::getInstance()->getIdentity()->getGrupoLoja()->getLojas() as $loja) {
            if ($objLinha != null) {

                // Deleta carrinho
                $pedidos = $daoCarrinho->fetchAll(
                    $daoCarrinho->select()
                        ->from($daoCarrinho)
                        ->where('car_idLinha = ?', $objLinha->getCodigo())
                        ->where('car_idLoja = ?', $loja->getCodigo())
                        ->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
                        ->where('ISNULL(car_idPedido)')
                );
                $where = array();
                if ($pedidos->count() == 0) {
                    // Deleta outros pedidos no carrinho com outra linha
                    $where[] = $daoCarrinho->getAdapter()->quoteInto('car_idLinha <> ?', $objLinha->getCodigo());
                    $where[] = $daoCarrinho->getAdapter()->quoteInto('car_idLoja = ?', $loja->getCodigo());
                    $where[] = $daoCarrinho->getAdapter()->quoteInto('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo());
                    $where[] = $daoCarrinho->getAdapter()->quoteInto('ISNULL(car_idPedido)', null);
                    $daoCarrinho->delete($where);
                }

                $car = $daoCarrinho->fetchRow(
                    $daoCarrinho->select()
                        ->from($daoCarrinho)
                        ->where('car_idLinha = ?', $objLinha->getCodigo())
                        ->where('car_idLoja = ?', $loja->getCodigo())
                        ->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
                        ->where('ISNULL(car_idPedido)')
                );
                if ($car == null) {
                    // Cria um novo pedido
                    $novoCarrinho = $daoCarrinho->getInstance()->createRow();
                    $novoCarrinho->setLinha($objLinha)
                        ->setLoja($loja)
                        ->setUsuario(App_Plugin_Login::getInstance()->getIdentity())
                        ->setStatus(0)
                        ->setGrade(1);

                    if (App_Plugin_Login::getInstance()->hasMasterUser() == true) {
                        $novoCarrinho->setUsuarioAntilhas(App_Plugin_Login::getInstance()->getMasterUser());
                    } else {
                        $novoCarrinho->setUsuarioAntilhas(null);
                    }
                    $novoCarrinho->save();
                }
            }
        }

        // Guarda no Carrinho
        $carrinho = new Zend_Session_Namespace('carrinho');
        $carrinho->pedidos = $daoCarrinho->fetchAll($selectCarrinhos);

        $selectLojas = $daoLojas->getAdapter()->select()
            ->from($daoLojas->info('name'))
            ->joinInner($daoLojasEnderedos->info('name'), 'loja_end_idLoja = loja_idLoja AND loja_end_padrao = 1')
            ->where('loja_idGrupo = ?', App_Plugin_Login::getInstance()->getIdentity()->getGrupo()->getCodigo())
            ->group('loja_idLoja');

        // UF
        if ($this->getRequest()->getParam('uf', false)) {
            $selectLojas->where('loja_end_uf = ?', $this->getRequest()->getParam('uf', false));
            $this->view->uf = $this->getRequest()->getParam('uf');
        }

        // Bairro
        if ($this->getRequest()->getParam('bairro', false)) {
            $selectLojas->where('loja_end_bairro LIKE ?', "%{$this->getRequest()->getParam('bairro', false)}%");
            $this->view->bairro = $this->getRequest()->getParam('bairro');
        }

        // Número do cliente
        if ($this->getRequest()->getParam('cliente', false)) {
            $selectLojas->where('loja_codigoLojaCliente = ?', $this->getRequest()->getParam('cliente', false));
            $this->view->cliente = $this->getRequest()->getParam('cliente');
        }

        $quoteLinhas = $this->view->Combo()->getQuoteLinhas('lin_idLinha');
        $selectLinhas = $daoLinhas->select()->where($quoteLinhas);
        if ($objLinha) {
            $selectLinhas->where('lin_idLinha = ?', $objLinha->getCodigo());
        } else {
            $qtdLinhas = explode(',', $quoteLinhas);
            if (count($qtdLinhas) > 1)
                $selectLinhas->where('lin_compraUnica = ?', 0);
        }
        $this->view->linhas = $daoLinhas->fetchAll($selectLinhas);
        $this->view->idLinha = $this->getRequest()->getParam('linha', false);
        $this->view->linha = $objLinha;
        $this->view->lojas = $daoLojas->createRowset($daoLojas->getAdapter()->fetchAll($selectLojas));
        $this->view->alterarQtde = true;
        $this->view->error = $this->getRequest()->getParam('error', false);

        //verificando se tem bloqueio de lojas e linhas
        $dados = array();
        $todasLinhas = array();
        foreach ($this->view->lojas as $loja) {
            $lojaAtual = $loja->toArray();
            foreach ($this->view->linhas as $linha) {
                if ($loja->getTipoBloqueio() != 1 || $linha->getBloqueioAtraso() != 1) {
                    $lojaAtual['linhas'][] = $linha;
                    $todasLinhas[$linha->getCodigo()] = $linha;
                }
            }
            $dados[] = $lojaAtual;
        }
        $this->view->dados = $dados;
        sort($todasLinhas);
        $this->view->linhasDisponiveis = $todasLinhas;

        unset($daoCarrinho, $daoLinhas, $daoProdutos, $daoPrecos, $daoCarrinhoItens, $daoPedidos);
        $this->render('fazer-pedidos-grade');
    }

    public function fazerPedidosDetalhe()
    {

        try {
            Zend_Layout::getMvcInstance()->disableLayout();

            $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Fazer Pedido Detalhado');

            $daoCarrinho = App_Model_DAO_Carrinho::getInstance();
            $daoLinhas = App_Model_DAO_Linhas::getInstance();
            $daoProdutos = App_Model_DAO_Produtos::getInstance();
            $daoPrecos = App_Model_DAO_Precos::getInstance();
            $daoCarrinhoItens = App_Model_DAO_Carrinho_Itens::getInstance();
            $daoPedidos = App_Model_DAO_Pedidos::getInstance();
            $daoLojas = App_Model_DAO_Lojas::getInstance();
            $daoLojasEnderedos = App_Model_DAO_Lojas_Enderecos::getInstance();

            $loja = $daoLojas->fetchRow(
                $daoLojas->select()->from($daoLojas)
                    ->where('loja_idLoja = ?', $this->getRequest()->getParam('loja'))
            );
            if ($loja == null)
                throw new Exception('Nenhuma Loja encontrada');

            $objLinha = null;
            if ($this->getRequest()->getParam('linha')) {
                $objLinha = $daoLinhas->find($this->getRequest()->getParam('linha'))->current();
            }

            $quoteLinhas = $this->view->Combo()->getQuoteLinhas('lin_idLinha');
            $selectLinhas = $daoLinhas->select()->where($quoteLinhas);
            if ($objLinha) {
                $selectLinhas->where('lin_idLinha = ?', $objLinha->getCodigo());
            } else {
                $qtdLinhas = explode(',', $quoteLinhas);
                if (count($qtdLinhas) > 1)
                    $selectLinhas->where('lin_compraUnica = ?', 0);
            }

            $this->view->linhas = $daoLinhas->fetchAll($selectLinhas);
            $this->view->alterarQtde = true;

            //verificando se tem bloqueio de lojas e linhas
            $dados = '';
            $todasLinhas = array();
            $lojaAtual = $loja->toArray();
            foreach ($this->view->linhas as $linha) {
                if ($loja->getTipoBloqueio() != 1 || $linha->getBloqueioAtraso() != 1) {
                    $lojaAtual['linhas'][] = $linha;
                    $todasLinhas[$linha->getCodigo()] = $linha;
                }
            }
            $dados = $lojaAtual;
            $this->view->loja = $loja;
            $this->view->dados = $dados;
            sort($todasLinhas);
            $this->view->linhasDisponiveis = $todasLinhas;

            unset($daoCarrinho, $daoLinhas, $daoProdutos, $daoPrecos, $daoCarrinhoItens, $daoPedidos);
        } catch (Exception $e) {
            $this->view->error = $e->getMessage();
        }
        $this->render('fazer-pedidos-detalhado');
    }

    public function limparCarrinhoAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);

        $this->getResponse()->setHeader('Content-Type', 'text/json');
        $retorno = array(
            'success' => false
        );

        $daoCarrinho = App_Model_DAO_Carrinho::getInstance();
        $selectCarrinhos = $daoCarrinho->select()->from($daoCarrinho)
            ->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo());

        if ($this->getRequest()->getParam('tipo', false) == 's') {
            $selectCarrinhos->where('car_idLoja = ?', App_Plugin_Login::getInstance()->getIdentity()->getLoja()->getCodigo());
        }

        try {
            $listaCarrinhos = $daoCarrinho->fetchAll($selectCarrinhos);
            foreach ($listaCarrinhos as $carrinho) {
                foreach ($carrinho->getItens() as $itemCarrinho) {
                    $itemCarrinho->delete();
                }
            }
        } catch (Exception $e) {
            $retorno['success'] = false;
        }

        $retorno['success'] = true;
        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function adicionarAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);

        $this->getResponse()->setHeader('Content-Type', 'text/json');
        $retorno = array(
            'success' => false,
            'message' => 'Dados incorretos',
            'precoProdTotal' => 0,
            'totalPedido' => 0,
            'prosseguir' => true
        );

        try {

            $daoCarrinho = App_Model_DAO_Carrinho::getInstance();
            $selectCarrinhos = $daoCarrinho->select()->from($daoCarrinho);

            if (false != ($id = $this->getRequest()->getParam('produto', false))) {

                if ($this->getRequest()->getParam('pedido')) {
                    $selectCarrinhos->where('car_idPedido = ?', $this->getRequest()->getParam('pedido'));
                } else {
                    $selectCarrinhos->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
                        ->where('ISNULL(car_idPedido)');

                    //se não for pedido em grade, procura pela linha
                    if ($this->getRequest()->getParam('grade', false) == false)
                        $selectCarrinhos->where('car_idLinha = ?', $this->getRequest()->getParam('linha'));
                }

                if (App_Plugin_Login::getInstance()->getIdentity()->getGrupo()->getTipo() == 'GL') {
                    $daoLojas = App_Model_DAO_Lojas::getInstance();
                    $objLoja = $daoLojas->fetchRow(
                        $daoLojas->select()->from($daoLojas)
                            ->where('loja_idLoja = ?', $this->getRequest()->getParam('loja'))
                    );
                } else {
                    if (App_Plugin_Login::getInstance()->getIdentity()->getLoja()) {
                        $objLoja = App_Plugin_Login::getInstance()->getIdentity()->getLoja();
                    }
                }

                $selectCarrinhos->where('car_idLoja = ?', $objLoja->getCodigo());
                $carrinho = $daoCarrinho->fetchRow($selectCarrinhos);

                $daoProdutos = App_Model_DAO_Produtos::getInstance();
                $daoPrecos = App_Model_DAO_Precos::getInstance();

                $itemProd = $daoProdutos->fetchRow(
                    $daoProdutos->select()
                        ->from($daoProdutos)
                        ->where('prod_idProduto = ?', $id)
                        ->where('prod_status = ?', 1)
                );
                if (null == $itemProd) {
                    throw new Exception('O produto especificado não foi encontrado.');
                }
                if ($itemProd->getPcEstoque() === "X") {
                    $tipo = $itemProd->getPTipoControle();
 
                    if ($tipo  == "A" || $tipo == "B" || $tipo == "C") {
                      if (($itemProd->getPQtde() - $itemProd->getPEmpenho()) < $this->getRequest()->getParam('qtde', 1)) {
                        if($tipo == "A") {
							//$retorno['maximo'] = $this->getRequest()->getParam('qtde', 1);
                            $retorno['message'] = "O item est&aacute; em ruptura.";
                        }
                        if($tipo == "B"){
							
                            $retorno['prosseguir'] = false;
							$retorno['maximo'] = ($itemProd->getPQtde() - $itemProd->getPEmpenho());
                            throw new Exception('Quantidade maxima excedida, valor maximo: ' . $retorno['maximo']);
                        }
						
						if($tipo == "C"){
							$retorno['prosseguir'] = false;
							$retorno['indisponivel'] = true;
						}
                       }
                    }
                }

                $retorno['totalPecas'] = $itemProd->getPcModulo() * $this->getRequest()->getParam('qtde');

                $preco = $daoPrecos->fetchRow(
                    $daoPrecos->select()
                        ->from($daoPrecos)
                        ->where('prec_idProduto = ?', $itemProd->getCodigo())
                        ->where('prec_idTipo = ?', $objLoja->getTipo()->getCodigo())
                );

                $itemPedido = $carrinho->getItens()->find('car_item_idProduto', $id);
                $qtde = $this->getRequest()->getParam('qtde', 1);
                if ($itemPedido != null) {
                    if ($qtde > 0) {
                        $itemPedido->setQtde($this->getRequest()->getParam('qtde', 1))
                            ->setPreco($preco->getPreco());
                        $itemPedido->save();
                    } else {
                        $itemPedido->delete();
                    }
                } else {
                    $rangeAlpha = range('A', 'Z');
                    if ($qtde > 0) {
                        $itemPedido = App_Model_DAO_Carrinho_Itens::getInstance()->createRow();
                        $itemPedido->setProduto($itemProd)
                            ->setCarrinho($carrinho)
                            ->setPreco($preco->getPreco())
                            ->setQtde($this->getRequest()->getParam('qtde', 1));
                        $itemPedido->save();
                        unset($daoProdutos);
                    }
                }
                $valorProd = $qtde * $preco->getPreco();
                $valorTotal = App_Funcoes_Calculo::calculo($valorProd, $itemProd->getCcf(), $objLoja->getIcms(), $itemProd->getAliquotaIpi());
                $retorno['precoProdTotal'] = App_Funcoes_Money::toCurrency($valorTotal);

                // Calcula o total dos carrinhos
                $carrinho = new Zend_Session_Namespace('carrinho');
                $selectCarrinhos = $daoCarrinho->select()->from($daoCarrinho);

                if ($this->getRequest()->getParam('pedido')) {
                    $selectCarrinhos->where('car_idPedido = ?', $this->getRequest()->getParam('pedido'));
                } else {
                    $selectCarrinhos->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
                        ->where('ISNULL(car_idPedido)');
                }

                if ($this->getRequest()->getParam('grade', false) == false) {
                    $selectCarrinhos->where('car_idLoja = ?', $objLoja->getCodigo());
                }
                $carrinho->pedidos = $daoCarrinho->fetchAll($selectCarrinhos);
                $valorLojaTotal = array();
                $valor = 0;
                foreach ($carrinho->pedidos as $pedido) {
                    $valorLojaTotal[$pedido->getLoja()->getCodigo()] = App_Funcoes_Money::toCurrency($pedido->getValor(true));
                    $valor += $pedido->getValor(true);
                }

                $retorno['totalPedido'] = App_Funcoes_Money::toCurrency($valor);
                if ($this->getRequest()->getParam('loja') != null) {
                    $retorno['totalPedidoLoja'] = $valorLojaTotal[$this->getRequest()->getParam('loja')];
                }
                if ($retorno['message'] === 'Dados incorretos') {
                    $retorno['message'] = "Produto <strong>{$itemProd->getDescricao()}</strong> atualizado com sucesso.";
                }
            }
            $retorno['success'] = true;
        } catch (Exception $e) {
            $retorno['success'] = false;
            $retorno['message'] = $e->getMessage();
        }
        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function atualizarNumeroPedidoAction()
    {

        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);

        $this->getResponse()->setHeader('Content-Type', 'text/json');
        $retorno = array(
            'success' => false,
            'message' => 'Dados incorretos'
        );

        try {

            $daoCarrinho = App_Model_DAO_Carrinho::getInstance();
            $selectCarrinhos = $daoCarrinho->select()->from($daoCarrinho);

            if ($this->getRequest()->getParam('pedido')) {
                $selectCarrinhos->where('car_idPedido = ?', $this->getRequest()->getParam('pedido'));
            } else {
                $selectCarrinhos->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
                    ->where('car_idLoja = ?', $this->getRequest()->getParam('loja'))
                    ->where('ISNULL(car_idPedido)');
            }

            $carrinho = $daoCarrinho->fetchRow($selectCarrinhos);
            $carrinho->setSeuNumero($this->getRequest()->getParam('seunumero'));
            $carrinho->save();

            $retorno['message'] = "Número atualizado com sucesso.";
        } catch (Exception $e) {
            $retorno['success'] = false;
            $retorno['message'] = $e->getMessage();
        }
        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function reprovarAction()
    {

        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->getResponse()->setHeader('Content-Type', 'text/json');

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoCarrinho = App_Model_DAO_Carrinho::getInstance();

        $retorno = array(
            'success' => false,
            'message' => 'Não foi possível efetuar a reprovação do pedido'
        );

        try {

            // Não é aprovador
            if (App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosSemQuantidade() == false && App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosComQuantidade() == false && App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosIntermediario() == false) {
                throw new Exception('Você não tem permissão para fazer reprovações de pedidos');
            }

            $pedido = $daoPedidos->fetchRow(
                $daoPedidos->select()->from($daoPedidos)
                    ->where('ped_idPedido = ?', $this->getRequest()->getParam('pedido'))
            );
            if ($pedido == null)
                throw new Exception('Pedido não encontrado');

            try {
                $objStatus = App_Model_DAO_Pedidos_Status::getInstance()->createRow();
                $objStatus->setPedido($pedido)
                    ->setValor(11)
                    ->setData(date('Y-m-d H:i:s'))
                    ->setDescricao(App_Funcoes_Rotulos::$statusPedidos[11]);

                $objStatus->save();

                $objLogs = App_Model_DAO_Pedidos_Logs::getInstance()->createRow();
                $objLogs->setPedido($pedido)
                    ->setUsuario(App_Plugin_Login::getInstance()->getIdentity())
                    ->setAcao('reprovou')
                    ->setData(date('Y-m-d H:i:s'));

                $objLogs->save();
            } catch (Exception $e) {
                throw new Exception('Não foi possível atualizar o status do pedido.');
            }

            // Envia o email
            $template = new Zend_View();
            $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

            $template->pedido = $pedido;

            $mailTransport = new App_Model_Email();
            $mail = new Zend_Mail('ISO-8859-1');
            $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
            $mail->addTo($pedido->getUsuario()->getEmail(), $pedido->getUsuario()->getNome());

            $mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido Reprovado');
            $mail->setBodyHtml($template->render('emails/pedido-reprovado.phtml'));
            $mail->send($mailTransport->getFormaEnvio());

            try {
                // Exclui o carrinho temporário do pedido
                $where = $daoCarrinho->getAdapter()->quoteInto('car_idPedido = ?', $this->getRequest()->getParam('pedido'));
                $daoCarrinho->delete($where);
            } catch (Exception $e) {

            }

            $retorno['success'] = true;
            $retorno['message'] = "Pedido reprovado com sucesso.";
        } catch (Exception $e) {
            $retorno['message'] = $e->getMessage();
        }

        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function aprovarAction()
    {
        // não é aprovador
        if (App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosSemQuantidade() == false && App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosComQuantidade() == false && App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosIntermediario() == false) {
            $this->getResponse()->setHttpResponseCode(401)
                ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/index/access-denied')
                ->sendResponse();
        }

        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Fazer Pedido');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/fazer-pedido.css', 'screen');
        $this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');

        $daoCarrinho = App_Model_DAO_Carrinho::getInstance();
        $daoLinhas = App_Model_DAO_Linhas::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoPrecos = App_Model_DAO_Precos::getInstance();
        $daoCarrinhoItens = App_Model_DAO_Carrinho_Itens::getInstance();
        $daoPedidos = App_Model_DAO_Pedidos::getInstance();

        $selectCarrinhos = $daoCarrinho->select()->from($daoCarrinho)
            ->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
            ->where('car_idPedido = ?', $this->getRequest()->getParam('pedido'));

        $pedidoCar = $daoCarrinho->fetchRow($selectCarrinhos);

        // Recupera o pedido
        $pedido = $daoPedidos->fetchRow(
            $daoPedidos->select()->from($daoPedidos)->where('ped_idPedido = ?', $this->getRequest()->getParam('pedido'))
        );

        if ($pedido == null)
            throw new Exception('Pedido não encontrado');
        $objLinha = null;
        if ($pedido->getLinha()) {
            $objLinha = $daoLinhas->fetchRow(
                $daoLinhas->select()->from($daoLinhas)
                    ->where('lin_idLinha = ?', $pedido->getLinha())
            );
        }

        if ($pedidoCar == null) {
            // Cria um novo pedido
            $novoCarrinho = $daoCarrinho->getInstance()->createRow();
            $novoCarrinho->setLinha($objLinha)
                ->setLoja($pedido->getLoja())
                ->setUsuario(App_Plugin_Login::getInstance()->getIdentity())
                ->setPedido($pedido)
				->setSeuNumero($pedido->getSeuNumero())
                ->setStatus(0)
                ->setGrade($pedido->getGrade());

            if (App_Plugin_Login::getInstance()->hasMasterUser() == true) {
                $novoCarrinho->setUsuarioAntilhas(App_Plugin_Login::getInstance()->getMasterUser());
            } else {
                $novoCarrinho->setUsuarioAntilhas(null);
            }

            foreach ($pedido->getItens() as $item) {

                $itemPedido = App_Model_DAO_Carrinho_Itens::getInstance()->createRow();
                $itemPedido->setProduto($item->getProduto())
                    ->setCarrinho($novoCarrinho)
                    ->setPreco($item->getValorUnitario())
                    ->setQtde($item->getQtdTotal());
                $novoCarrinho->getItens()->offsetAdd($itemPedido);
            }
            $novoCarrinho->save();
        } else if ($pedidoCar->getItens()->count() == 0) {
            foreach ($pedido->getItens() as $item) {
                $itemPedido = App_Model_DAO_Carrinho_Itens::getInstance()->createRow();
                $itemPedido->setProduto($item->getProduto())
                    ->setCarrinho($pedidoCar)
                    ->setPreco($item->getValorUnitario())
                    ->setQtde($item->getQtdTotal());
                $pedidoCar->getItens()->offsetAdd($itemPedido);
            }
            $pedidoCar->save();
        }

        $listaLinhas = $this->view->Combo()->getLinhas();
        $arrayLinhas = array();
        foreach ($listaLinhas as $linha) {
            $arrayLinhas[] = $linha->getCodigo();
        }
        $stringLinhas = implode("','", $arrayLinhas);
        // Guarda no Carrinho
        $carrinho = new Zend_Session_Namespace('carrinho');
        $carrinho->pedidos = $daoCarrinho->fetchAll($selectCarrinhos);

        // Seleciona os produtos com suas quantidades feitas no carrinho
        $selectCar = $daoCarrinho->getAdapter()->select()->from($daoCarrinho->info('name'), 'car_idCarrinho')
            ->where('car_idLoja = ?', $pedido->getLoja()->getCodigo())
            ->where('car_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
            ->where('car_idPedido = ?', $pedido->getCodigo());

        if ($objLinha) {
            $selectCar->where('car_idLinha = ?', $objLinha->getCodigo());
        }

        $select = $daoProdutos->getAdapter()->select()
            ->from($daoProdutos->info('name'))
            ->joinLeft($daoPrecos->info('name'), 'prec_idProduto = prod_idProduto')
            ->joinLeft($daoCarrinhoItens->info('name'), "prod_idProduto = car_item_idProduto AND car_item_idCarrinho = ({$selectCar})")
            ->where('prod_status = ?', 1)
            ->where('prec_idTipo = ?', $pedido->getLoja()->getTipo()->getCodigo())
            ->group('prod_idProduto');

        if (!$pedido->getGrade()) {
            if ($objLinha) {
                $select->where('prod_idLinha = ?', $objLinha->getCodigo());
            } else {
                $select->where("prod_idLinha IN('{$stringLinhas}')");
            }
        } else {
            $select->where("prod_idLinha IN('{$stringLinhas}')");
        }

        $this->view->aprovador = true;
        $this->view->alterarQtde = App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosSemQuantidade() ? false : true;
        $this->view->produtos = $daoProdutos->getAdapter()->fetchAll($select);
        $this->view->linha = $objLinha;
        $this->view->pedido = $pedido;

        unset($daoCarrinho, $daoLinhas, $daoProdutos, $daoPrecos, $daoCarrinhoItens, $daoPedidos, $objLinha);
        $this->render('fazer-pedidos');
    }

    public function resumoAction()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Resumo');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/resumo-pedido.css', 'screen');

        $this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
        $this->view->HeadScript()->appendFile('scripts/messages_ptbr.js');
        $this->view->HeadScript()->appendFile('scripts/functions.js');
        //$this->view->HeadScript()->appendFile('scripts/datepicker/jquery-ui.js');
        $this->view->HeadScript()->appendFile('scripts/calendario.js');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');
        $this->view->HeadLink()->appendStylesheet('scripts/datepicker/jquery-ui.css', 'screen');

        if(isset($_SESSION['frete']['valor'])) {
            unset($_SESSION['frete']);
        }

        if(isset($_SESSION['totalPedido'])) {
            unset($_SESSION['totalPedido']);
        }

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoFrete = App_Model_DAO_Frete::getInstance();

        $carrinho = new Zend_Session_Namespace('carrinho');
        if ($carrinho->pedidos == null || $carrinho->pedidos->count() == 0) {
            $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . '/faca-seu-pedido/fazer-pedidos')->sendResponse();
            exit();
        }

        $pedido = $carrinho->pedidos->offsetGet(0);

        $totalPeso = 0;

        foreach ($pedido->getItens() as $itemPedido) {
            $totalPeso +=  ((float) $itemPedido->getProduto()->getPesoPacotes() * (int) $itemPedido->getQtde());
        }

        // Verifica se o pedido tem produtos=
        if (!$pedido->getItens()->count()) {
            $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . '/faca-seu-pedido/fazer-pedidos')->sendResponse();
            exit();
        }

        $endereco = $pedido->getLoja()->getEnderecos()->find('loja_end_padrao', 1);

        if ($this->getRequest()->isPost()) {
            try {

                $daoPagamentos = App_Model_DAO_Pagamentos::getInstance();

                $pagamento = $daoPagamentos->fetchRow(
                    $daoPagamentos->select()->from($daoPagamentos)->where('pag_idPagamento = ?', $this->getRequest()->getParam('opcao'))
                );

                // Taxa de Servico
                $taxaServico = $pedido->getValor(true) < $pagamento->getFatMinimo() ? $pagamento->getTaxaServico() : 0;

                if ($pedido->getPedido() != null) {
                    $pedido->getPedido()->getItens()->offsetRemoveAll();

                    $objLogs = App_Model_DAO_Pedidos_Logs::getInstance()->createRow();
                    $objLogs->setUsuario(App_Plugin_Login::getInstance()->getIdentity())
                        ->setAcao('atualizou')
                        ->setData(date('Y-m-d H:i:s'));

                    $pedido->getPedido()->getLogs()->offsetAdd($objLogs);
                    $objPedido = $pedido->getPedido();
                } else {
                    $objPedido = $daoPedidos->createRow();
                    $objPedido->setGrade(0);
                    $objLogs = App_Model_DAO_Pedidos_Logs::getInstance()->createRow();

                    if (App_Plugin_Login::getInstance()->hasMasterUser() == true) {
                        $objLogs->setUsuario(App_Plugin_Login::getInstance()->getMasterUser());
                    } else {
                        $objLogs->setUsuario(App_Plugin_Login::getInstance()->getIdentity());
                    }

                    $objLogs->setAcao('criou')
                        ->setData(date('Y-m-d H:i:s'));

                    $objPedido->getLogs()->offsetAdd($objLogs);
                }

                if (!$this->getRequest()->getParam('ped_data'))
                    throw new Exception('Por favor, preecher a data de entrega!');

                $dataPrevisao = App_Funcoes_Date::conversion($this->getRequest()->getParam('ped_data', ''));
                $dataFaturamento = App_Funcoes_Date::SubtraiDiasUteis($this->getRequest()->getParam('ped_data'), $endereco->getPrazo());

                $dataFat = App_Funcoes_Date::conversion($dataFaturamento);
                $dataAtual = date('Y-m-d');

                if ($dataFat < $dataAtual)
                    $dataFaturamento = App_Funcoes_Date::conversion($dataAtual);

                $objPedido->setLoja($pedido->getLoja())
                    ->setDataCadastro(date('Y-m-d H:i:s'))
                    ->setNumeroSAP('')
                    ->setObservacoes('')
                    ->setDataFaturamento(App_Funcoes_Date::conversion($dataFaturamento))
                    ->setPrevisaoEntrega($dataPrevisao)
                    ->setPrazoEntrega($endereco->getPrazo())
                    ->setCondicaoPagamento($pagamento)
                    ->setTotalItens($pedido->getValor())
                    ->setTotalPeso($pedido->getPeso())
                    //->setLinha($pedido->getLinha()->getCodigo())
                    ->setSeuNumero($this->getRequest()->getParam('ped_seuNumero'))
                    ->setProgramacao($this->getRequest()->getParam('ped_programacao', ''));

                if ($pedido->getPedido() != null) {
                    $objPedido->setUsuario($pedido->getPedido()->getUsuario());
                } else {
                    $objPedido->setUsuario($pedido->getUsuario());
                }

                if (App_Plugin_Login::getInstance()->hasMasterUser() == true) {
                    $objPedido->setUsuarioAntilhas(App_Plugin_Login::getInstance()->getMasterUser());
                } else {
                    $objPedido->setUsuarioAntilhas(null);
                }

                $itensOrdenados = App_Funcoes_Ordenar::ordenar($pedido->getItens());
                $daoProdutos = App_Model_DAO_Produtos::getInstance();
                $daoLinhas = App_Model_DAO_Linhas::getInstance();
                $totipi = $toticms = 0;

                foreach ($itensOrdenados as $key => $item) {

                    $itemProd = $daoProdutos->fetchRow(
                        $daoProdutos
                            ->select()
                            ->from($daoProdutos)
                            ->where('prod_idProduto = ?', $item->getProduto()->getCodigo())
                    );

                    $itemProd->setPEmpenho(($itemProd->getPEmpenho() + $item->getQtde()));

                    $linha = $itemProd->getLinha();

                    if($linha->getVm() == 1) {
                        $objPedido->setPrefixo(98);
                    }

                    $itemProd->save();

                    $objItem = App_Model_DAO_Pedidos_Itens::getInstance()->createRow();
                    $objItem->setProduto($item->getProduto())
                        ->setNumeroItem($key + 1)
                        ->setQtdTotal($item->getQtde())
                        ->setUnidade($item->getProduto()->getUnidade())
                        ->setSequencialItens($item->getProduto()->getSequenciaItens())
                        ->setTotalPeso($item->getPesoTotal())
                        ->setIPI(0)
                        ->setICMS(0);

                    $icms = 0;
                    if (strtoupper($item->getProduto()->getCcf()) == 'X') {
                        if ($pedido->getLoja()->getIcms() > 0) {
                            //$icms = ($item->getPrecoTotal() / $pedido->getLoja()->getIcms()) - $item->getPrecoTotal();

                            $icms = ($item->getPrecoTotal(true) * $pedido->getLoja()->getIcms());
                            $toticms += $icms;
                            $objItem->setICMS($icms);
                        }

                        if ($item->getProduto()->getAliquotaIpi() > 0) {
                            //$ipi = (($item->getPrecoTotal() + $icms) * $item->getProduto()->getAliquotaIpi()) / 100;
                            $ipi = ($item->getPrecoTotal(true) * $item->getProduto()->getAliquotaIpi()) / 100;
                            $totipi += $ipi;
                            $objItem->setIPI($ipi);
                        }
                    }

                    $valorProd = App_Funcoes_Calculo::calculo($item->getPreco(), $item->getProduto()->getCcf(), $pedido->getLoja()->getIcms(), $item->getProduto()->getAliquotaIpi());
                    $objItem->setValorUnitario($valorProd)
                        ->setValorTotal($valorProd * $item->getQtde());

                    $objPedido->getItens()->offsetAdd($objItem);
                }

                // Valor Total Aplicado a Regra
                $valorPedido = $pedido->getValor(true);
                $desconto = $valorPedido * ($pagamento->getDesconto() > 0 ? $pagamento->getDesconto() / 100 : 0);
                $juros = $valorPedido * ($pagamento->getJuros() > 0 ? $pagamento->getJuros() / 100 : 0);
                $valorPedido += $juros - $desconto + $taxaServico;

                $freteValor = 0;
                $freteTipo = null;
                if (!empty($this->getRequest()->getParam('freteID'))) {
                    $selecFrete = $daoFrete->fetchRow(
                        $daoFrete
                            ->select()
                            ->from($daoFrete)
                            ->where('frete_codigo = ?', $this->getRequest()->getParam('freteID'))
                    );

                    $freteValor = $selecFrete->getValor();
                    $freteTipo = $selecFrete->getTipo();
                }

                $objPedido->setICMS($pedido->getLoja()->getIcms())
                    ->setTotalICMS($toticms)
                    ->setValorFrete($taxaServico)
                    ->setTotalIPI($totipi)
                    ->setJuros($pagamento->getJuros())
                    ->setTotalJuros($juros)
                    ->setTotalLiquido(($valorPedido+$freteValor) - $totipi - $toticms)
                    ->setTotalPedido($valorPedido+$freteValor);

                // Se caso a loja precisa de uma aprovador intermediário
                if (App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosIntermediario()) {
                    $objStatus = App_Model_DAO_Pedidos_Status::getInstance()->createRow();
                    $objStatus->setValor(0)
                        ->setPedido($objPedido)
                        ->setData(date('Y-m-d H:i:s'))
                        ->setDescricao(App_Funcoes_Rotulos::$statusPedidos[0]);
                } else {
                    $objStatus = App_Model_DAO_Pedidos_Status::getInstance()->createRow();
                    $objStatus->setValor(1)
                        ->setData(date('Y-m-d H:i:s'))
                        ->setDescricao(App_Funcoes_Rotulos::$statusPedidos[1]);
                }

                $objPedido->getStatus()->offsetAdd($objStatus);
                    
                $objPedido->setTipoFrete($freteTipo);
                $objPedido->setValorFrete($freteValor);
                $objPedido->save();
                $idPedido = $objPedido->getCodigo();
                $pedido->delete();

                try {
                    if (App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosIntermediario()) {
                        // Avisa a todos os aprovadores
                        $login = App_Plugin_Login::getInstance()->getIdentity();
                        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
                        $daoLojas = App_Model_DAO_Lojas::getInstance();
                        $where = $this->view->Usuario()->quoteIntoUsuarios('usr_idUsuario');
                        $select = $daoUsuarios->select()->from($daoUsuarios->info('name'))->where($where);

                        if ($login->getLoja()) {
                            $selectGrupoLoja = $daoUsuarios->select()
                                ->from($daoUsuarios)
                                ->where('usr_idGrupoLoja = ?', $login->getLoja()->getCodigo());
                        } else {
                            $selectGrupoLoja = $daoUsuarios->select()
                                ->from($daoUsuarios)
                                ->where('usr_idGrupoLoja = ?', $login->getGrupoLoja()->getCodigo());
                        }

                        $selectUnion = $daoUsuarios->select()->union(array($select, $selectGrupoLoja));
                        $usuarios = $daoUsuarios->fetchAll($selectUnion);

                        foreach ($usuarios as $usuario) {
                            if ($usuario->podeAprovarPedidosSemQuantidade() == true || $usuario->podeAprovarPedidosComQuantidade() == true) {
                                $template = new Zend_View();
                                $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

                                $template->usuario = $usuario;
                                $template->pedidos = array($objPedido);

                                $mailTransport = new App_Model_Email();
                                $mail = new Zend_Mail('ISO-8859-1');
                                $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
                                $mail->addTo($usuario->getEmail(), $usuario->getNome());
                                $mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido Aguardando Aprovação');
                                $mail->setBodyHtml($template->render('emails/aviso-aprovacao.phtml'));
                                $mail->send($mailTransport->getFormaEnvio());
                            }
                        }
                    } else {
                        // Envia o email
                        $template = new Zend_View();
                        $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

                        $template->usuario = $objPedido->getUsuario();
                        $template->pedido = $objPedido;

                        $mailTransport = new App_Model_Email();
                        $mail = new Zend_Mail('ISO-8859-1');
                        $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
                        $mail->addTo($objPedido->getUsuario()->getEmail(), $objPedido->getUsuario()->getNome());

                        $mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido Aprovado');
                        $mail->setBodyHtml($template->render('emails/pedido-aprovado.phtml'));
                        $mail->send($mailTransport->getFormaEnvio());
                    }
                } catch (Exception $e) {
                    App_Funcoes_Log::email('erro', $e);
                }

                Zend_Session::namespaceUnset('carrinho');

                if (!App_Plugin_Login::getInstance()->hasMasterUser() && !App_Plugin_Login::getInstance()->getIdentity()->podeAprovarPedidosIntermediario()) {
                    $pesquisa = $this->getPesquisa();
                    $sessionpesquisa = new Zend_Session_Namespace('pesquisa');
                    if ($pesquisa != null && $sessionpesquisa->pular != true) {
                        if (!$pesquisa->getResponderAntes()) {
                            $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/prepesquisa/{$pesquisa->getCodigo()}/{$pesquisa->getNome()}?inicio=false&pedido={$idPedido}")->sendResponse();
                            exit();
                        }
                    }
                }

                $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/finalizacao/pedido/{$idPedido}")->sendResponse();
                exit();
            } catch (App_Validate_Exception $e) {
                $fieldErrors = array();
                foreach ($e->getFields() as $field => $message) {
                    $fieldErrors[$field] = $message;
                }
                App_Funcoes_UTF8::encode($fieldErrors);
                App_Funcoes_Log::email('form', $e);
                $this->view->fieldErrors = Zend_Json::encode($fieldErrors);
                $this->view->message = count($fieldErrors) ? 'Por favor, verifique o(s) campo(s) marcado(s) em vermelho.' : $e->getMessage();
            } catch (Exception $e) {
                App_Funcoes_Log::email('erro', $e);
                $this->view->message = $e->getMessage();
            }
        } else {
            if (App_Plugin_Login::getInstance()->hasMasterUser() == false) {
                // Só para quem precisa de aprovação
                if (App_Plugin_Login::getInstance()->getIdentity()->podeCriarPedidosComAprovacao() == true) {

                    $dataPrevisao = App_Funcoes_Date::SomaDiasUteis(date('d/m/Y'), $endereco->getPrazo());

                    $objPedido = $daoPedidos->createRow();
                    $objPedido->setUsuario($pedido->getUsuario())
                        ->setLoja($pedido->getLoja())
                        ->setDataCadastro(date('Y-m-d H:i:s'))
                        ->setNumeroSAP('')
                        ->setObservacoes('')
                        ->setPrazoEntrega($endereco->getPrazo())
                        ->setPrevisaoEntrega(App_Funcoes_Date::conversion($dataPrevisao))
                        ->setTotalItens($pedido->getValor())
                        ->setTotalPedido($pedido->getValor())
                        ->setJuros(0)
                        ->setTotalJuros(0)
                        ->setTotalPeso($pedido->getPeso())
                        ->setSeuNumero($this->getRequest()->getParam('seuNumero'))
                        //->setLinha($pedido->getLinha()->getCodigo())
                        ->setGrade(0);

                    $itensOrdenados = App_Funcoes_Ordenar::ordenar($pedido->getItens());
                    foreach ($itensOrdenados as $key => $item) {

                        $objItem = App_Model_DAO_Pedidos_Itens::getInstance()->createRow();
                        $objItem->setProduto($item->getProduto())
                            ->setNumeroItem($key + 1)
                            ->setQtdTotal($item->getQtde())
                            ->setValorUnitario($item->getPreco())
                            ->setValorTotal($item->getPrecoTotal())
                            ->setIpi($item->getProduto()->getAliquotaIpi())
                            ->setUnidade($item->getProduto()->getUnidade())
                            ->setSequencialItens($item->getProduto()->getSequenciaItens())
                            ->setTotalPeso($item->getPesoTotal());

                        $objPedido->getItens()->offsetAdd($objItem);
                    }

                    $daoLojas = App_Model_DAO_Lojas::getInstance();
                    $login = App_Plugin_Login::getInstance()->getIdentity();
                    $objStatus = App_Model_DAO_Pedidos_Status::getInstance()->createRow();
                    $tempAprovadorInter = 0;

                    if ($login->getLoja()) {
                        $loja = $daoLojas->find($login->getLoja()->getCodigo())->current();
                        $tempAprovadorInter = ($login->getLoja()->getTemAprovadorIntermediario() || $login->getLoja()->getGrupo()->getTemAprovadorIntermediario()) ? 1 : 0;
                    } else {
                        $tempAprovadorInter = $login->getGrupoLoja()->getTemAprovadorIntermediario();
                    }
                    if ($tempAprovadorInter) {
                        $objStatus->setValor(12)
                            ->setData(date('Y-m-d H:i:s'))
                            ->setDescricao(App_Funcoes_Rotulos::$statusPedidos[12]);
                    } else {
                        $objStatus->setValor(0)
                            ->setData(date('Y-m-d H:i:s'))
                            ->setDescricao(App_Funcoes_Rotulos::$statusPedidos[0]);
                    }

                    $objPedido->getStatus()->offsetAdd($objStatus);

                    $objLogs = App_Model_DAO_Pedidos_Logs::getInstance()->createRow();
                    $objLogs->setUsuario(App_Plugin_Login::getInstance()->getIdentity())
                        ->setAcao('criou')
                        ->setData(date('Y-m-d H:i:s'));

                    $objPedido->getLogs()->offsetAdd($objLogs);

                    $objPedido->save();
                    $idPedido = $objPedido->getCodigo();
                    $pedido->delete();

                    try {
                        // Avisa a todos os aprovadores sobre o pedido

                        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
                        $where = $this->view->Usuario()->quoteIntoUsuarios('usr_idUsuario');
                        $select = $daoUsuarios->select()->from($daoUsuarios)->where($where);

                        if ($login->getLoja()) {
                            $loja = $daoLojas->find($login->getLoja()->getCodigo())->current();
                            $selectGrupoLoja = $daoUsuarios->select()->from($daoUsuarios)->where('usr_idGrupoLoja = ?', $loja->getGrupo()->getCodigo());
                        } else {
                            $selectGrupoLoja = $daoUsuarios->select()->from($daoUsuarios)->where('usr_idGrupoLoja = ?', $login->getGrupoLoja()->getCodigo());
                        }

                        $selectUnion = $daoUsuarios->select()->union(array($select, $selectGrupoLoja));
                        $usuarios = $daoUsuarios->fetchAll($selectUnion);

                        foreach ($usuarios as $usuario) {
                            $template = new Zend_View();
                            $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');
                            $template->usuario = $usuario;
                            $template->pedidos = array($objPedido);
                            $mailTransport = new App_Model_Email();
                            $mail = new Zend_Mail('ISO-8859-1');
                            $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
                            if ($tempAprovadorInter && $usuario->podeAprovarPedidosIntermediario()) {
                                $mail->addTo($usuario->getEmail(), $usuario->getNome());
                                $mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido Aguardando Aprovação Intermediária');
                                $mail->setBodyHtml($template->render('emails/aviso-segunda-aprovacao.phtml'));
                                $mail->send($mailTransport->getFormaEnvio());
                            } else if (!$tempAprovadorInter && ($usuario->podeAprovarPedidosSemQuantidade() == true || $usuario->podeAprovarPedidosComQuantidade() == true)) {
                                $mail->addTo($usuario->getEmail(), $usuario->getNome());
                                $mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido Aguardando Aprovação');
                                $mail->setBodyHtml($template->render('emails/aviso-aprovacao.phtml'));
                                $mail->send($mailTransport->getFormaEnvio());
                            }
                        }
                    } catch (Exception $e) {
                        App_Funcoes_Log::email('erro', $e);
                    }

                    Zend_Session::namespaceUnset('carrinho');

                    if (!App_Plugin_Login::getInstance()->hasMasterUser()) {
                        $pesquisa = $this->getPesquisa();
                        $sessionpesquisa = new Zend_Session_Namespace('pesquisa');
                        if ($pesquisa != null && $sessionpesquisa->pular != true) {
                            if (!$pesquisa->getResponderAntes()) {
                                $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/prepesquisa/{$pesquisa->getCodigo()}/{$pesquisa->getNome()}?inicio=false&pedido={$idPedido}")->sendResponse();
                                exit();
                            }
                        }
                    }

                    $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/finalizacao/pedido/{$idPedido}?pesquisa=false")->sendResponse();
                    exit();
                }
            }
        }

        // Pagamentos referente a loja
        $daoPagamentos = App_Model_DAO_Pagamentos::getInstance();
        $select = $daoPagamentos->select()->from($daoPagamentos)
            ->where("pag_classificacao = ? OR ISNULL(pag_classificacao) OR pag_classificacao = ''", $pedido->getLoja()->getClassificacao())
            ->where('pag_rede = ?', $pedido->getLoja()->getGrupo()->getRede()->getCodigo())
            ->where("pag_tipoLoja = ? OR ISNULL(pag_tipoLoja) OR pag_tipoLoja = ''", $pedido->getLoja()->getTipo()->getCodigo())
            ->where('NOW() BETWEEN pag_validadeI AND pag_validadeF')
            ->where('(?-pag_desconto)+(?*(pag_juros/100)) BETWEEN pag_faixaInicial AND pag_faixaFinal', $pedido->getValor(true))
            ->where('pag_minPecasPedido < ?', $pedido->getQtde());

        if ($pedido->getLinha()) {
            $select->where("pag_linha = ? OR ISNULL(pag_linha) OR pag_linha = ''", $pedido->getLinha()->getCodigo());
        }

        if ($endereco != null)
            $select->where("pag_uf = ? OR ISNULL(pag_uf) OR pag_uf = ''", $endereco->getEndereco()->getUf());

        $select->group(array(
            'pag_uf',
            'pag_classificacao',
            'pag_rede',
            'pag_linha',
            'pag_tipoLoja',
            'pag_fatMinimo',
            'pag_minPecasPedido',
            'pag_qtdMaxPedido',
            'pag_taxaServico',
            'pag_mensagem',
            'pag_faixaInicial',
            'pag_faixaFinal',
            'pag_juros',
            'pag_desconto',
            'pag_codigo',
            'pag_validadeI',
            'pag_validadeF'
        ));

        $pagamentos = $daoPagamentos->fetchAll($select);

        // calculo dos prazos de entrega
        $minData = 0;
        foreach ($pedido->getItens() as $key => $item) {
            //pega a data de entrega
            $dataVitrine = $item->getProduto()->getLinha()->getDataVitrine();
            $qtdDias = 0;
            if ($dataVitrine == '0000-00-00' || empty($dataVitrine)) {
                $qtdDias = $endereco->getPrazo();
            } else {
                $qtdDias = App_Funcoes_Date::DiasUteis(date('d/m/Y'), App_Funcoes_Date::conversion($dataVitrine));
                if ($qtdDias < $endereco->getPrazo()) {
                    $qtdDias = $endereco->getPrazo();
                }
            }

            if ($minData < $qtdDias)
                $minData = $qtdDias;
			
			if($key != 0)  $freteLinhaWhere .= ' AND';
			
			$freteLinhaWhere .= " frete_linha = '" . $item->getProduto()->getLinha()->lin_idLinha."'";
			
			if($key != 0)  $freteRedeWhere .= ' AND';
			
			$freteRedeWhere .= " frete_rede = '" . $item->getProduto()->getLinha()->lin_idRede."'"; 
			
			if($key != 0)  $freteProdutoWhere .= ' AND';  
			
			$freteProdutoWhere .= " frete_produto like '%" . $item->getProduto()->prod_idProduto ."%'";
			
        }
		
		$freteLinhaWhere .= " or frete_linha = ''";
		$freteRedeWhere .= " or frete_rede = ''";
		$freteProdutoWhere .= " or frete_produto = ''";
	/*		$sql = 
            $daoFrete->select()->from($daoFrete)
                ->where('frete_pesoInicial <= ? AND frete_pesoFinal >= ?', $totalPeso, $totalPeso)
                ->where('frete_regiao = ?   ', $pedido->getLoja()->getEnderecos()->find('loja_end_padrao', 1)->getUf())
				->where($freteLinhaWhere)
				->where($freteRedeWhere)
				->where($freteProdutoWhere)->__toString();
echo "$sql\n";
		echo "<pre>"; print_r("a");die();*/
		
        $fretes = $daoFrete->fetchAll( 
            $daoFrete->select()->from($daoFrete)
                ->where('frete_pesoInicial <= ? AND frete_pesoFinal >= ?', $totalPeso, $totalPeso)
                ->where("frete_regiao = ? or frete_regiao = '' ", $pedido->getLoja()->getEnderecos()->find('loja_end_padrao', 1)->getUf())
				->where($freteLinhaWhere)
				->where($freteRedeWhere)
				->where($freteProdutoWhere)
        );

        $endereco = $pedido->getLoja()->getEnderecos()->find('loja_end_padrao', 1);

        $daoCiclos = App_Model_DAO_Ciclos::getInstance();

        $ciclos = $daoCiclos->fetchAll(
            $daoCiclos
                ->select()
                ->from($daoCiclos)
                ->where('ci_data >= ?', date('Y-m-d'))
                ->order('ci_data ASC')
        );

        $proximoCiclo = null;
        foreach ($ciclos as $key => $ciclo) {
            if($ciclo->getData() == date('Y-m-d') &&
               strtotime($ciclo->getHoraDeCorte()) <= strtotime(date('H:i:s'))
            ){
                unset($ciclos[$key]);
            }else {
                if (empty($proximoCiclo)) {
                    $proximoCiclo = $ciclo;
                }
            }
        }

        $this->view->fretes = $fretes;
        $this->view->proximoCiclo = $proximoCiclo ? $proximoCiclo->getData() : '0000-00-00';
        $this->view->prazo = $endereco->getPrazo();
        $this->view->minDate = $minData;
        $this->view->pagamentos = $pagamentos;
        $this->view->pedido = $pedido;
    }

    public function resumoGradeAction()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Resumo');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/resumo-pedido.css', 'screen');

        $this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
        $this->view->HeadScript()->appendFile('scripts/messages_ptbr.js');
        $this->view->HeadScript()->appendFile('scripts/functions.js');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoLojas = App_Model_DAO_Lojas::getInstance();

        $carrinho = new Zend_Session_Namespace('carrinho');
        $log = clone $carrinho;
        if ($carrinho->pedidos == null) {
            $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . '/faca-seu-pedido/fazer-pedidos')->sendResponse();
            exit();
        }

        if ($this->getRequest()->isPost()) {
            set_time_limit(0);
            try {
                $idPedidos = array();
                $daoPedidos->getAdapter()->beginTransaction();
                foreach ($carrinho->pedidos as $pedido) {

                    if ($pedido->getItens()->count() > 0) {
                        /*
                          $codigoMatriz = (int) App_Plugin_Login::getInstance()->getIdentity()->getGrupoLoja()->getClienteMatriz();
                          $loja = $daoLojas->fetchRow(
                          $daoLojas->select()->from($daoLojas)
                          ->F('loja_idLoja = ?', $codigoMatriz)
                          );
                          if(!$loja) throw new Exception('Loja matriz, não encontrada'+$codigoMatriz);
                         */

                        $endereco = $pedido->getLoja()->getEnderecos()->find('loja_end_padrao', 1);
                        if (!$endereco)
                            throw new Exception('Endereço da matriz, não encontrado');

                        // Taxa de Servico
                        $taxaServico = $pedido->getValor() < $carrinho->pagamento->getFatMinimo() ? $carrinho->pagamento->getTaxaServico() : 0;

                        if ($pedido->getPedido() != null) {
                            $pedido->getPedido()->getItens()->offsetRemoveAll();

                            $objLogs = App_Model_DAO_Pedidos_Logs::getInstance()->createRow();
                            $objLogs->setUsuario(App_Plugin_Login::getInstance()->getIdentity())
                                ->setAcao('atualizou')
                                ->setData(date('Y-m-d H:i:s'));

                            $pedido->getPedido()->getLogs()->offsetAdd($objLogs);
                            $objPedido = $pedido->getPedido();
                        } else {
                            $objPedido = $daoPedidos->createRow();
                            $objLogs = App_Model_DAO_Pedidos_Logs::getInstance()->createRow();

                            if (App_Plugin_Login::getInstance()->hasMasterUser() == true) {
                                $objLogs->setUsuario(App_Plugin_Login::getInstance()->getMasterUser());
                            } else {
                                $objLogs->setUsuario(App_Plugin_Login::getInstance()->getIdentity());
                            }

                            $objLogs->setAcao('criou')
                                ->setData(date('Y-m-d H:i:s'));

                            $objPedido->getLogs()->offsetAdd($objLogs);
                        }

                        $objPedido->setLoja($pedido->getLoja())
                            ->setDataCadastro(date('Y-m-d H:i:s'))
                            ->setNumeroSAP('')
                            ->setObservacoes('')
                            ->setPrazoEntrega($endereco->getPrazo())
                            ->setCondicaoPagamento($carrinho->pagamento)
                            ->setTotalItens($pedido->getValor())
                            ->setTotalPeso($pedido->getPeso())
                            //->setLinha($pedido->getLinha()->getCodigo())
                            ->setSeuNumero($pedido->getSeuNumero())
                            ->setGrade(1);

                        if ($pedido->getPedido() != null) {
                            $objPedido->setUsuario($pedido->getPedido()->getUsuario());
                        } else {
                            $objPedido->setUsuario($pedido->getUsuario());
                        }

                        if (App_Plugin_Login::getInstance()->hasMasterUser() == true) {
                            $objPedido->setUsuarioAntilhas(App_Plugin_Login::getInstance()->getMasterUser());
                        } else {
                            $objPedido->setUsuarioAntilhas(null);
                        }

                        $minData = $valorPedido = $totipi = $toticms = 0;
                        foreach ($pedido->getItens() as $item) {
                            //pega a data de entrega
                            $dataVitrine = $item->getProduto()->getLinha()->getDataVitrine();
                            $qtdDias = 0;
                            if ($dataVitrine == '0000-00-00' || empty($dataVitrine)) {
                                $qtdDias = $endereco->getPrazo();
                            } else {
                                $qtdDias = App_Funcoes_Date::DiasUteis(date('d/m/Y'), App_Funcoes_Date::conversion($dataVitrine));
                                if ($qtdDias < $endereco->getPrazo()) {
                                    $qtdDias = $endereco->getPrazo();
                                }
                            }

                            if ($minData < $qtdDias) {
                                $minData = $qtdDias;
                            }

                            $objItem = App_Model_DAO_Pedidos_Itens::getInstance()->createRow();
                            $objItem->setProduto($item->getProduto())
                                ->setQtdTotal($item->getQtde())
                                ->setUnidade($item->getProduto()->getUnidade())
                                ->setTotalPeso($item->getPesoTotal())
                                ->setIpi(0)
                                ->setIcms(0)
                                ->setValorTotal($item->getPrecoTotal());

                            $icms = 0;
                            if (strtoupper($item->getProduto()->getCcf()) == 'X') {
                                if ($pedido->getLoja()->getIcms() > 0) {
                                    $icms = ($item->getPrecoTotal() / $pedido->getLoja()->getIcms()) - $item->getPrecoTotal();
                                    $toticms += $icms;
                                    $objItem->setICMS($icms);
                                }

                                if ($item->getProduto()->getAliquotaIpi() > 0) {
                                    $ipi = (($item->getPrecoTotal() + $icms) * $item->getProduto()->getAliquotaIpi()) / 100;
                                    $totipi += $ipi;
                                    $objItem->setIPI($ipi);
                                }
                            }

                            $valorProd = App_Funcoes_Calculo::calculo($item->getPreco(), $item->getProduto()->getCcf(), $pedido->getLoja()->getIcms(), $item->getProduto()->getAliquotaIpi());
                            $objItem->setValorUnitario($valorProd)
                                ->setValorTotal($valorProd * $item->getQtde());

                            $valorPedido += $objItem->getValorTotal();

                            $objPedido->getItens()->offsetAdd($objItem);
                        }

                        // Valor Total Aplicado a Regra
                        $desconto = ($valorPedido * (($carrinho->pagamento->getDesconto() > 0 ? $carrinho->pagamento->getDesconto() / 100 : 0)));
                        $juros = ($valorPedido * (($carrinho->pagamento->getJuros() > 0 ? $carrinho->pagamento->getJuros() / 100 : 0)));
                        $valorPedido += $juros - $desconto + $taxaServico;

                        $objPedido->setICMS($pedido->getLoja()->getIcms())
                            ->setTotalICMS($toticms)
                            ->setTotalIPI($totipi)
                            ->setJuros($carrinho->pagamento->getJuros())
                            ->setTotalJuros($juros)
                            ->setValorFrete($taxaServico)
                            ->setTotalLiquido($valorPedido - $totipi - $toticms)
                            ->setTotalPedido($valorPedido);

                        $previsaoEntrega = App_Funcoes_Date::SomaDiasUteis(date('d/m/Y'), $minData);
                        $objPedido->setPrevisaoEntrega(App_Funcoes_Date::conversion($previsaoEntrega));

                        $dataFaturamento = App_Funcoes_Date::SubtraiDiasUteis($previsaoEntrega, $endereco->getPrazo());
                        $dataFat = App_Funcoes_Date::conversion($dataFaturamento);
                        $dataAtual = date('Y-m-d');

                        if ($dataFat < $dataAtual)
                            $dataFaturamento = App_Funcoes_Date::conversion($dataAtual);

                        $objPedido->setDataFaturamento(App_Funcoes_Date::conversion($dataFaturamento));

                        $objStatus = App_Model_DAO_Pedidos_Status::getInstance()->createRow();
                        $objStatus->setValor(1)
                            ->setData(date('Y-m-d H:i:s'))
                            ->setDescricao(App_Funcoes_Rotulos::$statusPedidos[1]);

                        $objPedido->getStatus()->offsetAdd($objStatus);

                        $objPedido->save(false);
                        $idPedidos[] = $objPedido->getCodigo();
                        $pedido->delete();
                    } else {
                        $pedido->delete();
                    }
                }

                $daoPedidos->getAdapter()->commit();
                Zend_Session::namespaceUnset('carrinho');
                $listaIds = implode(',', $idPedidos);

                $pedidosResult = $daoPedidos->fetchAll(
                    $daoPedidos->select()->from($daoPedidos)->where('ped_idPedido IN ("' . implode('","', $idPedidos) . '")')
                );

                foreach ($pedidosResult as $objPedido) {
                    try {
                        // Envia o email
                        $template = new Zend_View();
                        $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

                        $template->usuario = $objPedido->getUsuario();
                        $template->pedido = $objPedido;

                        $mailTransport = new App_Model_Email();
                        $mail = new Zend_Mail('ISO-8859-1');
                        $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
                        $mail->addTo($objPedido->getUsuario()->getEmail(), $objPedido->getUsuario()->getNome());

                        $mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido Aprovado');
                        $mail->setBodyHtml($template->render('emails/pedido-aprovado.phtml'));
                        $mail->send($mailTransport->getFormaEnvio());
                    } catch (Exception $e) {
                        App_Funcoes_Log::email('erro', $e);
                    }
                }
                if (!App_Plugin_Login::getInstance()->hasMasterUser()) {
                    $pesquisa = $this->getPesquisa();
                    $sessionpesquisa = new Zend_Session_Namespace('pesquisa');
                    if ($pesquisa != null && $sessionpesquisa->pular != true) {
                        if (!$pesquisa->getResponderAntes()) {
                            $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/prepesquisa/{$pesquisa->getCodigo()}/{$pesquisa->getNome()}?inicio=false&pedido={$listaIds}")->sendResponse();
                            exit();
                        }
                    }
                }
                $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/finalizacao/pedido/{$listaIds}")->sendResponse();
                exit();
            } catch (App_Validate_Exception $e) {
                $daoPedidos->getAdapter()->rollBack();
                $fieldErrors = array();
                foreach ($e->getFields() as $field => $message) {
                    $fieldErrors[$field] = $message;
                }
                App_Funcoes_UTF8::encode($fieldErrors);
                $this->view->fieldErrors = Zend_Json::encode($fieldErrors);

                App_Funcoes_Log::email('form', $e);

                $this->view->message = count($fieldErrors) ? 'Por favor, verifique o(s) campo(s) marcado(s) em vermelho.' : $e->getMessage();
            } catch (Exception $e) {
                App_Funcoes_Log::email('erro', $e, $log);
                $this->view->message = $e->getMessage();
            }
        } 
		else {

            try {
                // Só para quem precisa de aprovação
                if (App_Plugin_Login::getInstance()->getIdentity()->podeCriarPedidosComAprovacao() == true && App_Plugin_Login::getInstance()->hasMasterUser() == false) {

                    $idPedidos = array();
                    $objPedidos = array();
                    $daoPedidos->getAdapter()->beginTransaction();
                    foreach ($carrinho->pedidos as $pedido) {
                        if ($pedido->getItens()->count() > 0) {
                            /*
                              $codigoMatriz = App_Plugin_Login::getInstance()->getIdentity()->getGrupoLoja()->getRede()->getCodigoMatriz();
                              $loja = $daoLojas->fetchRow(
                              $daoLojas->select()->from($daoLojas)
                              ->where('loja_codigoLojaCliente = ?', $codigoMatriz)
                              );
                              if(!$loja) throw new Exception('Loja matriz, não encontrada');
                             */

                            $endereco = $pedido->getLoja()->getEnderecos()->find('loja_end_padrao', 1);
                            if (!$endereco) {
                                throw new Exception('Endereço da loja não encontrado');
                            }

                            $dataPrevisao = App_Funcoes_Date::SomaDiasUteis(date('d/m/Y'), $endereco->getPrazo());

                            $objPedido = $daoPedidos->createRow();
                            $objPedido->setUsuario($pedido->getUsuario())
                                ->setLoja($pedido->getLoja())
                                ->setDataCadastro(date('Y-m-d H:i:s'))
                                ->setNumeroSAP('')
                                ->setObservacoes('')
                                ->setPrazoEntrega($endereco->getPrazo())
                                ->setPrevisaoEntrega(App_Funcoes_Date::conversion($dataPrevisao))
                                ->setTotalItens($pedido->getValor())
                                ->setTotalPedido($pedido->getValor())
                                ->setJuros(0)
                                ->setTotalJuros(0)
                                ->setTotalPeso($pedido->getPeso())
                                //->setLinha($pedido->getLinha()->getCodigo())
                                ->setGrade(1);

                            $minData = 0;
                            foreach ($pedido->getItens() as $item) {
                                //pega a data de entrega
                                $dataVitrine = $item->getProduto()->getLinha()->getDataVitrine();
                                $qtdDias = 0;
                                if ($dataVitrine == '0000-00-00' || empty($dataVitrine)) {
                                    $qtdDias = $endereco->getPrazo();
                                } else {
                                    $qtdDias = App_Funcoes_Date::DiasUteis(date('d/m/Y'), App_Funcoes_Date::conversion($dataVitrine));
                                    if ($qtdDias < $endereco->getPrazo()) {
                                        $qtdDias = $endereco->getPrazo();
                                    }
                                }

                                if ($minData < $qtdDias)
                                    $minData = $qtdDias;

                                $objItem = App_Model_DAO_Pedidos_Itens::getInstance()->createRow();
                                $objItem->setProduto($item->getProduto())
                                    ->setQtdTotal($item->getQtde())
                                    ->setValorUnitario($item->getPreco())
                                    ->setValorTotal($item->getPrecoTotal())
                                    ->setIpi($item->getProduto()->getAliquotaIpi())
                                    ->setUnidade($item->getProduto()->getSequenciaItens())
                                    ->setTotalPeso($item->getPesoTotal());

                                $objPedido->getItens()->offsetAdd($objItem);
                            }

                            $previsaoEntrega = App_Funcoes_Date::SomaDiasUteis(date('d/m/Y'), $minData);
                            $objPedido->setPrevisaoEntrega(App_Funcoes_Date::conversion($previsaoEntrega));

                            $dataFaturamento = App_Funcoes_Date::SubtraiDiasUteis($previsaoEntrega, $endereco->getPrazo());
                            $dataFat = App_Funcoes_Date::conversion($dataFaturamento);
                            $dataAtual = date('Y-m-d');

                            if ($dataFat < $dataAtual) {
                                $dataFaturamento = App_Funcoes_Date::conversion($dataAtual);
                            }
                            $objPedido->setDataFaturamento(App_Funcoes_Date::conversion($dataFaturamento));

                            $daoLojas = App_Model_DAO_Lojas::getInstance();
                            $login = App_Plugin_Login::getInstance()->getIdentity();
                            $objStatus = App_Model_DAO_Pedidos_Status::getInstance()->createRow();
                            $tempAprovadorInter = 0;
                            if ($login->getLoja()) {
                                $loja = $daoLojas->find($login->getLoja()->getCodigo())->current();
                                $tempAprovadorInter = ($login->getLoja()->getTemAprovadorIntermediario() || $login->getLoja()->getGrupo()->getTemAprovadorIntermediario()) ? 1 : 0;
                            } else {
                                $tempAprovadorInter = $login->getGrupoLoja()->getTemAprovadorIntermediario();
                            }
                            if ($tempAprovadorInter) {
                                $objStatus->setValor(12)
                                    ->setData(date('Y-m-d H:i:s'))
                                    ->setDescricao(App_Funcoes_Rotulos::$statusPedidos[12]);
                            } else {
                                $objStatus->setValor(0)
                                    ->setData(date('Y-m-d H:i:s'))
                                    ->setDescricao(App_Funcoes_Rotulos::$statusPedidos[0]);
                            }
                            $objPedido->getStatus()->offsetAdd($objStatus);

                            $objLogs = App_Model_DAO_Pedidos_Logs::getInstance()->createRow();
                            $objLogs->setUsuario(App_Plugin_Login::getInstance()->getIdentity())
                                ->setAcao('criou')
                                ->setData(date('Y-m-d H:i:s'));

                            $objPedido->getLogs()->offsetAdd($objLogs);

                            $objPedido->save(false);
                            $idPedidos[] = $objPedido->getCodigo();
                            $objPedidos[] = $objPedido;
                            $pedido->delete();
                        }
                    }

                    try {
                        // Avisa a todos os aprovadores sobre o pedido
                        $login = App_Plugin_Login::getInstance()->getIdentity();
                        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
                        $daoLojas = App_Model_DAO_Lojas::getInstance();
                        $where = $this->view->Usuario()->quoteIntoUsuarios('usr_idUsuario');
                        $select = $daoUsuarios->select()->from($daoUsuarios)->where($where);

                        $tempAprovadorInter = 0;
                        if ($login->getLoja()) {
                            $loja = $daoLojas->find($login->getLoja()->getCodigo())->current();
                            $tempAprovadorInter = ($login->getLoja()->getTemAprovadorIntermediario() || $login->getLoja()->getGrupo()->getTemAprovadorIntermediario()) ? 1 : 0;
                            $selectGrupoLoja = $daoUsuarios->select()->from($daoUsuarios)->where('usr_idGrupoLoja = ?', $loja->getGrupo()->getCodigo());
                        } else {
                            $tempAprovadorInter = $login->getGrupoLoja()->getTemAprovadorIntermediario();
                            $selectGrupoLoja = $daoUsuarios->select()->from($daoUsuarios)->where('usr_idGrupoLoja = ?', $login->getGrupoLoja()->getCodigo());
                        }

                        $selectUnion = $daoUsuarios->select()->union(array($select, $selectGrupoLoja));
                        $usuarios = $daoUsuarios->fetchAll($selectUnion);

                        foreach ($usuarios as $usuario) {
                            $template = new Zend_View();
                            $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');
                            $template->usuario = $usuario;
                            $template->pedidos = $objPedidos;
                            $mailTransport = new App_Model_Email();
                            $mail = new Zend_Mail('ISO-8859-1');
                            $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
                            if ($tempAprovadorInter) {
                                $mail->addTo($usuario->getEmail(), $usuario->getNome());
                                $mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido Aguardando Aprovação Intermediária');
                                $mail->setBodyHtml($template->render('emails/aviso-segunda-aprovacao.phtml'));
                                $mail->send($mailTransport->getFormaEnvio());
                            } else if ($usuario->podeAprovarPedidosSemQuantidade() == true || $usuario->podeAprovarPedidosComQuantidade() == true) {
                                $mail->addTo($usuario->getEmail(), $usuario->getNome());
                                $mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido Aguardando Aprovação');
                                $mail->setBodyHtml($template->render('emails/aviso-aprovacao.phtml'));
                                $mail->send($mailTransport->getFormaEnvio());
                            }
                        }
                    } catch (Exception $e) {
                        App_Funcoes_Log::email('erro', $e);
                    }

                    $daoPedidos->getAdapter()->commit();
                    Zend_Session::namespaceUnset('carrinho');
                    $listaIds = implode(',', $idPedidos);

                    if (!App_Plugin_Login::getInstance()->hasMasterUser()) {
                        $pesquisa = $this->getPesquisa();
                        $sessionpesquisa = new Zend_Session_Namespace('pesquisa');
                        if ($pesquisa != null && $sessionpesquisa->pular != true) {
                            if (!$pesquisa->getResponderAntes()) {
                                $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/prepesquisa/{$pesquisa->getCodigo()}/{$pesquisa->getNome()}?inicio=false&pedido={$listaIds}")->sendResponse();
                                exit();
                            }
                        }
                    }

                    $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/finalizacao/pedido/{$listaIds}")->sendResponse();
                    exit();
                }
            } catch (Exception $e) {
                App_Funcoes_Log::email('erro', $e, $log);
                $daoPedidos->getAdapter()->rollBack();
                $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . "/faca-seu-pedido/fazer-pedidos?error={$e->getMessage()}")->sendResponse();
                exit();
            }
        }

        // Pagamentos referente a loja
        $daoPagamentos = App_Model_DAO_Pagamentos::getInstance();
        $daoCarrinho = App_Model_DAO_Carrinho::getInstance();

        // Seleciona     todos os pagamento das linhas
        $quoteLinhas = $this->view->Combo()->getQuoteLinhas('pag_linha');

        $selects = array();
        foreach ($carrinho->pedidos as $pedido) {
            if ($pedido->getItens()->count() > 0) {
                $selects[] = $daoPagamentos->getAdapter()->select()->from($daoPagamentos->info('name'))
                    ->where('NOW() BETWEEN pag_validadeI AND pag_validadeF')
                    ->where("pag_classificacao = ? OR ISNULL(pag_classificacao) OR pag_classificacao = ''", $pedido->getLoja()->getClassificacao())
                    ->where('pag_rede = ?', $pedido->getLoja()->getGrupo()->getRede()->getCodigo())
                    ->where("pag_tipoLoja = ? OR ISNULL(pag_tipoLoja) OR pag_tipoLoja = ''", $pedido->getLoja()->getTipo()->getCodigo())
                    ->where('pag_minPecasPedido < ?', $pedido->getQtde())
                    ->where('pag_linha = ?', $pedido->getLinha()->getCodigo());
            }
        }
        $pagamentos = $daoPagamentos->createRowset(
            $daoPagamentos->getAdapter()->fetchAll(
                $daoPagamentos->getAdapter()->select()->from(array('temp' => $daoPagamentos->getAdapter()->select()->union($selects)))
                    ->where($quoteLinhas)
                    ->group(array('pag_fatMinimo', 'pag_mensagem', 'pag_taxaServico', 'pag_juros', 'pag_desconto'))
            )
        );
        $this->view->pagamentos = $pagamentos;
        $this->view->error = $this->getRequest()->getParam('error', false);
        $this->view->pedidos = $carrinho->pedidos;
    }

    public function calculaPagamentoAction()
    {

        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->getResponse()->setHeader('Content-Type', 'text/json');
        $retorno = array('message' => '', 'success' => false, 'totalPedido' => 0, 'totalJuros' => 0, 'totalDesconto' => 0, 'totalICMS' => 0);
        try {
            $daoPagamentos = App_Model_DAO_Pagamentos::getInstance();
            $pagamento = $daoPagamentos->fetchRow(
                $daoPagamentos->select()->from($daoPagamentos)
                    ->where('pag_idPagamento = ?', $this->getRequest()->getParam('pagamento'))
            );

            if ($pagamento == null)
                throw new Exception('Nenhum pagamento encontrado');


            $valorFrete = 0;

            if (isset($_SESSION['frete']['valor'])) {
                $valorFrete = $_SESSION['frete']['valor'];
            }

            $carrinho = new Zend_Session_Namespace('carrinho');
            $carrinho->pagamento = $pagamento;
            foreach ($carrinho->pedidos as $key => $pedido) {
                if ($pedido->getItens()->count()) {

                    $valorPedido = $pedido->getValor(true);
                    $taxa = $valorPedido < $pagamento->getFatMinimo() ? $pagamento->getTaxaServico() : 0;
					//$taxa =  $pagamento->getTaxaServico();
                    if ($pagamento->getJuros() > 0 && $taxa > 0 ) {
                        $retorno['totalJuros'] += ($valorPedido * ($pagamento->getJuros() / 100));
                        $retorno['totalPedido'] += $valorPedido + ($valorPedido * ($pagamento->getJuros() / 100)) + $valorFrete;
                        $valorPedido = $valorPedido + ($valorPedido * ($pagamento->getJuros() / 100));
                    } else {
                        $desconto = ($valorPedido * (($pagamento->getDesconto() > 0 ? $pagamento->getDesconto() / 100 : 0)));
                        $retorno['totalDesconto'] += $desconto;
						
                        $retorno['totalPedido'] += ($valorPedido - $desconto) + $valorFrete;
                        $valorPedido = ($valorPedido - $desconto);
                    }
                    $icms = 0;
					
					//$valorFrete = $key === 0 ? $valorFrete : 0;
                    //$retorno['totalPedido'] += $taxa + $valorFrete;
					$retorno['totalPedido'] += $taxa;
					
                    $retorno['pedidos'][] = array(
                        'cliente' => $pedido->getLoja()->getCodigo(),
                        'produtos' => App_Funcoes_Money::toCurrency($pedido->getValor(true)),
                        'minimo' => $pagamento->getFatMinimo(),
                        'taxa' => App_Funcoes_Money::toCurrency($taxa),
                        'total' => App_Funcoes_Money::toCurrency($valorPedido)
                    );

                    $_SESSION['totalPedido'] = $retorno['totalPedido'];
                }
            }
            $retorno['success'] = true;
        } catch (Exception $e) {
            $retorno['message'] = $e->getMessage();
        }
        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
    }

    public function calculaFreteAction() 
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);
        $this->getResponse()->setHeader('Content-Type', 'text/json');

        $carrinho = new Zend_Session_Namespace('carrinho');
        $valorFrete = (float) $this->getRequest()->getParam('valor');
        $ultimoFrete = isset($_SESSION['frete']['valor']) ? $_SESSION['frete']['valor'] : 0;
        $total = 0;

        foreach ($carrinho->pedidos as $pedido) {
            $valorPedido = isset($_SESSION['totalPedido']) ? $_SESSION['totalPedido'] : $pedido->getValor(true);

            $total = ($valorPedido + $valorFrete) - $ultimoFrete;

            $_SESSION['frete']['valor'] = $valorFrete;
            $_SESSION['totalPedido'] = $total;
        }

        $carrinho->valorFrete = $valorFrete;
        $carrinho->pagamento = $total;

        echo Zend_Json::encode($total);
    }

    public function finalizacaoAction()
    {

        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Finalização');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/protocolo-pedido.css', 'screen');

        $this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
        $this->view->HeadScript()->appendFile('scripts/messages_ptbr.js');
        $this->view->HeadScript()->appendFile('scripts/functions.js');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');

        if ($this->getRequest()->getParam('pular') == true) {
            $pesquisa = $this->getPesquisa();
            if ($pesquisa != null) {
                $sessionpesquisa = new Zend_Session_Namespace('pesquisa');
                $daoAcessos = App_Model_DAO_Pesquisas_Acessos::getInstance();
                $daoAcessos->insert(array(
                    'pes_ace_idPesquisa' => $pesquisa->getCodigo(),
                    'pes_ace_idUsuario' => App_Plugin_Login::getInstance()->getIdentity()->getCodigo()
                ));
                $sessionpesquisa->pular = true;
            }
        }

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        if (($ids = $this->getRequest()->getParam('pedido')) != '') {
            $pedidos = $daoPedidos->fetchAll(
                $daoPedidos->select()->from($daoPedidos)
                    ->where("ped_idPedido IN ({$ids})")
            );
            $this->view->pedidos = $pedidos;
        } else {
            $this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base . '/faca-seu-pedido/meus-pedidos')->sendResponse();
            exit();
        }

        $foram = ($pedidos->count() > 1 ? 'foram' : 'foi');
        $mensagemFinal = "Seu pedido foi confirmado com sucesso!";
        if (App_Plugin_Login::getInstance()->getIdentity()->podeCriarPedidosComAprovacao() == true) {
            $mensagemFinal = "Seu(s) pedido(s) {$foram} concluído(s) com sucesso e está(ão) em aprovação";
        }
        $this->view->mensagemFinal = $mensagemFinal;
    }

    public function meusPedidosAction()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Meus Pedidos');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/meus-pedidos.css', 'screen');
        $this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoLoja = App_Model_DAO_Lojas::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoPedidoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoPedidosItens = App_Model_DAO_Pedidos_Itens::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoStatusPedidos = App_Model_DAO_Pedidos_Status::getInstance();
        $daoStatus = App_Model_DAO_Status::getInstance();
        $daoStatusRelacionados = App_Model_DAO_Status_Relacionados::getInstance();

        $this->view->listaStatus = $daoStatus->fetchAll(
            $daoStatus->select()
                ->where('sta_status = ?', 1)
                ->order('sta_nome')
        );

        $selectStatus = $daoStatusPedidos->select()->from($daoStatusPedidos->info('name'), 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = ped_idPedido');

        $listaLojas = $this->view->Combo()->getlojas();
        $arrayLojas = array();
        $this->view->arrayUF = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
            $this->view->arrayUF[$loja->getEnderecoPadrao()->getEndereco()->getUF()] = App_Funcoes_Rotulos::$UF[$loja->getEnderecoPadrao()->getEndereco()->getUF()];
        }

        $stringLojas = implode(',', $arrayLojas);

		
		$sql = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoUsuarios->info('name'), 'ped_idUsuario = usr_idUsuario')
            ->joinInner($daoLoja->info('name'), 'ped_idLoja = loja_idLoja')
            ->joinInner($daoLojaEndereco->info('name'), 'loja_end_idLoja = loja_idLoja')
            ->joinLeft($daoPedidoNotas->info('name'), 'ped_nota_idPedido = ped_idPedido', 'ped_nota_numero')
            ->joinLeft($daoPedidosItens->info('name'), 'ped_item_idPedido = ped_idPedido')
            ->joinLeft($daoProdutos->info('name'), 'prod_idProduto = ped_item_produto')
            ->joinLeft($daoStatusPedidos->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
            //->where($this->view->Usuario()->quoteIntoUsuarios('ped_idUsuario'))
            ->where("loja_idLoja IN({$stringLojas}) AND (ped_prefixo IS NULL OR ped_prefixo = '')")
            ->group('ped_idPedido')
            ->order('ped_dataCadastro DESC')->__toString();
//echo "$sql\n";
//
//echo "<pre>";print_r("aa");die();
        $select = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoUsuarios->info('name'), 'ped_idUsuario = usr_idUsuario')
            ->joinInner($daoLoja->info('name'), 'ped_idLoja = loja_idLoja')
            ->joinInner($daoLojaEndereco->info('name'), 'loja_end_idLoja = loja_idLoja')
            ->joinLeft($daoPedidoNotas->info('name'), 'ped_nota_idPedido = ped_idPedido', 'ped_nota_numero')
            ->joinLeft($daoPedidosItens->info('name'), 'ped_item_idPedido = ped_idPedido')
            ->joinLeft($daoProdutos->info('name'), 'prod_idProduto = ped_item_produto')
            ->joinLeft($daoStatusPedidos->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
            //->where($this->view->Usuario()->quoteIntoUsuarios('ped_idUsuario'))
            ->where("loja_idLoja IN({$stringLojas}) AND (ped_prefixo IS NULL OR ped_prefixo = '')")
            ->group('ped_idPedido')
            ->order('ped_dataCadastro DESC');

        $selectMeses = clone $select;
        $selectMeses->reset(Zend_Db_Select::COLUMNS);
        $selectMeses->columns(array('meses' => new Zend_Db_Expr("DISTINCT(CONCAT(YEAR(ped_dataCadastro), '-', LPAD(MONTH(ped_dataCadastro) , 2, '0')))")))->order('meses DESC');

        $this->view->filtroMeses = array();

        $listaMeses = $daoPedidos->getAdapter()->fetchAll($selectMeses);
        foreach ($listaMeses as $k => $v) {
            $this->view->filtroMeses[] = $v['meses'];
        }

        //filtro de status de pedido
        //filtro de cnpj
        if ($this->getRequest()->getParam('cnpj', false)) {
            $select->where('loja_cnpj = ?', $this->getRequest()->getParam('cnpj'));
            $this->view->cnpj = $this->getRequest()->getParam('cnpj');
        }
        //filtro de numero do Pedido
        if ($this->getRequest()->getParam('pedido', false)) {
            $select->where('ped_idPedido = ?', $this->getRequest()->getParam('pedido'));
            $this->view->pedido = $this->getRequest()->getParam('pedido');
        }
        //filtro de data
        $this->view->data = $this->getRequest()->getParam('data', false);
        if ($this->view->data) {
            $select->where('LEFT(ped_dataCadastro, 7) = ?', $this->view->data);
            $this->view->data = $this->getRequest()->getParam('data');
        }

        //filtro de nome de loja
        if ($this->getRequest()->getParam('loja', false)) {
            $select->where('loja_idLoja = ?', $this->getRequest()->getParam('loja'));
            $this->view->loja = $this->getRequest()->getParam('loja');
        }

        //filtro de codigo de loja
        if ($this->getRequest()->getParam('codigoLoja', false)) {
            $select->where('loja_idLoja = ?', $this->getRequest()->getParam('codigoLoja'));
            $this->view->codigoLoja = $this->getRequest()->getParam('codigoLoja');
        }

        //filtro por codigo do produto
        if ($this->getRequest()->getParam('produto', false)) {
            $select->where('prod_idProduto = ?', $this->getRequest()->getParam('produto'));
            $this->view->produto = $this->getRequest()->getParam('produto');
        }
        //filtro por nota fiscal
        if ($this->getRequest()->getParam('nota', false)) {
            $select->where('ped_nota_numero = ?', $this->getRequest()->getParam('nota'));
            $this->view->nota = $this->getRequest()->getParam('nota');
        }
        //filtro por estado
        if ($this->getRequest()->getParam('regiao', false)) {
            $select->where('loja_end_uf = ?', $this->getRequest()->getParam('regiao'));
            $this->view->regiao = $this->getRequest()->getParam('regiao');
        }
        //filtro por nome de usuario
        if ($this->getRequest()->getParam('usuario', false)) {
            $select->where('usr_nome LIKE ?', "%{$this->getRequest()->getParam('usuario')}%");
            $this->view->usuario = $this->getRequest()->getParam('usuario');
        }

        //filtro por status
        if ($this->getRequest()->getParam('status', '') != '') {
            $listaStatus = $daoStatus->getAdapter()->fetchOne(
                $daoStatus->getAdapter()->select()
                    ->from($daoStatus->info('name'), null)
                    ->joinInner($daoStatusRelacionados->info('name'), 'sta_status_idStatus = sta_idStatus', 'GROUP_CONCAT(sta_status_idStatusRelacionado)')
                    ->where('sta_idStatus = ?', $this->getRequest()->getParam('status'))
                    ->where('sta_status = ?', 1)
            );

            $select->where("ped_status_valor IN({$listaStatus})");
            $this->view->status = $this->getRequest()->getParam('status', '');

            // faz a separação de pedidos entregues e não entregues
        } else {
            $selectEntregue = clone $select;
            $select->where("ped_status_valor <> 9")->limit(1000000);
            $selectEntregue->where("ped_status_valor = 9");

            $select = $daoPedidos->getAdapter()
                ->select()
                ->union(array("($select)", "($selectEntregue)"));
        }

        $this->view->filtro = $select;

        // Paginação
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

    public function vitrineAction()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Meus Pedidos');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/meus-pedidos.css', 'screen');
        $this->view->HeadScript()->appendFile('scripts/jquery.maskedinput-1.2.2.min.js');

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoLoja = App_Model_DAO_Lojas::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoPedidoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoPedidosItens = App_Model_DAO_Pedidos_Itens::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoStatusPedidos = App_Model_DAO_Pedidos_Status::getInstance();
        $daoStatus = App_Model_DAO_Status::getInstance();
        $daoStatusRelacionados = App_Model_DAO_Status_Relacionados::getInstance();

        $this->view->listaStatus = $daoStatus->fetchAll(
            $daoStatus->select()
                ->where('sta_status = ?', 1)
                ->order('sta_nome')
        );

        $selectStatus = $daoStatusPedidos->select()->from($daoStatusPedidos->info('name'), 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = ped_idPedido');

        $listaLojas = $this->view->Combo()->getlojas();
        $arrayLojas = array();
        $this->view->arrayUF = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
            $this->view->arrayUF[$loja->getEnderecoPadrao()->getEndereco()->getUF()] = App_Funcoes_Rotulos::$UF[$loja->getEnderecoPadrao()->getEndereco()->getUF()];
        }

        $stringLojas = implode(',', $arrayLojas);

        $select = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoUsuarios->info('name'), 'ped_idUsuario = usr_idUsuario')
            ->joinInner($daoLoja->info('name'), 'ped_idLoja = loja_idLoja')
            ->joinInner($daoLojaEndereco->info('name'), 'loja_end_idLoja = loja_idLoja')
            ->joinLeft($daoPedidoNotas->info('name'), 'ped_nota_idPedido = ped_idPedido', 'ped_nota_numero')
            ->joinLeft($daoPedidosItens->info('name'), 'ped_item_idPedido = ped_idPedido')
            ->joinLeft($daoProdutos->info('name'), 'prod_idProduto = ped_item_produto')
            ->joinLeft($daoStatusPedidos->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
            //->where($this->view->Usuario()->quoteIntoUsuarios('ped_idUsuario'))
            ->where("loja_idLoja IN({$stringLojas}) AND ped_prefixo = 99")
            ->group('ped_idPedido')
            ->order('ped_dataCadastro DESC');

        $selectMeses = clone $select;
        $selectMeses->reset(Zend_Db_Select::COLUMNS);
        $selectMeses->columns(array('meses' => new Zend_Db_Expr("DISTINCT(CONCAT(YEAR(ped_dataCadastro), '-', LPAD(MONTH(ped_dataCadastro) , 2, '0')))")))->order('meses DESC');

        $this->view->filtroMeses = array();

        $listaMeses = $daoPedidos->getAdapter()->fetchAll($selectMeses);
        foreach ($listaMeses as $k => $v) {
            $this->view->filtroMeses[] = $v['meses'];
        }

        //filtro de status de pedido
        //filtro de cnpj
        if ($this->getRequest()->getParam('cnpj', false)) {
            $select->where('loja_cnpj = ?', $this->getRequest()->getParam('cnpj'));
            $this->view->cnpj = $this->getRequest()->getParam('cnpj');
        }
        //filtro de numero do Pedido
        if ($this->getRequest()->getParam('pedido', false)) {
            $select->where('ped_idPedido = ?', $this->getRequest()->getParam('pedido'));
            $this->view->pedido = $this->getRequest()->getParam('pedido');
        }
        //filtro de data
        $this->view->data = $this->getRequest()->getParam('data', false);
        if ($this->view->data) {
            $select->where('LEFT(ped_dataCadastro, 7) = ?', $this->view->data);
            $this->view->data = $this->getRequest()->getParam('data');
        }

        //filtro de nome de loja
        if ($this->getRequest()->getParam('loja', false)) {
            $select->where('loja_idLoja = ?', $this->getRequest()->getParam('loja'));
            $this->view->loja = $this->getRequest()->getParam('loja');
        }

        //filtro de codigo de loja
        if ($this->getRequest()->getParam('codigoLoja', false)) {
            $select->where('loja_idLoja = ?', $this->getRequest()->getParam('codigoLoja'));
            $this->view->codigoLoja = $this->getRequest()->getParam('codigoLoja');
        }

        //filtro por codigo do produto
        if ($this->getRequest()->getParam('produto', false)) {
            $select->where('prod_idProduto = ?', $this->getRequest()->getParam('produto'));
            $this->view->produto = $this->getRequest()->getParam('produto');
        }
        //filtro por nota fiscal
        if ($this->getRequest()->getParam('nota', false)) {
            $select->where('ped_nota_numero = ?', $this->getRequest()->getParam('nota'));
            $this->view->nota = $this->getRequest()->getParam('nota');
        }
        //filtro por estado
        if ($this->getRequest()->getParam('regiao', false)) {
            $select->where('loja_end_uf = ?', $this->getRequest()->getParam('regiao'));
            $this->view->regiao = $this->getRequest()->getParam('regiao');
        }
        //filtro por nome de usuario
        if ($this->getRequest()->getParam('usuario', false)) {
            $select->where('usr_nome LIKE ?', "%{$this->getRequest()->getParam('usuario')}%");
            $this->view->usuario = $this->getRequest()->getParam('usuario');
        }

        //filtro por status
        if ($this->getRequest()->getParam('status', '') != '') {
            $listaStatus = $daoStatus->getAdapter()->fetchOne(
                $daoStatus->getAdapter()->select()
                    ->from($daoStatus->info('name'), null)
                    ->joinInner($daoStatusRelacionados->info('name'), 'sta_status_idStatus = sta_idStatus', 'GROUP_CONCAT(sta_status_idStatusRelacionado)')
                    ->where('sta_idStatus = ?', $this->getRequest()->getParam('status'))
                    ->where('sta_status = ?', 1)
            );

            $select->where("ped_status_valor IN({$listaStatus})");
            $this->view->status = $this->getRequest()->getParam('status', '');

            // faz a separação de pedidos entregues e não entregues
        } else {
            $selectEntregue = clone $select;
            $select->where("ped_status_valor <> 9")->limit(1000000);
            $selectEntregue->where("ped_status_valor = 9");

            $select = $daoPedidos->getAdapter()
                ->select()
                ->union(array("($select)", "($selectEntregue)"));
        }

        $this->view->filtro = $select;

        // Paginação
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

    public function exportarPedidosAction()
    {
        $this->getFrontController()->setParam('noViewRenderer', true);
        Zend_Layout::getMvcInstance()->disableLayout();

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoLoja = App_Model_DAO_Lojas::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoPedidoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
        $daoPedidosItens = App_Model_DAO_Pedidos_Itens::getInstance();
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoStatusPedidos = App_Model_DAO_Pedidos_Status::getInstance();

        $selectStatus = $daoStatusPedidos->select()->from($daoStatusPedidos->info('name'), 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = ped_idPedido');

        $listaLojas = $this->view->Combo()->getLojas();
        $arrayLojas = array();
        $this->view->arrayUF = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
            $this->view->arrayUF[$loja->getEnderecoPadrao()->getEndereco()->getUF()] = App_Funcoes_Rotulos::$UF[$loja->getEnderecoPadrao()->getEndereco()->getUF()];
        }

        $stringLojas = implode(',', $arrayLojas);

        $inicio = App_Funcoes_Date::conversion($this->getRequest()->getParam('inicio'));
        $fim = App_Funcoes_Date::conversion($this->getRequest()->getParam('fim'));

        $select = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoUsuarios->info('name'), 'ped_idUsuario = usr_idUsuario')
            ->joinInner($daoLoja->info('name'), 'ped_idLoja = loja_idLoja')
            ->joinInner($daoLojaEndereco->info('name'), 'loja_end_idLoja = loja_idLoja')
            ->joinLeft($daoPedidoNotas->info('name'), 'ped_nota_idPedido = ped_idPedido', 'ped_nota_numero')
            ->joinLeft($daoPedidosItens->info('name'), 'ped_item_idPedido = ped_idPedido')
            ->joinLeft($daoProdutos->info('name'), 'prod_idProduto = ped_item_produto')
            ->joinLeft($daoStatusPedidos->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
            ->where("loja_idLoja IN({$stringLojas})")
            //->where('ped_tipo = ?', 0)
            ->group('ped_idPedido')
            ->order('ped_dataCadastro DESC')
            ->where("ped_dataCadastro BETWEEN '{$inicio}' AND '{$fim}'");

        require_once('Spreadsheet/Excel/Writer.php');
        header("Content-type: application/Octet-Stream");
        header("Content-Disposition: inline; filename=Pedidos.xls");
        header("Content-Disposition: attachment; filename=Pedidos.xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        $xls = new Spreadsheet_Excel_Writer();
        $xls->SetVersion(8);

        $format['titulo'] = & $xls->addFormat(array('bold' => 1, 'align' => 'center', 'size' => 11, 'fontFamily' => 'Arial'));
        $format['subtitulo'] = & $xls->addFormat(array('bold' => 1, 'size' => 8, 'fontFamily' => 'arial', 'align' => 'center', 'fgColor' => 'silver', 'borderColor' => 'black', 'border' => 1));
        $format['subtitulo-esq'] = & $xls->addFormat(array('bold' => 1, 'size' => 8, 'fontFamily' => 'arial', 'align' => 'left', 'fgColor' => 'silver', 'borderColor' => 'black', 'border' => 1));
        $format['normal'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1));
        $format['normal-esq'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'left'));
        $format['normal-cen'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'center'));
        $format['normal-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'fgColor' => 'silver'));
        $format['normal-esq-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'left', 'fgColor' => 'silver'));
        $format['normal-cen-gray'] = & $xls->addFormat(array('bold' => 0, 'size' => 8, 'fontFamily' => 'arial', 'borderColor' => 'black', 'border' => 1, 'align' => 'center', 'fgColor' => 'silver'));
        $plan = & $xls->addWorksheet('Pedidos');

        $plan->writeString(0, 0, 'Listagem de Pedidos', $format['titulo']);
        $plan->setMerge(0, 0, 0, 9);
        $plan->setColumn(0, 0, 15);
        $plan->setColumn(1, 1, 15);
        $plan->setColumn(2, 2, 20);
        $plan->setColumn(3, 3, 20);
        $plan->setColumn(4, 4, 20);
        $plan->setColumn(5, 5, 15);
        $plan->setColumn(6, 6, 30);
        $plan->setColumn(6, 7, 20);
        $plan->setColumn(6, 8, 15);
        $plan->setColumn(6, 9, 20);

        $plan->writeString(2, 0, 'Pedido', $format['subtitulo']);
        $plan->writeString(2, 1, 'NF', $format['subtitulo']);
        $plan->writeString(2, 2, 'Data', $format['subtitulo']);
        $plan->writeString(2, 3, 'Previsão de Entrega', $format['subtitulo']);
        $plan->writeString(2, 4, 'Cod. Cliente', $format['subtitulo']);
        $plan->writeString(2, 5, 'Cod. da Loja', $format['subtitulo']);
        $plan->writeString(2, 6, 'Usuário', $format['subtitulo']);
        $plan->writeString(2, 7, 'Valor', $format['subtitulo']);
        $plan->writeString(2, 8, 'UF', $format['subtitulo']);
        $plan->writeString(2, 9, 'Status', $format['subtitulo']);

        $select->limit(null, null);
        $totalRegistros = $daoPedidos->getAdapter()->fetchOne(
            $daoPedidos->getAdapter()->select()
                ->from($select, 'COUNT(1)')
        );

        if ($totalRegistros > 0) {
            $rsRegistros = $daoPedidos->getAdapter()->fetchAll($select);
            $linha = 3;
            foreach ($rsRegistros as $record) {
                $pedido = App_Model_DAO_Pedidos::getInstance()->createRow($record);
                $statusOriginal = App_Funcoes_Rotulos::$statusPedidos[$pedido->getUltimoStatus()->getValor()];
                $statusExibicao = $pedido->getStatusExibicao();
                $uf = $pedido->getLoja()->getEnderecoPadrao();
                if ($statusExibicao) {
                    $statusOriginal = $statusExibicao['sta_nome'];
                }
                $plan->writeString($linha, 0, $record['ped_idPedido'], $format["normal-cen"]);
                $plan->writeString($linha, 1, $record['ped_nota_numero'], $format["normal-cen"]);
                $plan->writeString($linha, 2, App_Funcoes_Date::conversion($record['ped_dataCadastro']), $format["normal-cen"]);
                $plan->writeString($linha, 3, App_Funcoes_Date::conversion(substr($pedido->getPrevisaoEntrega(), 0, 10)), $format["normal-cen"]);
                $plan->writeString($linha, 4, $pedido->getUsuario()->getGrupo()->getCodigo(), $format["normal-cen"]);
                $plan->writeString($linha, 5, $record['ped_idLoja'], $format["normal-cen"]);
                $plan->writeString($linha, 6, $record['usr_nome'], $format["normal-cen"]);
                $plan->writeString($linha, 7, 'R$ ' . App_Funcoes_Money::toCurrency($record['ped_totalPedido']), $format["normal-cen"]);
                $plan->writeString($linha, 8, $uf['loja_end_uf'], $format["normal-cen"]);
                $plan->writeString($linha, 9, ($statusOriginal ? $statusOriginal : 'Não informado'), $format["normal-cen"]);
                $linha++;

                unset($record);
            }
            unset($rsRegistros);
        } else {
            $plan->writeString(3, 0, 'Sem registros para exibição', $format["normal-cen"]);
            for ($c = 1; $c <= 9; $c++) {
                $plan->writeString(3, $c, '', $format["normal-cen"]);
            }
            $plan->setMerge(3, 0, 3, 9);
        }
        unset($daoPedidos, $select);
        $xls->close();
    }

    public function meusPedidosDetalheAction()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Meus Pedidos Detalhe');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/meus-pedidos-detalhe.css', 'screen');
        $this->view->HeadScript()->appendFile('scripts/calendario.js');

        $daoPedidos = App_Model_DAO_Pedidos::getInstance();
        $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
        $daoLoja = App_Model_DAO_Lojas::getInstance();
        $daoLojaEndereco = App_Model_DAO_Lojas_Enderecos::getInstance();
        $daoPedidoNotas = App_Model_DAO_Pedidos_Notas::getInstance();

        $listaLojas = $this->view->Combo()->getlojas();
        $arrayLojas = array();
        foreach ($listaLojas as $loja) {
            $arrayLojas[] = $loja->getCodigo();
        }
        $stringLojas = implode(',', $arrayLojas);

        $select = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'))
            ->joinInner($daoUsuarios->info('name'), 'ped_idUsuario = usr_idUsuario')
            ->joinInner($daoLoja->info('name'), 'ped_idLoja = loja_idLoja')
            ->joinInner($daoLojaEndereco->info('name'), 'loja_end_idLoja = loja_idLoja')
            ->where('ped_idPedido = ?', $this->getRequest()->getParam('id'))
            ->where("loja_idLoja IN({$stringLojas})");

        $registro = $daoPedidos->getAdapter()->fetchRow($select);

        if ($registro == NULL) {
            $this->getResponse()
                ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/faca-seu-pedido/meus-pedidos')
                ->sendResponse();
        }


        $notas = $daoPedidoNotas->fetchAll($daoPedidoNotas->select()->from($daoPedidoNotas)
                ->where('ped_nota_idPedido = ?', $this->getRequest()->getParam('id')));
        $this->view->pedido = $registro;
        $this->view->notas = $notas;

        $objPedido = $daoPedidos->createRow($registro);

        $statusQuadro = '';
        $statusOriginal = App_Funcoes_Rotulos::$statusPedidos[$objPedido->getUltimoStatus()->getValor()];
        $statusExibicao = $objPedido->getStatusExibicao();
        if ($statusExibicao) {
            $statusOriginal = $statusExibicao['sta_nome'];
            $statusQuadro = $statusExibicao['sta_referenciaQuadro'];
        }

        $this->view->dataEntregaParcial = $objPedido->getUltimoStatus()->getDataEntregaParcial() ? App_Funcoes_Date::conversion($objPedido->getUltimoStatus()->getDataEntregaParcial()) : false;
        $this->view->statusQuadro = $statusQuadro;
        $this->view->statusExibicao = $statusOriginal;
    }

    /**
     * @return App_Model_Entity_Pesquisa|null
     */
    public function getPesquisa()
    {
        $daoPesquisas = App_Model_DAO_Pesquisas::getInstance();
        $daoRespostas = App_Model_DAO_Pesquisas_Respostas::getInstance();
        $daoPerguntas = App_Model_DAO_Pesquisas_Perguntas::getInstance();

        $selectPermissoes = $this->view->Permissoes()->select('p')->reset(Zend_Db_Select::COLUMNS)->columns('pes_idPesquisa');

        $selectRespondido = $daoRespostas->getAdapter()->select()
            ->from($daoRespostas->info('name'), new Zend_Db_Expr('COUNT(*)'))
            ->where('pes_resp_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
            ->where('pes_resp_idPergunta = pes_perg_idPergunta');

        $selectLimite = $daoRespostas->getAdapter()->select()
            ->from($daoRespostas->info('name'), new Zend_Db_Expr('COUNT(*)'))
            ->where('pes_resp_idPergunta = pes_perg_idPergunta');

        $select = $daoPesquisas->getAdapter()->select()
            ->from($daoPesquisas->info('name'), array(
                new Zend_Db_Expr('MIN(pes_idPesquisa)'),
                'expirado' => new Zend_Db_Expr('IF(NOW() > pes_perg_dataLimite, 1, 0)'),
                'respondido' => "({$selectRespondido})",
                'limite' => new Zend_Db_Expr("IF(({$selectLimite}) > pes_perg_qtdeLimite, 1, 0)"),
                '*'
                )
            )
            ->joinInner($daoPerguntas->info('name'), 'pes_perg_idPesquisa = pes_idPesquisa')
            ->where('pes_perg_status = ?', 1)
            ->where('NOW() BETWEEN pes_dataInicial AND pes_dataFinal')
            ->where('pes_idPesquisa IN (?)', $selectPermissoes)
            ->having('expirado = 0')
            ->having('respondido = 0')
            ->having('limite = 0')
            ->group('pes_perg_idPergunta');


        $pesquisa = $daoPesquisas->getAdapter()->fetchRow($select);

        return $pesquisa != null ? $daoPesquisas->createRow($pesquisa) : null;
    }

    public function prepesquisaAction()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Pesquisa');
        $daoAcessos = App_Model_DAO_Pesquisas_Acessos::getInstance();
        $resultado = $daoAcessos->getAdapter()->fetchRow(
            $daoAcessos->getAdapter()->select()
                ->from(
                    $daoAcessos->info('name'), array('total' => new Zend_Db_Expr('COUNT(*)'))
                )
                ->where('pes_ace_idPesquisa = ?', $this->getRequest()->getParam('codigo'))
                ->where('pes_ace_idUsuario = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())
        );

        $daoPesquisas = App_Model_DAO_Pesquisas::getInstance();
        $this->view->pesquisa = $daoPesquisas->fetchRow(
            $daoPesquisas->select()->from($daoPesquisas)->where('pes_idPesquisa = ?', $this->getRequest()->getParam('codigo'))
        );
        $this->view->bloqueiaResponderDepois = $resultado['total'] >= 3 ? true : false;
        $this->view->params = $this->getRequest()->getParams();
    }

    public function pesquisaAction()
    {
        $this->view->HeadTitle(utf8_decode('Faça Seu Pedido'))->HeadTitle('Pesquisa');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/protocolo-pedido.css', 'screen');

        $this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
        $this->view->HeadScript()->appendFile('scripts/messages_ptbr.js');
        $this->view->HeadScript()->appendFile('scripts/functions.js');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');

        $daoPesquisa = App_Model_DAO_Pesquisas::getInstance();
        $pesquisa = null;
        $success = false;
        if ($this->getRequest()->getParam('codigo') != '') {
            $pesquisa = $daoPesquisa->fetchRow(
                $daoPesquisa->select()->from($daoPesquisa)->where('pes_idPesquisa = ?', $this->getRequest()->getParam('codigo'))
            );
        }
        if ($pesquisa == null) {
            throw new Exception('Pesquisa não encontrada');
        }

        if ($this->getRequest()->isPost()) {

            $daoPerguntas = App_Model_DAO_Pesquisas_Perguntas::getInstance();
            $daoAlternativas = App_Model_DAO_Pesquisas_Alternativas::getInstance();
            $daoRespostas = App_Model_DAO_Pesquisas_Respostas::getInstance();

            try {

                $perguntas = $this->getRequest()->getParam('pergunta', '');
                $perguntaCheck = $this->getRequest()->getParam('perguntaCheck', '');
                $outros = $this->getRequest()->getParam('outro');

                 //----------
                foreach($_POST['perguntaCheck'] as $item){
                   $resultCheck .= $item ." - ";
                }

                 foreach ($perguntaCheck as $perg => $resp) {
                    $ret = explode("|",$perg);
                    $pergunta = $daoPerguntas->fetchRow(
                        $daoPerguntas->select()
                            ->from($daoPerguntas)
                            ->where('pes_perg_idPergunta = ?', $ret[0])
                    );        
                 }

                /* if($pergunta->getTipo() == 'C'){
                    $resposta = $daoRespostas->createRow();
                    $resposta->setPergunta($pergunta)
                    ->setUsuario($this->view->Usuario()->getLogin()->getIdentity())
                    ->setData(date('Y-m-d H:i:s'));
                    $resposta->setResposta($resultCheck); 
                    $resposta->save(); 
                 }*/

                //----------
                foreach ($perguntas as $perg => $resp) {

                    $pergunta = $daoPerguntas->fetchRow(
                        $daoPerguntas->select()
                            ->from($daoPerguntas)
                            ->where('pes_perg_idPergunta = ?', $perg)
                    );

                    $resposta = $daoRespostas->createRow();
                    $resposta->setPergunta($pergunta)
                        ->setUsuario($this->view->Usuario()->getLogin()->getIdentity())
                        ->setData(date('Y-m-d H:i:s'));

                    if ($pergunta->getTipo() == 'D') {
                       $resposta->setResposta($resp);
                    }
                   // else if($pergunta->getTipo() == 'M'){
                   //     $resposta->setResposta($resp);   
                    //}
                    else {
                        if ($pergunta->getObrigatorio()) {
                            $alternativa = $daoAlternativas->fetchRow(
                                $daoAlternativas->select()->from($daoAlternativas)
                                    ->where('pes_alt_idAlternativa = ?', $resp)
                            );
                            if ($alternativa == null) {
                                throw new Exception('Alternativa não encontrada.');
                            }
                            $resposta->setAlternativa($alternativa);
                            if ($alternativa->getTipo() == 'O') {
                                $resposta->setResposta($outros[$resp]);
                            }
                        } else {
                            $resposta->setResposta($resp['vazio']);
                        }
                    }
                    $resposta->save();
                }
                $success = true;
                $this->view->message = "Pesquisa respondida com succeso!";
            } catch (App_Validate_Exception $e) {
                $fieldErrors = array();
                foreach ($e->getFields() as $field => $message) {
                    $fieldErrors[$field] = $message;
                }
                App_Funcoes_UTF8::encode($fieldErrors);
                $this->view->fieldErrors = Zend_Json::encode($fieldErrors);
                $this->view->message = count($fieldErrors) ? 'Por favor, verifique o(s) campo(s) marcado(s) em vermelho.' : $e->getMessage();
            } catch (Exception $e) {
                $this->view->message = $e->getMessage();
            }
        }
        $this->view->success = $success;
        $this->view->pesquisa = $pesquisa;
        $this->view->inicio = $this->getRequest()->getParam('inicio', 'false');
        $this->view->ids = $this->getRequest()->getParam('pedido');
    }

}
