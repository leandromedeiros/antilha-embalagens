<?php

class ComunicadosController extends Zend_Controller_Action
{
	public function comunicadosDisponibilizadosAction()
	{
		$this->view->HeadTitle('Comunicados')->HeadTitle('Comunicados Disponibilizados');
		$this->view->HeadLink()->appendStylesheet('styles/desktop/historico-comunicados.css', 'screen');

		$daoComunicados = App_Model_DAO_Comunicados::getInstance();
		$daoLinhas = App_Model_DAO_Linhas::getInstance();

		// Lista os comunicados ativos com a consulta que verifica as permiss�es
		$data = date("Y-m-d");
		$filtroComunicadosAtivos = $this->view->Permissoes()->selectComunicados();
		$filtroComunicadosAtivos->where('com_status = ?', 1)
			->where("'{$data}' BETWEEN com_dataInicial AND com_dataFinal")
			->order('com_dataCadastro DESC');

		$this->view->disponibilizados = $daoComunicados->createRowset(
			$daoComunicados->getAdapter()->fetchAll($filtroComunicadosAtivos)
		);

		// Meses
		$selectMeses = $this->view->Permissoes()->selectComunicados();
		$selectMeses->reset( Zend_Db_Select::COLUMNS );
		$selectMeses->columns(array('meses' => new Zend_Db_Expr("DISTINCT(CONCAT(YEAR(com_dataCadastro), '-', LPAD(MONTH(com_dataCadastro) , 2, '0')))")))
		->order('meses DESC');

		// Combo de meses
		$this->view->filtroMeses = array();
		$listaMeses = $daoComunicados->getAdapter()->fetchAll($selectMeses);
		foreach($listaMeses as $k => $v) {
			$this->view->filtroMeses[] = $v['meses'];
		}

		// Consulta da listagem dos comunicados com as permiss�es
		$filtro = $this->view->Permissoes()->selectComunicados()
			->group('com_idComunicado')
			->where('com_status = ?', 1)
			->where("com_dataFinal < '{$data}'")
			->order('com_dataCadastro DESC');

		// Filtro por linha
		if($this->getRequest()->getParam('linha') != '') {
			$filtro->where('com_perm_idLinha = ?', $this->getRequest()->getParam('linha'));
			$this->view->linha = $this->getRequest()->getParam('linha');
		}

		// Filtro por data
		$this->view->mes = $this->getRequest()->getParam('data', false);
		if($this->view->mes) {
			$filtro->where('LEFT(com_dataCadastro, 7) = ?', $this->view->mes);
		}

		// Pagina��o
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
		$paginator = Zend_Paginator::factory($filtro);
		$paginator->setItemCountPerPage(3);
		$paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
		$this->view->paginator = $paginator;
	}

	public function detalheAction()
	{
		Zend_Layout::getMvcInstance()->disableLayout();
		//$this->view->HeadTitle('Detalhe do Comunicado', Zend_View_Helper_Placeholder_Container_Abstract::PREPEND);

		if (false != ($comunicado = $this->getRequest()->getParam('comunicado'))) {
			$daoComunicado = App_Model_DAO_Comunicados::getInstance();
			$objComunicado = $daoComunicado->fetchRow(
				$daoComunicado->select()->where('com_idComunicado = ?', $comunicado)
			);
			unset($daoComunicado);

			if (null == $objComunicado) {
				throw new Exception('Comunicado n�o encontrado.');
			}
			$this->view->comunicado = $objComunicado;
		}
	}
}