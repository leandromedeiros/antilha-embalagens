<?php

class ProdutosController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $this->view->HeadTitle('Produtos');
    }

    public function videosAction()
    {
        $this->view->HeadTitle('Produtos')->HeadTitle('V�deos');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/videos.css', 'screen');

        $daoVideos = App_Model_DAO_Videos::getInstance();
        $daoVideosPermissoes = App_Model_DAO_Videos_Permissoes::getInstance();
        $daoLinhas = App_Model_DAO_Linhas::getInstance();

        // Consulta com a verifica��o das permiss�es dos v�deos
        $videoDestaque = $this->view->Permissoes()->selectVideos();

        // Video destaque
        if ($this->getRequest()->getParam('idVideo')) {
            $videoDestaque->where('vid_status = ?', 1)
                ->where('vid_idVideo = ?', $this->getRequest()->getParam('idVideo'));
        } else {
            $videoDestaque->where('vid_status = ?', 1)
                ->where('vid_destaque = ?', 1)
                ->order('vid_dataCadastro DESC')
                ->limit(1);
        }
        $this->view->destaqueVideo = $daoVideos->createRowset(
            $daoVideos->getAdapter()->fetchAll($videoDestaque)
        );

        // Lista os videos inativos
        $this->view->inativoVideo = $daoVideos->createRowset(
            $daoVideos->getAdapter()->fetchAll(
                $this->view->Permissoes()->selectVideos()
                    ->where('vid_status = ?', 0)
                    ->order('vid_dataCadastro DESC')
                    ->limit(4)
            )
        );

        // Consulta da listagem dos v�deos com as permiss�es
        $filtro = $this->view->Permissoes()->selectVideos()
            ->group('vid_idVideo')
            ->order('vid_dataCadastro DESC');

        $selectMeses = $this->view->Permissoes()->selectVideos();
        $selectMeses->reset(Zend_Db_Select::COLUMNS);
        $selectMeses->columns(array('meses' => new Zend_Db_Expr("DISTINCT(CONCAT(YEAR(vid_dataCadastro), '-', LPAD(MONTH(vid_dataCadastro) , 2, '0')))")))
            ->order('meses DESC');

        $this->view->filtroMeses = array();

        $listaMeses = $daoVideos->getAdapter()->fetchAll($selectMeses);
        foreach ($listaMeses as $k => $v) {
            $this->view->filtroMeses[] = $v['meses'];
        }

        // Filtro por linha
        if ($this->getRequest()->getParam('linha') != '') {
            $filtro->where('vid_perm_idLinha = ?', $this->getRequest()->getParam('linha'));
            $this->view->linha = $this->getRequest()->getParam('linha');
        }

        // Filtro por data
        $this->view->mes = $this->getRequest()->getParam('data', false);

        if ($this->view->mes) {
            $filtro->where('LEFT(vid_dataCadastro, 7) = ?', $this->view->mes);
        }

        // Filtro por status
        if ($this->getRequest()->getParam('status') != '') {
            $filtro->where('vid_status = ?', $this->getRequest()->getParam('status'));
            $this->view->status = $this->getRequest()->getParam('status');
        } else {
            $this->view->status = '';
            $filtro->where('vid_status = ?', 1);
        }

        if ($this->getRequest()->getParam('idVideo', false)) {
            $filtro->where('vid_idVideo != ?', $this->getRequest()->getParam('idVideo'));
        }

        // Pagina��o
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($filtro);
        $paginator->setItemCountPerPage(8);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

    public function produtosHistoricoAction()
    {
        $this->view->HeadTitle('Produtos')->HeadTitle('Hist�rico');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/historico-produtos.css', 'screen');

        //instancias de produtos e linhas
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoLinha = App_Model_DAO_Linhas::getInstance();

        //select base produtos
        $filtroBase = $daoProdutos->getAdapter()->select()->from($daoProdutos->info('name'))
                ->joinInner($daoLinha->info('name'), 'prod_idLinha = lin_idLinha')
                ->where($this->view->Combo()->getQuoteLinhas('prod_idLinha'))->where('lin_dataVitrine < ?', date('Y-m-d'));

        //select base linhas
        $selLinhaBase = $daoLinha->select()
            ->from($daoLinha)
            ->where($this->view->Combo()->getQuoteLinhas('lin_idLinha'))
            ->where('lin_dataVitrine < ?', date('Y-m-d'));

        //casos de uso
        $filtro = clone $filtroBase;
        $filtro->where('prod_status = 1');
        $inativos = clone $filtroBase;
        $inativos->where('prod_status = 0');
        $linhas = $daoLinha->fetchAll($selLinhaBase);
        foreach ($linhas as $linha) {
            $linhasPerm[] = $linha->getCodigo();
        }

        //filtro por produto
        if ($this->getRequest()->getParam('produto', false)) {
            $filtro->where('prod_descricao LIKE ?', "%{$this->getRequest()->getParam('produto')}%");
            $inativos->where('prod_descricao LIKE ?', "%{$this->getRequest()->getParam('produto')}%");
            $this->view->produto = $this->getRequest()->getParam('produto');
        }

        //filtro por linha
        if ($this->getRequest()->getParam('linha', false) && in_array($this->getRequest()->getParam('linha'), $linhasPerm)) {
            $filtro->where('prod_idLinha = ?', $this->getRequest()->getParam('linha'));
            $inativos->where('prod_idLinha = ?', $this->getRequest()->getParam('linha'));
            $this->view->linha = $this->getRequest()->getParam('linha');
        }

        //filtro por data
        if ($this->getRequest()->getParam('data', false)) {
            $filtro->where('LEFT(lin_dataVitrine, 7) = ?', $this->getRequest()->getParam('data'));
            $inativos->where('LEFT(lin_dataVitrine, 7) = ?', $this->getRequest()->getParam('data'));
            $this->view->mes = $this->getRequest()->getParam('data');
        }

        // Pagina��o
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($filtro);
        $paginator->setItemCountPerPage(9);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;

        //produtos inativos
        $this->view->inativos = $daoProdutos->createRowset(
            $daoProdutos->getAdapter()->fetchAll(
                $inativos));

        //select de linhas
        $selLinhas = clone $selLinhaBase;

        //combo de linhas
        $this->view->linhas = $daoLinha->fetchAll($selLinhas);

        // combo de datas
        $filtro = $selLinhas
            ->group('lin_idLinha')
            ->order('lin_dataVitrine DESC');
        $filtro->reset(Zend_Db_Select::COLUMNS);
        $filtro->columns(array('meses' => new Zend_Db_Expr("DISTINCT(CONCAT(YEAR(lin_dataVitrine), '-', LPAD(MONTH(lin_dataVitrine) , 2, '0')))")))
            ->order('meses DESC')
            ->where('lin_dataVitrine <> ?', '0000-00-00');

        $this->view->filtroMeses = array();
        $listaMeses = $daoLinha->getAdapter()->fetchAll($filtro);
        foreach ($listaMeses as $k => $v) {
            $this->view->filtroMeses[] = $v['meses'];
        }
    }

    public function produtosCatalogoAction()
    {
        $this->view->HeadTitle('Produtos')->HeadTitle('Cat�logo');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/manual.css', 'screen');

        $daoEmbalagens = App_Model_DAO_Embalagens::getInstance();

        $listaLinhas = $this->view->Combo()->getLinhas();
        $arrayLinhas = array();
        foreach ($listaLinhas as $linha) {
            $arrayLinhas[] = $linha->getCodigo();
        }

        $stringLinhas = implode(',', $arrayLinhas);
        $filtroEmbalagens = $this->view->Permissoes()->selectEmbalagens();
        $filtroEmbalagens->where('emb_status = ?', 1)->order('emb_data DESC');
        $selectDatas = clone $filtroEmbalagens;

        //filtro por data
        if ($this->getRequest()->getParam('data', false)) {
            $filtroEmbalagens->where('LEFT(emb_data, 7) = ?', $this->getRequest()->getParam('data'));
            $this->view->mes = $this->getRequest()->getParam('data');
        }

        $selectDestaque = clone $filtroEmbalagens;
        $selectDestaque->where('emb_destaque = ?', 1);
        $selectDestaque->order('RAND()');
        $selectDestaque->limit(1);

        if ($daoEmbalagens->getCount($selectDestaque)) {
            $destaque = $daoEmbalagens->createRow($daoEmbalagens->getAdapter()->fetchRow($selectDestaque));
            $this->view->destaque = $destaque;

            $filtroEmbalagens->where('emb_idEmbalagem != ?', $destaque->getCodigo());
        }

        //combo de datas
        $selectDatas->reset(Zend_Db_Select::COLUMNS);
        $selectDatas->columns(array('meses' => new Zend_Db_Expr("DISTINCT(CONCAT(YEAR(emb_data), '-', LPAD(MONTH(emb_data) , 2, '0')))")))
            ->order('meses DESC');

        $this->view->filtroMeses = array();
        $listaMeses = $daoEmbalagens->getAdapter()->fetchAll($selectDatas);
        foreach ($listaMeses as $k => $v) {
            $this->view->filtroMeses[] = $v['meses'];
        }

        // Pagina��o
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.phtml');
        $paginator = Zend_Paginator::factory($filtroEmbalagens);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $this->view->paginator = $paginator;
    }

}
