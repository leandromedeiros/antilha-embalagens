<?php

class CentralDeAtendimentoController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $this->view->HeadTitle('Central de Atendimento');
    }

    public function ouvidoriaAction()
    {
        $this->view->HeadTitle('Central de Atendimento')->HeadTitle('Ouvidoria');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/sugestoes.css', 'screen');
        $this->view->HeadScript()->appendFile('scripts/lib/jquery.placeholder.js');
        $this->view->HeadScript()->appendFile('scripts/vm2.placeholder.js');
        $this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');

        $this->view->HeadLink()->appendStylesheet('styles/jquery.placeholder.css', 'screen');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');
        $user = App_Plugin_Login::getInstance();
        $userInfo = $user->getIdentity();

        $this->view->user = $userInfo;
        $this->view->aviso = $userInfo->getAvisoOuvidoria();
        if (!$userInfo->getAvisoOuvidoria()) {
            $userInfo->setAvisoOuvidoria(1);
            $userInfo->save();
        }

        // Emails Ouvidoria
        $daoConfiguracao = App_Model_DAO_Sistema_Configuracoes::getInstance();
        $config = $daoConfiguracao->fetchRow($daoConfiguracao->select());
        $config = $config->toArray();
        $config['conf_emailsOuvidoria'] = Zend_Json::decode(utf8_encode($config['conf_emailsOuvidoria']));
        unset($daoConfiguracao);

        if ($this->getRequest()->isPost()) {

            $daoOuvidoria = App_Model_DAO_Ouvidoria::getInstance();

            $registro = $daoOuvidoria->createRow();
            $registro->setUsuario($userInfo)
                ->setDataCadastro(date('Y-m-d H:i:s'))
                ->setMensagem($this->getRequest()->getParam('ouv_mensagem'))
                ->setNome($this->getRequest()->getParam('ouv_nome'))
                ->setEmail($this->getRequest()->getParam('ouv_email'));

            try {
                //salva no banco
                $registro->save();
                $message = "Seu contato foi enviado e est� em an�lise. Anote o n�mero do seu protocolo: {$registro->getProtocolo()}";

                //Envia o email
                try {
                    $template = new Zend_View();
                    $template->setBasePath(Zend_Registry::get('config')->paths->site->root);

                    $template->dados = $registro;
                    $template->ip = $_SERVER['REMOTE_ADDR'];
                    $mailTransport = new App_Model_Email();
                    $mail = new Zend_Mail('ISO-8859-1');

                    // Verifica os e-mails
                    $i = 0;
                    foreach ($config['conf_emailsOuvidoria'] as $email) {
                        if ($i == 0) {
                            $mail->addTo($email['ouvidoria_email']);
                        } else {
                            $mail->addCc($email['ouvidoria_email']);
                        }
                        $i++;
                    }

                    $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
                    $mail->setSubject(Zend_Registry::get('config')->project . ' - Ouvidoria');
                    $mail->setBodyHtml($template->render('emails/ouvidoria.phtml'));
                    $mail->send($mailTransport->getFormaEnvio());
                } catch (Exception $e) {
                    throw new Exception('Houve um problema ao enviar o e-mail');
                }
            } catch (App_Validate_Exception $e) {
                $fieldErrors = array();
                foreach ($e->getFields() as $field => $message) {
                    $fieldErrors[$field] = $message;
                }

                App_Funcoes_UTF8::encode($fieldErrors);
                $this->view->fieldErrors = Zend_Json::encode($fieldErrors);
                $message = count($fieldErrors) ? 'Por favor, verifique o(s) campo(s) marcado(s) em vermelho.' : $e->getMessage();
                $this->view->redirect = false;
            } catch (Exception $e) {
                $message = "Erro ao tentar efetuar o cadastro";
                $this->view->redirect = false;
            }

            $this->view->message = $message;
        }
    }

    public function contatoAction()
    {
        $this->view->HeadTitle('Central de Atendimento')->HeadTitle('Contato');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/sugestoes.css', 'screen');
        $this->view->HeadScript()->appendFile('scripts/lib/jquery.placeholder.js');
        $this->view->HeadScript()->appendFile('scripts/vm2.placeholder.js');
        $this->view->HeadScript()->appendFile('scripts/jquery.validate.min.js');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.placeholder.css', 'screen');
        $this->view->HeadLink()->appendStylesheet('styles/jquery.validate.css', 'screen');
		
		$daoAssunto = App_Model_DAO_SugestoesReclamacoes_Assuntos::getInstance();
		$this->view->assuntos = $daoAssunto->fetchAll(
			$daoAssunto->select()->from($daoAssunto)
		);

        // E-mails de sugest�o e reclama��o
        $daoConfiguracao = App_Model_DAO_Sistema_Configuracoes::getInstance();
        $config = $daoConfiguracao->fetchRow($daoConfiguracao->select());
        $config = $config->toArray();
        $config['conf_emailsReclamacao'] = Zend_Json::decode(utf8_encode($config['conf_emailsReclamacao']));
        App_Funcoes_UTF8::decode($config['conf_emailsReclamacao']);
        unset($daoConfiguracao);

        $user = App_Plugin_Login::getInstance();
        $userInfo = $user->getIdentity();

        $this->view->user = $userInfo;
        $this->view->sugestoes = $config['conf_emailsReclamacao'];

        if ($this->getRequest()->isPost()) {
			try {
				$daoSugestoesReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();
				$daoSugestoesReclamacoesStatus = App_Model_DAO_SugestoesReclamacoes_Status::getInstance();
				
				$registro = $daoSugestoesReclamacoes->createRow();
				$registro->setUsuario($userInfo)
					->setDataCadastro(date('Y-m-d H:i:s'))
					->setMensagem($this->getRequest()->getParam('sug_rec_mensagem'))
					->setTipo($this->getRequest()->getParam('sug_rec_tipo'))
					->setAssunto(
						$daoAssunto->fetchRow(
							$daoAssunto->select()->from($daoAssunto)
								->where('rec_ass_idAssunto = ?', $this->getRequest()->getParam('sug_rec_idAssunto'))
						)
					)				
					->setNome($this->getRequest()->getParam('sug_rec_nome'))
					->setEmail($this->getRequest()->getParam('sug_rec_email'))
					->setStatus('1');
					
				$motivo = null;
				if($this->getRequest()->getParam('sug_rec_idMotivo', false)) {
					$daoMotivos = App_Model_DAO_SugestoesReclamacoes_Assuntos_Motivos::getInstance();
					$motivo = $daoMotivos->fetchRow(
						$daoMotivos->select()->from($daoMotivos)
							->where('rec_ass_mot_idMotivo = ?', $this->getRequest()->getParam('sug_rec_idMotivo'))
					);
					$registro->setMotivo($motivo);
				}
					
				if($this->getRequest()->getParam('sug_rec_idPedido', false)) {
					$daoPedidos = App_Model_DAO_Pedidos::getInstance();
					$pedido = $daoPedidos->fetchRow(
						$daoPedidos->select()->from($daoPedidos)
							->where('ped_idPedido = ?', $this->getRequest()->getParam('sug_rec_idPedido'))
					);
					if(!$pedido) {
						throw new Exception("Pedido {$this->getRequest()->getParam('sug_rec_idPedido')} n�o encontrado!");
					}
					$registro->setPedido($pedido);
				}

				$objStatus = $daoSugestoesReclamacoesStatus->createRow();
				$objStatus->setData(date('Y-m-d H:i:s'))
					->setCodigo(1)
					->setNomeAtendente('Sistema')
					->setDescricao('Gerado pelo sistema');
				$registro->getHistoricoStatus()->offsetAdd($objStatus);

                $registro->save();
                $message = "Formul�rio enviado com com sucesso.";
                $this->view->redirect = true;

                //Envia o email
                try {
                    $template = new Zend_View();
                    $template->setBasePath(Zend_Registry::get('config')->paths->site->root);

                    $template->dados = $registro;
                    $template->ip = $_SERVER['REMOTE_ADDR'];
                    $mailTransport = new App_Model_Email();
                    $mail = new Zend_Mail('ISO-8859-1');

                    $i = 0;
                    $listaDestino = '';
                    foreach ($config['conf_emailsReclamacao'] as $emailConfig) {
                        if ($emailConfig['reclamacao_tipo'] == $this->getRequest()->getParam('sug_rec_tipo') && $emailConfig['reclamacao_assunto'] == $this->getRequest()->getParam('sug_rec_assunto')) {
                            $listaDestino = trim($emailConfig['reclamacao_email']);
                            break;
                        }
                    }

                    $emailDestino = explode(";", $listaDestino);
					/* foreach ($emailDestino as $email) {
                        if ($i == 0) {
                            $mail->addTo($email);
                        } else {
                            $mail->addCc($email);
                        }
                        $i++;
                    } */
					$mail->addTo('alexandre@vm2.com.br');
                    $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
                    $mail->setSubject(Zend_Registry::get('config')->project . ' - Sugest�es / Reclama��es');
                    $mail->setBodyHtml($template->render('emails/sugestoes-reclamacoes.phtml'));
                    $mail->send($mailTransport->getFormaEnvio());
                } catch (Exception $e) {
                    throw new Exception('Houve um problema ao enviar o e-mail');
                }
            } catch (App_Validate_Exception $e) {
                $fieldErrors = array();
                foreach ($e->getFields() as $field => $message) {
                    $fieldErrors[$field] = $message;
                }
                App_Funcoes_UTF8::encode($fieldErrors);
                $this->view->fieldErrors = Zend_Json::encode($fieldErrors);
                $message = count($fieldErrors) ? 'Por favor, verifique o(s) campo(s) marcado(s) em vermelho.' : $e->getMessage();
                $this->view->redirect = false;
            } catch (Exception $e) {
				$message = $e->getMessage();
                //$message = "Erro ao tentar efetuar o cadastro";
                $this->view->redirect = false;
            }

            $this->view->message = $message;
        }
    }

    public function duvidasFrequentesAction()
    {
        $this->view->HeadTitle('Central de Atendimento')->HeadTitle('D�vidas Frequentes');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/duvidas-frequentes.css', 'screen');

        $daoTopicos = App_Model_DAO_Topicos::getInstance();
        $daoFaqs = App_Model_DAO_Faqs::getInstance();

        $select = $daoFaqs->getAdapter()->select()
            ->from($daoFaqs->info('name'))
            ->joinInner($daoTopicos->info('name'), 'top_idTopico = faq_idTopico')
            ->where('faq_status = ?', 1)
            ->where('top_status = ?', 1)
            ->order('faq_ordem ASC');

        if (($busca = $this->getRequest()->getParam('busca', '')) != '') {
            $select->where('faq_pergunta LIKE ? OR faq_resposta LIKE ? OR top_nome LIKE ?', "%{$busca}%");
            $this->view->busca = $busca;
        }

        $resultados = array();
        $faqs = $daoFaqs->createRowset($daoFaqs->getAdapter()->fetchAll($select));
        foreach ($faqs as $faq) {
            $resultados[$faq->getTopico()->getCodigo()]['nome'] = $faq->getTopico()->getNome();
            $resultados[$faq->getTopico()->getCodigo()]['faqs'][] = $faq;
        }
        $this->view->faqs = $resultados;
        unset($daoFaqs, $daoTopicos);
    }

    public function politicaDeTrocaEDevolucaoAction()
    {
        $this->view->HeadTitle('Central de Atendimento')->HeadTitle('Pol�tica de Troca e Devolu��o');
        $this->view->HeadLink()->appendStylesheet('styles/desktop/politica-de-troca-e-devolucao.css', 'screen');
    }
	
	public function motivosAction()
	{
		Zend_Layout::getMvcInstance()->disableLayout();
        $this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');

        $daoMotivos = App_Model_DAO_SugestoesReclamacoes_Assuntos_Motivos::getInstance();
		$retorno = $daoMotivos->getAdapter()->fetchAll(
			$daoMotivos->select()->from($daoMotivos)
				->where('rec_ass_mot_idAssunto = ?', $this->getRequest()->getParam('assunto'))
		);		
        
        App_Funcoes_UTF8::encode($retorno);
        echo Zend_Json::encode($retorno);
	}

}
