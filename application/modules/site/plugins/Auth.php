<?php

class App_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{

    //controladores e a��es que n�o requer login
    private $semLogin = array(
        'index' => array('index', 'login', 'validar', 'logout'),
        'gerenciamento-de-usuarios' => array('dados-de-acesso'),
        'webservice' => array('index', 'soap'),
        'robos' => array('verifica-carrinho', 'importar-imagens', 'verifica-atendimentos', 'teste', 'emails-pendentes')
    );
    //controladores e a��es que n�o requer permiss�o
    private $semPermissao = array(
        'webservice' => array('index', 'soap'),
        'faca-seu-pedido' => array('access-denied', 'imprimir-pedido', 'limpar-carrinho', 'atualizar-numero-pedido', 'calcula-pagamento', 'calcula-frete', 'prepesquisa', 'pesquisa', 'pedidos-salvos', 'detalhe-do-pedido', 'exportar-pedidos'),
        'index' => array('access-denied', 'acesso-administrador', 'atualizacao-cadastro'),
        'relatorios' => array('pedidos-analiticos-ajax', 'historico-financeiro-email'),
        'central-de-atendimento' => array('motivos')
    );

    public function postDispatch($request)
    {
        $login = App_Plugin_Login::getInstance();
        if (!$login->hasMasterUser() && $login->hasIdentity() && $request->getActionName() != 'home') {
            $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            $view->Permissoes();

            // Verifica se tem comunicado ativo para exibir
            $daoLeituras = App_Model_DAO_Comunicados_Leituras::getInstance();
            $daoComunicados = App_Model_DAO_Comunicados::getInstance();

            $filtroComunicadosAtivos = $view->Permissoes()->selectComunicados();
            $filtroComunicadosAtivos
                ->joinLeft($daoLeituras->info('name'), "com_idComunicado = com_leit_idComunicado AND com_leit_idUsuario = {$login->getIdentity()->getCodigo()}")
                ->where('com_status = ?', 1)
                ->where('? BETWEEN com_dataInicial AND com_dataFinal', date('Y-m-d'))
                ->where('ISNULL(com_leit_idComunicado)')
                ->where('com_leituraObrigatoria = ?', 1)
                ->order('com_dataCadastro ASC')
                ->limit(1);

            $arrComunicado = $daoComunicados->getAdapter()->fetchRow($filtroComunicadosAtivos);
            if ($arrComunicado) {
                $this->getResponse()->setHttpResponseCode(401)
                    ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/home')
                    ->sendResponse();
                exit();
            }
        }
    }

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        //a��es que n�o requerem login para serem executadas
        if (array_key_exists($request->getControllerName(), $this->semLogin) && (in_array($request->getActionName(), $this->semLogin[$request->getControllerName()]))) {
            return true;
        }

        $validacao = new Zend_Session_Namespace('validacao');
        if (isset($validacao->loja) && ($validacao->loja instanceof App_Model_Entity_Loja)) {
            if ($request->getControllerName() != 'gerenciamento-de-usuarios' || 'cadastro' != $request->getActionName()) {
                $url = Zend_Registry::get('config')->paths->site->base . '/gerenciamento-de-usuarios/dados-de-acesso';
                $this->getResponse()->setRedirect($url)->sendResponse();
                return true;
            }
        }

        $login = App_Plugin_Login::getInstance();
        if ($login->hasIdentity()) {

            //verfica se eh master user
            if ($login->hasMasterUser() && ($login->getMasterUser()->getCodigo() == $login->getIdentity()->getCodigo()) && $request->getActionName() != 'acesso-administrador') {
                $this->getResponse()->setHttpResponseCode(401)
                    ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/acesso-administrador')
                    ->sendResponse();
                exit();
            }

            //a��es que n�o requer permiss�o para serem executadas
            if (array_key_exists($request->getControllerName(), $this->semPermissao) && (in_array($request->getActionName(), $this->semPermissao[$request->getControllerName()]))) {
                return true;
            } else {
                //verifica a permissao
                $found = false;
                
                foreach ($login->getIdentity()->getPermissoes() as $acao) {
                    if ($acao->getNome() == $request->getActionName() && $acao->getModulo()->getNome() == $request->getControllerName()) {
                        $found = true;
                        break;
                    }
                }

                if (false === $found) {
                    $this->getResponse()->setHttpResponseCode(401)
                        ->setRedirect(Zend_Registry::get('config')->paths->site->base . '/index/access-denied')
                        ->sendResponse();
                    exit();
                }
            }
        } else {
            $this->getResponse()->setHttpResponseCode(401)
                ->setRedirect(Zend_Registry::get('config')->paths->site->base)
                ->sendResponse();
            exit();
        }
    }

}
