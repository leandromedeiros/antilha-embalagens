<?php

class App_Model_Webservice
{

  /**
   * @throws SoapFault
   */
  public function __construct()
  {
    set_time_limit(0);
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
      if (isset($_SERVER['HTTP_AUTHORIZATION']) && (strlen($_SERVER['HTTP_AUTHORIZATION']) > 0)) {
        list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
        if (strlen($_SERVER['PHP_AUTH_USER']) == 0 || strlen($_SERVER['PHP_AUTH_PW']) == 0) {
          unset($_SERVER['PHP_AUTH_USER']);
          unset($_SERVER['PHP_AUTH_PW']);
        }
      }
    }

    // Throw SOAP fault se a autenticação falhar
    if (!$this->autenticacao($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
      throw new SoapFault("Usuário o usenha inválidos.", $_SERVER['PHP_AUTH_USER']);
    }
  }

  /**
   * Faz a autenticação do usuário para acessar as funções do webservice
   * @param string $username
   * @param string $password
   * @return bool
   */
  protected function autenticacao($username, $password)
  {
    $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
    $retorno = false;
    if (strlen(trim($username)) > 0 && strlen(trim($password)) > 0) {
      $objUsuario = $daoUsuarios->fetchRow(
        $daoUsuarios->select()
          ->where('usr_status = ?', 1)
          ->where('usr_login = ?', $username)
          ->where('usr_senha = ?', md5($password))
      );

      if ($objUsuario) {
        foreach ($objUsuario->getPermissoes() as $acao) {
          if ($acao->getModulo()->getNome() == 'webservice') {
            $retorno = true;
            break;
          }
        }
      }
    }
    return $retorno;
  }

  /**
   * Insere e atualiza os grupos de rede
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Grupo de Rede do Cod.: 587 inserido com sucesso.</item>
   * 		<item>Grupo de Rede do Cod.: 587 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_GrupoRede[] $gruposRede
   * @return App_Model_Response
   */
  public function insereAtualizaGruposRede($gruposRede)
  {
    $logs = array();
    $success = false;
    $acao = 'atualizado';
    $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();

    if (count($gruposRede)) {
      foreach ($gruposRede as $grupo) {
        try {
          //verifica se o grupo ja existe
          $objGrupo = $daoGrupoRedes->find($grupo->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objGrupo) {
            $acao = 'inserido';
            $objGrupo = $daoGrupoRedes->createRow();
            $objGrupo->setCodigo($grupo->codigo);
          }

          $objGrupo->setNome($grupo->nome);
          $objGrupo->save();
          $logs[] = "Grupo de Rede do Cod.: {$grupo->codigo} {$acao} com sucesso.";
          unset($objGrupo);
          $success = true;
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados do grupo de cod.: {$grupo->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados do grupo de cod.: {$grupo->codigo}. Motivo: {$e->getMessage()}";
        }
      }
      unset($gruposRede);
    } else {
      $logs[] = "Nenhum grupo informado.";
    }

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Grupo de Redes {$acao}(s) com sucesso" : "Alguns grupos não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove grupos de rede
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $grupos = array('110', 20);
   * removeGruposRede($grupos);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Grupo de Rede do Cod.: 587 excluído com sucesso.</item>
   * 		<item>Grupo de Rede do Cod.: 587 excluído com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $gruposRede
   * @return App_Model_Response
   */
  public function removeGruposRede($gruposRede)
  {
    $messages = array();
    $success = true;
    $daoGrupoRedes = App_Model_DAO_GrupoRedes::getInstance();

    if (count($gruposRede)) {
      foreach ($gruposRede as $idGrupo) {
        try {
          //verifica se o grupo já existe
          $objGrupo = $daoGrupoRedes->find($idGrupo)->current();

          //se não existe, grava o erro no log
          if (!$objGrupo) {
            throw new Exception("Grupo de rede de cod.: {$idGrupo} inexistente.");
          } else {
            $objGrupo->delete();
            $logs[] = "Grupo de rede de cod.: {$idGrupo} excluído com sucesso.";
          }
          unset($objGrupo);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o grupo de rede de cod.: {$idGrupo}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o grupo de rede de cod.: {$idGrupo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum grupo informado.";
    }
    unset($gruposRede, $daoGrupoRedes);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Grupo(s) de rede removido(s) com sucesso' : 'Alguns grupos não foram removidos, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  //FIM GRUPOS DE REDE

  /**
   * Insere e atualiza as redes
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Rede do Cod.: 587 inserida com sucesso.</item>
   * 		<item>Rede do Cod.: 587 atualizada com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Rede[] $redes
   * @return App_Model_Response
   */
  public function insereAtualizaRede($redes)
  {
    $logs = array();
    $success = true;
    $acao = 'atualizada';
    $daoRedes = App_Model_DAO_Redes::getInstance();
    $daoGruposRedes = App_Model_DAO_GrupoRedes::getInstance();

    if (count($redes)) {
      foreach ($redes as $rede) {
        try {
          //verifica se o grupo já existe
          $objRede = $daoRedes->find($rede->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objRede) {
            $acao = 'inserida';
            $objRede = $daoRedes->createRow();
            $objRede->setCodigo($rede->codigo);
          }

          $grupoRede = $daoGruposRedes->fetchRow(
            $daoGruposRedes->select()->where('grp_rede_idGrupo = ?', $rede->grupo)
          );
          if (!$grupoRede)
            throw new Exception('Grupo não encontrado.');

          $objRede->setGrupo($grupoRede)
            ->setNome($rede->nome)
            ->setCodigoMatriz($rede->codigoMatriz);

          $objRede->save();
          $logs[] = "Rede de cod.: {$rede->codigo} {$acao} com sucesso.";
          unset($objRede);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados da rede de cod.: {$rede->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados da rede de cod.: {$rede->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma rede informada.";
    }
    unset($redes, $daoRedes, $daoGruposRedes);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Rede(s) {$acao}(s) com sucesso" : "Algumas redes não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove redes
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $redes = array('110', 20);
   * removeRedes($redes);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Rede do Cod.: 587 excluída com sucesso.</item>
   * 		<item>Rede do Cod.: 587 excluída com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $redes
   * @return App_Model_Response
   */
  public function removeRedes($redes)
  {
    $logs = array();
    $success = true;
    $daoRedes = App_Model_DAO_Redes::getInstance();

    if (count($redes)) {
      foreach ($redes as $idRede) {
        try {
          //verifica se o grupo ja existe
          $objRede = $daoRedes->find($idRede)->current();

          //se não existe, grava o erro no log
          if (!$objRede) {
            throw new Exception("Rede de cod.: {$idRede} inexistente.");
          } else {
            $objRede->delete();
            $logs[] = "Rede de cod.: {$idRede} excluído com sucesso.";
          }
          unset($objRede);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a rede de cod.: {$idRede}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a rede de cod.: {$idRede}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma rede informada.";
    }
    unset($redes, $daoRedes);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Rede(s) removida(s) com sucesso' : 'Algumas redes não foram removidas, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  // FIM REDES

  /**
   * Insere e atualiza os grupos de lojas
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Grupo de Loja do Cod.: 587 inserido com sucesso.</item>
   * 		<item>Grupo de Loja do Cod.: 587 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_GrupoLoja[] $gruposLoja
   * @return App_Model_Response
   */
  public function insereAtualizaGruposLoja($gruposLoja)
  {
    $logs = array();
    $success = true;
    $acao = 'atualizado';
    $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();
    $daoRedes = App_Model_DAO_Redes::getInstance();

    if (count($gruposLoja)) {
      foreach ($gruposLoja as $grupo) {
        try {
          //verifica se o grupo ja existe
          $objGrupo = $daoGrupoLojas->find($grupo->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objGrupo) {
            $acao = 'inserido';
            $objGrupo = $daoGrupoLojas->createRow();
            $objGrupo->setCodigo($grupo->codigo);
          }

          $rede = $daoRedes->fetchRow(
            $daoRedes->select()->from($daoRedes)->where('rede_idRede = ?', $grupo->rede)
          );
          if (!$rede)
            throw new Exception('Rede não encontrada.');

          $objGrupo->setRede($rede)
            ->setNome($grupo->nome)
            ->setTipoPedido($grupo->tipoPedido)
            ->setClienteMatriz($grupo->clienteMatriz)
            ->setTemAprovadorIntermediario($grupo->temAprovadorIntermediario ? 1 : 0);

          $objGrupo->save();
          $logs[] = "Grupo de loja de cod.: {$grupo->codigo} {$acao} com sucesso.";

          unset($objGrupo);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados do grupo de cod.: {$grupo->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados do grupo de cod.: {$grupo->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma loja informada.";
    }
    unset($gruposLoja, $daoGrupoLojas, $daoRedes);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Grupo(s) de loja {$acao}(s) com sucesso" : "Alguns grupos não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove grupos de loja
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $grupos = array('110', 20);
   * removeGruposLoja($grupos);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Grupo de Loja do Cod.: 587 excluído com sucesso.</item>
   * 		<item>Grupo de Loja do Cod.: 587 excluído com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $gruposLoja
   * @return App_Model_Response
   */
  public function removeGruposLoja($gruposLoja)
  {
    $logs = array();
    $success = true;
    $daoGrupoLojas = App_Model_DAO_GrupoLojas::getInstance();

    if (count($gruposLoja)) {
      foreach ($gruposLoja as $idGrupo) {
        try {
          //verifica se o grupo ja existe
          $objGrupo = $daoGrupoLojas->find($idGrupo)->current();

          //se não existe, grava o erro no log
          if (!$objGrupo) {
            throw new Exception("Grupo de loja de cod.: {$idGrupo} inexistente.");
          } else {
            $objGrupo->delete();
            $logs[] = "Grupo de loja de cod.: {$idGrupo} excluído com sucesso.";
          }
          unset($objGrupo);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o grupo de loja de cod.: {$idGrupo}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o grupo de loja de cod.: {$idGrupo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma loja informada.";
    }
    unset($gruposLoja, $daoGrupoLojas);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Grupo(s) de loja removido(s) com sucesso' : 'Alguns grupos não foram removidos, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  //FIM GRUPOS DE LOJA

  /**
   * Insere e atualiza as lojas
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Loja do Cod.: 587 inserida com sucesso.</item>
   * 		<item>Loja do Cod.: 588 atualizada com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Loja[] $lojas
   * @return App_Model_Response
   */
  public function insereAtualizaLojas($lojas)
  {

    $logs = array();
    $success = true;
    $acao = 'atualizada';

    $daoLojas = App_Model_DAO_Lojas::getInstance();
    $daoGrupos = App_Model_DAO_GrupoLojas::getInstance();
    $daoTipos = App_Model_DAO_TiposLoja::getInstance();
    $daoEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance();
    $daoContatos = App_Model_DAO_Lojas_Contatos::getInstance();

    if (count($lojas)) {
      foreach ($lojas as $loja) {
        try {
          //verifica se a loja já existe
          $objLoja = $daoLojas->find($loja->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objLoja) {
            $acao = 'inserida';
            $objLoja = $daoLojas->createRow();
            $objLoja->setCodigo($loja->codigo);
          }

          $grupo = $daoGrupos->fetchRow(
            $daoGrupos->select()->from($daoGrupos)->where('grp_loja_idGrupo = ?', $loja->grupo)
          );
          if (!$grupo)
            throw new Exception('Grupo não encontrado.');

          $tipo = $daoTipos->fetchRow(
            $daoTipos->select()->from($daoTipos)->where('tipo_loja_idTipo = ?', $loja->tipo)
          );
          if (!$tipo)
            throw new Exception('Tipo da loja não encontrado.');

          $objLoja->setGrupo($grupo)
            ->setTipo($tipo)
            ->setCodigoLojaCliente($loja->codigoLojaCliente)
            ->setCnpj($loja->cnpj)
            ->setEmail($loja->email)
            ->setIcms($loja->icms)
            ->setIe($loja->ie)
            ->setNomeCompleto(utf8_decode($loja->nomeCompleto))
            ->setNomeReduzido(utf8_decode($loja->nomeReduzido))
            ->setTransportadora($loja->transportadora)
            ->setCondicaoFrete($loja->condicaoFrete)
            ->setTipoBloqueio($loja->tipoBloqueio)
            ->setClassificacao($loja->classificacao)
            ->setTemAprovadorIntermediario(0);

          $objLoja->getEnderecos()->offsetRemoveAll();
          foreach ($loja->enderecos as $endereco) {
            $objEndereco = $daoEnderecos->createRow();
            $objEndereco->setTipo($endereco->tipo)
              ->setPrazo($endereco->prazo)
              ->setPadrao($endereco->padrao)
              ->getEndereco()
              ->setCep($endereco->cep)
              ->setLogradouro(utf8_decode($endereco->logradouro))
              ->setNumero(utf8_decode($endereco->numero))
              ->setComplemento(utf8_decode($endereco->complemento))
              ->setBairro(utf8_decode($endereco->bairro))
              ->setCidade(utf8_decode($endereco->cidade))
              ->setUf($endereco->uf);
            $objLoja->getEnderecos()->offsetAdd($objEndereco);
          }

          $objLoja->getContatos()->offsetRemoveAll();
          foreach ($loja->contatos as $contato) {
            $objContato = $daoContatos->createRow();
            $objContato->setTipo($contato->tipo)
              ->setObjetivo(utf8_decode($contato->objetivo))
              ->setValor(utf8_decode($contato->valor));
            $objLoja->getContatos()->offsetAdd($objContato);
          }

          $objLoja->save();
          $logs[] = "Loja de código {$loja->codigo} {$acao} com sucesso.";

          unset($objLoja);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados da loja de cod.: {$loja->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados da loja de cod.: {$loja->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma loja informada.";
    }
    unset($lojas, $daoLojas, $daoGrupos, $daoTipos, $daoEnderecos, $daoContatos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Loja(s) {$acao}(s) com sucesso" : "Algumas lojas não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove loja
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $grupos = array('110', 20);
   * removeLojas($lojas);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Loja do Cod.: 587 excluída com sucesso.</item>
   * 		<item>Loja do Cod.: 587 excluída com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $lojas
   * @return App_Model_Response
   */
  public function removeLojas($lojas)
  {
    $logs = array();
    $success = true;
    $daoLojas = App_Model_DAO_Lojas::getInstance();

    if (count($lojas)) {
      foreach ($lojas as $idLoja) {
        try {
          //verifica se a loja já existe
          $objLoja = $daoLojas->find($idLoja)->current();

          //se não existe, grava o erro no log
          if (!$objLoja) {
            throw new Exception("Loja de cod.: {$idLoja} inexistente.");
          } else {
            $objLoja->delete();
            $logs[] = "Loja de cod.: {$idLoja} excluída com sucesso.";
          }
          unset($objLoja);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a loja de cod.: {$idLoja}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a loja de cod.: {$idLoja}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma loja informada.";
    }
    unset($lojas, $daoLojas);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Loja(s) removida(s) com sucesso' : 'Algumas lojas não foram removidas, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  //FIM LOJA

  /**
   * Insere e atualiza as linhas
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Linha do Cod.: 587 inserida com sucesso.</item>
   * 		<item>Linha do Cod.: 587 atualizada com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Linha[] $linhas
   * @return App_Model_Response
   */
  public function insereAtualizaLinhas($linhas)
  {
    $logs = array();
    $success = true;
    $acao = 'atualizada';
    $daoLinhas = App_Model_DAO_Linhas::getInstance();
    $daoRedes = App_Model_DAO_Redes::getInstance();

    if (count($linhas)) {
      foreach ($linhas as $linha) {
        try {
          //verifica se a linha já existe
          $objLinha = $daoLinhas->find($linha->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objLinha) {
            $acao = 'inserida';
            $objLinha = $daoLinhas->createRow();
            $objLinha->setCodigo($linha->codigo);
          }

          $rede = $daoRedes->fetchRow(
            $daoRedes->select()->from($daoRedes)->where('rede_idRede = ?', $linha->rede)
          );
          if (!$rede)
            throw new Exception('Rede não encontrada.');

          $objLinha->setRede($rede)
            ->setNome(utf8_decode($linha->nome))
            ->setBloqueioAtraso($linha->bloqueioAtraso)
            ->setCompraUnica($linha->compraUnica)
            ->setStatus(1)
            ->setVm($linha->vm);

          if ($linha->dataVitrine)
            $objLinha->setDataVitrine(App_Funcoes_Date::conversion($linha->dataVitrine));

          $objLinha->save();
          $logs[] = "Linha de cod.: {$linha->codigo} {$acao} com sucesso.";

          unset($objLinha);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados da linha de cod.: {$linha->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados da linha de cod.: {$linha->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma linha informada.";
    }
    unset($linhas, $daoLinhas, $daoRedes);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Linha(s) {$acao}(s) com sucesso" : "Algumas linhas não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove Linha
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $grupos = array('110', 20);
   * removeLinhas($linhas);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Linha do Cod.: 587 excluída com sucesso.</item>
   * 		<item>Linha do Cod.: 587 excluída com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $linhas
   * @return App_Model_Response
   */
  public function removeLinhas($linhas)
  {
    $logs = array();
    $success = true;
    $daoLinhas = App_Model_DAO_Linhas::getInstance();

    if (count($linhas)) {
      foreach ($linhas as $idLinha) {
        try {
          //verifica se a loja já existe
          $objLinha = $daoLinhas->find($idLinha)->current();

          //se não existe, grava o erro no log
          if (!$objLinha) {
            throw new Exception("Linha de cod.: {$idLinha} inexistente.");
          } else {
            $objLinha->delete();
            $logs[] = "Linha de cod.: {$idLinha} excluída com sucesso.";
          }
          unset($objLinha);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a linha de cod.: {$idLinha}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a linha de cod.: {$idLinha}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma linha informada.";
    }
    unset($linhas, $daoLinhas);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Linha(s) removida(s) com sucesso' : 'Algumas linhas não foram removidas, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Insere e atualiza os produtos
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Produto do Cod.: 587 inserido com sucesso.</item>
   * 		<item>Produto do Cod.: 587 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Produto[] $produtos
   * @return App_Model_Response
   */
  public function insereAtualizaProdutos($produtos)
  {

    $logs = array();
    $success = true;
    $acao = 'atualizado';
    $daoProdutos = App_Model_DAO_Produtos::getInstance();
    $daoLinhas = App_Model_DAO_Linhas::getInstance();

    if (count($produtos)) {
      foreach ($produtos as $produto) {
        try {
          //verifica se o produto já existe
          $objProduto = $daoProdutos->find($produto->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objProduto) {
            $acao = 'inserido';
            $objProduto = $daoProdutos->createRow();
            $objProduto->setCodigo($produto->codigo);
          }

          $linha = $daoLinhas->fetchRow(
            $daoLinhas->select()->from($daoLinhas)->where('lin_idLinha = ?', $produto->linha)
          );
          if (!$linha)
            throw new Exception('Linha não encontrada.');

          $objProduto->setLinha($linha)
            ->setDescricao(htmlentities($produto->descricao))
            ->setUnidade($produto->unidade)
            ->setIdClienteExterno($produto->idClienteExterno)
            ->setCcf($produto->ccf)
            ->setMedidasNet($produto->medidasNet)
            ->setUnidDesc($produto->unidDesc)
            ->setPcModulo($produto->pcModulo)
            ->setQtdePacotes($produto->qtdePacotes)
            ->setPesoPacotes($produto->pesoPacote)
            ->setAliquotaIpi($produto->aliquotaIpi)
            ->setCodResumo($produto->codResumo)
            ->setSequenciaItens($produto->sequenciaItens)
            ->setStatus($produto->status)
            ->setPQtde($produto->qtde)
            ->setPEmpenho($produto->empenho)
            ->setPcEstoque($produto->cEstoque)
            ->setPTipoControle($produto->tipocontrole);

          $objProduto->save();
          $logs[] = "Produto de cod.: {$produto->codigo} {$acao} com sucesso.";
          unset($objProduto);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados do produto de cod.: {$produto->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados do produto de cod.: {$produto->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum produto informado.";
    }
    unset($produtos, $daoProdutos, $daoLinhas);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Produto(s) {$acao}(s) com sucesso" : "Alguns produtos não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove Produtos
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Produto do Cod.: 587 excluido com sucesso.</item>
   * 		<item>Produto do Cod.: 587 excluido com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Produto[] $produtos
   * @return App_Model_Response
   */
  public function removeProdutos($produtos)
  {

    $logs = array();
    $success = true;
    $daoProdutos = App_Model_DAO_Produtos::getInstance();

    if (count($produtos)) {
      foreach ($produtos as $idProduto) {
        try {
          //verifica se o produto já existe
          $objProduto = $daoProdutos->find($idProduto)->current();

          //se não existe, grava o erro no log
          if (!$objProduto) {
            throw new Exception("Produto de cod.: {$idProduto} inexistente.");
          } else {
            $objProduto->delete();
            $logs[] = "Produto de cod.: {$idProduto} excluída com sucesso.";
          }
          unset($objProduto);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o produto de cod.: {$idProduto}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o produto de cod.: {$idProduto}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum produto informado.";
    }
    unset($produtos, $daoProdutos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Produto(s) removido(s) com sucesso' : 'Alguns produtos não foram removidos, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Lista Pedidos
   *
   * @param string $tipo Tipos: (data|codigo)
   * @param string $de
   * @param string $ate
   * @return App_Model_Entity_Pedido[]
   */
  public function listaPedidos($tipo = null, $de = null, $ate = null)
  {

    $logs = array();
    $filters = array('*' => new Zend_Filter_StringTrim());

    $validators = array(
      'tipo' => array(
        Zend_Filter_Input::ALLOW_EMPTY => true,
        new Zend_Validate_InArray(array('data', 'codigo'))
      ),
      'de' => array(
        Zend_Filter_Input::ALLOW_EMPTY => true
      ),
      'ate' => array(
        Zend_Filter_Input::ALLOW_EMPTY => true
      )
    );

    $dados = array(
      'tipo' => $tipo,
      'de' => $de,
      'ate' => $ate
    );

    $filter = new Zend_Filter_Input($filters, $validators, $dados, array('escapeFilter' => 'StringTrim'));
    if (!$filter->isValid()) {
      $objErro = new stdClass();
      $objErro->message = utf8_encode('Por favor, preecha o parâmetros corretamente.');
      $objErro->success = false;
      foreach ($filter->getMessages() as $field => $message) {
        $erro = "Campo: {$field}";
        App_Funcoes_UTF8::encode($erro);
        $objErro->logs[] = $erro;
      }
      $retorno = $objErro;
    } else {

      $retorno = array();
      $daoPedidos = App_Model_DAO_Pedidos::getInstance();
      $daoItems = App_Model_DAO_Pedidos_Itens::getInstance();
      $daoLogs = App_Model_DAO_Pedidos_Logs::getInstance();
      $daoStatus = App_Model_DAO_Pedidos_Status::getInstance();
      $daoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
      $daoNotasItens = App_Model_DAO_Pedidos_Notas_Itens::getInstance();

      $select = $daoPedidos->getAdapter()->select()->from($daoPedidos->info('name'));
      $coluna = '';
      switch ($tipo) {
        case 'codigo':
          $coluna = 'ped_idPedido';
          break;
        case 'data':
          $coluna = 'DATE(ped_dataCadastro)';
          break;
      }

      if ($de && $ate) {
        $select->where(sprintf("%s BETWEEN {$de} AND {$ate}", $coluna));
      } else if ($de) {
        $select->where(sprintf("'{$de}' <= %s", $coluna));
      } else if ($ate) {
        $select->where(sprintf("%s <= '{$ate}'", $coluna));
      }

      /* $selectStatus = $daoStatus->select()->from($daoStatus->info('name'), 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = ped_idPedido');
              $select->joinLeft($daoStatus->info('name'), "ped_idPedido = ped_status_idPedido AND ped_status_idStatus = ({$selectStatus})")
              ->where('ped_status_valor <> 0'); */

      $pedidos = $daoPedidos->createRowset($daoPedidos->getAdapter()->fetchAll($select));
      foreach ($pedidos as $pedido) {
        // Dados do pedido
        $objPedido = new stdClass();
        $objPedido->codigo = $pedido->getCodigo();
        $objPedido->loja = $pedido->getLoja()->getCodigo();
        $objPedido->usuario = $pedido->getUsuario()->getCodigo();
        $objPedido->dataCadastro = $pedido->getDataCadastro();
        $objPedido->dataFaturamento = $pedido->getDataFaturamento();
        $objPedido->previsaoEntrega = $pedido->getPrevisaoEntrega();
        $objPedido->prazoEntrega = $pedido->getPrazoEntrega();
        $objPedido->numeroSAP = $pedido->getNumeroSAP();
        $objPedido->numeroNF = $pedido->getNumeroNF();
        $objPedido->condicaoPagamento = $pedido->getCondicaoPagamento() != null ? $pedido->getCondicaoPagamento()->getCodigo() : '';
        $objPedido->diasPagamento = $pedido->getDiasPagamento();
        $objPedido->transportadora = '';
        $objPedido->totalItens = $pedido->getTotalItens();
        $objPedido->totalPedido = $pedido->getTotalPedido();
        $objPedido->totalLiquido = $pedido->getTotalLiquido();
        $objPedido->totalFaturado = $pedido->getTotalFaturado();
        $objPedido->juros = $pedido->getJuros();
        $objPedido->totalJuros = $pedido->getTotalJuros();
        $objPedido->icms = $pedido->getICMS();
        $objPedido->tipoFrete = $pedido->getTipoFrete();
        $objPedido->valorFrete = $pedido->getValorFrete();
        $objPedido->origem = $pedido->getOrigem();
        $objPedido->tipoPreco = $pedido->getTipoPreco();
        $objPedido->totalICMS = $pedido->getTotalICMS();
        $objPedido->totalIPI = $pedido->getTotalIPI();
        $objPedido->totalPagamento = $pedido->getTotalPagamento();
        $objPedido->totalPeso = $pedido->getTotalPeso();
        $objPedido->totalLiquidoFaturado = $pedido->getTotalLiquidoFaturado();
        $objPedido->observacoes = utf8_encode($pedido->getObservacoes());
        $objPedido->prioridade = $pedido->getPrioridade();
        $objPedido->flagImpressao = $pedido->getFlagImpressao();
        $objPedido->matriz = $pedido->getMatriz();
        $objPedido->faturista = $pedido->getFaturista();
        $objPedido->recebedor = $pedido->getRecebedor();
        $objPedido->programacao = $pedido->getProgramacao();
        $objPedido->linha = $pedido->getLinha();
        $objPedido->seuNumero = utf8_encode($pedido->getSeuNumero());
        $objPedido->prefixo = $pedido->getPrefixo();
        $objPedido->checouDuplicata = $pedido->getChecouDuplicata();
        $objPedido->jurosEspecial = $pedido->getJurosEspecial();
        $objPedido->condicaoPagamentoEspecial = $pedido->getCondicaoPagamentoEspecial();
        $objPedido->diasEspecial = $pedido->getDiasEspecial();
        $objPedido->dataHoraAcred = $pedido->getDataHoraAcred();
        $objPedido->usuarioAcred = $pedido->getUsuarioAcred();
        $objPedido->gerado = $pedido->getGerado();
        $objPedido->dataColeta = $pedido->getDataColeta();
        $objPedido->codEmpre = $pedido->getCodEmpre();
        $objPedido->grade = $pedido->getGrade();
        $objPedido->status = array();
        $objPedido->logs = array();
        $objPedido->notas = array();
        $objPedido->itens = array();


        // Status
        foreach ($pedido->getStatus() as $status) {
          $objStatus = new stdClass();
          $objStatus->pedido = $pedido->getCodigo();
          $objStatus->data = $status->getData();
          $objStatus->dataEntregaParcial = $status->getDataEntregaParcial();
          $objStatus->descricao = utf8_encode($status->getDescricao());
          $objStatus->valor = $status->getValor();
          $objStatus->enviarEmail = $status->getEnviarEmail();
          $objPedido->status[] = $objStatus;
        }

        // Itens
        foreach ($pedido->getItens() as $item) {
          $objItem = new stdClass();
          $objItem->produto = $item->getProduto()->getCodigo();
          $objItem->numeroItem = $item->getNumeroItem();
          $objItem->qtdTotal = $item->getQtdTotal();
          $objItem->valorUnitario = $item->getValorUnitario();
          $objItem->valorTotal = $item->getValorTotal();
          $objItem->ipi = $item->getIPI();
          $objItem->icms = $item->getICMS();
          $objItem->unidade = $item->getUnidade();
          $objItem->status = $item->getStatus();
          $objItem->sequencialItens = $item->getSequencialItens();
          $objItem->totalPeso = $item->getTotalPeso();
          $objItem->saldoProduto = $item->getSaldoProduto();
          $objItem->saldoWcs = $item->getSaldoWcs();
          $objItem->qtdFaturado = $item->getQtdFaturado();
          $objItem->prefixo = $item->getPrefixo();
          $objPedido->itens[] = $objItem;
        }

        // Logs
        foreach ($pedido->getLogs() as $log) {
          $objLog = new stdClass();
          $objLog->usuario = $log->getUsuario()->getCodigo();
          $objLog->acao = utf8_encode($log->getAcao());
          $objLog->data = $log->getData();
          $objPedido->logs[] = $objLog;
        }

        // Notas
        foreach ($pedido->getNotas() as $nota) {

          $objNota = new stdClass();
          $objNota->pedido = $pedido->getCodigo();
          $objNota->codigo = $nota->getCodigo();
          $objNota->status = $nota->getStatus();
          $objNota->numero = $nota->getNumero();
          $objNota->serie = $nota->getSerie();
          $objNota->cliente = $nota->getCliente();
          $objNota->dataEmissao = $nota->getDataEmissao();
          $objNota->valorTotal = $nota->getValorTotal();
          $objNota->numeroPedidoCliente = $nota->getNumeroPedidoCliente();
          $objNota->transportadora = $nota->getTransportadora();
          $objNota->descricaoStatus = utf8_encode($nota->getDescricaoStatus());
          $objNota->prazoEntrega = $nota->getPrazoEntrega();
          $objNota->numeroFilial = $nota->getNumeroFilial();
          $objNota->caminhoXML = utf8_encode($nota->getCaminhoXML());
          $objNota->caminhoPDF = utf8_encode($nota->getCaminhoPDF());
          $objNota->itens = array();
          $objNota->boletos = array();

          // Itens da Nota
          foreach ($nota->getItens() as $item) {
            $objItem = new stdClass();
            $objItem->nota = $nota->getCodigo();
            $objItem->codigo = $item->getCodigo();
            $objItem->numeroFilial = $item->getNumeroFilial();
            $objItem->quantidade = $item->getQuantidade();
            $objItem->status = $item->getStatus();
            $objItem->produto = utf8_encode($item->getProduto());
            $objItem->valorTotal = $item->getValorTotal();
            $objItem->descricaoStatus = utf8_encode($item->getDescricaoStatus());
            $objNota->itens[] = $objItem;
          }

          // Boletos
          foreach ($nota->getBoletos() as $boleto) {
            $objBoleto = new stdClass();
            $objBoleto->nota = $nota->getCodigo();
            $objBoleto->codigo = $boleto->getCodigo();
            $objBoleto->banco = $boleto->getBanco();
            $objBoleto->docto = utf8_encode($boleto->getDocto());
            $objBoleto->cliente = utf8_encode($boleto->getCliente());
            $objBoleto->duplicata = utf8_encode($boleto->getDuplicata());
            $objBoleto->valor = $boleto->getValor();
            $objBoleto->dataEmissao = $boleto->getDataEmissao();
            $objBoleto->dataVencimento = $boleto->getDataVencimento();
            $objBoleto->texto = utf8_encode($boleto->getTexto());
            $objBoleto->dias = $boleto->getDias();
            $objBoleto->dataBase = $boleto->getDataBase();
            $objBoleto->tipoDoc = $boleto->getTipoDoc();
            $objBoleto->link_boleto_2v = $boleto->getLinkBoleto2v();
            $objNota->boletos[] = $objBoleto;
          }
          $objPedido->notas[] = $objNota;
        }
        $retorno[] = $objPedido;
      }
    }
    return $retorno;
  }

  /**
   * Insere e atualiza tipos de loja
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Tipo do Cod.: 587 inserido com sucesso.</item>
   * 		<item>Tipo do Cod.: 587 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_TipoLoja[] $tipos
   * @return App_Model_Response
   */
  public function insereAtualizaTipos($tipos)
  {

    $logs = array();
    $success = true;
    $acao = 'atualizado';
    $daoTipos = App_Model_DAO_TiposLoja::getInstance();

    if (count($tipos)) {
      foreach ($tipos as $tipo) {
        try {
          //verifica se o tipo já existe
          $objTipo = $daoTipos->find($tipo->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objTipo) {
            $acao = 'inserido';
            $objTipo = $daoTipos->createRow();
            $objTipo->setCodigo($tipo->codigo);
          }

          $objTipo->setNome($tipo->nome);

          $objTipo->save();
          $logs[] = "Tipo do cod.: {$objTipo->getCodigo()} {$acao} com sucesso.";
          unset($objTipo);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados do tipo de cod.: {$tipo->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados do tipo de cod.: {$tipo->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum tipo informado.";
    }
    unset($tipos, $daoTipos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Tipo(s) de loja {$acao}(s) com sucesso" : "Alguns tipos de loja não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove Tipo
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $tipos = array('110', 20);
   * removeTipos($tipos);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Tipos do Cod.: 587 excluído com sucesso.</item>
   * 		<item>Tipos do Cod.: 587 excluído com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $tipos
   * @return App_Model_Response
   */
  public function removeTipos($tipos)
  {

    $logs = array();
    $success = true;
    $daoTipos = App_Model_DAO_TiposLoja::getInstance();

    if (count($tipos)) {
      foreach ($tipos as $idTipo) {
        try {
          //verifica se o tipo já existe
          $objTipo = $daoTipos->find($idTipo)->current();

          //se não existe, grava o erro no log
          if (!$objTipo) {
            throw new Exception("Tipo de cod.: {$idTipo} inexistente.");
          } else {
            $objTipo->delete();
            $logs[] = "Tipo de cod.: {$idTipo} excluída com sucesso.";
          }
          unset($objTipo);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o tipo de cod.: {$idTipo}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o tipo de cod.: {$idTipo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum tipo informado.";
    }
    unset($tipos, $daoTipos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Tipo(s) de loja removido(s) com sucesso' : 'Alguns tipos de loja não foram removidos, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Insere e atualiza os preços de produto
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Preço do Cod.: 587 inserido com sucesso.</item>
   * 		<item>Preço do Cod.: 588 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Preco[] $precos
   * @return App_Model_Response
   */
  public function insereAtualizaPrecos($precos)
  {

    $logs = array();
    $success = true;
    $acao = 'atualizado';
    $daoPrecos = App_Model_DAO_Precos::getInstance();
    $daoProdutos = App_Model_DAO_Produtos::getInstance();
    $daoTipos = App_Model_DAO_TiposLoja::getInstance();

    if (count($precos)) {
      foreach ($precos as $preco) {
        try {

          $produto = $daoProdutos->fetchRow(
            $daoProdutos->select()->from($daoProdutos)->where('prod_idProduto = ?', $preco->produto)
          );
          if (!$produto)
            throw new Exception('Produto não encontrado.');

          $tipo = $daoTipos->fetchRow(
            $daoTipos->select()->from($daoTipos)->where('tipo_loja_idTipo = ?', $preco->tipo)
          );
          if (!$tipo)
            throw new Exception('Tipo não encontrado.');

          //verifica se o preço já existe
          $objPreco = null;
          $objPreco = $daoPrecos->fetchRow(
            $daoPrecos->select()->from($daoPrecos)
              ->where('prec_idProduto = ?', $preco->produto)
              ->where('prec_idTipo = ?', $preco->tipo)
          );

          //se não existe, cria um registro novo
          if (!$objPreco) {
            $acao = 'inserido';
            $objPreco = $daoPrecos->createRow();
          }

          $objPreco->setProduto($produto)
            ->setTipo($tipo)
            ->setTbPreco($preco->tpPreco)
            ->setPreco($preco->preco)
            ->setTpPrecoDesc($preco->tpPrecoDesc);

          $objPreco->save();
          $logs[] = "Preço do cod.: {$preco->produto}/{$preco->tipo} {$acao} com sucesso.";
          unset($objPreco);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados do preço do cod.: {$preco->produto}/{$preco->tipo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados do preço do cod.: {$preco->produto}/{$preco->tipo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum preço informado.";
    }
    unset($precos, $daoPrecos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Preço(s) {$acao}(s) com sucesso" : "Alguns preços não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove Preço
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $precos = array('110', 20);
   * removePrecos($precos);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Preço do Cod.: 587 excluído com sucesso.</item>
   * 		<item>Preço do Cod.: 587 excluído com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param App_Model_Entity_Preco[] $precos
   * @return App_Model_Response
   */
  public function removePrecos($precos)
  {
    $logs = array();
    $success = true;
    $daoPrecos = App_Model_DAO_Precos::getInstance();
    $daoProdutos = App_Model_DAO_Produtos::getInstance();
    $daoTipos = App_Model_DAO_TiposLoja::getInstance();

    if (count($precos)) {
      foreach ($precos as $preco) {
        try {
          $produto = $daoProdutos->fetchRow(
            $daoProdutos->select()->from($daoProdutos)->where('prod_idProduto = ?', $preco->produto)
          );
          if (!$produto)
            throw new Exception('Produto não encontrado.');

          $tipo = $daoTipos->fetchRow(
            $daoTipos->select()->from($daoTipos)->where('tipo_loja_idTipo = ?', $preco->tipo)
          );
          if (!$tipo)
            throw new Exception('Tipo não encontrado.');

          // Verifica se o preço já existe
          $objPreco = $daoPrecos->fetchRow(
            $daoPrecos->select()->from($daoPrecos)
              ->where('prec_idProduto = ?', $preco->produto)
              ->where('prec_idTipo = ?', $preco->tipo)
          );

          // Se não existe, grava o erro no log
          if (!$objPreco) {
            throw new Exception("Preço inexistente.");
          } else {
            $objPreco->delete();
            $logs[] = "Preço excluído com sucesso.";
          }
          unset($objPreco);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o preço cod.: {$preco->produto}/{$preco->tipo}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o preço cod.: {$preco->produto}/{$preco->tipo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum preço informado.";
    }
    unset($precos, $daoPrecos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Preço(s) removido(s) com sucesso' : 'Alguns preços não foram removidos, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Insere e atualiza as condições de pagamento
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Condição do Cod.: 587 inserida com sucesso.</item>
   * 		<item>Condição do Cod.: 588 atualizada com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Pagamento[] $pagamentos
   * @return App_Model_Response
   */
  public function insereAtualizaCondicaoPgto($pagamentos)
  {

    $logs = array();
    $success = true;
    $acao = 'atualizada';
    $daoPagamentos = App_Model_DAO_Pagamentos::getInstance();
    $daoLinhas = App_Model_DAO_Linhas::getInstance();
    $daoRedes = App_Model_DAO_Redes::getInstance();

    if (count($pagamentos)) {
      foreach ($pagamentos as $pagamento) {
        try {
          //verifica se o condição já existe
          $objPagamento = $daoPagamentos->find($pagamento->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objPagamento) {
            $acao = 'inserida';
            $objPagamento = $daoPagamentos->createRow();
            if ($pagamento->codigo)
              $objPagamento->setCodigo($pagamento->codigo);
          }

          $rede = $daoRedes->fetchRow(
            $daoRedes->select()->from($daoRedes)->where('rede_idRede = ?', $pagamento->rede)
          );
          if (!$rede)
            throw new Exception('Rede não encontrada.');

          $objPagamento->setUF($pagamento->uf)
            ->setClassificacao($pagamento->classificacao)
            ->setRede($rede)
            ->setTipoLoja($pagamento->tipoLoja)
            ->setFatMinimo($pagamento->fatMinimo)
            ->setMinPecasPedido($pagamento->minPecasPedido)
            ->setQtdMaxPedido($pagamento->qtdMaxPedido)
            ->setTaxaServico($pagamento->taxaServico)
            ->setMensagem($pagamento->mensagem)
            ->setFaixaInicial($pagamento->faixaInicial)
            ->setFaixaFinal($pagamento->faixaFinal)
            ->setJuros($pagamento->juros)
            ->setDesconto($pagamento->desconto)
            ->setCodigoPgto($pagamento->codigoPgto)
            ->setValidadeI(App_Funcoes_Date::conversion($pagamento->validadeI))
            ->setValidadeF(App_Funcoes_Date::conversion($pagamento->validadeF));

          if ($pagamento->linha) {
            $linha = $daoLinhas->fetchRow(
              $daoLinhas->select()->from($daoLinhas)->where('lin_idLinha = ?', $pagamento->linha)
            );
            if (!$linha)
              throw new Exception('Linha não encontrada.');
            $objPagamento->setLinha($linha);
          } else {
            $objPagamento->setLinha('');
          }

          $objPagamento->save();
          $logs[] = "Condição do cod.: {$objPagamento->getCodigo()} {$acao} com sucesso.";
          unset($objPagamento);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com as condições de cod.: {$pagamento->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com as condições de cod.: {$pagamento->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma condição de pgto. informada.";
    }
    unset($pagamentos, $daoPagamentos, $daoLinhas, $daoRedes);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Condição(ões) de pgto. {$acao}(s) com sucesso" : "Algumas condições de pgto. não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove Condições de Pgto
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $condicoes = array('110', 20);
   * removeCondicoesPgto($condicoes);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Condição do Cod.: 587 excluída com sucesso.</item>
   * 		<item>Condição do Cod.: 587 excluída com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $condicoes
   * @return App_Model_Response
   */
  public function removeCondicoesPgto($condicoes)
  {

    $logs = array();
    $success = true;
    $daoPagamentos = App_Model_DAO_Pagamentos::getInstance();

    if (count($condicoes)) {
      foreach ($condicoes as $idPagamento) {
        try {
          //verifica se a condição já existe
          $objPagamento = $daoPagamentos->find($idPagamento)->current();

          //se não existe, grava o erro no log
          if (!$objPagamento) {
            throw new Exception("Condição de cod.: {$idPagamento} inexistente.");
          } else {
            $objPagamento->delete();
            $logs[] = "Condição de cod.: {$idPagamento} excluída com sucesso.";
          }
          unset($objPagamento);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a condição de cod.: {$idPagamento}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a condição de cod.: {$idPagamento}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma condição de pgto. informada.";
    }
    unset($condicoes, $daoPagamentos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Condição(ões) de pgto. removida(s) com sucesso' : 'Algumas condições de pgto. não foram removidas, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Insere e atualiza as Notas Fiscais
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Nota do Cod.: 587 inserida com sucesso.</item>
   * 		<item>Nota do Cod.: 588 atualizada com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Pedido_Nota[] $notas
   * @return App_Model_Response
   */
  public function insereAtualizaNotas($notas)
  {
    $logs = array();
    $success = true;
    $acao = 'atualizada';
    $daoPedidos = App_Model_DAO_Pedidos::getInstance();
    $daoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
    $daoNotasItens = App_Model_DAO_Pedidos_Notas_Itens::getInstance();
    $daoNotasBoletos = App_Model_DAO_Pedidos_Notas_Boletos::getInstance();

    if (count($notas)) {
      foreach ($notas as $nota) {
        try {
          $daoNotas->getAdapter()->beginTransaction();
          //verifica se a nota já existe
          $objNota = null;
          $objNota = $daoNotas->fetchRow($daoNotas->select()
            ->where('ped_nota_numero = ?', $nota->numero)
            ->where('ped_nota_serie = ?', $nota->serie));

          //se não existe, cria um registro novo
          if (!$objNota) {
            $acao = 'inserida';
            $objNota = $daoNotas->createRow();
          }

          $pedido = $daoPedidos->fetchRow(
            $daoPedidos->select()->from($daoPedidos)->where('ped_idPedido = ?', $nota->pedido)
          );
          if (!$pedido)
            throw new Exception('Pedido não encontrado.');

          $objNota->setPedido($pedido)
            ->setStatus($nota->status)
            ->setCliente($nota->cliente)
            ->setNumero($nota->numero)
            ->setSerie($nota->serie)
            ->setDataEmissao(App_Funcoes_Date::conversion($nota->dataEmissao))
            ->setValorTotal($nota->valorTotal)
            ->setNumeroPedidoCliente($nota->numeroPedidoCliente)
            ->setTransportadora($nota->transportadora)
            ->setDescricaoStatus($nota->descricaoStatus)
            ->setPrazoEntrega($nota->prazoEntrega)
            ->setNumeroFilial($nota->numeroFilial);

          $objNota->getItens()->offsetRemoveAll();
          foreach ($nota->itens as $item) {

            $objNotaItem = null;
            if (isset($item->codigo))
              $objNotaItem = $daoNotasItens->find($item->codigo)->current();

            if (!$objNotaItem) {
              $objNotaItem = $daoNotasItens->createRow();
            }

            $objNotaItem->setNumeroFilial($item->numeroFilial)
              ->setQuantidade($item->quantidade)
              ->setStatus($item->status)
              ->setProduto($item->produto)
              ->setValorTotal($item->valorTotal)
              ->setDescricaoStatus($item->descricaoStatus);

            $objNota->getItens()->offsetAdd($objNotaItem);
          }

          $objNota->save();

          //tratamento da nota fiscal
          $update = false;
          if (strlen($nota->caminhoPDF)) {
            $extensoesPermitidas = array('pdf');
            $pastaDestino = Zend_Registry::get('config')->documentos->notasFiscais;
            $binario = base64_decode($nota->caminhoPDF);

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $tipoArquivo = $finfo->buffer($binario);
            $auxExtensao = explode('/', $tipoArquivo);

            if (count($auxExtensao) && $auxExtensao[1]) {
              if (!in_array($auxExtensao[1], $extensoesPermitidas)) {
                throw new Exception('Arquivo da nota fiscal com extensão inválida. Extentão permitida: PDF.');
              }
            } else {
              throw new Exception('Arquivo da nota fiscal com extensão inválida. Extentão permitida: PDF.');
            }

            $arquivoDestino = $pastaDestino . $objNota->getCodigo() . ".{$auxExtensao[1]}";
            if (!file_put_contents($arquivoDestino, $binario))
              throw new Exception('Problema ao receber arquivo da nota fiscal.');
            chmod($arquivoDestino, 0777);

            $objNota->setCaminhoPDF($objNota->getCodigo() . ".{$auxExtensao[1]}");
            $update = true;
          } else if (strlen($objNota->getCaminhoPDF())) {
            $update = true;
            $arquivo = Zend_Registry::get('config')->documentos->notasFiscais . $objNota->getCaminhoPDF();
            if (file_exists($arquivo))
              unlink($arquivo);
            $objNota->setCaminhoPDF();
          }
          // fim nota fiscal
          //tratamento do arquivo de XML
          if (strlen($nota->caminhoXML)) {
            $extensoesPermitidas = array('xml');
            $pastaDestino = Zend_Registry::get('config')->documentos->xmls;
            $binario = base64_decode($nota->caminhoXML);

            $auxExtensao[1] = 'xml';

            $arquivoDestino = $pastaDestino . $objNota->getCodigo() . ".{$auxExtensao[1]}";
            if (!file_put_contents($arquivoDestino, $binario))
              throw new Exception('Problema ao receber arquivo XML.');;
            chmod($arquivoDestino, 0777);

            $objNota->setCaminhoXML($objNota->getCodigo() . ".{$auxExtensao[1]}");
            $update = true;
          } else if (strlen($objNota->getCaminhoXML())) {
            $update = true;
            $arquivo = Zend_Registry::get('config')->documentos->xmls . $objNota->getCaminhoXML();
            if (file_exists($arquivo))
              unlink($arquivo);
            $objNota->setCaminhoXML();
          }
          // fim XML

          if ($update)
            $objNota->save(true);

          $daoNotas->getAdapter()->commit();
          $logs[] = "Nota {$nota->numero}-{$nota->serie} {$acao} com sucesso.";
        } catch (App_Validate_Exception $e) {
          $daoNotas->getAdapter()->rollBack();
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com as notas {$nota->numero}-{$nota->serie}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $daoNotas->getAdapter()->rollBack();
          $success = false;
          $logs[] = "Houve um problema com as notas {$nota->numero}-{$nota->serie}. Motivo: {$e->getMessage()}";
        }
        unset($objNota);
      }
    } else {
      $logs[] = "Nenhuma nota informada.";
    }
    unset($notas, $daoNotas, $daoPedidos, $daoNotasItens);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Notas {$acao}(s) com sucesso" : "Algumas notas não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove Notas Fiscais
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $notas = array('110', 20);
   * removeNotas($notas);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Nota do Cod.: 587 excluída com sucesso.</item>
   * 		<item>Nota do Cod.: 587 excluída com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $notas
   * @return App_Model_Response
   */
  public function removeNotas($notas)
  {

    $logs = array();
    $success = true;
    $daoNotas = App_Model_DAO_Pedidos_Notas::getInstance();

    if (count($notas)) {
      foreach ($notas as $idNota) {
        try {
          //verifica se a nota já existe
          $objNota = $daoNotas->find($idNota)->current();

          //se não existe, grava o erro no log
          if (!$objNota) {
            throw new Exception("Nota de cod.: {$idNota} inexistente.");
          } else {
            $objNota->delete();
            $logs[] = "Notas de cod.: {$idNota} excluída com sucesso.";
          }
          unset($objNota);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a nota de cod.: {$idNota}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a nota de cod.: {$idNota}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma nota informada.";
    }
    unset($notas, $daoNotas);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Nota(s) removida(s) com sucesso' : 'Algumas notas não foram removidas, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Atualiza os status das notas
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros atualizados com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Status da nota 587 atualizado com sucesso.</item>
   * 		<item>Status da nota 588 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Pedido_Nota_Status[] $lista
   * @return App_Model_Response
   */
  public function atualizaStatusNota($lista)
  {

    $logs = array();
    $success = true;
    $daoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
    $daoStatus = App_Model_DAO_Pedidos_Notas_Status::getInstance();

    if (count($lista)) {
      foreach ($lista as $status) {
        try {
          $objStatus = $daoStatus->createRow();
          $nota = $daoNotas->fetchRow(
            $daoNotas->select()->from($daoNotas)->where('ped_nota_numero = ?', $status->nota)
          );

          if (!$nota)
            throw new Exception('Nota não encontrada.');

          $objStatus->setNota($nota->getCodigo())
            ->setData(App_Funcoes_Date::conversion($status->data))
            ->setCodigoOcorrencia($status->codOcorrencia)
            ->setHistorico(utf8_decode($status->historico));

          $objStatus->save();

          $logs[] = "Status da nota {$status->nota} atualizado com sucesso.";
          unset($objStatus);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com o status da nota: {$status->nota}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com o status da nota: {$status->nota}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma nota informada.";
    }
    unset($lista, $daoPedidos, $daoStatus);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Status da(s) nota(s) atualizada(s) com sucesso' : 'Alguns status das notas não foram atualizados, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Atualiza os status do pedido
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros atualizados com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Status do pedido: 587 atualizado com sucesso.</item>
   * 		<item>Status do pedido: 588 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Pedido_Status[] $lista
   * @return App_Model_Response
   */
  public function atualizaStatusPedido($lista)
  {
    set_time_limit(0);

    $logs = array();
    $success = true;
    $daoPedidos = App_Model_DAO_Pedidos::getInstance();
    $daoStatus = App_Model_DAO_Pedidos_Status::getInstance();

    if (count($lista)) {
      foreach ($lista as $status) {
        try {
          $objStatus = $daoStatus->createRow();
          $pedido = $daoPedidos->fetchRow(
            $daoPedidos->select()->from($daoPedidos)->where('ped_idPedido = ?', $status->pedido)
          );
          if (!$pedido)
            throw new Exception('Pedido não encontrado.');

          $objStatus->setPedido($pedido)
            ->setValor($status->valor)
            ->setenviarEmail($status->enviarEmail)
            ->setEnvioPendente($status->enviarEmail)
            ->setData(App_Funcoes_Date::conversion($status->data))
            ->setDescricao($status->descricao);

          if ($status->dataEntregaParcial)
            $objStatus->setDataEntregaParcial(App_Funcoes_Date::conversion($status->dataEntregaParcial));
          $objStatus->save();

          /* try {
                      if($status->enviarEmail) {
                      $daoStatus->enviaEmail($objStatus);
                      }
                      } catch (Exception $e) {} */

          $logs[] = "Status do pedido: {$status->pedido} atualizado com sucesso.";
          unset($objStatus);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com o status do pedido: {$status->pedido}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com o status do pedido: {$status->pedido}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum pedido informado.";
    }
    unset($lista, $daoPedidos, $daoStatus);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Status do(s) pedido(s) atualizado(s) com sucesso' : 'Alguns status dos pedidos não foram atualizados, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Cria e Atualiza Pedidos
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros inseridos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Pedido: 587 inserido com sucesso.</item>
   * 		<item>Pedido: 588 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Pedido[] $lista
   * @return App_Model_Response
   */
  public function insereAtualizaPedidos($lista)
  {
    set_time_limit(0);

    $logs = array();
    $success = true;
    $daoPedidos = App_Model_DAO_Pedidos::getInstance();
    $daoStatus = App_Model_DAO_Pedidos_Status::getInstance();
    $daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
    $daoLojas = App_Model_DAO_Lojas::getInstance();
    $daoTransp = App_Model_DAO_Transportadoras::getInstance();
    $daoPagamentos = App_Model_DAO_Pagamentos::getInstance();

    if (count($lista)) {
      foreach ($lista as $pedido) {
        try {

          $usuario = $daoUsuarios->fetchRow(
            $daoUsuarios->select()->from($daoUsuarios)->where('usr_idUsuario = ?', $pedido->usuario)
          );

          /*$usuarioAntilhas = $daoUsuarios->fetchRow(
                        $daoUsuarios->select()->from($daoUsuarios)->where('usr_idUsuario = ?', $pedido->idUsuarioAntilhas)
                    );*/

          //verifica se o pedido já existe
          $objPedido = $daoPedidos->fetchRow(
            $daoPedidos->select()->from($daoPedidos)->where('ped_idPedido = ?', $pedido->codigo)
          );

          //se não existe, cria um registro novo
          if (!$objPedido) {
            $acao = 'inserido';

            $objPedido = $daoPedidos->createRow();
            if ($pedido->codigo && $pedido->codigo != 0)
              $objPedido->setCodigo($pedido->codigo);
          } else {
            $acao = 'atualizado';
          }

          $objLoja = $daoLojas->find($pedido->loja)->current();
          $objTransp = $daoTransp->find($pedido->transportadora)->current();
          $objPagamento = $daoPagamentos->find($pedido->condicaoPagamento)->current();

          $objPedido->setLoja($objLoja)
            ->setUsuario($usuario)
            ->setTransportadora($objTransp)
            ->setPrazoEntrega($pedido->prazoEntrega)
            ->setNumeroSAP($pedido->numeroSAP)
            ->setNumeroNF($pedido->numeroNF)
            ->setCondicaoPagamento($objPagamento)
            ->setDiasPagamento($pedido->diasPagamento)
            ->setTotalItens($pedido->totalItens)
            ->setTotalPedido($pedido->totalPedido)
            ->setTotalLiquido($pedido->totalLiquido)
            ->setTotalFaturado($pedido->totalFaturado)
            ->setJuros($pedido->juros)
            ->setTotalJuros($pedido->totalJuros)
            ->setICMS($pedido->icms)
            ->setTipoFrete($pedido->tipoFrete)
            ->setValorFrete($pedido->valorFrete)
            ->setOrigem($pedido->origem)
            ->setTipoPreco($pedido->tipoPreco)
            ->setTotalICMS($pedido->totalICMS)
            ->setTotalIPI($pedido->totalIPI)
            ->setTotalPagamento($pedido->totalPagamento)
            ->setTotalPeso($pedido->totalPeso)
            ->setTotalLiquidoFaturado($pedido->totalLiquidoFaturado)
            ->setObservacoes($pedido->observacoes)
            ->setPrioridade($pedido->prioridade)
            ->setFlagImpressao($pedido->flagImpressao)
            ->setMatriz($pedido->matriz)
            ->setFaturista($pedido->faturista)
            ->setRecebedor($pedido->recebedor)
            ->setProgramacao($pedido->programacao)
            ->setLinha($pedido->linha)
            ->setSeuNumero($pedido->seuNumero)
            ->setPrefixo($pedido->prefixo)
            ->setChecouDuplicata($pedido->checouDuplicata)
            ->setJurosEspecial($pedido->jurosEspecial)
            ->setCondicaoPagamentoEspecial($pedido->condicaoPagamentoEspecial)
            ->setDiasEspecial($pedido->diasEspecial)
            ->setUsuarioAcred($pedido->usuarioAcred)
            ->setGerado($pedido->gerado)
            ->setCodEmpre($pedido->codEmpre)
            ->setGrade($pedido->grade)
            //->setUsuarioAntilhas($usuarioAntilhas)

            ->setItens($pedido->itens);

          if ($pedido->dataHoraAcred)
            $objPedido->setDataHoraAcred($pedido->dataHoraAcred);

          if ($pedido->dataCadastro)
            $objPedido->setDataCadastro($pedido->dataCadastro);

          if ($pedido->dataFaturamento)
            $objPedido->setDataFaturamento($pedido->dataFaturamento);

          if ($pedido->previsaoEntrega)
            $objPedido->setPrevisaoEntrega($pedido->previsaoEntrega);

          if ($pedido->dataColeta)
            $objPedido->setDataColeta($pedido->dataColeta);

          $objPedido->saveService();

          //$itens[] = $pedido->itens;

          if ($acao == 'inserido') {
            $logs[] = "Pedido: {$objPedido->getCodigo()} criado com sucesso.";
          } else {
            $logs[] = "Pedido: {$pedido->pedido} atualizado com sucesso.";
          }

          unset($objPedido);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com o pedido: {$pedido->pedido}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com o pedido: {$pedido->pedido}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum pedido informado.";
    }
    unset($lista, $daoPedidos, $daoStatus);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Pedido(s) {$acao}(s) com sucesso" : "Alguns dos pedidos não foram {$acao}(s), verifique o log de erro.");
    $retorno->success = $success;

    /*if (count($itens)) {
            foreach ($itens as $key => $value) {
                $retorno->itens[] = utf8_encode($value);
            }
        }*/

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Insere e atualiza os Boletos Internos
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Boleto do cod.: 587 inserido com sucesso.</item>
   * 		<item>Boleto do cod.: 588 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Pedido_Nota_Boleto[] $boletos
   * @return App_Model_Response
   */
  public function insereAtualizaBoletosInternos($boletos)
  {
    $logs = array();
    $success = true;
    $acao = 'atualizado';
    $daoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
    $daoBoletos = App_Model_DAO_Pedidos_Notas_Boletos::getInstance();

    if (count($boletos)) {
      foreach ($boletos as $boleto) {
        try {
          //verifica se o boleto já existe
          $objBoleto = $daoBoletos->find($boleto->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objBoleto) {
            $acao = 'inserido';
            $objBoleto = $daoBoletos->createRow();
            if ($boleto->codigo)
              $objBoleto->setCodigo($boleto->codigo);
          }

          $objBoleto->setNota($boleto->nota)
            ->setBanco($boleto->banco)
            ->setDocto($boleto->docto)
            ->setCliente($boleto->cliente)
            ->setDuplicata($boleto->duplicata)
            ->setValor($boleto->valor)
            ->setDataEmissao(App_Funcoes_Date::conversion($boleto->dataEmissao))
            ->setDataVencimento(App_Funcoes_Date::conversion($boleto->dataVencimento))
            ->setTexto($boleto->texto)
            ->setDias($boleto->dias)
            ->setDataBase(App_Funcoes_Date::conversion($boleto->dataBase))
            ->setTipoDoc($boleto->tipoDoc)
            ->setLinkBoleto2v($boleto->link_boleto_2v);

          $objBoleto->save();

          //tratamento do boleto
          $update = false;
          if (strlen($boleto->caminhoPDF)) {
            $extensoesPermitidas = array('pdf');
            $pastaDestino = Zend_Registry::get('config')->documentos->boletos;
            $binario = base64_decode($boleto->caminhoPDF);

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $tipoArquivo = $finfo->buffer($binario);
            $auxExtensao = explode('/', $tipoArquivo);

            if (count($auxExtensao) && $auxExtensao[1]) {
              if (!in_array($auxExtensao[1], $extensoesPermitidas)) {
                throw new Exception('Arquivo do boleto com extensão inválida. Extentão permitida: PDF.');
              }
            } else {
              throw new Exception('Arquivo do boleto com extensão inválida. Extentão permitida: PDF.');
            }

            $arquivoDestino = $pastaDestino . $objBoleto->getCodigo() . ".{$auxExtensao[1]}";
            if (!file_put_contents($arquivoDestino, $binario))
              throw new Exception('Problema ao receber arquivo do boleto.');
            chmod($arquivoDestino, 0777);

            $objBoleto->setCaminhoPDF($objBoleto->getCodigo() . ".{$auxExtensao[1]}");
            $update = true;
          } else if (strlen($objBoleto->getCaminhoPDF())) {
            $update = true;
            $arquivo = Zend_Registry::get('config')->documentos->boletos . $objBoleto->getCaminhoPDF();
            if (file_exists($arquivo))
              unlink($arquivo);
            $objBoleto->setCaminhoPDF();
          }
          // fim boleto

          if ($update)
            $objBoleto->save(true);


          $logs[] = "Boleto do cod.: {$objBoleto->getCodigo()} {$acao} com sucesso.";
          unset($objBoleto);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com as notas de cod.: {$objBoleto->getCodigo()}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com as notas de cod.: {$objBoleto->getCodigo()}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum boleto informado.";
    }
    unset($boletos, $daoNotas, $daoBoletos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Boleto(s) {$acao}(s) com sucesso" : "Alguns boletos não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }


  /**
   * Insere e atualiza as Boletos
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Boleto do cod.: 587 inserido com sucesso.</item>
   * 		<item>Boleto do cod.: 588 atualizado com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Pedido_Nota_Boleto[] $boletos
   * @return App_Model_Response
   */
  public function insereAtualizaBoletos($boletos)
  {

    $logs = array();
    $success = true;
    $acao = 'atualizado';
    $daoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
    $daoBoletos = App_Model_DAO_Pedidos_Notas_Boletos::getInstance();

    if (count($boletos)) {
      foreach ($boletos as $boleto) {
        try {
          //verifica se o boleto já existe
          $objBoleto = $daoBoletos->find($boleto->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objBoleto) {
            $acao = 'inserido';
            $objBoleto = $daoBoletos->createRow();
            if ($boleto->codigo)
              $objBoleto->setCodigo($boleto->codigo);
          }

          $objBoleto->setNota($boleto->nota)
            ->setBanco($boleto->banco)
            ->setDocto($boleto->docto)
            ->setCliente($boleto->cliente)
            ->setDuplicata($boleto->duplicata)
            ->setValor($boleto->valor)
            ->setDataEmissao(App_Funcoes_Date::conversion($boleto->dataEmissao))
            ->setDataVencimento(App_Funcoes_Date::conversion($boleto->dataVencimento))
            ->setTexto($boleto->texto)
            ->setDias($boleto->dias)
            ->setDataBase(App_Funcoes_Date::conversion($boleto->dataBase))
            ->setTipoDoc($boleto->tipoDoc)
            ->setLinkBoleto2v($boleto->link_boleto_2v);

          $objBoleto->save();
          $logs[] = "Boleto do cod.: {$objBoleto->getCodigo()} {$acao} com sucesso.";
          unset($objBoleto);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com as notas de cod.: {$objBoleto->getCodigo()}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com as notas de cod.: {$objBoleto->getCodigo()}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum boleto informado.";
    }
    unset($boletos, $daoNotas, $daoBoletos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Boleto(s) {$acao}(s) com sucesso" : "Alguns boletos não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove Boletos
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $notas = array('110', 20);
   * removeBoletos($boletos);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Boleto do Cod.: 587 excluído com sucesso.</item>
   * 		<item>Boleto do Cod.: 587 excluído com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $boletos
   * @return App_Model_Response
   */
  public function removeBoletos($boletos)
  {

    $logs = array();
    $success = true;
    $daoBoletos = App_Model_DAO_Pedidos_Notas_Boletos::getInstance();

    if (count($boletos)) {
      foreach ($boletos as $idBoleto) {
        try {
          //verifica se o boleto já existe
          $objBoleto = $daoBoletos->find($idBoleto)->current();

          //se não existe, grava o erro no log
          if (!$objBoleto) {
            throw new Exception("Boleto de cod.: {$idBoleto} inexistente.");
          } else {
            $objBoleto->delete();
            $logs[] = "Boleto de cod.: {$idBoleto} excluída com sucesso.";
          }
          unset($objBoleto);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o boleto de cod.: {$idBoleto}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover o boleto de cod.: {$idBoleto}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum boleto informado.";
    }
    unset($boletos, $daoBoletos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Boleto(s) removido(s) com sucesso' : 'Alguns boletos não foram removidos, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Insere e atualiza as transportadoras
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Transportadora do cod.: 587 inserida com sucesso.</item>
   * 		<item>Transportadora do cod.: 588 atualizada com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Transportadora[] $transportadoras
   * @return App_Model_Response
   */
  public function insereAtualizaTransportadora($transportadoras)
  {

    $logs = array();
    $success = true;
    $acao = 'atualizada';
    $daoTransp = App_Model_DAO_Transportadoras::getInstance();

    if (count($transportadoras)) {
      foreach ($transportadoras as $transp) {
        try {
          //verifica se a transportadora já existe
          $objTransp = $daoTransp->find($transp->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objTransp) {
            $acao = 'inserida';
            $objTransp = $daoTransp->createRow();
            if ($transp->codigo)
              $objTransp->setCodigo($transp->codigo);
          }
          $objTransp->setNome($transp->nome);

          $objTransp->save();
          $logs[] = "Transportadora do cod.: {$objTransp->getCodigo()} {$acao} com sucesso.";
          unset($objBoleto);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com as transportadoras de cod.: {$transp->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com as transportadoras de cod.: {$transp->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma transportadora informada.";
    }
    unset($transportadoras, $daoTransp);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Transportadora(s) {$acao}(s) com sucesso" : "Algumas transportadoras não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove Transportadoras
   *
   * @internal
   * Exemplo de uso
   * <code>
   *
   * $notas = array('110', 20);
   * removeTransportadoras($transportadoras);
   *
   * 	<message>Registros excluídos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Transportadora do Cod.: 587 excluída com sucesso.</item>
   * 		<item>Transportadora do Cod.: 587 excluída com sucesso.</item>
   * 		...
   * 	</logs>
   *
   * </code>
   *
   * @param array $transportadoras
   * @return App_Model_Response
   */
  public function removeTransportadoras($transportadoras)
  {

    $logs = array();
    $success = true;
    $daoTransp = App_Model_DAO_Transportadoras::getInstance();

    if (count($transportadoras)) {
      foreach ($transportadoras as $idTransp) {
        try {
          //verifica se a transportadora já existe
          $objTransp = $daoTransp->find($idTransp)->current();

          //se não existe, grava o erro no log
          if (!$objTransp) {
            throw new Exception("Transportadora de cod.: {$idTransp} inexistente.");
          } else {
            $objTransp->delete();
            $logs[] = "Transportadora de cod.: {$idTransp} excluída com sucesso.";
          }
          unset($objTransp);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a transportadora de cod.: {$idTransp}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema ao remover a transportadora de cod.: {$idTransp}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma transportadora informada.";
    }
    unset($transportadoras, $daoTransp);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Transportadora(s) removida(s) com sucesso' : 'Algumas transportadoras não foram removidas, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Atualiza os status das reclamações
   * Este método retorna um XML
   * <code>
   * 	<retorno>
   * 		<message>Registros atualizados com sucesso</message>
   * 		<success>1</success>
   * 		<logs>
   * 			<log1>Status da reclamação: 587 atualizado com sucesso.</log1>
   * 			<log2>Status da reclamação: 588 atualizado com sucesso.</log2>
   * 			...
   * 		</logs>
   * 	</retorno>
   * </code>
   *
   * @param App_Model_Entity_SugestaoReclamacao_Status[] $lista
   * @return App_Model_Response
   */
  public function atualizaStatusReclamacoes($lista)
  {

    $logs = array();
    $success = true;
    $daoReclamacoes = App_Model_DAO_SugestoesReclamacoes::getInstance();
    $daoStatus = App_Model_DAO_SugestoesReclamacoes_Status::getInstance();

    if (count($lista)) {
      foreach ($lista as $status) {
        try {
          $objStatus = $daoStatus->createRow();
          $reclamacao = $daoReclamacoes->fetchRow(
            $daoReclamacoes->select()->from($daoReclamacoes)->where('sug_rec_idSugestaoReclamacao = ?', $status->reclamacao)
          );
          if (!$reclamacao)
            throw new Exception('Reclamação não encontrada.');

          $daoReclamacoes->getAdapter()->beginTransaction();
          $reclamacao->setStatus($status->status);
          $reclamacao->save();

          $objStatus->setReclamacao($reclamacao)
            ->setCodigo($status->status)
            ->setNomeAtendente($status->atendente)
            ->setData(App_Funcoes_Date::conversion($status->data))
            ->setDescricao($status->descricao);

          $objStatus->save();
          $daoReclamacoes->getAdapter()->commit();
          $logs[] = "Status da reclamação: {$status->reclamacao} atualizado com sucesso.";
          unset($objStatus);
        } catch (App_Validate_Exception $e) {
          $daoReclamacoes->getAdapter()->rollBack();
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com o status da reclamação: {$status->reclamacao}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $daoReclamacoes->getAdapter()->rollBack();
          $success = false;
          $logs[] = "Houve um problema com o status da reclamação: {$status->reclamacao}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhuma reclamação informada.";
    }
    unset($lista, $daoReclamacoes, $daoStatus);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Status da reclamação atualizado(s) com sucesso' : 'Alguns status da reclamação não foram atualizados, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  //INICIO - MELHORIA PERMISSÃO DE PRODUTOS PARA VENDA

  /**
   * Insere e atualiza as permissoes de produto
   * Este metodo retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * </code>
   *
   * @param App_Model_Entity_ProdutoPermissao[] $produtoPermissao
   * @return App_Model_Response
   */
  public function insereAtualizaProdutoPermissao($produtoPermissao)
  {
    $logs = array();
    $success = false;
    $acao = 'atualizado';
    $daoProdutoPermissao = App_Model_DAO_ProdutoPermissao::getInstance();

    if (count($produtoPermissao)) {
      foreach ($produtoPermissao as $prodPermissao) {
        try {
          //verifica se a permissão ja existe
          $objProdPermissao = $daoProdutoPermissao->find($prodPermissao->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objProdPermissao) {
            $acao = 'inserido';
            $objProdPermissao = $daoProdutoPermissao->createRow();
          }

          if ($prodPermissao->codigo) {
            $objProdPermissao->setCodigo($prodPermissao->codigo);
          }

          $objProdPermissao->setCodigoGrupo($prodPermissao->codigoGrupo);
          $objProdPermissao->setCodigoRede($prodPermissao->codigoRede);
          $objProdPermissao->setCodigoLinha($prodPermissao->codigoLinha);
          $objProdPermissao->setCodigoProduto($prodPermissao->codigoProduto);

          $objProdPermissao->save();

          $logs[] = "Produto permissão do Cod.: {$prodPermissao->codigoProduto} {$acao} com sucesso.";

          unset($objProdPermissao);

          $success = true;
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados do produto permissão de cod.: {$prodPermissao->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "2-Houve um problema com os dados do  produto permissão de cod.: {$prodPermissao->codigo}. Motivo: {$e->getMessage()}";
        }
      }
      unset($produtoPermissao);
    } else {
      $logs[] = "Nenhum produto informado.";
    }

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Produto permissão {$acao}(s) com sucesso" : "Algumas permissões não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Remove permissao dos produtos
   * Este metodo retorna um objeto App_Model_Response
   * <code>
   * 	<message>Permissao excluida com sucesso</message>
   * 	<success>1</success>
   * </code>
   *
   * @param App_Model_Entity_ProdutoPermissao[] $produtoPermissao
   * @return App_Model_Response
   */
  public function removeProdutoPermissao($produtoPermissao)
  {
    $logs = array();
    $success = false;
    $daoProdutoPermissao = App_Model_DAO_ProdutoPermissao::getInstance();

    if (count($produtoPermissao)) {
      foreach ($produtoPermissao as $id) {
        try {
          //verifica se o grupo já existe
          $objProdPermissao = $daoProdutoPermissao->find($id->codigoProduto)->current();

          //se não existe, grava o erro no log
          if (!$objProdPermissao) {
            throw new Exception("Antilhas - Produto permissão de cod.: {$id->codigoProduto} inexistente.");
          } else {
            $objProdPermissao->delete();
            $success = true;
            $logs[] = "Antilhas - Produto permissão de cod.: {$id->codigoProduto} excluído com sucesso.";
          }
          unset($objProdPermissao);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $logs[] = "Antilhas - Houve um problema ao remover a permissão de cod.: {$id->codigoProduto}. Motivo: {$e->getMessage()}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Antilhas - Houve um problema ao remover a permissão de cod.: {$id->codigoProduto}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Antilhas - Nenhum grupo informado.";
    }

    unset($produtosPermissao);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? 'Permissões removidas com sucesso' : 'Algumas permissões não foram removidas, verifique o log de erro.');
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  //FIM - MELHORIA PERMISSÃO DE PRODUTOS PARA VENDA

  /**
   * Insere e atualiza os fretes
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Linha do Cod.: 587 inserida com sucesso.</item>
   * 		<item>Linha do Cod.: 587 atualizada com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Frete[] $fretes
   * @return App_Model_Response
   */
  public function insereAtualizaFrete($fretes)
  {
    $logs = array();
    $success = true;
    $acao = 'atualizada';
    $daoFrete = App_Model_DAO_Frete::getInstance();

    if (count($fretes)) {
      foreach ($fretes as $frete) {
        try {
          //verifica se o frete já existe
          $objFrete = $daoFrete->find($frete->codigo)->current();

          //se não existe, cria um registro novo
          if (!$objFrete) {
            $acao = 'inserida';
            $objFrete = $daoFrete->createRow();
            $objFrete->setCodigo($frete->codigo);
          }


          $objFrete
            ->setTipo($frete->tipo)
            ->setDescricao($frete->descricao)
            ->setPrazo($frete->prazo)
            ->setGrupoRede($frete->grupoRede)
            ->setRede($frete->rede)
            ->setLinha($frete->linha)
            ->setProduto($frete->produto)
            ->setRegiao($frete->regiao)
            ->setValor($frete->valor)
            ->setPesoInicial($frete->pesoInicial)
            ->setPesoFinal($frete->pesoFinal);

          $objFrete->save();
          $logs[] = "Frete de cod.: {$frete->codigo} {$acao} com sucesso.";

          unset($objFrete);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados do frete de cod.: {$frete->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados do frete de cod.: {$frete->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum frete informada.";
    }
    unset($fretes, $daoFrete);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Frete(s) {$acao}(s) com sucesso" : "Alguns fretes não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }

  /**
   * Insere e atualiza os ciclos
   * Este método retorna um objeto App_Model_Response
   * <code>
   * 	<message>Registros recebidos com sucesso</message>
   * 	<success>1</success>
   * 	<logs>
   * 		<item>Linha do Cod.: 587 inserida com sucesso.</item>
   * 		<item>Linha do Cod.: 587 atualizada com sucesso.</item>
   * 		...
   * 	</logs>
   * </code>
   *
   * @param App_Model_Entity_Ciclos[] $ciclos
   * @return App_Model_Response
   */
  public function insereAtualizaCiclo($ciclos)
  {
    $logs = array();
    $success = true;
    $acao = 'atualizada';
    $daoCiclos = App_Model_DAO_Ciclos::getInstance();

    if (count($ciclos)) {
      foreach ($ciclos as $ciclo) {
        try {
          //verifica se o ciclo já existe
          $objCiclos = $daoCiclos->find($ciclo->idCiclo)->current();

          //se não existe, cria um registro novo
          if (!$objCiclos) {
            $acao = 'inserida';
            $objCiclos = $daoCiclos->createRow();
            $objCiclos->setIdCiclo($ciclo->codigo);
          }


          $objCiclos
            ->setIdCiclo($ciclo->idCiclo)
            ->setDescricao($ciclo->descricao)
            ->setData($ciclo->data)
            ->setHoraDeCorte($ciclo->horaDeCorte);

          $objCiclos->save();
          $logs[] = "Ciclo de cod.: {$ciclo->codigo} {$acao} com sucesso.";

          unset($objCiclos);
        } catch (App_Validate_Exception $e) {
          $success = false;
          $campos = count($e->getFields()) ? implode('|', $e->getFields()) : '';
          $logs[] = "Houve um problema com os dados do ciclo de cod.: {$ciclo->codigo}. Motivo: {$e->getMessage()} - {$campos}";
        } catch (Exception $e) {
          $success = false;
          $logs[] = "Houve um problema com os dados do ciclo de cod.: {$ciclo->codigo}. Motivo: {$e->getMessage()}";
        }
      }
    } else {
      $logs[] = "Nenhum ciclo informada.";
    }
    unset($ciclos, $daoCiclos);

    $retorno = new stdClass();
    $retorno->message = utf8_encode($success == true ? "Ciclos(s) {$acao}(s) com sucesso" : "Alguns ciclos não foram {$acao}s, verifique o log de erro.");
    $retorno->success = $success;

    if (count($logs)) {
      foreach ($logs as $key => $value) {
        $retorno->logs[] = utf8_encode($value);
      }
    }
    return $retorno;
  }
}
