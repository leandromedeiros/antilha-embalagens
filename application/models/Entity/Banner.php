<?php

/**
 * Defini��o do objeto Banner
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Banner extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Collection of App_Model_Entity_Galeria_Arquivo
	 */
	protected $objImagem = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Banner_Permissao
	 */
	protected $objPermissoes = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Banner_Estado
	 */
	protected $objEstados = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objImagem', 'objPermissoes', 'objEstados'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Banners::getInstance());
	}

	public function save()
	{
		$this->ban_status = (int) $this->ban_status;
		$this->ban_ordem = (int) $this->ban_ordem;

		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'ban_idBanner' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ban_idImagem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ban_titulo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 150)
			),
			'ban_link' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true,
				new Zend_Validate_StringLength(1, 150)
			),
			'ban_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ban_ordem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());
		
		try {
			$this->getTable()->getAdapter()->beginTransaction();
			$trans = true;
		} catch (Exception $e) {
			$trans = false;
		}
		
		//persiste os dados no banco
		try {
			parent::save();
			
			// Permiss�es
			$daoPermissoes = App_Model_DAO_Banners_Permissoes::getInstance();
			$this->getPermissoes(); //for�a o carregamento das permiss�es
			$daoPermissoes->delete($daoPermissoes->getAdapter()->quoteInto('ban_perm_idBanner = ?', $this->getCodigo()));
			foreach ($this->getPermissoes() as $permissao) {
				$daoPermissoes->insert(array(
					'ban_perm_idBanner' => $this->getCodigo(),
					'ban_perm_idGrupoRede' => $permissao->getGrupoRede() != null ? $permissao->getGrupoRede()->getCodigo() : null,
					'ban_perm_idRede' => $permissao->getRede() != null ? $permissao->getRede()->getCodigo() : null,
					'ban_perm_idGrupoLoja' => $permissao->getGrupoLoja() != null ? $permissao->getGrupoLoja()->getCodigo() : null,
					'ban_perm_idLoja' => $permissao->getLoja() != null ? $permissao->getLoja()->getCodigo() : null,
					'ban_perm_idLinha' => $permissao->getLinha() != null ? $permissao->getLinha()->getCodigo() : null
				));
			}
			unset($daoPermissoes);
			
			// Estados
			$daoEstados = App_Model_DAO_Banners_Estados::getInstance();
			$this->getEstados(); //for�a o carregamento das permiss�es
			$daoEstados->delete($daoEstados->getAdapter()->quoteInto('ban_est_idBanner = ?', $this->getCodigo()));
			foreach ($this->getEstados() as $estado) {
				$daoEstados->insert(array(
					'ban_est_idBanner' => $this->getCodigo(),
					'ban_est_uf' => $estado->getUf()
				));
			}
			unset($daoEstados);
			
			if ($trans) $this->getTable()->getAdapter()->commit();
			
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Recupera o c�digo identificador do banner
	 * 
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->ban_idBanner;
	}

	/**
	 * Define a imagem a qual o banner pertence
	 * 
	 * @params App_Model_Entity_Galeria_Arquivo $value
	 * @return App_Model_Entity_Banner
	 */
	public function setImagem(App_Model_Entity_Galeria_Arquivo $value)
	{
		$this->objImagem = $value;
		$this->ban_idImagem = (int) $value->getCodigo();
		return $this;
	}

	/**
	 * Recupera a imagem a qual o banner pertence
	 * 
	 * @return App_Model_Entity_Galeria_Arquivo
	 */
	public function getImagem()
	{
		if (null == $this->objImagem && $this->getCodigo()) {
			$this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
		}
		return $this->objImagem;
	}
	
	/**
	 * Define o t�tulo do banner
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Banner
	 */
	public function setTitulo($value)
	{
		$this->ban_titulo = (string) $value;
		return $this;
	}

	/**
	 * Recupera o t�tulo do banner
	 * 
	 * @return string
	 */
	public function getTitulo()
	{
		return (string) $this->ban_titulo;
	}
	
	/**
	 * Define se o banner est� ativo ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_Banner
	 */
	
	/**
	 * Define o link do banner
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Banner
	 */
	public function setLink($value)
	{
		$this->ban_link = (string) $value;
		return $this;
	}

	/**
	 * Recupera o link do banner
	 * 
	 * @return string
	 */
	public function getLink()
	{
		return (string) $this->ban_link;
	}
	
	/**
	 * Define se o banner est� ativo ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_Banner
	 */
	
	public function setStatus($value)
	{
		$this->ban_status = (bool) $value;
		return $this;
	}

	/**
	 * Recupera se o banner est� ativo ou n�o
	 * @return boolean
	 */
	public function getStatus()
	{
		return (bool) $this->ban_status;
	}
	
	/**
	 * Define a ordem do banner
	 * 
	 * @param integer $value
	 * @return App_Model_Entity_Banner
	 */
	public function setOrdem($value)
	{
		$this->ban_ordem = (int) $value;
		return $this;
	}
	
	/**
	 * Recupera a ordem do banner
	 * 
	 * @return integer
	 */
	public function getOrdem()
	{
		return (int) $this->ban_ordem;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Banner_Permissao
	 */
	public function getPermissoes()
	{
		if (null == $this->objPermissoes) {
			if ($this->getCodigo()) {
				$this->objPermissoes = $this->findDependentRowset(App_Model_DAO_Banners_Permissoes::getInstance(), 'Banner');
				foreach ($this->objPermissoes as $permissao) {
					$permissao->setBanner($this);
				}
				$this->objPermissoes->rewind();
			} else {
				$this->objPermissoes = App_Model_DAO_Banners_Permissoes::getInstance()->createRowset();
			}
		}
		return $this->objPermissoes;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Banner_Estado
	 */
	public function getEstados()
	{
		if (null == $this->objEstados) {
			if ($this->getCodigo()) {
				$this->objEstados = $this->findDependentRowset(App_Model_DAO_Banners_Estados::getInstance(), 'Banner');
				foreach ($this->objEstados as $estado) {
					$estado->setBanner($this);
				}
				$this->objEstados->rewind();
			} else {
				$this->objEstados = App_Model_DAO_Banners_Estados::getInstance()->createRowset();
			}
		}
		return $this->objEstados;
	}
}