<?php

/**
 * Defini��o do objeto Produto
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Produto extends App_Model_Entity_Abstract
{
    /*     * ** In�cio Propriedades do WebService ************ */

    /** @var string */
    public $codigo = null;

    /** @var string */
    public $linha = null;

    /** @var string */
    public $descricao = null;

    /** @var string */
    public $unidade = null;

    /** @var string */
    public $idClienteExterno = null;

    /** @var string */
    public $ccf = null;

    /** @var string */
    public $medidasNet = null;

    /** @var string */
    public $unidDesc = null;

    /** @var float */
    public $pcModulo = null;

    /** @var float */
    public $qtdePacotes = null;

    /** @var float */
    public $pesoPacote = null;

    /** @var float */
    public $aliquitaIpi = null;

    /** @var float */
    public $codResumo = null;

    /** @var float */
    public $sequenciaItens = null;

    /** @var int */
    public $status = null;

    /** @var int */
    public $qtde = null;
    
    /** @var int */
    public $empenho = null;

    /** @var string */
    public $cEstoque = null;

    /** @var string */
    public $tipocontrole = null;


    /*     * ** Fim Propriedades do WebService ************ */

    /**
     * @var App_Model_Entity_Linha
     */
    protected $objLinha = null;

    /**
     * @var App_Model_Entity_Galeria_Arquivo
     */
    protected $objImagemPequena = null;

    /**
     * @var App_Model_Entity_Galeria_Arquivo
     */
    protected $objImagem = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objLinha', 'objImagem', 'objImagemPequena'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Produtos::getInstance());
    }

    public function save()
    {
        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'prod_idProduto' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 18)
            ),
            'prod_idLinha' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 6)
            ),
            'prod_idImagem' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'prod_idImagemPequena' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'prod_descricao' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 100)
            ),
            'prod_unidade' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 3)
            ),
            'prod_idClienteExterno' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 14)
            ),
            'prod_ccf' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 1)
            ),
            'prod_medidasNet' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 20)
            ),
            'prod_unidDesc' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 15)
            ),
            'prod_pcModulo' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'prod_qtdePacotes' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'prod_pesoPacote' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'prod_aliquotaIpi' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'prod_codResumo' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'prod_sequenciaItens' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'prod_status' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'prod_cEstoque' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        //persiste os dados no banco
        try {
            parent::save();
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function _postInsert()
    {
        try {
            $template = new Zend_View();
            $template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');

            $template->produto = $this;
            $template->usuario = Zend_Registry::get('config')->avisoproduto->nome;

            $mailTransport = new App_Model_Email();
            $mail = new Zend_Mail('ISO-8859-1');
            $mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
            $mail->addTo(Zend_Registry::get('config')->avisoproduto->email, Zend_Registry::get('config')->avisoproduto->nome);
            //$mail->addTo('alexandre@vm2.com.br', 'teste');

            $mail->setSubject(Zend_Registry::get('config')->project . ' - Inclus�o de Produto');
            $mail->setBodyHtml($template->render('emails/aviso-produto.phtml'));
            $mail->send($mailTransport->getFormaEnvio());
        } catch (Exception $e) {

        }
    }

    /**
     * Define o codigo identificador do produto
     *
     * @param string $value
     * @return App_Model_Entity_Produto
     */
    public function setCodigo($value)
    {
        $this->prod_idProduto = $value;
        return $this;
    }

    /**
     * Recupera o c�digo identificador da produto
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->prod_idProduto;
    }

    /**
     * Define a linha a qual o produto pertence
     *
     * @param App_Model_Entity_Linha $value
     * @return App_Model_Entity_Produto
     */
    public function setLinha(App_Model_Entity_Linha $value)
    {
        $this->objLinha = $value;
        $this->prod_idLinha = $value->getCodigo();
        return $this;
    }

    /**
     * Recupera a linha a qual o produto pertence
     *
     * @return App_Model_Entity_Linha
     */
    public function getLinha()
    {
        if (null == $this->objLinha && $this->getCodigo()) {
            $this->objLinha = $this->findParentRow(App_Model_DAO_Linhas::getInstance(), 'Linha');
        }
        return $this->objLinha;
    }

    /**
     * Define a imagem a qual o produto pertence
     *
     * @param App_Model_Entity_Galeria_Arquivo $value
     * @return App_Model_Entity_Produto
     */
    public function setImagem(App_Model_Entity_Galeria_Arquivo $value = null)
    {
        $this->objImagem = $value;
        $this->prod_idImagem = $value != null ? $value->getCodigo() : '';
        return $this;
    }

    /**
     * Recupera a imagem a qual o produto pertence
     *
     * @return App_Model_Entity_Galeria_Arquivo
     */
    public function getImagem()
    {
        if (null == $this->objImagem && $this->getCodigo()) {
            $this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
        }
        return $this->objImagem;
    }

    /**
     * Define a imagem a qual o produto pertence
     *
     * @param App_Model_Entity_Galeria_Arquivo $value
     * @return App_Model_Entity_Produto
     */
    public function setImagemPequena(App_Model_Entity_Galeria_Arquivo $value = null)
    {
        $this->objImagemPequena = $value;
        $this->prod_idImagemPequena = $value != null ? $value->getCodigo() : '';
        return $this;
    }

    /**
     * Recupera a imagem a qual o produto pertence
     *
     * @return App_Model_Entity_Galeria_Arquivo
     */
    public function getImagemPequena()
    {
        if (null == $this->objImagemPequena && $this->getCodigo()) {
            $this->objImagemPequena = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'ImagemPequena');
        }
        return $this->objImagemPequena;
    }

    /**
     * Define o descri�ao do produto
     *
     * @param string $value
     * @return App_Model_Entity_Produto
     */
    public function setDescricao($value)
    {
        $this->prod_descricao = (string) $value;
        return $this;
    }

    /**
     * Recupera a descri��o do produto
     *
     * @return string
     */
    public function getDescricao()
    {
        return (string) $this->prod_descricao;
    }

    /**
     * Define a unidade a qual o produto pertence
     *
     * @param string $value
     * @return App_Model_Entity_Produto
     */
    public function setUnidade($value)
    {
        $this->prod_unidade = $value;
        return $this;
    }

    /**
     * Recupera a unidade a qual o produto pertence
     *
     * @return string
     */
    public function getUnidade()
    {
        return $this->prod_unidade;
    }

    /**
     * Define o c�digo do cliente externo do produto
     *
     * @param string $value
     * @return App_Model_Entity_Produto
     */
    public function setIdClienteExterno($value)
    {
        $this->prod_idClienteExterno = $value;
        return $this;
    }

    /**
     * Recupera o c�digo do cliente externo do produto
     *
     * @return string
     */
    public function getIdClienteExterno()
    {
        return $this->prod_idClienteExterno;
    }

    /**
     * Define o CCF do produto
     *
     * @param string $value
     * @return App_Model_Entity_Produto
     */
    public function setCcf($value)
    {
        $this->prod_ccf = $value;
        return $this;
    }

    /**
     * Recupera o CCF do produto
     *
     * @return string
     */
    public function getCcf()
    {
        return $this->prod_ccf;
    }

    /**
     * Define as medidas do produto
     *
     * @param string $value
     * @return App_Model_Entity_Produto
     */
    public function setMedidasNet($value)
    {
        $this->prod_medidasNet = $value;
        return $this;
    }

    /**
     * Recupera as medidas do produto
     *
     * @return string
     */
    public function getMedidasNet()
    {
        return $this->prod_medidasNet;
    }

    /**
     * Define as unidades de desconto do produto
     *
     * @param string $value
     * @return App_Model_Entity_Produto
     */
    public function setUnidDesc($value)
    {
        $this->prod_unidDesc = $value;
        return $this;
    }

    /**
     * Recupera as unidades de desconto do produto
     *
     * @return string
     */
    public function getUnidDesc()
    {
        return $this->prod_unidDesc;
    }

    /**
     * Define as pe�as por m�dulo do produto
     *
     * @param float $value
     * @return App_Model_Entity_Produto
     */
    public function setPcModulo($value)
    {
        $this->prod_pcModulo = (float) $value;
        return $this;
    }

    /**
     * Recupera as pe�as por m�dulo do produto
     *
     * @return float
     */
    public function getPcModulo()
    {
        return (float) $this->prod_pcModulo;
    }

    /**
     * Define a quantidade de pacotes do produto
     *
     * @param float $value
     * @return App_Model_Entity_Produto
     */
    public function setQtdePacotes($value)
    {
        $this->prod_qtdePacotes = (float) $value;
        return $this;
    }

    /**
     * Recupera a quantidade de pacotes do produto
     *
     * @return float
     */
    public function getQtdePacotes()
    {
        return (float) $this->prod_qtdePacotes;
    }

    /**
     * Define o peso dos pacotes do produto
     *
     * @param float $value
     * @return App_Model_Entity_Produto
     */
    public function setPesoPacotes($value)
    {
        $this->prod_pesoPacote = (float) $value;
        return $this;
    }

    /**
     * Recupera o peso dos pacotes do produto
     *
     * @return float
     */
    public function getPesoPacotes()
    {
        return (float) $this->prod_pesoPacote;
    }

    /**
     * Define a aliquota de IPI do produto
     *
     * @param float $value
     * @return App_Model_Entity_Produto
     */
    public function setAliquotaIpi($value)
    {
        $this->prod_aliquotaIpi = (float) $value;
        return $this;
    }

    /**
     * Recupera a aliquota de IPI do produto
     *
     * @return float
     */
    public function getAliquotaIpi()
    {
        return (float) $this->prod_aliquotaIpi;
    }

    /**
     * Define o c�digo resumo do produto
     *
     * @param float $value
     * @return App_Model_Entity_Produto
     */
    public function setCodResumo($value)
    {
        $this->prod_codResumo = (float) $value;
        return $this;
    }

    /**
     * Recupera o c�digo resumo do produto
     *
     * @return float
     */
    public function getCodResumo()
    {
        return (float) $this->prod_codResumo;
    }

    /**
     * Define a sequ�ncia de itens do produto
     *
     * @param float $value
     * @return App_Model_Entity_Produto
     */
    public function setSequenciaItens($value)
    {
        $this->prod_sequenciaItens = (float) $value;
        return $this;
    }

    /**
     * Recupera a sequ�ncia de itens do produto
     *
     * @return float
     */
    public function getSequenciaItens()
    {
        return (float) $this->prod_sequenciaItens;
    }

    /**
     * Define o status do produto
     *
     * @param float $value
     * @return App_Model_Entity_Produto
     */
    public function setStatus($value)
    {
        $this->prod_status = (bool) $value;
        return $this;
    }

    /**
     * Recupera o status do produto
     *
     * @return float
     */
    public function getStatus()
    {
        return (bool) $this->prod_status;
    }

    /**
     * Retorna os precos do produto e pode ser filtrado por tipo de loja
     * @param App_Model_Entity_TipoLoja $tipo
     * @return App_Model_Collection of App_Model_Entity_Preco
     */
    public function getPrecos(App_Model_Entity_TipoLoja $tipo = null)
    {

        $daoPrecos = App_Model_DAO_Precos::getInstance();
        $select = $daoPrecos->select()
            ->from($daoPrecos)
            ->where('prec_idProduto = ?', $this->getCodigo());

        if ($tipo != null)
            $select->where('prec_idTipo = ?', $tipo->getCodigo());
        return $daoPrecos->fetchAll($select);
    }

    /**
     * Retorna define o cEstoque
     * 
     * @param string $value
     * @return App_Model_Entity_Produto
     */
    public function setPcEstoque($value)
    {
        $this->prod_cEstoque = (string) $value;
        return $this;
    }

    /**
     * Recupera o cEstoque
     *
     * @return float
     */
    public function getPcEstoque()
    {
        return (string) $this->prod_cEstoque;
    }

    /**
     * Define o Tipo Controle
     * 
     * @param string $value
     * @return App_Model_Entity_Produto
     */
    public function setPTipoControle($value)
    {
        $this->prod_tipocontrole = (string) $value;
        return $this;
    }

    /**
     * Recupera o Tipo Controle
     *
     * @return string
     */
    public function getPTipoControle()
    {
        return (string) $this->prod_tipocontrole;
    }

    /**
     * Define a Qtde
     * 
     * @param int $value
     * @return App_Model_Entity_Produto
     */
    public function setPQtde($value)
    {
        $this->prod_Qtde = (int) $value;
        return $this;
    }

    /**
     * Recupera a Qtde
     *
     * @return int
     */
    public function getPQtde()
    {
        return (int) $this->prod_Qtde;
    }

    /**
     * Define o ProdEmpenho
     * 
     * @param int $value
     * @return App_Model_Entity_Produto
     */
    public function setPEmpenho($value)
    {
        $this->prod_Empenho = (int) $value;
        return $this;
    }
    /**
     * Recupera o ProdEmpenho
     *
     * @return int
     */
    public function getPEmpenho()
    {
        return (int) $this->prod_Empenho;
    }
}
