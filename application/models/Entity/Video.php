<?php

/**
 * Defini��o do objeto V�deo
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Video extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Collection of App_Model_Entity_Video_Permissao
	 */
	protected $objPermissoes = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Video_Estado
	 */
	protected $objEstados = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objPermissoes', 'objEstados'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Videos::getInstance());
	}

	public function save()
	{
		if (!$this->getCodigo()) $this->vid_dataCadastro = date('Y-m-d');

		$this->vid_status = (int) $this->vid_status;
		$this->vid_destaque = (int) $this->vid_destaque;
		
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'vid_dataCadastro' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'vid_titulo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 150)
			),
			'vid_codigoYoutube' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 150)
			),
			'vid_destaque' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'vid_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			$this->getTable()->getAdapter()->beginTransaction();
			$trans = true;
		} catch (Exception $e) {
			$trans = false;
		}
		
		//persiste os dados no banco
		try {
			parent::save();
			
			// Permiss�es
			$daoPermissoes = App_Model_DAO_Videos_Permissoes::getInstance();
			$this->getPermissoes(); //for�a o carregamento das permiss�es
			$daoPermissoes->delete($daoPermissoes->getAdapter()->quoteInto('vid_perm_idVideo = ?', $this->getCodigo()));
			foreach ($this->getPermissoes() as $permissao) {
				$daoPermissoes->insert(array(
					'vid_perm_idVideo' => $this->getCodigo(),
					'vid_perm_idGrupoRede' => $permissao->getGrupoRede() != null ? $permissao->getGrupoRede()->getCodigo() : null,
					'vid_perm_idRede' => $permissao->getRede() != null ? $permissao->getRede()->getCodigo() : null,
					'vid_perm_idGrupoLoja' => $permissao->getGrupoLoja() != null ? $permissao->getGrupoLoja()->getCodigo() : null,
					'vid_perm_idLoja' => $permissao->getLoja() != null ? $permissao->getLoja()->getCodigo() : null,
					'vid_perm_idLinha' => $permissao->getLinha() != null ? $permissao->getLinha()->getCodigo() : null
				));
			}
			unset($daoPermissoes);
			
			// Estados
			$daoEstados = App_Model_DAO_Videos_Estados::getInstance();
			$this->getEstados(); //for�a o carregamento das permiss�es
			$daoEstados->delete($daoEstados->getAdapter()->quoteInto('vid_est_idVideo = ?', $this->getCodigo()));
			foreach ($this->getEstados() as $estado) {
				$daoEstados->insert(array(
					'vid_est_idVideo' => $this->getCodigo(),
					'vid_est_uf' => $estado->getUf()
				));
			}
			unset($daoEstados);
			
			if ($trans) $this->getTable()->getAdapter()->commit();
			
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Recupera o c�digo identificador do v�deo
	 * 
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->vid_idVideo;
	}

	/**
	 * Define a data de cadastro do v�deo
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Video
	 */
	public function setDataCadastro($value)
	{
		$this->vid_dataCadastro = (string) $value;
		return $this;
	}
	
	/**
	 * Recupera a data de cadastro do v�deo
	 * 
	 * @return string
	 */
	public function getDataCadastro()
	{
		return (string) $this->vid_dataCadastro;
	}

	/**
	 * Define o t�tulo do v�deo
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Video
	 */
	public function setTitulo($value)
	{
		$this->vid_titulo = (string) $value;
		return $this;
	}

	/**
	 * Recupera o t�tulo do v�deo
	 * 
	 * @return string
	 */
	public function getTitulo()
	{
		return (string) $this->vid_titulo;
	}
	
	/**
	 * Define o chamada do v�deo
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Video
	 */
	public function setChamada($value)
	{
		$this->vid_chamada = (string) $value;
		return $this;
	}

	/**
	 * Recupera o chamada do v�deo
	 * 
	 * @return string
	 */
	public function getChamada()
	{
		return (string) $this->vid_chamada;
	}
	
	/**
	 * Define o c�digo youtube do v�deo
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Video
	 */
	public function setLinkYoutube($value)
	{
		$this->vid_linkYoutube = (string) $value;
		return $this;
	}

	/**
	 * Recupera o c�digo youtube do v�deo
	 * 
	 * @return string
	 */
	public function getLinkYoutube()
	{
		return (string) $this->vid_linkYoutube;
	}
	
	public function getCodigoYoutube()
	{
		$codigo = explode("=", $this->vid_linkYoutube);
		$codigo = "https://www.youtube.com/embed/" . $codigo[1];
		return (string) $codigo;
	}
	
	/**
	 * Define se o v�deo � destaque ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_Noticia
	 */
	public function setDestaque($value)
	{
		$this->vid_destaque = (bool) $value;
		return $this;
	}

	/**
	 * Recupera se o v�deo � destaque ou n�o
	 * @return boolean
	 */
	public function getDestaque()
	{
		return (bool) $this->vid_destaque;
	}
	
	/**
	 * Define se o v�deo est� ativo ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_Video
	 */
	public function setStatus($value)
	{
		$this->vid_status = (bool) $value;
		return $this;
	}

	/**
	 * Recupera se o v�deo est� ativo ou n�o
	 * @return boolean
	 */
	public function getStatus()
	{
		return (bool) $this->vid_status;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Video_Permissao
	 */
	public function getPermissoes()
	{
		if (null == $this->objPermissoes) {
			if ($this->getCodigo()) {
				$this->objPermissoes = $this->findDependentRowset(App_Model_DAO_Videos_Permissoes::getInstance(), 'Video');
				foreach ($this->objPermissoes as $permissao) {
					$permissao->setVideo($this);
				}
				$this->objPermissoes->rewind();
			} else {
				$this->objPermissoes = App_Model_DAO_Videos_Permissoes::getInstance()->createRowset();
			}
		}
		return $this->objPermissoes;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Video_Estado
	 */
	public function getEstados()
	{
		if (null == $this->objEstados) {
			if ($this->getCodigo()) {
				$this->objEstados = $this->findDependentRowset(App_Model_DAO_Videos_Estados::getInstance(), 'Video');
				foreach ($this->objEstados as $estado) {
					$estado->setVideo($this);
				}
				$this->objEstados->rewind();
			} else {
				$this->objEstados = App_Model_DAO_Videos_Estados::getInstance()->createRowset();
			}
		}
		return $this->objEstados;
	}
}