<?php

/**
 * Defini��o do objeto Status
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Status extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Collection of App_Model_Entity_Status_Relacionado
	 */
	protected $objRelacionados = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep('objRelacionados'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Status::getInstance());
	}

	public function save()
	{
		$this->sta_status = (int) $this->sta_status;

		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'sta_idStatus' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'sta_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 150)
			),
			'sta_referenciaQuadro' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'sta_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());
		
		try {
			$this->getTable()->getAdapter()->beginTransaction();
			$trans = true;
		} catch (Exception $e) {
			$trans = false;
		}
		
		//persiste os dados no banco
		try {
			parent::save();
			
			// Relacionados
			$daoRelacionados = App_Model_DAO_Status_Relacionados::getInstance();
			$this->getRelacionados();
			$daoRelacionados->delete($daoRelacionados->getAdapter()->quoteInto('sta_status_idStatus = ?', $this->getCodigo()));
			foreach ($this->getRelacionados() as $relacionado) {
				$daoRelacionados->insert(array(
					'sta_status_idStatus' => $this->getCodigo(),
					'sta_status_idStatusRelacionado' => $relacionado->getRelacionado()
				));
			}
			unset($daoRelacionados);
			
			if ($trans) $this->getTable()->getAdapter()->commit();
			
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Recupera o c�digo identificador do status
	 * 
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->sta_idStatus;
	}

	/**
	 * Define o nome do status
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Status
	 */
	public function setNome($value)
	{
		$this->sta_nome = (string) $value;
		return $this;
	}

	/**
	 * Recupera o nome do status
	 * 
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->sta_nome;
	}

	/**
	 * Define se a referencia est� ativo ou n�o
	 * 
	 * @param int $value
	 * @return App_Model_Entity_Status
	 */
	
	public function setReferencia($value)
	{
		$this->sta_referenciaQuadro = (int) $value;
		return $this;
	}

	/**
	 * Recupera se a referencia est� ativo ou n�o
	 * @return int
	 */
	public function getReferencia()
	{
		return (int) $this->sta_referenciaQuadro;
	}
	
	/**
	 * Define se o status est� ativo ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_Status
	 */
	
	public function setStatus($value)
	{
		$this->sta_status = (bool) $value;
		return $this;
	}

	/**
	 * Recupera se o status est� ativo ou n�o
	 * @return boolean
	 */
	public function getStatus()
	{
		return (bool) $this->sta_status;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Status_Relacionado
	 */
	public function getRelacionados()
	{
		if (null == $this->objRelacionados) {
			if ($this->getCodigo()) {
				$this->objRelacionados = $this->findDependentRowset(App_Model_DAO_Status_Relacionados::getInstance(), 'Status');
				foreach ($this->objRelacionados as $relacionado) {
					$relacionado->setStatus($this);
				}
				$this->objRelacionados->rewind();
			} else {
				$this->objRelacionados = App_Model_DAO_Status_Relacionados::getInstance()->createRowset();
			}
		}
		return $this->objRelacionados;
	}
}