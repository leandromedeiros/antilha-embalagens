<?php
	class App_Model_Entity_Web_ServiceClass  
	{
	    /**
	     *  ParcelDetails
	     */
	    public function registerParcel($username, $pasword) {
	
	        $group = new App_Model_Entity_Web_ParcelDetailsSpace();
	
	        //fill in the array
	        for ($i = 1; $i <= 3; $i++) {
	            $countryType = new App_Model_Entity_Web_CountryType();
	            $countryType->country = 'country';
	
	            $addressType = new App_Model_Entity_Web_AddressTypeSpace();
	            $addressType->address = 'Adresas';
	            $addressType->area = 'Area';
	
	
	            $group->countryType = $countryType;
	            $group->addressType = $addressType;
	        }
	
	        return $group;
	    }       
	}
