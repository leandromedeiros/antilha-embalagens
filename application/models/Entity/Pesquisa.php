<?php

/**
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Pesquisa extends App_Model_Entity_Abstract
{

    /**
     * @var App_Model_Collection of App_Model_Entity_Pesquisa_Pergunta
     */
    protected $objPerguntas = null;

    /**
     * @var App_Model_Entity_Galeria_Arquivo
     */
    protected $objImagem = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Pesquisa_Permissao
     */
    protected $objPermissoes = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Pesquisa_Estado
     */
    protected $objEstados = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objPerguntas', 'objPermissoes', 'objEstados', 'objImagem'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Pesquisas::getInstance());
    }

    public function save()
    {
        $this->pes_status = (int) $this->pes_status;
        $this->pes_responderAntes = (int) $this->pes_responderAntes;
        $this->pes_exibirTodas = (int) $this->pes_exibirTodas;

        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'pes_idPesquisa' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'pes_nome' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 100)
            ),
            'pes_dataInicial' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'pes_dataFinal' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'pes_status' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        // persiste os dados no banco
        try {
            $this->getTable()->getAdapter()->beginTransaction();
            $trans = true;
        } catch (Exception $e) {
            $trans = false;
        }

        //persiste os dados no banco
        try {
            parent::save();

            // Permiss�es
            $daoPermissoes = App_Model_DAO_Pesquisas_Permissoes::getInstance();
            $this->getPermissoes(); //for�a o carregamento das permiss�es
            $daoPermissoes->delete($daoPermissoes->getAdapter()->quoteInto('pes_perm_idPesquisa = ?', $this->getCodigo()));
            foreach ($this->getPermissoes() as $permissao) {
                $daoPermissoes->insert(array(
                    'pes_perm_idPesquisa' => $this->getCodigo(),
                    'pes_perm_idGrupoRede' => $permissao->getGrupoRede() != null ? $permissao->getGrupoRede()->getCodigo() : null,
                    'pes_perm_idRede' => $permissao->getRede() != null ? $permissao->getRede()->getCodigo() : null,
                    'pes_perm_idGrupoLoja' => $permissao->getGrupoLoja() != null ? $permissao->getGrupoLoja()->getCodigo() : null,
                    'pes_perm_idLoja' => $permissao->getLoja() != null ? $permissao->getLoja()->getCodigo() : null,
                    'pes_perm_idLinha' => $permissao->getLinha() != null ? $permissao->getLinha()->getCodigo() : null
                ));
            }
            unset($daoPermissoes);

            // Estados
            $daoEstados = App_Model_DAO_Pesquisas_Estados::getInstance();
            $this->getEstados(); //for�a o carregamento das permiss�es
            $daoEstados->delete($daoEstados->getAdapter()->quoteInto('pes_est_idPesquisa = ?', $this->getCodigo()));
            foreach ($this->getEstados() as $estado) {
                $daoEstados->insert(array(
                    'pes_est_idPesquisa' => $this->getCodigo(),
                    'pes_est_uf' => $estado->getUf()
                ));
            }
            unset($daoEstados);

            // Perguntas
            foreach ($this->getPerguntas() as $pergunta) {
                $pergunta->setPesquisa($this);
                $pergunta->save();
            }

            if ($trans)
                $this->getTable()->getAdapter()->commit();
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            if ($trans)
                $this->getTable()->getAdapter()->rollBack();
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return integer
     */
    public function getCodigo()
    {
        return (int) $this->pes_idPesquisa;
    }

    /**
     * @param App_Model_Entity_Galeria_Arquivo $value
     * @return App_Model_Entity_Pesquisa
     */
    public function setImagem(App_Model_Entity_Galeria_Arquivo $value = null)
    {
        $this->objImagem = $value;
        $this->pes_idImagem = $value != null ? $value->getCodigo() : null;
        return $this;
    }

    /**
     * @return App_Model_Entity_Galeria_Arquivo
     */
    public function getImagem()
    {
        if (null == $this->objImagem && $this->getCodigo()) {
            $this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
        }
        return $this->objImagem;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return (string) $this->pes_nome;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pesquisa
     */
    public function setNome($value)
    {
        $this->pes_nome = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataInicial()
    {
        return (string) $this->pes_dataInicial;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pesquisa
     */
    public function setDataFinal($value)
    {
        $this->pes_dataFinal = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataFinal()
    {
        return (string) $this->pes_dataFinal;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pesquisa
     */
    public function setDataInicial($value)
    {
        $this->pes_dataInicial = (string) $value;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getResponderAntes()
    {
        return (bool) $this->pes_responderAntes;
    }

    /**
     * @param boolean $value
     * @return App_Model_Entity_Pesquisa
     */
    public function setResponderAntes($value)
    {
        $this->pes_responderAntes = (bool) $value;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getStatus()
    {
        return (bool) $this->pes_status;
    }

    /**
     * @param boolean $value
     * @return App_Model_Entity_Pesquisa
     */
    public function setStatus($value)
    {
        $this->pes_status = (bool) $value;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getExibirTodas()
    {
        return (bool) $this->pes_exibirTodas;
    }

    /**
     * @param boolean $value
     * @return App_Model_Entity_Pesquisa
     */
    public function setExibirTodas($value)
    {
        $this->pes_exibirTodas = (bool) $value;
        return $this;
    }

    public function getPerguntas()
    {
        if (null == $this->objPerguntas) {
            if ($this->getCodigo()) {
                $this->objPerguntas = $this->findDependentRowset(App_Model_DAO_Pesquisas_Perguntas::getInstance(), 'Pesquisa', $this->getTable()->select()->order('pes_perg_ordem ASC'));
                foreach ($this->objPerguntas as $pergunta) {
                    $pergunta->setPesquisa($this);
                }
                $this->objPerguntas->rewind();
            } else {
                $this->objPerguntas = App_Model_DAO_Pesquisas_Perguntas::getInstance()->createRowset();
            }
        }
        return $this->objPerguntas;
    }

    /**
     * @return App_Model_Collection of App_Model_Entity_Pesquisa_Permissao
     */
    public function getPermissoes()
    {
        if (null == $this->objPermissoes) {
            if ($this->getCodigo()) {
                $this->objPermissoes = $this->findDependentRowset(App_Model_DAO_Pesquisas_Permissoes::getInstance(), 'Pesquisa');
                foreach ($this->objPermissoes as $permissao) {
                    $permissao->setPesquisa($this);
                }
                $this->objPermissoes->rewind();
            } else {
                $this->objPermissoes = App_Model_DAO_Pesquisas_Permissoes::getInstance()->createRowset();
            }
        }
        return $this->objPermissoes;
    }

    /**
     * @return App_Model_Collection of App_Model_Entity_Pesquisa_Estado
     */
    public function getEstados()
    {
        if (null == $this->objEstados) {
            if ($this->getCodigo()) {
                $this->objEstados = $this->findDependentRowset(App_Model_DAO_Pesquisas_Estados::getInstance(), 'Pesquisa');
                foreach ($this->objEstados as $estado) {
                    $estado->setPesquisa($this);
                }
                $this->objEstados->rewind();
            } else {
                $this->objEstados = App_Model_DAO_Pesquisas_Estados::getInstance()->createRowset();
            }
        }
        return $this->objEstados;
    }

    public function getDisponibilidade()
    {
        $retorno = true;
        foreach ($this->getPerguntas() as $pergunta) {
            if ($pergunta->getDisponibilidade()) {
                $retorno = false;
            }
        }

        if (strtotime('Y-m-d') >= strtotime($this->getDataInicial()) && strtotime('Y-m-d') <= strtotime($this->getDataFinal()) && !$this->getStatus()) {
            $retorno = false;
        }

        return $retorno;
    }

}
