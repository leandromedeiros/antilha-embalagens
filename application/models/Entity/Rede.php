<?php

/**
 * Defini��o do objeto Rede
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Rede extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var string */
	public $codigo = null;
	
	/** @var string */
	public $grupo = null;
	
	/** @var string */
	public $nome = null;
	
	/** @var string */
	public $codigoMatriz = null;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_GrupoRede
	 */
	protected $objGrupo = null;
	
	/**
	 * @var App_Model_Entity_Galeria_Arquivo
	 */
	protected $objImagem = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_GrupoLoja
	 */
	protected $objGrupos = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Sistema_Usuario
	 */
	protected $objUsuarios = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Linhas
	 */
	protected $objLinhas = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objGrupo', 'objImagem', 'objGrupos', 'objUsuarios', 'objLinhas'));
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Redes::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'rede_idRede' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 6)
			),
			'rede_idGrupo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 6)
			),
			'rede_idImagem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'rede_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 45)
			),
			'rede_codigoMatriz' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			)		
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * Define o c�digo identificador da rede
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Rede
	 */
	public function setCodigo($value)
	{
		$this->rede_idRede = (string) $value;
		return $this;
	}

	/**
	 * Recupera o c�digo identificador da rede
	 * 
	 * @return string
	 */
	public function getCodigo()
	{
		return (string) $this->rede_idRede;
	}

	/**
	 * Define a imagem a qual a rede pertence
	 * 
	 * @param App_Model_Entity_Galeria_Arquivo $value
	 * @return App_Model_Entity_Rede
	 */
	public function setImagem(App_Model_Entity_Galeria_Arquivo $value = null)
	{
		$this->objImagem = $value;
		$this->rede_idImagem = $value != null ? $value->getCodigo() : '';
		return $this;
	}

	/**
	 * Recupera a imagem a qual a rede pertence
	 * 
	 * @return App_Model_Entity_Galeria_Arquivo
	 */
	public function getImagem()
	{
		if (null == $this->objImagem && $this->getCodigo()) {
			$this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
		}
		return $this->objImagem;
	}
	
	/**
	 * Define o nome da rede
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Rede
	 */
	public function setNome($value)
	{
		$this->rede_nome = (string) $value;
		return $this;
	}

	/**
	 * Recupera o nome da rede
	 * 
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->rede_nome;
	}	
	
	/**
	 * Define o grupo a qual a rede pertence
	 * 
	 * @param App_Model_Entity_GrupoRede $value
	 * @return App_Model_Entity_Rede
	 */
	public function setGrupo(App_Model_Entity_GrupoRede $value)
	{
		$this->objGrupo = $value;
		$this->rede_idGrupo = $value->getCodigo();
		return $this;
	}

	/**
	 * Recupera o grupo a qual a rede pertence
	 * 
	 * @return App_Model_Entity_GrupoRede
	 */
	public function getGrupo()
	{
		if (null == $this->objGrupo && $this->getCodigo()) {
			$this->objGrupo = $this->findParentRow(App_Model_DAO_GrupoRedes::getInstance(), 'Grupo');
		}
		return $this->objGrupo;
	}
	
	/**
	 * Define o c�digo matriz da rede
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Rede
	 */
	public function setCodigoMatriz($value)
	{
		$this->rede_codigoMatriz = (string) $value;
		return $this;
	}

	/**
	 * Recupera o c�digo matriz da rede
	 * 
	 * @return string
	 */
	public function getCodigoMatriz()
	{
		return (string) $this->rede_codigoMatriz;
	}	
	
	/**
	 * Retorna todas os grupos desta rede
	 * 
	 * @return App_Model_Collection of App_Model_Entity_GrupoLoja
	 */
	public function getGrupos() {
		if (null == $this->objGrupos) {
			if ($this->getCodigo()) {
				$this->objGrupos = $this->findDependentRowset(App_Model_DAO_GrupoLojas::getInstance(), 'Rede');
				foreach ($this->objGrupos as $rede) {
					$rede->setRede($this);
				}
				$this->objGrupos->rewind();
			} else {
				$this->objGrupos = App_Model_DAO_Redes::getInstance()->createRowset();
			}
		}
		return $this->objGrupos;
	}
	
	/**
	 * Retorna todas os usu�rios desta rede
	 * 
	 * @return App_Model_Collection of App_Model_Entity_Sistema_Usuario
	 */
	public function getUsuarios() {
		if (null == $this->objUsuarios) {
			if ($this->getCodigo()) {
				$this->objUsuarios = $this->findDependentRowset(App_Model_DAO_Sistema_Usuarios::getInstance(), 'Rede');
				foreach ($this->objUsuarios as $usuario) {
					$usuario->setRede($this);
				}
				$this->objUsuarios->rewind();
			} else {
				$this->objUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance()->createRowset();
			}
		}
		return $this->objUsuarios;
	}
	
	/**
	 * Retorna todas as linhas
	 * 
	 * @return App_Model_Collection of App_Model_Entity_Linha
	 */
	public function getLinhas() {
		if (null == $this->objLinhas) {
			if ($this->getCodigo()) {
				$this->objLinhas = $this->findDependentRowset(App_Model_DAO_Linhas::getInstance(), 'Rede');
				foreach ($this->objLinhas as $linha) {
					$linha->setRede($this);
				}
				$this->objLinhas->rewind();
			} else {
				$this->objLinhas = App_Model_DAO_Linhas::getInstance()->createRowset();
			}
		}
		return $this->objLinhas;
	}

	public function setCodigoGrupo($value)
	{
		$this->rede_idGrupo = (string) $value;
		return $this;
	}
	
	public function getCodigoGrupo()
	{
		return (string) $this->rede_idGrupo;
	}	
}