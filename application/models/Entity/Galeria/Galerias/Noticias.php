<?php

class App_Model_Entity_Galeria_Galerias_Noticias extends App_Model_Entity_Galeria_Abstract
{

    protected $codigo = 'noticias';
    protected $nome = 'Not�cias';
    private $thumbs = array(
        'thumb' => array('w' => 121, 'h' => 71),
        'detalhe' => array('w' => 363, 'h' => 213)
    );

    public function init()
    {
        $this->filePath = Zend_Registry::get('config')->paths->site->file . '/images/noticias';
        $this->basePath = Zend_Registry::get('config')->paths->site->base . '/images/noticias';
    }

    public function receive($file, App_Model_Entity_Galeria_Arquivo $arquivo)
    {
        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, array('jpg', 'gif', 'png'));
        $upload = $adapter->getFileInfo($file);

        $fileName = App_Funcoes_SEO::toString(substr($upload[$file]['name'], 0, strrpos($upload[$file]['name'], '.')));
        if (strlen($fileName) > 60) {
            throw new Exception('Tamanho m�ximo de 60 caracteres para o nome do arquivo excedido.');
        }
        $ext = strtolower(substr($upload[$file]['name'], strrpos($upload[$file]['name'], '.')));
        $fileName = App_Funcoes_SEO::renameFile($fileName, $ext, $arquivo->getGaleria()->getFilePath());
        //$fileName .= substr($upload[$file]['name'], strrpos($upload[$file]['name'], '.'));

        $arquivo->setNome($fileName);
        $filePath = "{$arquivo->getGaleria()->getFilePath()}/{$fileName}";

        $adapter->addFilter('Rename', $filePath, $file);
        if ($adapter->receive() == true) {
            chmod($filePath, 0777);

            // recebe os arquivos auxiliares
            require_once('galeriaThumb.inc.php');

            foreach ($this->thumbs as $path => $size) {
                $objimagem = new galeriaThumb();
                $objimagem->format = 'jpg';
                $objimagem->resize_mode = 0;
                $objimagem->thumb_width = $size['w'];
                $objimagem->thumb_height = $size['h'];

                $thumbPath = dirname($arquivo->getFilePath()) . "/{$path}/" . basename($arquivo->getFilePath());
                $objimagem->thumbnail($arquivo->getFilePath(), $thumbPath);
            }
        } else {
            foreach ($adapter->getMessages() as $validator => $message) {
                throw new Exception($message);
            }
        }
    }

    public function deleteFile(App_Model_Entity_Galeria_Arquivo $arquivo)
    {
        foreach ($this->thumbs as $path => $size) {
            $thumbPath = dirname($arquivo->getFilePath()) . "/{$path}/" . basename($arquivo->getFilePath());
            if (file_exists($thumbPath)) {
                unlink($thumbPath);
            }
        }

        if (file_exists($arquivo->getFilePath())) {
            unlink($arquivo->getFilePath());
        }
    }

}
