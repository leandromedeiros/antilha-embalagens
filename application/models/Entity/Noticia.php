<?php

/**
 * Defini��o do objeto Not�cia
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Noticia extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Collection of App_Model_Entity_Galeria_Arquivo
	 */
	protected $objImagem = null;

	/**
	 * @var App_Model_Collection of App_Model_Entity_Noticia_Permissao
	 */
	protected $objPermissoes = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Noticia_Estado
	 */
	protected $objEstados = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objImagem', 'objPermissoes', 'objEstados'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Noticias::getInstance());
	}

	public function save()
	{
		if (!$this->getCodigo()) $this->not_dataCadastro = date('Y-m-d');
		$this->not_destaque = (int) $this->not_destaque;
		$this->not_status = (int) $this->not_status;
		
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'not_dataCadastro' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'not_titulo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 150)
			),
			'not_descricao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'not_destaque' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'not_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());
		
		//persiste os dados no banco
		try {
			$this->getTable()->getAdapter()->beginTransaction();
			$trans = true;
		} catch (Exception $e) {
			$trans = false;
		}

		//persiste os dados no banco
		try {
			parent::save();
			
			// Permiss�es
			$daoPermissoes = App_Model_DAO_Noticias_Permissoes::getInstance();
			$this->getPermissoes(); //for�a o carregamento das permiss�es
			$daoPermissoes->delete($daoPermissoes->getAdapter()->quoteInto('not_perm_idNoticia = ?', $this->getCodigo()));
			foreach ($this->getPermissoes() as $permissao) {
				$daoPermissoes->insert(array(
					'not_perm_idNoticia' => $this->getCodigo(),
					'not_perm_idGrupoRede' => $permissao->getGrupoRede() != null ? $permissao->getGrupoRede()->getCodigo() : null,
					'not_perm_idRede' => $permissao->getRede() != null ? $permissao->getRede()->getCodigo() : null,
					'not_perm_idGrupoLoja' => $permissao->getGrupoLoja() != null ? $permissao->getGrupoLoja()->getCodigo() : null,
					'not_perm_idLoja' => $permissao->getLoja() != null ? $permissao->getLoja()->getCodigo() : null,
					'not_perm_idLinha' => $permissao->getLinha() != null ? $permissao->getLinha()->getCodigo() : null
				));
			}
			unset($daoPermissoes);
			
			// Estados
			$daoEstados = App_Model_DAO_Noticias_Estados::getInstance();
			$this->getEstados(); //for�a o carregamento das permiss�es
			$daoEstados->delete($daoEstados->getAdapter()->quoteInto('not_est_idNoticia = ?', $this->getCodigo()));
			foreach ($this->getEstados() as $estado) {
				$daoEstados->insert(array(
					'not_est_idNoticia' => $this->getCodigo(),
					'not_est_uf' => $estado->getUf()
				));
			}
			unset($daoEstados);
			
			if ($trans) $this->getTable()->getAdapter()->commit();
			
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Recupera o c�digo identificador da not�cia
	 * 
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->not_idNoticia;
	}

	/**
	 * Define a data de cadastro da not�cia
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Noticia
	 */
	public function setDataCadastro($value)
	{
		$this->not_dataCadastro = (string) $value;
		return $this;
	}
	
	/**
	 * Recupera a data de cadastro da not�cia
	 * 
	 * @return string
	 */
	public function getDataCadastro()
	{
		return (string) $this->not_dataCadastro;
	}

	/**
	 * Define o t�tulo da not�cia
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Noticia
	 */
	public function setTitulo($value)
	{
		$this->not_titulo = (string) $value;
		return $this;
	}

	/**
	 * Recupera o t�tulo da not�cia
	 * 
	 * @return string
	 */
	public function getTitulo()
	{
		return (string) $this->not_titulo;
	}
	
	/**
	 * Define o descri�ao da not�cia
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Noticia
	 */
	public function setDescricao($value)
	{
		$this->not_descricao = (string) $value;
		return $this;
	}

	/**
	 * Recupera a descri��o da not�cia
	 * 
	 * @return string
	 */
	public function getDescricao()
	{
		return (string) $this->not_descricao;
	}	
	
	/**
	 * Define se a not�cia � destaque ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_Noticia
	 */
	public function setDestaque($value)
	{
		$this->not_destaque = (bool) $value;
		return $this;
	}

	/**
	 * Recupera se a not�cia � destaque ou n�o
	 * @return boolean
	 */
	public function getDestaque()
	{
		return (bool) $this->not_destaque;
	}
	
	/**
	 * Define se a not�cia est� ativa ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_Noticia
	 */
	public function setStatus($value)
	{
		$this->not_status = (bool) $value;
		return $this;
	}

	/**
	 * Recupera se a not�cia est� ativa ou n�o
	 * @return boolean
	 */
	public function getStatus()
	{
		return (bool) $this->not_status;
	}
	
	/**
	 * Define a imagem a qual o banner pertence
	 * 
	 * @params App_Model_Entity_Galeria_Arquivo $value
	 * @return App_Model_Entity_Banner
	 */
	public function setImagem(App_Model_Entity_Galeria_Arquivo $value)
	{
		$this->objImagem = $value;
		$this->not_idImagem = (int) $value->getCodigo();
		return $this;
	}

	/**
	 * Recupera a imagem a qual o banner pertence
	 * 
	 * @return App_Model_Entity_Galeria_Arquivo
	 */
	public function getImagem()
	{
		if (null == $this->objImagem && $this->getCodigo()) {
			$this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
		}
		return $this->objImagem;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Noticia_Permissao
	 */
	public function getPermissoes()
	{
		if (null == $this->objPermissoes) {
			if ($this->getCodigo()) {
				$this->objPermissoes = $this->findDependentRowset(App_Model_DAO_Noticias_Permissoes::getInstance(), 'Noticia');
				foreach ($this->objPermissoes as $permissao) {
					$permissao->setNoticia($this);
				}
				$this->objPermissoes->rewind();
			} else {
				$this->objPermissoes = App_Model_DAO_Noticias_Permissoes::getInstance()->createRowset();
			}
		}
		return $this->objPermissoes;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Noticia_Estado
	 */
	public function getEstados()
	{
		if (null == $this->objEstados) {
			if ($this->getCodigo()) {
				$this->objEstados = $this->findDependentRowset(App_Model_DAO_Noticias_Estados::getInstance(), 'Noticia');
				foreach ($this->objEstados as $estado) {
					$estado->setNoticia($this);
				}
				$this->objEstados->rewind();
			} else {
				$this->objEstados = App_Model_DAO_Noticias_Estados::getInstance()->createRowset();
			}
		}
		return $this->objEstados;
	}
}