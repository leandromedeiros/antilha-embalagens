<?php

/**
 * Defini��o do objeto GrupoLoja
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_GrupoLoja extends App_Model_Entity_Abstract
{
    /*     * ** In�cio Propriedades do WebService ************ */

    /** @var string */
    public $codigo = null;

    /** @var string */
    public $rede = null;

    /** @var string */
    public $nome = null;

    /** @var string */
    public $tipoPedido = null;

    /** @var string */
    public $clienteMatriz = null;
    
    /** @var int */
    public $temAprovadorIntermediario = 0;


    /*     * ** Fim Propriedades do WebService ************ */

    /**
     * @var App_Model_Entity_Rede
     */
    protected $objRede = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Loja
     */
    protected $objLojas = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Sistema_Usuario
     */
    protected $objUsuarios = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objRede', 'objLojas', 'objUsuarios'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_GrupoLojas::getInstance());
    }

    public function save()
    {
        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'grp_loja_idGrupo' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 6)
            ),
            'grp_loja_idRede' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 6)
            ),
            'grp_loja_nome' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 45)
            ),
            'grp_loja_tipoPedido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_InArray(array('G', 'I'))
            ),
            'grp_loja_clienteMatriz' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 10)
            ),
            'grp_loja_temAprovadorIntermediario' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        //persiste os dados no banco
        try {
            parent::save();
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Define o c�digo identificador do grupo de loja
     *
     * @param string $value
     * @return App_Model_Entity_GrupoLoja
     */
    public function setCodigo($value)
    {
        $this->grp_loja_idGrupo = (string) $value;
        return $this;
    }

    /**
     * Recupera o c�digo identificador do grupo de loja
     *
     * @return string
     */
    public function getCodigo()
    {
        return (string) $this->grp_loja_idGrupo;
    }

    /**
     * Define o nome do grupo de loja
     *
     * @param string $value
     * @return App_Model_Entity_GrupoLoja
     */
    public function setNome($value)
    {
        $this->grp_loja_nome = (string) $value;
        return $this;
    }

    /**
     * Recupera o nome do grupo de loja
     *
     * @return string
     */
    public function getNome()
    {
        return (string) $this->grp_loja_nome;
    }

    /**
     * Define a rede a qual o grupo de loja pertence
     *
     * @param App_Model_Entity_Rede $value
     * @return App_Model_Entity_GrupoLoja
     */
    public function setRede(App_Model_Entity_Rede $value)
    {
        $this->objRede = $value;
        $this->grp_loja_idRede = $value->getCodigo();
        return $this;
    }

    /**
     * Recupera a rede a qual o grupo de loja pertence
     * @return App_Model_Entity_Rede
     */
    public function getRede()
    {
        if (null == $this->objRede && $this->getCodigo()) {
            $this->objRede = $this->findParentRow(App_Model_DAO_Redes::getInstance(), 'Rede');
        }
        return $this->objRede;
    }

    /**
     * Define o tipo de pedido do grupo de loja
     *
     * @param string $value
     * @return App_Model_Entity_GrupoLoja
     */
    public function setTipoPedido($value)
    {
        $this->grp_loja_tipoPedido = (string) $value;
        return $this;
    }

    /**
     * Recupera o tipo de pedido do grupo de loja
     *
     * @return string
     */
    public function getTipoPedido()
    {
        return (string) $this->grp_loja_tipoPedido;
    }

    /**
     * Define o cliente matriz do grupo de loja
     *
     * @param string $value
     * @return App_Model_Entity_GrupoLoja
     */
    public function setClienteMatriz($value)
    {
        $this->grp_loja_clienteMatriz = (string) $value;
        return $this;
    }

    /**
     * Recupera o cliente matriz do grupo de loja
     *
     * @return string
     */
    public function getClienteMatriz()
    {
        return (string) $this->grp_loja_clienteMatriz;
    }

    /**
     * @param bool $value
     * @return App_Model_Entity_GrupoLoja
     */
    public function setTemAprovadorIntermediario($value)
    {
        $this->grp_loja_temAprovadorIntermediario = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function getTemAprovadorIntermediario()
    {
        return (bool) $this->grp_loja_temAprovadorIntermediario;
    }

    /**
     * Retorna todas as lojas deste grupo
     *
     * @return App_Model_Collection of App_Model_Entity_Loja
     */
    public function getLojas()
    {
        if (null == $this->objLojas) {
            if ($this->getCodigo()) {
                $this->objLojas = $this->findDependentRowset(App_Model_DAO_Lojas::getInstance(), 'Grupo');
                foreach ($this->objLojas as $rede) {
                    $rede->setGrupo($this);
                }
                $this->objLojas->rewind();
            } else {
                $this->objLojas = App_Model_DAO_Lojas::getInstance()->createRowset();
            }
        }
        return $this->objLojas;
    }

    /**
     * Retorna todas os usu�rios deste grupo
     *
     * @return App_Model_Collection of App_Model_Entity_Sistema_Usuario
     */
    public function getUsuarios()
    {
        if (null == $this->objUsuarios) {
            if ($this->getCodigo()) {
                $this->objUsuarios = $this->findDependentRowset(App_Model_DAO_Sistema_Usuarios::getInstance(), 'GrupoLoja');
                foreach ($this->objUsuarios as $usuario) {
                    $usuario->setGrupoLoja($this);
                }
                $this->objUsuarios->rewind();
            } else {
                $this->objUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance()->createRowset();
            }
        }
        return $this->objUsuarios;
    }

}
