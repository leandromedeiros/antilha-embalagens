<?php

class App_Model_Entity_SugestaoReclamacao_Status extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var int */
	public $reclamacao = null;
	
	/** @var string */
	public $atendente = null;
	
	/** @var int */
	public $status = null;
	
	/** @var string */
	public $data = null;
	
	/** @var string */
	public $descricao = null;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_Reclamacao
	 */
	protected $objReclamacao = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objReclamacao'));
		return $fields;
	}
	
	public function __wakeup()
	{
		$this->init();
		parent::__wakeup();
		$this->setTable(App_Model_DAO_SugestoesReclamacoes_Status::getInstance());
	}
		
	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'rec_status_idStatus' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'rec_status_idReclamacao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'rec_status_codigo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'sug_rec_nomeAtendente' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'sug_rec_status_data' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'sug_rec_status_descricao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 255)
			)			
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}	
	
	public function getCodigoAuto()
	{
		return (int) $this->sug_rec_status_idStatus;
	}
	
	public function setReclamacao(App_Model_Entity_SugestaoReclamacao $value)
	{
		$this->objReclamacao = $value;
		$this->sug_rec_status_idSugestaoReclamacao = $value->getCodigo();
		return $this;
	}

	public function getReclamacao()
	{
		if (null == $this->objReclamacao && $this->getCodigo()) {
			$this->objReclamacao = $this->findParentRow(App_Model_DAO_Usuarios::getInstance(), 'Reclamacao');
		}
		return $this->objReclamacao;
	}
	
	public function setCodigo($value)
	{
		$this->sug_rec_status_codigo = (int) $value;
		return $this;
	}

	public function getCodigo()
	{
		return (int) $this->sug_rec_status_codigo;
	}

	public function setData($value)
	{
		$this->sug_rec_status_data = (string) $value;
		return $this;
	}

	public function getData()
	{
		return (string) $this->sug_rec_status_data;
	}

	public function setNomeAtendente($value)
	{
		$this->sug_rec_nomeAtendente = (string) $value;
		return $this;
	}
	
	public function getNomeAtendente()
	{
		return (string) $this->sug_rec_nomeAtendente;
	}
	
	public function setDescricao($value)
	{
		$this->sug_rec_status_descricao = (string) $value;
		return $this;
	}

	public function getDescricao()
	{
		return (string) $this->sug_rec_status_descricao;
	}
	
	public function getNome(){
		
		return (string)$this->sug_rec_nomeAtendente;
	}
	
	public function setNome($value){
		
		$this->sug_rec_nomeAtendente = (string)$value;
		return $this;
	}
}