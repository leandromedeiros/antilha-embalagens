<?php

/**
 * Defini��o do objeto Pre�o
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Preco extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var string */
	public $produto = null;
	
	/** @var string */
	public $tipo = null; 
	
	/** @var string */
	public $tpPreco = null;
	
	/** @var float */
	public $preco = null;
	
	/** @var string */
	public $tpPrecoDesc = null;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_Produto
	 */
	protected $objProduto = null;
	
	/**
	 * @var App_Model_Entity_TipoLoja
	 */
	protected $objTipo = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objProduto', 'objTipo'));
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Precos::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'prec_idProduto' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 18)
			),
			'prec_idTipo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			),
			'prec_tpPreco' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 2)
			),
			'prec_preco' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prec_tpPrecoDesc' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)			
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

	 	// persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * Define o produto a qual o pre�o pertence
	 * 
	 * @param App_Model_Entity_Produto $value
	 * @return App_Model_Entity_Preco
	 */
	public function setProduto(App_Model_Entity_Produto $value)
	{
		$this->objProduto = $value;
		$this->prec_idProduto = (string) $value->getCodigo();
		return $this;
	}

	/**
	 * Recupera produto a qual o pre�o pertence
	 * 
	 * @return App_Model_Entity_Produto
	 */
	public function getProduto()
	{
		if (null == $this->objProduto) {
			$this->objProduto = $this->findParentRow(App_Model_DAO_Produtos::getInstance(), 'Produto');
		}
		return $this->objProduto;
	}
	
	/**
	 * Define o tipo a qual o pre�o pertence
	 * 
	 * @param App_Model_Entity_TipoLoja $value
	 * @return App_Model_Entity_Preco
	 */
	public function setTipo(App_Model_Entity_TipoLoja $value)
	{
		$this->objTipo = $value;
		$this->prec_idTipo = (string) $value->getCodigo();
		return $this;
	}

	/**
	 * Recupera tipo a qual o pre�o pertence
	 * 
	 * @return App_Model_Entity_TipoLoja
	 */
	public function getTipo()
	{
		if (null == $this->objTipo) {
			$this->objTipo = $this->findParentRow(App_Model_DAO_TiposLoja::getInstance(), 'Tipo');
		}
		return $this->objTipo;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Preco
	 */
	public function setTbPreco($value)
	{
		$this->prec_tpPreco = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTbPreco()
	{
		return (string) $this->prec_tpPreco;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Preco
	 */
	public function setPreco($value)
	{
		$this->prec_preco = (float) $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getPreco()
	{
		return (float) $this->prec_preco;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Preco
	 */
	public function setTpPrecoDesc($value)
	{
		$this->prec_tpPrecoDesc = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTpPrecoDesc()
	{
		return (string) $this->prec_tpPrecoDesc;
	}
}