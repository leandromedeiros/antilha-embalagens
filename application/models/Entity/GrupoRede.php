<?php

/**
 * Defini��o do objeto GrupoRede
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_GrupoRede extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var string */
	public $codigo = null;
	
	/** @var string */
	public $nome = null;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Rede
	 */
	protected $objRedes = null; 
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Sistema_Usuario
	 */
	protected $objUsuarios = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objRedes', 'objUsuarios'));
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_GrupoRedes::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'grp_rede_idGrupo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 6)
			),
			'grp_rede_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 45)
			)			
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * Define o c�digo do grupo de rede
	 * 
	 * @param int $value
	 * @return App_Model_Entity_GrupoRede
	 */
	public function setCodigo($value)
	{
		$this->grp_rede_idGrupo = (string) $value;
		return $this;
	}

	/**
	 * Recupera o c�digo identificador do grupo de rede
	 * 
	 * @return string
	 */
	public function getCodigo()
	{
		return (string) $this->grp_rede_idGrupo;
	}

	/**
	 * Define o nome do grupo de rede
	 * 
	 * @param string $value
	 * @return App_Model_Entity_GrupoRede
	 */
	public function setNome($value)
	{
		$this->grp_rede_nome = (string) $value;
		return $this;
	}

	/**
	 * Recupera o nome do grupo de rede
	 * 
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->grp_rede_nome;
	}	
	
	/**
	 * Retorna todas as redes deste grupo
	 * 
	 * @return App_Model_Collection of App_Model_Entity_Rede
	 */
	public function getRedes() {
		if (null == $this->objRedes) {
			if ($this->getCodigo()) {
				$this->objRedes = $this->findDependentRowset(App_Model_DAO_Redes::getInstance(), 'Grupo');
				foreach ($this->objRedes as $rede) {
					$rede->setGrupo($this);
				}
				$this->objRedes->rewind();
			} else {
				$this->objRedes = App_Model_DAO_Redes::getInstance()->createRowset();
			}
		}
		return $this->objRedes;
	}
	
	/**
	 * Retorna todas os usu�rios deste grupo
	 * 
	 * @return App_Model_Collection of App_Model_Entity_Sistema_Usuario
	 */
	public function getUsuarios() {
		if (null == $this->objUsuarios) {
			if ($this->getCodigo()) {
				$this->objUsuarios = $this->findDependentRowset(App_Model_DAO_Sistema_Usuarios::getInstance(), 'GrupoRede');
				foreach ($this->objUsuarios as $usuario) {
					$usuario->setGrupoRede($this);
				}
				$this->objUsuarios->rewind();
			} else {
				$this->objUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance()->createRowset();
			}
		}
		return $this->objUsuarios;
	}
}