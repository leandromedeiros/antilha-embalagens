<?php

/**
 * Defini��o do objeto Faq
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Faq extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Topico
	 */
	protected $objTopico = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objTopico'));
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Faqs::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'faq_idFaq' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'faq_idTopico' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'faq_pergunta' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 150)
			),
			'faq_resposta' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'faq_ordem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'faq_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)		
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Recupera o c�digo identificador do faq
	 * 
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->faq_idFaq;
	}
	
	/**
	 * @param App_Model_Entity_Topico $value
	 * @return App_Model_Entity_Faq
	 */
	public function setTopico(App_Model_Entity_Topico $value)
	{
		$this->objTopico = $value;
		$this->faq_idTopico = $value->getCodigo();
		return $this;
	}
	
	/**
	 * @return App_Model_Entity_Topico
	 */
	public function getTopico()
	{
		if (null == $this->objTopico && $this->getCodigo()) {
			$this->objTopico = $this->findParentRow(App_Model_DAO_Topicos::getInstance(), 'Topico');
		}
		return $this->objTopico;
	}
	
	/**
	 * Recupera a pergunta do faq
	 * 
	 * @return string
	 */
	public function getPergunta()
	{
		return (string) $this->faq_pergunta;
	}

	/**
	 * Define a pergunta do faq
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Faq
	 */
	public function setPergunta($value)
	{
		$this->faq_pergunta = (string) $value;
		return $this;
	}	
	
	/**
	 * Recupera a resposta do faq
	 * 
	 * @return string
	 */
	public function getResposta()
	{
		return (string) $this->faq_resposta;
	}
	
	/**
	 * Define a resposta do faq
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Faq
	 */
	public function setResposta($value)
	{
		$this->faq_resposta = (string) $value;
		return $this;
	}

	/**
	 * Recupera a ordem do faq
	 * 
	 * @return string
	 */
	public function getOrdem()
	{
		return (int) $this->faq_ordem;
	}
	
	/**
	 * Define a ordem do faq
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Faq
	 */
	public function setOrdem($value)
	{
		$this->faq_ordem = (int) $value;
		return $this;
	}
	
	/**
	 * Recupera se o faq est� ativo ou n�o
	 * @return boolean
	 */
	public function getStatus()
	{
		return (int) $this->faq_status;
	}
	
	/**
	 * Define se o faq est� ativo ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_Faq
	 */
	public function setStatus($value)
	{
		$this->faq_status = (int) $value;
		return $this;
	}
}