<?php

/**
 * Defini��o do objeto Download
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Download extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Collection of App_Model_Entity_Galeria_Arquivo
	 */
	protected $objImagem = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Download_Permissao
	 */
	protected $objPermissoes = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Download_Estado
	 */
	protected $objEstados = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objImagem', 'objPermissoes', 'objEstados'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Downloads::getInstance());
	}

	public function save()
	{
		$this->down_status = (int) $this->down_status;

		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'down_idDownload' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'down_idImagem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'down_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 150)
			),
			'down_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());
		
		// persiste os dados no banco
        try {
            $this->getTable()->getAdapter()->beginTransaction();
            $trans = true;
        } catch (Exception $e) {
            $trans = false;
        }
		
		//persiste os dados no banco
		try {
			parent::save();
			
			// Permiss�es
			$daoPermissoes = App_Model_DAO_Downloads_Permissoes::getInstance();
			$this->getPermissoes(); //for�a o carregamento das permiss�es
			$daoPermissoes->delete($daoPermissoes->getAdapter()->quoteInto('down_perm_idDownload = ?', $this->getCodigo()));
			foreach ($this->getPermissoes() as $permissao) {
				$daoPermissoes->insert(array(
					'down_perm_idDownload' => $this->getCodigo(),
					'down_perm_idGrupoRede' => $permissao->getGrupoRede() != null ? $permissao->getGrupoRede()->getCodigo() : null,
					'down_perm_idRede' => $permissao->getRede() != null ? $permissao->getRede()->getCodigo() : null,
					'down_perm_idGrupoLoja' => $permissao->getGrupoLoja() != null ? $permissao->getGrupoLoja()->getCodigo() : null,
					'down_perm_idLoja' => $permissao->getLoja() != null ? $permissao->getLoja()->getCodigo() : null,
					'down_perm_idLinha' => $permissao->getLinha() != null ? $permissao->getLinha()->getCodigo() : null
				));
			}
			unset($daoPermissoes);
			
			// Estados
			$daoEstados = App_Model_DAO_Downloads_Estados::getInstance();
			$this->getEstados(); //for�a o carregamento das permiss�es
			$daoEstados->delete($daoEstados->getAdapter()->quoteInto('down_est_idDownload = ?', $this->getCodigo()));
			foreach ($this->getEstados() as $estado) {
				$daoEstados->insert(array(
					'down_est_idDownload' => $this->getCodigo(),
					'down_est_uf' => $estado->getUf()
				));
			}
			unset($daoEstados);
			
			if ($trans) $this->getTable()->getAdapter()->commit();
			
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Recupera o c�digo identificador do download
	 * 
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->down_idDownload;
	}

	/**
	 * Define a imagem a qual o download pertence
	 * 
	 * @params App_Model_Entity_Galeria_Arquivo $value
	 * @return App_Model_Entity_Download
	 */
	public function setImagem(App_Model_Entity_Galeria_Arquivo $value)
	{
		$this->objImagem = $value;
		$this->down_idImagem = (int) $value->getCodigo();
		return $this;
	}

	/**
	 * Recupera a imagem a qual o download pertence
	 * 
	 * @return App_Model_Entity_Galeria_Arquivo
	 */
	public function getImagem()
	{
		if (null == $this->objImagem && $this->getCodigo()) {
			$this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
		}
		return $this->objImagem;
	}
	
	/**
	 * Define o t�tulo do download
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Download
	 */
	public function setNome($value)
	{
		$this->down_nome = (string) $value;
		return $this;
	}

	/**
	 * Recupera o t�tulo do download
	 * 
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->down_nome;
	}
	
	public function setData($value){
		
		$this->down_data = (string) $value;
		return $this;
	}
	
	public function getData(){
	
		return (string) $this->down_data;
	}
	
	/**
	 * Define se o download est� ativo ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_Download
	 */
	public function setStatus($value)
	{
		$this->down_status = (bool) $value;
		return $this;
	}

	/**
	 * Recupera se o download est� ativo ou n�o
	 * @return boolean
	 */
	public function getStatus()
	{
		return (bool) $this->down_status;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Download_Permissao
	 */
	public function getPermissoes()
	{
		if (null == $this->objPermissoes) {
			if ($this->getCodigo()) {
				$this->objPermissoes = $this->findDependentRowset(App_Model_DAO_Downloads_Permissoes::getInstance(), 'Download');
				foreach ($this->objPermissoes as $permissao) {
					$permissao->setDownload($this);
				}
				$this->objPermissoes->rewind();
			} else {
				$this->objPermissoes = App_Model_DAO_Downloads_Permissoes::getInstance()->createRowset();
			}
		}
		return $this->objPermissoes;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Comunicado_Estado
	 */
	public function getEstados()
	{
		if (null == $this->objEstados) {
			if ($this->getCodigo()) {
				$this->objEstados = $this->findDependentRowset(App_Model_DAO_Downloads_Estados::getInstance(), 'Download');
				foreach ($this->objEstados as $estado) {
					$estado->setDownload($this);
				}
				$this->objEstados->rewind();
			} else {
				$this->objEstados = App_Model_DAO_Downloads_Estados::getInstance()->createRowset();
			}
		}
		return $this->objEstados;
	}
}