<?php

class App_Model_Entity_Loja_Contato extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var string */
	public $tipo = null;
	
	/** @var string */
	public $objetivo = null;
	
	/** @var string */
	public $valor = null;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_Loja
	 */
	protected $objLoja = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objLoja'));
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Lojas_Contatos::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'loja_con_idContato' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'loja_con_idLoja' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			),
			'loja_con_tipo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_InArray(array('TEL', 'EMA'))
			),
			'loja_con_objetivo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			),
			'loja_con_valor' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 60)
			)			
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->loja_con_idContato;
	}
	
	/**
	 * @param App_Model_Entity_Loja $value
	 * @return App_Model_Entity_Loja_Endereco
	 */
	public function setLoja(App_Model_Entity_Loja $value)
	{
		$this->objLoja = $value;
		$this->loja_con_idLoja = $value->getCodigo();
		return $this;
	}

	/**
	 * @return App_Model_Entity_Loja
	 */
	public function getLoja()
	{
		if (null == $this->objLoja && $this->getCodigo()) {
			$this->objLoja = $this->findParentRow(App_Model_DAO_Lojas::getInstance(), 'Loja');
		}
		return $this->objLoja;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Loja_Contato
	 */
	public function setTipo($value)
	{
		$this->loja_con_tipo = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTipo()
	{
		return (string) $this->loja_con_tipo;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Loja_Contato
	 */
	public function setObjetivo($value)
	{
		$this->loja_con_objetivo = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getObjetivo()
	{
		return (string) $this->loja_con_objetivo;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Loja_Contato
	 */
	public function setValor($value)
	{
		$this->loja_con_valor = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getValor()
	{
		return (string) $this->loja_con_valor;
	}
}