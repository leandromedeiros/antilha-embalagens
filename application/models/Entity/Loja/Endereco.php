<?php

class App_Model_Entity_Loja_Endereco extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/**
	 * Exemplo: FAT=Faturamento; ENT=Entrega; COB=Cobran�a
	 * @var string
	 **/
	public $tipo = null;
	
	/** @var string */
	public $cep = null;
	
	/** @var string */
	public $logradouro = null;
	
	/** @var string */
	public $numero = null;
	
	/** @var string */
	public $complemento = null;
	
	/** @var string */
	public $bairro = null;
	
	/** @var string */
	public $cidade = null;
	
	/** @var string */
	public $uf = null;
	
	/** @var int */
	public $prazo = null;
	
	/**
	 * Exemplo: 0=N�o; 1=Sim
	 * @var int
	 **/
	public $padrao = null;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_Loja
	 */
	protected $objLoja = null;
	
	/**
	 * @var App_Model_Endereco
	 */
	protected $objEndereco = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objLoja', 'objEndereco'));
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Lojas_Enderecos::getInstance());
	}
	
	public function init()
	{
		$this->objEndereco = new App_Model_Endereco($this, array(
			App_Model_Endereco::CEP => 'loja_end_cep',
			App_Model_Endereco::LOGRADOURO => 'loja_end_logradouro',
			App_Model_Endereco::NUMERO => 'loja_end_numero',
			App_Model_Endereco::COMPLEMENTO => 'loja_end_complemento',
			App_Model_Endereco::BAIRRO => 'loja_end_bairro',
			App_Model_Endereco::CIDADE => 'loja_end_cidade',
			App_Model_Endereco::UF => 'loja_end_uf'
		));
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'loja_end_idEndereco' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'loja_end_idLoja' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			),
			'loja_end_tipo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_InArray(array('FAT', 'ENT', 'COB'))
			),
			'loja_end_cep' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 9)
			),
			'loja_end_logradouro' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 70)
			),
			'loja_end_numero' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			),
			'loja_end_complemento' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 30)
			),
			'loja_end_bairro' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 30)
			),
			'loja_end_cidade' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 30)
			),
			'loja_end_uf' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(2, 2)
			),
			'loja_end_cnpj' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new App_Validate_CpfCnpj(),
				new App_Validate_Loja_Cnpj($this)
			),
			'loja_end_ie' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 20)
			),
			'loja_end_aliquotaICMS' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'loja_end_prazo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'loja_end_padrao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->loja_end_idEndereco;
	}
	
	/**
	 * @param App_Model_Entity_Loja $value
	 * @return App_Model_Entity_Loja_Endereco
	 */
	public function setLoja(App_Model_Entity_Loja $value)
	{
		$this->objLoja = $value;
		$this->loja_end_idLoja = $value->getCodigo();
		return $this;
	}

	/**
	 * @return App_Model_Entity_Loja
	 */
	public function getLoja()
	{
		if (null == $this->objLoja && $this->getCodigo()) {
			$this->objLoja = $this->findParentRow(App_Model_DAO_Lojas::getInstance(), 'Loja');
		}
		return $this->objLoja;
	}
	
	/**
	 * @return App_Model_Endereco
	 */
	public function getEndereco()
	{
		return $this->objEndereco;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Loja_Endereco
	 */
	public function setCnpj($value)
	{
		$this->loja_end_cnpj = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCnpj()
	{
		return (string) $this->loja_end_cnpj;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Loja_Endereco
	 */
	public function setIE($value)
	{
		$this->loja_end_ie = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getIE()
	{
		return (string) $this->loja_end_ie;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Loja_Endereco
	 */
	public function setAliquotaICMS($value)
	{
		$this->loja_end_aliquotaICMS = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAliquotaICMS()
	{
		return (string) $this->loja_end_aliquotaICMS;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Loja_Endereco
	 */
	public function setTipo($value)
	{
		$this->loja_end_tipo = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTipo()
	{
		return (string) $this->loja_end_tipo;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Loja_Endereco
	 */
	public function setPrazo($value)
	{
		$this->loja_end_prazo = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPrazo()
	{
		return (string) $this->loja_end_prazo;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Loja_Endereco
	 */
	public function setPadrao($value)
	{
		$this->loja_end_padrao = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPadrao()
	{
		return (string) $this->loja_end_padrao;
    }
    
    /**
     * @return string
     */
    public function getUf()
    {
        return (string) $this->loja_end_uf;
    }
}