<?php

class App_Model_Entity_SugestaoReclamacao extends App_Model_Entity_Abstract
{
	/*     * ** In�cio Propriedades do WebService ************ */

    /** @var string */
    public $codigo = null;
	
	/** @var int */
	public $usuario = null;
	
	/** @var int */
	public $assunto = null;
	
	/** @var int */
	public $motivo = null;
	
	/** @var int */
	public $pedido = null;
	
	/** @var int */
	public $canal = null;
	
	/** @var string */
	public $protocolo = null;
	
	/** @var string */
	public $nome = null;
	
	/** @var string */
	public $email = null;
	
	/** @var int */
	public $tipo = null;
	
	/** @var string */
	public $data = null;
	
	/** @var string */
	public $mensagem = null;
	
	 /** @var int */
    public $status = null;

    /*     * ** Fim Propriedades do WebService ************ */
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_SugestaoReclamacao_Status
	 */
	protected $objStatus = null;
	
	/**
	 * @var App_Model_Entity_SugestaoReclamacao_Assunto
	 */
	protected $objAssunto = null;
	
	/**
	 * @var App_Model_Entity_SugestaoReclamacao_Assunto_Motivo
	 */
	protected $objMotivo = null;
	
	/**
	 * @var App_Model_Entity_Pedido
	 */
	protected $objPedido = null;
	
	/**
     * @var App_Model_Entity_Usuario
     */
    protected $objUsuario = null;
    
    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array(
        	'objUsuario', 'objStatus', 'objAssunto', 'objMotivo', 'objPedido',
        ));
        return $fields;
    }
    
	public function __wakeup()
    {
        $this->init();
        parent::__wakeup();
        $this->setTable(App_Model_DAO_SugestoesReclamacoes::getInstance());
    }
    
    public function save()
    {
    	
    	$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'sug_rec_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(3)
			),
			'sug_rec_idAssunto' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'sug_rec_idMotivo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'sug_rec_idPedido' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'sug_rec_mensagem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'sug_rec_email' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_EmailAddress()
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());
    	
        try {
            $this->getTable()->getAdapter()->beginTransaction();
            $trans = true;
        } catch (Exception $e) {
            $trans = false;
        }
		
    	try {
			parent::save();		
			
    		// Status
			foreach ($this->getHistoricoStatus() as $status) {
				$status->setReclamacao($this);
				$status->save();
			}
			
			if ($trans) $this->getTable()->getAdapter()->commit();
		} catch (App_Validate_Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
    }
    
	/**
	 * Recupera o c�digo identificador da Sugest�o ou Reclama��o
	 * 
	 * @return integer
	 */
    public function getCodigo()
    {
        return (int) $this->sug_rec_idSugestaoReclamacao;
    }
    
	/**
	 * @params App_Model_Entity_Sistema_Usuario $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
	public function setUsuario(App_Model_Entity_Sistema_Usuario $value)
	{
		$this->objUsuario = $value;
		$this->sug_rec_idUsuario = $value->getCodigo();
		return $this;
	}

	/**	
	 * @return App_Model_Entity_Sistema_Usuario
	 */
	public function getUsuario()
	{
		if (null == $this->objUsuario && $this->getCodigo()) {
			$this->objUsuario = $this->findParentRow(App_Model_DAO_Sistema_Usuarios::getInstance(), 'Usuario');
		}
		return $this->objUsuario;
	}
	
	/**
	 * @params App_Model_Entity_SugestaoReclamacao_Assunto $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
	public function setAssunto(App_Model_Entity_SugestaoReclamacao_Assunto $value)
	{
		$this->objAssunto = $value;
		$this->sug_rec_idAssunto = $value->getCodigo();
		return $this;
	}

	/**
	 * @return App_Model_Entity_SugestaoReclamacao_Assunto
	 */
	public function getAssunto()
	{
		if (null == $this->objAssunto && $this->getCodigo()) {
			$this->objAssunto = $this->findParentRow(App_Model_DAO_SugestoesReclamacoes_Assuntos::getInstance(), 'Assunto');
		}
		return $this->objAssunto;
	}
	
	/**
	 * @params App_Model_Entity_SugestaoReclamacao_Assunto_Motivo $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
	public function setMotivo(App_Model_Entity_SugestaoReclamacao_Assunto_Motivo $value = null)
	{
		$this->objMotivo = $value;
		$this->sug_rec_idMotivo = $value ? $value->getCodigo() : null;
		return $this;
	}

	/**
	 * @return App_Model_Entity_SugestaoReclamacao_Assunto_Motivo
	 */
	public function getMotivo()
	{
		if (null == $this->objMotivo && $this->getCodigo()) {
			$this->objMotivo = $this->findParentRow(App_Model_DAO_SugestoesReclamacoes_Assuntos_Motivos::getInstance(), 'Motivo');
		}
		return $this->objMotivo;
	}
	
	/**
	 * @params App_Model_Entity_Pedido $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
	public function setPedido(App_Model_Entity_Pedido $value = null)
	{
		$this->objPedido = $value;
		$this->sug_rec_idPedido = $value ? $value->getCodigo() : null;
		return $this;
	}

	/**
	 * @return App_Model_Entity_Pedido
	 */
	public function getPedido()
	{
		if (null == $this->objMotivo && $this->getCodigo()) {
			$this->objPedido = $this->findParentRow(App_Model_DAO_Pedidos::getInstance(), 'Pedido');
		}
		return $this->objPedido;
	}
	
	/**
	 * Recupera a data de cadastro da sugest�o ou reclama��o
	 * 
	 * @return string
	 */
	public function getDataCadastro()
	{
		return (string) $this->sug_rec_dataCadastro;
	}
	
	/**
	 * Define a data de cadastro da sugest�o ou reclama��o
	 * 
	 * @param string $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
	public function setDataCadastro($value)
    {
        $this->sug_rec_dataCadastro = (string) $value;
        return $this;
    }
    
	/**
	 * Define a mensagem da sugest�o ou reclama��o
	 * 
	 * @param string $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
	public function setMensagem($value)
    {
        $this->sug_rec_mensagem = (string) $value;
        return $this;
    }

    /**
	 * Recupera a mensagem da sugest�o ou reclama��o
	 * 
	 * @return string
	 */
    public function getMensagem()
    {
        return (string) $this->sug_rec_mensagem;
    }
    
    /**
	 * Define o tipo da sugest�o ou reclama��o
	 * 
	 * @param string $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
	public function setTipo($value)
    {
        $this->sug_rec_tipo = (string) $value;
        return $this;
    }

    /**
	 * Recupera o tipo da sugest�o ou reclama��o
	 * 
	 * @return string
	 */
    public function getTipo()
    {
        return (string) $this->sug_rec_tipo;
    }
    
	/**
	 * Seta o nome do usuario
	 * 
	 * @param string $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
    public function setNome($value)
    {
        $this->sug_rec_nome = (string) $value;
        return $this;
    }

    /**
	 * @return string
	 */
    public function getCanal()
    {
        return (int) $this->sug_rec_canal;
    }
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
    public function setCanal($value)
    {
        $this->sug_rec_canal = (int) $value;
        return $this;
    }
	
	/**
	 * @return string
	 */
    public function getProtocolo()
    {
        return (string) $this->sug_rec_protocolo;
    }
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
    public function setProtocolo($value)
    {
        $this->sug_rec_protocolo = (string) $value;
        return $this;
    }

    /**
	 * Recupera o nome
	 * @return string
	 */
    public function getNome()
    {
        return (string) $this->sug_rec_nome;
    }
    
	/**
	 * Seta o email do usuario
	 * 
	 * @param string $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
    public function setEmail($value)
    {
        $this->sug_rec_email = (string) $value;
        return $this;
    }

    /**
	 * Recupera email do usuario
	 * @return string
	 */
    public function getEmail()
    {
        return (string) $this->sug_rec_email;
    }
    
	/**
	 * Define se a reclama��o ou sugest�o est� ativa ou n�o
	 * 
	 * @param boolean $value
	 * @return App_Model_Entity_SugestaoReclamacao
	 */
    public function setStatus($value)
    {
        $this->sug_rec_status = (int) $value;
        return $this;
    }

    /**
	 * Recupera se a reclama��o ou sugest�o est� ativa ou n�o
	 * @return boolean
	 */
    public function getStatus()
    {
        return (int) $this->sug_rec_status;
    }
    
	/**
	 * Recupera o hist�rico de status
	 * 
	 * @return App_Model_Collection of App_Model_Entity_SugestaoReclamacao_Status
	 */
	public function getHistoricoStatus()
	{
		if (null == $this->objStatus) {
			if ($this->getCodigo()) {
				$this->objStatus = $this->findDependentRowset(
					App_Model_DAO_SugestoesReclamacoes_Status::getInstance(),
					'SugestaoReclamacao'
				);
			} else {
				$this->objStatus = App_Model_DAO_SugestoesReclamacoes_Status::getInstance()->createRowset();
			}
		}
		return $this->objStatus;
	}
}