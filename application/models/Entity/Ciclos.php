<?php

/**
 * Defini��o do objeto Ciclos
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Ciclos extends App_Model_Entity_Abstract
{
    /*     * ** In�cio Propriedades do WebService ************ */

    /** @var int */
    public $idCiclo = null;

    /** @var string */
    public $descricao = null;

    /** 
     * Formato 0000-00-00
     * 
     * @var string 
     */
    public $data = null;

    /** @var string */
    public $horaDeCorte = null;

    /*     * ** Fim Propriedades do WebService ************ */
    
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Ciclos::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
        );
        
		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * @return integer
	 */
	public function getIdCiclo()
	{
		return (int) $this->ci_idCiclo;
    }

    /**
     * @param integer $value
	 * @return App_Model_Entity_Ciclos
	 */
	public function setIdCiclo($value)
	{
        $this->ci_idCiclo = $value;
		return $this;
    }
  
  /**
	 * @return integer
	 */
	public function getDescricao()
	{
		return (int) $this->ci_descricao;
	}
	
	
	/**
   * @param string
	 * @return App_Model_Entity_Ciclos
	 */
	public function setDescricao($value)
	{
		$this->ci_descricao = $value;
		return $this;
  }
  
   /**
	 * @return string
	 */
	public function getData()
	{
		return (string) $this->ci_data;
	}
	
	
	/**
   * @param string
	 * @return App_Model_Entity_Ciclos
	 */
	public function setData($value)
	{
		$this->ci_data = $value;
		return $this;
  }
  
   /**
	 * @return string
	 */
	public function getHoraDeCorte()
	{
		return (string) $this->ci_horaDeCorte;
	}
	
	
	/**
   * @param string
	 * @return App_Model_Entity_Ciclos
	 */
	public function setHoraDeCorte($value)
	{
		$this->ci_horaDeCorte = $value;
		return $this;
	}
}