<?php

class App_Model_Entity_Pedido_Nota extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var int */
	public $pedido;
	
	/** @var string */
	public $status;
	
	/** @var string */
	public $numero;
	
	/** @var string */
	public $serie;
	
	/** @var string */
	public $cliente;
	
	/** @var string */
	public $dataEmissao;
	
	/** @var float */
	public $valorTotal;
	
	/** @var string */
	public $numeroPedidoCliente;
	
	/** @var int */
	public $transportadora;
	
	/** @var string */
	public $descricaoStatus;
	
	/** @var int */
	public $prazoEntrega;
	
	/** @var int */
	public $numeroFilial;
	
	/** @var string */
	public $caminhoXML;
	
	/** @var string */
	public $caminhoPDF;
	
	/** @var App_Model_Entity_Pedido_Nota_Item[] **/
	public $itens;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_Pedido
	 */
	protected $objPedido = null;
	
	/**
	 * @var App_Model_Entity_Produto
	 */
	protected $objProduto = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Pedido_Nota_Item
	 */
	protected $objItens = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Pedido_Nota_Boleto
	 */
	protected $objBoletos = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Pedido_Nota_Status
	 */
	protected $objStatus = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objPedido', 'objProduto', 'objItens', 'objBoletos', 'objStatus'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Notas::getInstance());
	}
	
	public function save($semDependencias = false)
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'ped_nota_idNotaFiscal' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_idPedido' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_numero' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 9)
			),
			'ped_nota_serie' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 3)
			),
			'ped_nota_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_cliente' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_dataEmissao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true				
			),
			'ped_nota_valorTotal' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_numeroPedidoCliente' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_transportadora' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_descricaoStatus' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_prazoEntrega' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_numeroFilial' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_caminhoXML' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_caminhoPDF' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());
		
		// persiste os dados no banco
        try {
            $this->getTable()->getAdapter()->beginTransaction();
            $trans = true;
        } catch (Exception $e) {
            $trans = false;
        }

		//persiste os dados no banco
		try {
			parent::save();
			
			if($semDependencias) {
				// Itens
				$daoItens = App_Model_DAO_Pedidos_Notas_Itens::getInstance();
				$this->getItens(); //for�a o carregamento dos itens
				$daoItens->delete($daoItens->getAdapter()->quoteInto('ped_nota_item_idNota = ?', $this->getCodigo()));
				foreach ($this->getItens() as $item) {
					$item->setNota($this);
					$item->save();
				}
				unset($daoItens);
			}
			
			if ($trans) $this->getTable()->getAdapter()->commit();
			
		} catch (App_Validate_Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            if ($trans) $this->getTable()->getAdapter()->rollBack();
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setCodigo($value)
	{
		$this->ped_nota_idNotaFiscal = (int) $value;
		return $this;
	}
	
	/**
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->ped_nota_idNotaFiscal;
	}	
	
	/**
	 * @param App_Model_Entity_Pedido $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setPedido(App_Model_Entity_Pedido $value)
	{
		$this->objPedido = $value;
		$this->ped_nota_idPedido = $value->getCodigo();
		return $this;
	}

	/**
	 * @return App_Model_Entity_Pedido
	 */
	public function getPedido()
	{
		if (null == $this->objPedido && $this->getCodigo()) {
			$this->objPedido = $this->findParentRow(App_Model_DAO_Pedidos::getInstance(), 'Pedido');
		}
		return $this->objPedido;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota
	 */
	public function setStatus($value)
	{
		$this->ped_nota_status = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getNumero()
	{
		return (string) $this->ped_nota_numero;
	}	
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota
	 */
	public function setNumero($value)
	{
		$this->ped_nota_numero = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getSerie()
	{
		return (string) $this->ped_nota_serie;
	}	
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota
	 */
	public function setSerie($value)
	{
		$this->ped_nota_serie = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getStatus()
	{
		return (string) $this->ped_nota_status;
	}	
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setCliente($value)
	{
		$this->ped_nota_cliente = (float) $value;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getCliente()
	{
		return (float) $this->ped_nota_cliente;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setDataEmissao($value)
	{
		$this->ped_nota_dataEmissao = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDataEmissao()
	{
		return (string) $this->ped_nota_dataEmissao;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setValorTotal($value)
	{
		$this->ped_nota_valorTotal = (float) $value;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getValorTotal()
	{
		return (float) $this->ped_nota_valorTotal;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setNumeroPedidoCliente($value)
	{
		$this->ped_nota_numeroPedidoCliente = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getNumeroPedidoCliente()
	{
		return (string) $this->ped_nota_numeroPedidoCliente;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setTransportadora($value)
	{
		$this->ped_nota_transportadora = (float) $value;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getTransportadora()
	{
		return (float) $this->ped_nota_transportadora;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setDescricaoStatus($value)
	{
		$this->ped_nota_descricaoStatus = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDescricaoStatus()
	{
		return (string) $this->ped_nota_descricaoStatus;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setPrazoEntrega($value)
	{
		$this->ped_nota_prazoEntrega = (int) $value;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getPrazoEntrega()
	{
		return (int) $this->ped_nota_prazoEntrega;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setNumeroFilial($value)
	{
		$this->ped_nota_numeroFilial = (int) $value;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getNumeroFilial()
	{
		return (int) $this->ped_nota_numeroFilial;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setCaminhoXML($value)
	{
		$this->ped_nota_caminhoXML = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCaminhoXML()
	{
		return (string) $this->ped_nota_caminhoXML;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setCaminhoPDF($value)
	{
		$this->ped_nota_caminhoPDF = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCaminhoPDF()
	{
		return (string) $this->ped_nota_caminhoPDF;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Pedido_Nota_Item
	 */
	public function getItens()
	{
		if (null == $this->objItens) {
			if ($this->getCodigo()) {
				$this->objItens = $this->findDependentRowset(App_Model_DAO_Pedidos_Notas_Itens::getInstance(), 'Nota');
				foreach ($this->objItens as $item) {
					$item->setNota($this);
				}
				$this->objItens->rewind();
			} else {
				$this->objItens = App_Model_DAO_Pedidos_Notas_Itens::getInstance()->createRowset();
			}
		}
		return $this->objItens;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Pedido_Nota_Status
	 */
	public function getHistoricoStatus()
	{
		if (null == $this->objStatus) {
			if ($this->getCodigo()) {
				$this->objStatus = $this->findDependentRowset(App_Model_DAO_Pedidos_Notas_Status::getInstance(), 'Nota');
			} else {
				$this->objStatus = App_Model_DAO_Pedidos_Notas_Itens::getInstance()->createRowset();
			}
		}
		return $this->objStatus;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function getBoletos()
	{
		if (null == $this->objBoletos) {
			if ($this->getCodigo()) {
				$this->objBoletos = $this->findDependentRowset(App_Model_DAO_Pedidos_Notas_Boletos::getInstance(), 'Nota');
				foreach ($this->objBoletos as $item) {
					$item->setNota($this);
				}
				$this->objBoletos->rewind();
			} else {
				$this->objBoletos = App_Model_DAO_Pedidos_Notas_Boletos::getInstance()->createRowset();
			}
		}
		return $this->objBoletos;
	}
}