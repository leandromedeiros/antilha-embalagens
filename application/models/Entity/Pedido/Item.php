<?php

class App_Model_Entity_Pedido_Item extends App_Model_Entity_Abstract
{
    /*     * ** In�cio Propriedades do WebService ************ */

    /** @var string */
    public $produto;

    /** @var int */
    protected $reposicao;

    /** @var int */
    public $numeroItem;

    /** @var float */
    public $qtdTotal;

    /** @var float */
    public $valorUnitario;

    /** @var float */
    public $valorTotal;

    /** @var float */
    public $ipi;

    /** @var float */
    public $icms;

    /** @var string */
    public $unidade;

    /** @var string */
    public $status;

    /** @var int */
    public $sequencialItens;

    /** @var float */
    public $totalPeso;

    /** @var float */
    public $saldoProduto;

    /** @var float */
    public $saldoWcs;

    /** @var float */
    public $qtdFaturado;

    /** @var int */
    public $prefixo;
	
    /**
     * var App_Model_Entity_Pedido
     */
	public $objPedido = null;

    /*     * ** Fim Propriedades do WebService ************ */

    /**
     * @var App_Model_Entity_Pedido
     */
    protected $objReposicao = null;

    /**
     * @var App_Model_Entity_Produto
     */
    protected $objProduto = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objPedido', 'objReposicao', 'objProduto'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Pedidos_Itens::getInstance());
    }

    public function save()
    {
        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'ped_item_idItem' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_idPedido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'ped_item_idReposicao' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_produto' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_numeroItem' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_qtdTotal' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_ipi' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_icms' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_unidade' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_status' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_sequencialItens' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_totalPeso' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_saldoProduto' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_saldoWcs' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_qtdFaturado' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_item_prefixo' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        //persiste os dados no banco
        try {
            parent::save();
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setCodigo($value)
    {
        $this->ped_item_idItem = (int) $value;
        return $this;
    }

    /**
     * @return integer
     */
    public function getCodigo()
    {
        return (int) $this->ped_item_idItem;
    }

    /**
     * @param App_Model_Entity_Pedido $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setPedido(App_Model_Entity_Pedido $value)
    {
        $this->objPedido = $value;
        $this->ped_item_idPedido = $value->getCodigo();
        return $this;
    }

    /**
     * @return App_Model_Entity_Pedido
     */
    public function getPedido()
    {
        if (null == $this->objPedido && $this->getCodigo()) {
            $this->objPedido = $this->findParentRow(App_Model_DAO_Pedidos::getInstance(), 'Pedido');
        }
        return $this->objPedido;
    }

    /**
     * @param App_Model_Entity_Pedido $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setReposicao(App_Model_Entity_Pedido $value)
    {
        $this->objReposicao = $value;
        $this->ped_item_idReposicao = $value->getCodigo();
        return $this;
    }

    /**
     * @return App_Model_Entity_Pedido
     */
    public function getReposicao()
    {
        if (null == $this->objReposicao && $this->getCodigo()) {
            $this->objReposicao = $this->findParentRow(App_Model_DAO_Pedidos::getInstance(), 'Reposicao');
        }
        return $this->objReposicao;
    }

    /**
     * @param App_Model_Entity_Produto $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setProduto(App_Model_Entity_Produto $value = null)
    {
        $this->objProduto = $value;
        $this->ped_item_produto = $value != null ? $value->getCodigo() : null;
        return $this;
    }

    /**
     * @return App_Model_Entity_Produto
     */
    public function getProduto()
    {
        if (null == $this->objProduto && $this->getCodigo()) {
            $this->objProduto = $this->findParentRow(App_Model_DAO_Produtos::getInstance(), 'Produto');
        }
        return $this->objProduto;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setNumeroItem($value)
    {
        $this->ped_item_numeroItem = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumeroItem()
    {
        return (int) $this->ped_item_numeroItem;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setQtdTotal($value)
    {
        $this->ped_item_qtdTotal = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getQtdTotal()
    {
        return (float) $this->ped_item_qtdTotal;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setValorUnitario($value)
    {
        $this->ped_item_valorUnitario = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getValorUnitario()
    {
        return (float) $this->ped_item_valorUnitario;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setValorTotal($value)
    {
        $this->ped_item_valorTotal = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getValorTotal()
    {
        return (float) $this->ped_item_valorTotal;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setIPI($value)
    {
        $this->ped_item_ipi = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getIPI()
    {
        return (float) $this->ped_item_ipi;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setICMS($value)
    {
        $this->ped_item_icms = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getICMS()
    {
        return (float) $this->ped_item_icms;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setUnidade($value)
    {
        $this->ped_item_unidade = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnidade()
    {
        return (string) $this->ped_item_unidade;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setStatus($value)
    {
        $this->ped_item_status = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return (string) $this->ped_item_status;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setSequencialItens($value)
    {
        $this->ped_item_sequencialItens = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getSequencialItens()
    {
        return (int) $this->ped_item_sequencialItens;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setTotalPeso($value)
    {
        $this->ped_item_totalPeso = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPeso()
    {
        return (float) $this->ped_item_totalPeso;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setSaldoProduto($value)
    {
        $this->ped_item_saldoProduto = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getSaldoProduto()
    {
        return (float) $this->ped_item_saldoProduto;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setSaldoWcs($value)
    {
        $this->ped_item_saldoWcs = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getSaldoWcs()
    {
        return (float) $this->ped_item_saldoWcs;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setQtdFaturado($value)
    {
        $this->ped_item_qtdFaturado = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getQtdFaturado()
    {
        return (float) $this->ped_item_qtdFaturado;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido_Item
     */
    public function setPrefixo($value)
    {
        $this->ped_item_prefixo = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrefixo()
    {
        return (int) $this->ped_item_prefixo;
    }

}
