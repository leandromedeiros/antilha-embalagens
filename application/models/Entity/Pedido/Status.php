<?php

class App_Model_Entity_Pedido_Status extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var int */
	public $pedido;
	
	/** @var int */
	public $valor;
	
	/**
	 * Exemplo: DD/MM/YYYY hh:mm:ss
	 * @var string
	 **/
	public $data;
	
	/**
	 * Exemplo: DD/MM/YYYY
	 * @var string
	 **/
	public $dataEntregaParcial;
	
	/** @var string */
	public $descricao;
	
	/** @var int */
	public $enviarEmail;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_Pedido
	 */
	protected $objPedido = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objPedido'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Status::getInstance());
	}
	
	public function save($transaction = true)
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'ped_status_idStatus' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_status_idPedido' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_status_valor' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_status_data' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_status_dataEntregaParcial' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_status_descricao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false				
			),
			'ped_status_enviarEmail' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true				
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		// persiste os dados no banco
		if($transaction) {
	        try {
	            $this->getTable()->getAdapter()->beginTransaction();
	            $trans = true;
	        } catch (Exception $e) {
	            $trans = false;
	        }
		}
        
		//persiste os dados no banco
		try {
			parent::save();
			if ($transaction && $trans) $this->getTable()->getAdapter()->commit();
		} catch (App_Validate_Exception $e) {
			if ($transaction && $trans) $this->getTable()->getAdapter()->rollBack();
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            if ($transaction && $trans) $this->getTable()->getAdapter()->rollBack();
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Status
	 */
	public function setCodigo($value)
	{
		$this->ped_status_idStatus = (int) $value;
		return $this;
	}
	
	/**
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->ped_status_idStatus;
	}	
	
	/**
	 * @param App_Model_Entity_Pedido $value
	 * @return App_Model_Entity_Pedido_Status
	 */
	public function setPedido(App_Model_Entity_Pedido $value)
	{				
		$this->objPedido = $value;
		$this->ped_status_idPedido = (int) $value->getCodigo();
		return $this;
	}

	/**
	 * @return App_Model_Entity_Pedido
	 */
	public function getPedido()
	{
		if (null == $this->objPedido && $this->getCodigo()) {
			$this->objPedido = $this->findParentRow(App_Model_DAO_Pedidos::getInstance(), 'Pedido');
		}
		return $this->objPedido;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Status
	 */
	public function setValor($value)
	{
		$this->ped_status_valor = (int) $value;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getValor()
	{
		return (int) $this->ped_status_valor;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Status
	 */
	public function setData($value)
	{
		$this->ped_status_data = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getData()
	{
		return (string) $this->ped_status_data;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Status
	 */
	public function setDataEntregaParcial($value)
	{
		$this->ped_status_dataEntregaParcial = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDataEntregaParcial()
	{
		return (string) $this->ped_status_dataEntregaParcial;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Status
	 */
	public function setEnviarEmail($value)
	{
		$this->ped_status_enviarEmail = (int) $value;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getEnviarEmail()
	{
		return (int) $this->ped_status_enviarEmail;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Status
	 */
	public function setDescricao($value)
	{
		$this->ped_status_descricao = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDescricao()
	{
		return (string) $this->ped_status_descricao;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Status
	 */
	public function setEnvioPendente($value)
	{
		$this->ped_status_envioPendente = (int) $value;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getEnvioPendente()
	{
		return (int) $this->ped_status_envioPendente;
	}
}