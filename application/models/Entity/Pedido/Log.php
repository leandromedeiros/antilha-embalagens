<?php

class App_Model_Entity_Pedido_Log extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var int */
	public $usuario;
	
	/** @var string */
	public $acao;
	
	/** @var string */
	public $data;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_Pedido
	 */
	protected $objPedido = null;
	
	/**
	 * @var App_Model_Entity_Sistema_Usuario
	 */
	protected $objUsuario = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objPedido', 'objUsuario'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Logs::getInstance());
	}
	
	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'ped_log_idPedido' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_log_idUsuario' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_log_acao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_log_data' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * @param App_Model_Entity_Pedido $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setPedido(App_Model_Entity_Pedido $value)
	{
		$this->objPedido = $value;
		$this->ped_log_idPedido = $value->getCodigo();
		return $this;
	}

	/**
	 * @return App_Model_Entity_Pedido
	 */
	public function getPedido()
	{
		if (null == $this->objPedido) {
			$this->objPedido = $this->findParentRow(App_Model_DAO_Pedidos::getInstance(), 'Pedido');
		}
		return $this->objPedido;
	}
	
	/**
	 * @param App_Model_Entity_Sistema_Usuario $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setUsuario(App_Model_Entity_Sistema_Usuario $value = null)
	{
		$this->objUsuario = $value;
		$this->ped_log_idUsuario = $value->getCodigo();
		return $this;
	}

	/**
	 * @return App_Model_Entity_Sistema_Usuario
	 */
	public function getUsuario()
	{
		if (null == $this->objUsuario) {
			$this->objUsuario = $this->findParentRow(App_Model_DAO_Sistema_Usuarios::getInstance(), 'Usuario');
		}
		return $this->objUsuario;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setAcao($value)
	{
		$this->ped_log_acao = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAcao()
	{
		return (string) $this->ped_log_acao;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setData($value)
	{
		$this->ped_log_data = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getData()
	{
		return (string) $this->ped_log_data;
	}
}