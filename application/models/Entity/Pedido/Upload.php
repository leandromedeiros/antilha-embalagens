<?php

class App_Model_Entity_Upload extends App_Model_Entity_Abstract
{

	function uploadXls(){
		// Pasta onde o arquivo vai ser salvo
		//$_UP['pasta'] = UPFILES;
        
        $_UP['pasta'] = '/home/antilhaspedidos/public_html/upload/'; 
		$_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
		$_UP['extensoes'] = array('xls', 'xlsx');
	
		// Array com os tipos de erros de upload do PHP
		$_UP['erros'][0] = 'Nao houve erro';
		$_UP['erros'][1] = 'O arquivo no upload e maior do que o limite do PHP';
		$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
		$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
		$_UP['erros'][4] = 'Nao foi feito o upload do arquivo';
	
		// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
		if ($_FILES['fileXls']['error'] != 0) {
			return '{"status":"Nao foi possível fazer o upload, Erro: ' . $_UP['pasta'] . $_UP['erros'][$_FILES['fileXls']['error']].'"}';
			
		}
	
		// Faz a verifica�‹o da extens‹ao do arquivo
		$extensao = strtolower(end(explode('.', $_FILES['fileXls']['name'])));
		if (array_search($extensao, $_UP['extensoes']) === false) {
			return '{"status":"Por favor, envie arquivos com as seguintes extensões: xls, xlsx"}';
		}
		else if ($_UP['tamanho'] < $_FILES['fileXls']['size']) {
			return '{"status":"O arquivo enviado é muito grande, envie arquivos de até 2Mb."}';
		}
	
		else {
			$nome_final = time().$_FILES['fileXls']['name'];
			if (move_uploaded_file($_FILES['fileXls']['tmp_name'], $_UP['pasta'] . $nome_final)) {
				$_SESSION['caminhoarquivo'] = $_UP['pasta'] . $nome_final;
				
					//CHECA OS DADOS DA PLANILHA E INSERE NO BANCO
					$arquivo = new ImportXls();
					$arquivo->read($_SESSION['caminhoarquivo']);
					
					if($arquivo->testeReplaceInsert() > 1){
						switch ($_GET['param']){
							case 'replace':
								return	$arquivo->replacePlanilha();
								break;
							case 'normal':
								return	$arquivo->inserir();
								break;
						}
						return '{"status":"Upload efetuado com sucesso!"}';
					}
					
			} else {
				return '{"status":"Não foi possível enviar o arquivo, tente novamente"}';
			}
		}	
		
		return true;
	}

 }




?>