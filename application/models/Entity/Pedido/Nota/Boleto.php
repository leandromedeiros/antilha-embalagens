<?php

class App_Model_Entity_Pedido_Nota_Boleto extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var string */
	public $codigo;
	
	/** @var string */
	public $nota;
	
	/** @var string */
	public $banco;
	
	/** @var string */
	public $docto;
	
	/** @var string */
	public $cliente;
	
	/** @var string */
	public $duplicata;
	
	/** @var float */
	public $valor;
	
	/** @var string */
	public $dataEmissao;
	
	/** @var string */
	public $dataVencimento;
	
	/** @var string */
	public $texto;
	
	/** @var int */
	public $dias;
	
	/** @var string */
	public $dataBase;
	
	/** @var string */
	public $tipoDoc;
	
	/** @var string */
	public $link_boleto_2v;
	
	/** @var string */
	public $caminhoPDF;
	
	/**** Fim Propriedades do WebService *************/
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep());
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Notas_Boletos::getInstance());
	}
	
	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'ped_nota_bol_idBoleto' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_bol_notaFiscal' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_banco' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_docto' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_cliente' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false				
			),
			'ped_nota_bol_duplicata' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_valor' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_dataEmissao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_dataVencimento' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_texto' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_dias' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_dataBase' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_bol_tipoDoc' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_link_boleto_2v' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_bol_caminhoPDF' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();
			
		} catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setCodigo($value)
	{
		$this->ped_nota_bol_idBoleto = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCodigo()
	{
		return (int) $this->ped_nota_bol_idBoleto;
	}	
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setNota($value)
	{
		$this->ped_nota_bol_notaFiscal = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNota()
	{
		return (string) $this->ped_nota_bol_notaFiscal;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setBanco($value)
	{
		$this->ped_nota_bol_banco = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getBanco()
	{
		return (string) $this->ped_nota_bol_banco;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setDocto($value)
	{
		$this->ped_nota_bol_docto = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDocto()
	{
		return (string) $this->ped_nota_bol_docto;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setCliente($value)
	{
		$this->ped_nota_bol_cliente = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCliente()
	{
		return (string) $this->ped_nota_bol_cliente;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setDuplicata($value)
	{
		$this->ped_nota_bol_duplicata = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDuplicata()
	{
		return (string) $this->ped_nota_bol_duplicata;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setValor($value)
	{
		$this->ped_nota_bol_valor = (float) $value;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getValor()
	{
		return (float) $this->ped_nota_bol_valor;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setDataEmissao($value)
	{
		$this->ped_nota_bol_dataEmissao = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDataEmissao()
	{
		return (string) $this->ped_nota_bol_dataEmissao;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setDataVencimento($value)
	{
		$this->ped_nota_bol_dataVencimento = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDataVencimento()
	{
		return (string) $this->ped_nota_bol_dataVencimento;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setTexto($value)
	{
		$this->ped_nota_bol_texto = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTexto()
	{
		return (string) $this->ped_nota_bol_texto;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setDias($value)
	{
		$this->ped_nota_bol_dias = (int) $value;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getDias()
	{
		return (int) $this->ped_nota_bol_dias;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setDataBase($value)
	{
		$this->ped_nota_bol_dataBase = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDataBase()
	{
		return (string) $this->ped_nota_bol_dataBase;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setTipoDoc($value)
	{
		$this->ped_nota_bol_tipoDoc = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTipoDoc()
	{
		return (string) $this->ped_nota_bol_tipoDoc;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setLinkBoleto2v($value)
	{
		$this->ped_nota_bol_link_boleto_2v = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getLinkBoleto2v()
	{
		return (string) $this->ped_nota_bol_link_boleto_2v;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Boleto
	 */
	public function setCaminhoPDF($value)
	{
		$this->ped_nota_bol_caminhoPDF = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCaminhoPDF()
	{
		return (string) $this->ped_nota_bol_caminhoPDF;
	}
}