<?php

class App_Model_Entity_Pedido_Nota_Status extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var string */
	public $nota;
	
	/** @var string */
	public $data;
	
	/** @var string */
	public $codOcorrencia;
	
	/** @var string */
	public $historico;
	
	/**** Fim Propriedades do WebService *************/
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep());
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Notas_Status::getInstance());
	}
	
	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'ped_nota_status_idNota' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_status_data' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_status_codOcorrencia' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_status_historico' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();
			
		} catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Status
	 */
	public function setNota($value)
	{
		$this->ped_nota_status_idNota = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNota()
	{
		return (string) $this->ped_nota_status_idNota;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Status
	 */
	public function setData($value)
	{
		$this->ped_nota_status_data = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getData()
	{
		return (string) $this->ped_nota_status_data;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Status
	 */
	public function setCodigoOcorrencia($value)
	{
		$this->ped_nota_status_codOcorrencia = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCodigoOcorrencia()
	{
		return (string) $this->ped_nota_status_codOcorrencia;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Status
	 */
	public function setHistorico($value)
	{
		$this->ped_nota_status_historico = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getHistorico()
	{
		return (string) $this->ped_nota_status_historico;
	}

}