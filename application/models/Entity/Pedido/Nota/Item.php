<?php

class App_Model_Entity_Pedido_Nota_Item extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var int */
	public $numeroFilial;
	
	/** @var float */
	public $quantidade;
	
	/** @var string */
	public $status;
	
	/** @var string */
	public $produto;
	
	/** @var float */
	public $valorTotal;
	
	/** @var string */
	public $descricaoStatus;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_Pedido_Nota
	 */
	protected $objNota = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objNota'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Notas_Itens::getInstance());
	}
	
	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'ped_nota_item_idItem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_item_idNota' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_nota_item_numeroFilial' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_item_quantidade' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_item_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true				
			),
			'ped_nota_item_produto' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_item_valorTotal' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_nota_item_descricaoStatus' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();
		} catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Log
	 */
	public function setCodigo($value)
	{
		$this->ped_nota_item_idItem = (int) $value;
		return $this;
	}
	
	/**
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->ped_nota_item_idItem;
	}	
	
	/**
	 * @param App_Model_Entity_Pedido_Nota $value
	 * @return App_Model_Entity_Pedido_Nota_Item
	 */
	public function setNota(App_Model_Entity_Pedido_Nota $value)
	{
		$this->objNota = $value;
		$this->ped_nota_item_idNota = $value->getCodigo();
		return $this;
	}

	/**
	 * @return App_Model_Entity_Pedido_Nota
	 */
	public function getNota()
	{
		if (null == $this->objNota && $this->getCodigo()) {
			$this->objNota = $this->findParentRow(App_Model_DAO_Pedidos_Notas::getInstance(), 'Nota');
		}
		return $this->objNota;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pedido_Nota_Item
	 */
	public function setNumeroFilial($value)
	{
		$this->ped_nota_item_numeroFilial = (int) $value;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getNumeroFilial()
	{
		return (int) $this->ped_nota_item_numeroFilial;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pedido_Nota_Item
	 */
	public function setQuantidade($value)
	{
		$this->ped_nota_item_quantidade = (float) $value;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getQuantidade()
	{
		return (float) $this->ped_nota_item_quantidade;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Item
	 */
	public function setStatus($value)
	{
		$this->ped_nota_item_status = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getStatus()
	{
		return (string) $this->ped_nota_item_status;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Item
	 */
	public function setProduto($value)
	{
		$this->ped_nota_item_produto = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getProduto()
	{
		return (string) $this->ped_nota_item_produto;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pedido_Nota_Item
	 */
	public function setValorTotal($value)
	{
		$this->ped_nota_item_valorTotal = (float) $value;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getValorTotal()
	{
		return (float) $this->ped_nota_item_valorTotal;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Nota_Item
	 */
	public function setDescricaoStatus($value)
	{
		$this->ped_nota_item_descricaoStatus = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDescricaoStatus()
	{
		return (string) $this->ped_nota_item_descricaoStatus;
	}
}