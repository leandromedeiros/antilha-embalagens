<?php

/**
 * Defini��o do objeto Tipo de Loja
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_TipoLoja extends App_Model_Entity_Abstract
{	
	/**** In�cio Propriedades do WebService *************/
	
	/** @var string */
	public $codigo = null;
	
	/** @var string */
	public $nome = null;
	
	/**** Fim Propriedades do WebService *************/
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_TiposLoja::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'tipo_loja_idTipo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			),
			'tipo_loja_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 45)
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * Define o c�digo do tipo de loja
	 * 
	 * @param string $value
	 * @return App_Model_Entity_TipoLoja
	 */
	public function setCodigo($value)
	{
		$this->tipo_loja_idTipo = (string) $value;
		return $this;
	}

	/**
	 * Recupera o c�digo identificador do Tipo de Loja
	 * 
	 * @return string
	 */
	public function getCodigo()
	{
		return (string) $this->tipo_loja_idTipo;
	}

	/**
	 * Define o nome do tipo de loja
	 * 
	 * @param string $value
	 * @return App_Model_Entity_TipoLoja
	 */
	public function setNome($value)
	{
		$this->tipo_loja_nome = (string) $value;
		return $this;
	}

	/**
	 * Recupera o nome do tipo de loja
	 * 
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->tipo_loja_nome;
	}
}