<?php

/**
 * Defini��o do objeto Frete
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Frete extends App_Model_Entity_Abstract
{	
    /*     * ** In�cio Propriedades do WebService ************ */

    /** @var int */
    public $codigo = null;

    /** @var string */
    public $tipo = null;

    /** @var string */
    public $descricao = null;

    /** @var int */
    public $prazo = null;

    /** @var string */
    public $grupoRede = null;

    /** @var string */
    public $rede = null;

    /** @var string */
    public $linha = null;

    /** @var string */
    public $produto = null;

    /** @var string */
    public $regiao = null;

    /** @var float */
    public $valor = null;

    /** @var float */
    public $pesoInicial = null;
    
    /** @var float */
    public $pesoFinal = null;

    /*     * ** Fim Propriedades do WebService ************ */

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Frete::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
        );
        
		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->frete_codigo;
    }
    
    /**
     * @param integer
	 * @return App_Model_Entity_Frete
	 */
	public function setCodigo($value)
	{
		$this->frete_codigo = $value;
		return $this;
	}
	
	/**
     * @param string
	 * @return App_Model_Entity_Frete
	 */
	public function setTipo($value)
	{
		$this->frete_tipo = $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTipo()
	{
		return $this->frete_tipo;
	}
	
	/**
	 * @return string
	 */
	public function getDescricao()
	{
		return (string) $this->frete_descricao;
	}

	/**
	 * @param string $value
	 * @return App_Model_Entity_Frete
	 */
	public function setDescricao($value)
	{
		$this->frete_descricao = (string) $value;
		return $this;
	}	
	
	/**
	 * @return int
	 */
	public function getPrazo()
	{
		return (int) $this->frete_prazo;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Frete
	 */
	public function setPrazo($value)
	{
		$this->frete_prazo = (int) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getGrupoRede()
	{
		return (string) $this->frete_grupoRede;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Frete
	 */
	public function setGrupoRede($value)
	{
		$this->frete_grupoRede = (string) $value;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getRede()
	{
		return (string) $this->frete_rede;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Frete
	 */
	public function setRede($value)
	{
		$this->frete_rede = (string) $value;
		return $this;
    }
    
    /**
	 * @return string
	 */
	public function getLinha()
	{
		return (string) $this->frete_linha;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Frete
	 */
	public function setLinha($value)
	{
		$this->frete_linha = (string) $value;
		return $this;
    }
    
    /**
	 * @return string
	 */
	public function getProduto()
	{
		return (string) $this->frete_produto;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Frete
	 */
	public function setProduto($value)
	{
		$this->frete_produto = (string) $value;
		return $this;
    }

    /**
	 * @return string
	 */
	public function getRegiao()
	{
		return (string) $this->frete_regiao;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Frete
	 */
	public function setRegiao($value)
	{
		$this->frete_regiao = (string) $value;
		return $this;
    }
    
    /**
	 * @return double
	 */
	public function getValor()
	{
		return (double) $this->frete_valor;
	}
	
	/**
	 * @param double $value
	 * @return App_Model_Entity_Frete
	 */
	public function setValor($value)
	{
		$this->frete_valor = (double) $value;
		return $this;
    }
    
    /**
	 * @return double
	 */
	public function getPesoInicial()
	{
		return (double) $this->frete_pesoInicial;
	}
	
	/**
	 * @param double $value
	 * @return App_Model_Entity_Frete
	 */
	public function setPesoInicial($value)
	{
		$this->frete_pesoInicial = (double) $value;
		return $this;
    }
    
     /**
	 * @return double
	 */
	public function getPesoFinal()
	{
		return (double) $this->frete_pesoFinal;
	}
	
	/**
	 * @param double $value
	 * @return App_Model_Entity_Frete
	 */
	public function setPesoFinal($value)
	{
		$this->frete_pesoFinal = (double) $value;
		return $this;
	}
}