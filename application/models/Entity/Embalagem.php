<?php

class App_Model_Entity_Embalagem extends App_Model_Entity_Abstract
{

    /**
     * @var App_Model_Entity_Galeria_Arquivo
     */
    protected $objImagem = null;

    /**
     * @var App_Model_Entity_Galeria_Arquivo
     */
    protected $objImagemDestaque = null;

    /**
     * @var App_Model_Entity_Galeria_Arquivo
     */
    protected $objMiniatura = null;

    /**
     * @var App_Model_Entity_Galeria_Arquivo
     */
    protected $objManual = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Embalagem_Permissao
     */
    protected $objPermissoes = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Embalagem_Estado
     */
    protected $objEstados = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objImagem', 'objImagemDestaque', 'objMiniatura', 'objManual', 'objPermissoes', 'objEstados'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Embalagens::getInstance());
    }

    public function save()
    {
        $this->emb_status = (int) $this->emb_status;

        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'emb_idEmbalagem' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'emb_idImagem' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'emb_idImagemDestaque' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'emb_idMiniatura' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'emb_idManual' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'emb_data' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'emb_nome' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'emb_destaque' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'emb_status' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        // persiste os dados no banco
        try {
            $this->getTable()->getAdapter()->beginTransaction();
            $trans = true;
        } catch (Exception $e) {
            $trans = false;
        }

        //persiste os dados no banco
        try {
            parent::save();

            // Permiss�es
            $daoPermissoes = App_Model_DAO_Embalagens_Permissoes::getInstance();
            $this->getPermissoes(); //for�a o carregamento das permiss�es
            $daoPermissoes->delete($daoPermissoes->getAdapter()->quoteInto('emb_perm_idEmbalagem = ?', $this->getCodigo()));
            foreach ($this->getPermissoes() as $permissao) {
                $daoPermissoes->insert(array(
                    'emb_perm_idEmbalagem' => $this->getCodigo(),
                    'emb_perm_idGrupoRede' => $permissao->getGrupoRede() != null ? $permissao->getGrupoRede()->getCodigo() : null,
                    'emb_perm_idRede' => $permissao->getRede() != null ? $permissao->getRede()->getCodigo() : null,
                    'emb_perm_idGrupoLoja' => $permissao->getGrupoLoja() != null ? $permissao->getGrupoLoja()->getCodigo() : null,
                    'emb_perm_idLoja' => $permissao->getLoja() != null ? $permissao->getLoja()->getCodigo() : null,
                    'emb_perm_idLinha' => $permissao->getLinha() != null ? $permissao->getLinha()->getCodigo() : null
                ));
            }
            unset($daoPermissoes);

            // Estados
            $daoEstados = App_Model_DAO_Embalagens_Estados::getInstance();
            $this->getEstados(); //for�a o carregamento das permiss�es
            $daoEstados->delete($daoEstados->getAdapter()->quoteInto('emb_est_idEmbalagem = ?', $this->getCodigo()));
            foreach ($this->getEstados() as $estado) {
                $daoEstados->insert(array(
                    'emb_est_idEmbalagem' => $this->getCodigo(),
                    'emb_est_uf' => $estado->getUf()
                ));
            }
            unset($daoEstados);

            if ($trans)
                $this->getTable()->getAdapter()->commit();
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            if ($trans)
                $this->getTable()->getAdapter()->rollBack();
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return integer
     */
    public function getCodigo()
    {
        return (int) $this->emb_idEmbalagem;
    }

    /**
     * @param App_Model_Entity_Galeria_Arquivo $value
     * @return App_Model_Entity_Embalagem
     */
    public function setImagem(App_Model_Entity_Galeria_Arquivo $value = null)
    {
        $this->objImagem = $value;
        $this->emb_idImagem = $value == null ? null : $value->getCodigo();
        return $this;
    }

    /**
     * @return App_Model_Entity_Galeria_Arquivo
     */
    public function getImagem()
    {
        if (null == $this->objImagem && $this->getCodigo()) {
            $this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
        }
        return $this->objImagem;
    }

    /**
     * @param App_Model_Entity_Galeria_Arquivo $value
     * @return App_Model_Entity_Embalagem
     */
    public function setImagemDestaque(App_Model_Entity_Galeria_Arquivo $value = null)
    {
        $this->objImagemDestaque = $value;
        $this->emb_idImagemDestaque = $value == null ? null : $value->getCodigo();
        return $this;
    }

    /**
     * @return App_Model_Entity_Galeria_Arquivo
     */
    public function getImagemDestaque()
    {
        if (null == $this->objImagemDestaque && $this->getCodigo()) {
            $this->objImagemDestaque = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'ImagemDestaque');
        }
        return $this->objImagemDestaque;
    }

    /**
     * @param App_Model_Entity_Galeria_Arquivo $value
     * @return App_Model_Entity_Embalagem
     */
    public function setMiniatura(App_Model_Entity_Galeria_Arquivo $value)
    {
        $this->objMiniatura = $value;
        $this->emb_idMiniatura = $value->getCodigo();
        return $this;
    }

    /**
     * @return App_Model_Entity_Galeria_Arquivo
     */
    public function getMiniatura()
    {
        if (null == $this->objMiniatura && $this->getCodigo()) {
            $this->objMiniatura = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Miniatura');
        }
        return $this->objMiniatura;
    }

    /**
     * @param App_Model_Entity_Galeria_Arquivo $value
     * @return App_Model_Entity_Embalagem
     */
    public function setManual(App_Model_Entity_Galeria_Arquivo $value)
    {
        $this->objManual = $value;
        $this->emb_idManual = $value->getCodigo();
        return $this;
    }

    /**
     * @return App_Model_Entity_Galeria_Arquivo
     */
    public function getManual()
    {
        if (null == $this->objManual && $this->getCodigo()) {
            $this->objManual = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Manual');
        }
        return $this->objManual;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Embalagem
     */
    public function setData($value)
    {
        $this->emb_data = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return (string) $this->emb_data;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Embalagem
     */
    public function setNome($value)
    {
        $this->emb_nome = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->emb_nome;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Embalagem
     */
    public function setDestaque($value)
    {
        $this->emb_destaque = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDestaque()
    {
        return (int) $this->emb_destaque;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Embalagem
     */
    public function setStatus($value)
    {
        $this->emb_status = (bool) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return (bool) $this->emb_status;
    }

    /**
     * @return App_Model_Collection of App_Model_Entity_Embalagem_Permissao
     */
    public function getPermissoes()
    {
        if (null == $this->objPermissoes) {
            if ($this->getCodigo()) {
                $this->objPermissoes = $this->findDependentRowset(App_Model_DAO_Embalagens_Permissoes::getInstance(), 'Embalagem');
                foreach ($this->objPermissoes as $permissao) {
                    $permissao->setEmbalagem($this);
                }
                $this->objPermissoes->rewind();
            } else {
                $this->objPermissoes = App_Model_DAO_Embalagens_Permissoes::getInstance()->createRowset();
            }
        }
        return $this->objPermissoes;
    }

    /**
     * @return App_Model_Collection of App_Model_Entity_Embalagem_Estado
     */
    public function getEstados()
    {
        if (null == $this->objEstados) {
            if ($this->getCodigo()) {
                $this->objEstados = $this->findDependentRowset(App_Model_DAO_Embalagens_Estados::getInstance(), 'Embalagem');
                foreach ($this->objEstados as $estado) {
                    $estado->setEmbalagem($this);
                }
                $this->objEstados->rewind();
            } else {
                $this->objEstados = App_Model_DAO_Embalagens_Estados::getInstance()->createRowset();
            }
        }
        return $this->objEstados;
    }

}
