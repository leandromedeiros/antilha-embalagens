<?php

/**
 * Defini��o do objeto Ouvidoria
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Ouvidoria extends App_Model_Entity_Abstract
{

    /**
     * @var App_Model_Entity_Usuario
     */
    protected $objUsuario = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objUsuario'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Ouvidoria::getInstance());
    }

    public function save()
    {
        if (!$this->getCodigo())
            $this->ouv_dataCadastro = date('Y-m-d H:i:s');

        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'ouv_idContato' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ouv_idUsuario' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'ouv_nome' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 80)
            ),
            'ouv_email' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 80)
            ),
            'ouv_mensagem' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
            )
        );

//verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

//persiste os dados no banco
        try {
            parent::save();
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Recupera o c�digo identificador da ouvidoria
     *
     * @return integer
     */
    public function getCodigo()
    {
        return (int) $this->ouv_idContato;
    }

    /**
     * @return string
     */
    public function getProtocolo()
    {
        return $this->ouv_protocolo;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Ouvidoria
     */
    public function setProtocolo($value = null)
    {
        $this->ouv_protocolo = $value;
        return $this;
    }

    /**
     * Recupera a data de cadastro
     *
     * @return string
     */
    public function getDataCadastro()
    {
        return (string) $this->ouv_dataCadastro;
    }

    /**
     * Define a data de cadastro
     *
     * @param string $value
     * @return App_Model_Entity_Ouvidoria
     */
    public function setDataCadastro($value)
    {
        $this->ouv_dataCadastro = (string) $value;
        return $this;
    }

    /**
     * Define o usu�rio
     *
     * @params App_Model_Entity_Ouvidoria $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setUsuario(App_Model_Entity_Sistema_Usuario $value)
    {
        $this->objUsuario = $value;
        $this->ouv_idUsuario = $value->getCodigo();
        return $this;
    }

    /**
     * Recupera o usu�rio
     *
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function getUsuario()
    {
        if (null == $this->objUsuario && $this->getCodigo()) {
            $this->objUsuario = $this->findParentRow(App_Model_DAO_Sistema_Usuarios::getInstance(), 'Usuario');
        }
        return $this->objUsuario;
    }

    /**
     * Define o nome
     *
     * @param string $value
     * @return App_Model_Entity_Ouvidoria
     */
    public function setNome($value)
    {
        $this->ouv_nome = (string) $value;
        return $this;
    }

    /**
     * Recupera o nome
     *
     * @return string
     */
    public function getNome()
    {
        return (string) $this->ouv_nome;
    }

    /**
     * Define o email
     *
     * @param string $value
     * @return App_Model_Entity_Ouvidoria
     */
    public function setEmail($value)
    {
        $this->ouv_email = (string) $value;
        return $this;
    }

    /**
     * Recupera o email
     *
     * @return string
     */
    public function getEmail()
    {
        return (string) $this->ouv_email;
    }

    /**
     * Define a mensagem
     *
     * @param string $value
     * @return App_Model_Entity_Ouvidoria
     */
    public function setMensagem($value)
    {
        $this->ouv_mensagem = (string) $value;
        return $this;
    }

    /**
     * Recupera a mensagem
     *
     * @return string
     */
    public function getMensagem()
    {
        return (string) $this->ouv_mensagem;
    }

}
