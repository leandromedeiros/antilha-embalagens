<?php

/**
 * Defini��o do objeto T�picos
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Topico extends App_Model_Entity_Abstract
{
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep());
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Topicos::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'top_idTopico' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'top_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 100)
			),
			'top_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Topico
	 */
	public function setCodigo($value)
	{
		$this->top_idTopico = (int) $value;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getCodigo()
	{
		return (int) $this->top_idTopico;
	}

	/**
	 * @param string $value
	 * @return App_Model_Entity_Topico
	 */
	public function setNome($value)
	{
		$this->top_nome = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->top_nome;
	}	
	
	/**
	 * Recupera o bloqueio de atraso da linha
	 * 
	 * @return bool
	 */
	public function getStatus()
	{
		return (bool) $this->top_status;
	}	
	
	/**
	 * @param bool $value
	 * @return App_Model_Entity_Topico
	 */
	public function setStatus($value)
	{
		$this->top_status = (bool) $value;
		return $this;
	}
}