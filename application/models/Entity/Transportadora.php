<?php

/**
 * Defini��o do objeto Transportadora
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Transportadora extends App_Model_Entity_Abstract
{	
	/**** In�cio Propriedades do WebService *************/
	
	/** @var int */
	public $codigo = null;
	
	/** @var string */
	public $nome = null;
	
	/**** Fim Propriedades do WebService *************/
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Transportadoras::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'trans_codigo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			),
			'trans_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 45)
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Transportadora
	 */
	public function setCodigo($value)
	{
		$this->trans_codigo = (int) $value;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getCodigo()
	{
		return (int) $this->trans_codigo;
	}

	/**
	 * @param string $value
	 * @return App_Model_Entity_Transportadora
	 */
	public function setNome($value)
	{
		$this->trans_nome = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->trans_nome;
	}
}