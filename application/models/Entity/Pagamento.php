<?php

/**
 * Defini��o do objeto Pagamento
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Pagamento extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var string */
	public $codigo = null;
	
	/** @var string */
	public $uf = null;
	
	/** @var string */
	public $classificacao = null;
	
	/** @var string */
	public $rede = null;
	
	/** @var string */
	public $linha = null;
	
	/** @var string */
	public $tipoLoja = null;
	
	/** @var float */
	public $fatMinimo = null;
	
	/** @var int */
	public $minPecasPedido = null;
	
	/** @var int */
	public $qtdMaxPedido = null;
	
	/** @var float */
	public $taxaServico = null;
	
	/** @var string */
	public $mensagem = null;
	
	/** @var float */
	public $faixaInicial = null;
	
	/** @var float */
	public $faixaFinal = null;
	
	/** @var float */
	public $juros = null;
	
	/** @var float */
	public $desconto = null;
	
	/** @var string */
	public $codigoPgto = null;
	
	/** @var string */
	public $validadeI = null;
	
	/** @var string */
	public $validadeF = null;
	
	/**** Fim Propriedades do WebService *************/
	
	/**
	 * @var App_Model_Entity_Linha
	 */
	protected $objLinha = null;
	
	/**
	 * @var App_Model_Entity_Rede
	 */
	protected $objRede = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objLinha', 'objRede'));
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pagamentos::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'pag_idPagamento' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'pag_uf' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true,
				new Zend_Validate_StringLength(1, 2)
			),
			'pag_classificacao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true,
				new Zend_Validate_StringLength(1)
			),
			'pag_rede' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 6)
			),
			'pag_linha' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true,
				new Zend_Validate_StringLength(1, 6)
			),
			'pag_tipoLoja' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true,
				new Zend_Validate_StringLength(1, 2)
			),
			'pag_fatMinimo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'pag_fatMinimo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'pag_minPecasPedido' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'pag_qtdMaxPedido' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'pag_taxaServico' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'pag_mensagem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 255)
			),
			'pag_faixaInicial' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'pag_faixaFinal' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'pag_juros' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'pag_desconto' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'pag_codigo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			),
			'pag_validadeI' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'pag_validadeF' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);
		
		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * Define o codigo identificador da condi��o
	 * 
	 * @param int $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setCodigo($value)
	{
		$this->pag_idPagamento = (int) $value;
		return $this;
	}

	/**
	 * Recupera o c�digo identificador da condi��o
	 * 
	 * @return int
	 */
	public function getCodigo()
	{
		return (int) $this->pag_idPagamento;
	}

	/**
	 * @param App_Model_Entity_Rede $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setRede(App_Model_Entity_Rede $value = null)
	{
		$this->objRede = $value;
		$this->pag_rede = $value != null ? $value->getCodigo() : '';
		return $this;
	}

	/**
	 * @return App_Model_Entity_Rede
	 */
	public function getRede()
	{
		if (null == $this->objRede && $this->getCodigo()) {
			$this->objRede = $this->findParentRow(App_Model_DAO_Redes::getInstance(), 'Rede');
		}
		return $this->objRede;
	}
	
	/**
	 * @param App_Model_Entity_Linha $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setLinha(App_Model_Entity_Linha $value = null)
	{
		$this->objLinha = $value;
		$this->pag_linha = $value != null ? $value->getCodigo() : null;
		return $this;
	}

	/**
	 * @return App_Model_Entity_Linha
	 */
	public function getLinha()
	{
		if (null == $this->objLinha && $this->getCodigo()) {
			$this->objLinha = $this->findParentRow(App_Model_DAO_Linhas::getInstance(), 'Linha');
		}
		return $this->objLinha;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setUF($value)
	{
		$this->pag_uf = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUF()
	{
		return (string) $this->pag_uf;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setClassificacao($value)
	{
		$this->pag_classificacao = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getClassificacao()
	{
		return (string) $this->pag_classificacao;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setTipoLoja($value)
	{
		$this->pag_tipoLoja = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTipoLoja()
	{
		return (string) $this->pag_tipoLoja;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setFatMinimo($value)
	{
		$this->pag_fatMinimo = (float) $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getFatMinimo()
	{
		return (float) $this->pag_fatMinimo;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setMinPecasPedido($value)
	{
		$this->pag_minPecasPedido = (int) $value;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getMinPecasPedido()
	{
		return (int) $this->pag_minPecasPedido;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setQtdMaxPedido($value)
	{
		$this->pag_qtdMaxPedido = (int) $value;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getQtdMaxPedido()
	{
		return (int) $this->pag_qtdMaxPedido;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setTaxaServico($value)
	{
		$this->pag_taxaServico = (float) $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getTaxaServico()
	{
		return (float) $this->pag_taxaServico;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setMensagem($value)
	{
		$this->pag_mensagem = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMensagem()
	{
		return (string) $this->pag_mensagem;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setFaixaInicial($value)
	{
		$this->pag_faixaInicial = (float) $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getFaixaInicial()
	{
		return (float) $this->pag_faixaInicial;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setFaixaFinal($value)
	{
		$this->pag_faixaFinal = (float) $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getFaixaFinal()
	{
		return (float) $this->pag_faixaFinal;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setJuros($value)
	{
		$this->pag_juros = (float) $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getJuros()
	{
		return (float) $this->pag_juros;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setDesconto($value)
	{
		$this->pag_desconto = (float) $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getDesconto()
	{
		return (float) $this->pag_desconto;
	}
	
	/**
	 * @param float $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setCodigoPgto($value)
	{
		$this->pag_codigo = (float) $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getCodigoPgto()
	{
		return (float) $this->pag_codigo;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setValidadeI($value)
	{
		$this->pag_validadeI = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getValidadeI()
	{
		return (string) $this->pag_validadeI;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Pagamento
	 */
	public function setValidadeF($value)
	{
		$this->pag_validadeF = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getValidadeF()
	{
		return (string) $this->pag_validadeF;
	}
}