<?php

/**
 * Defini��o do objeto Linha
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Linha extends App_Model_Entity_Abstract
{
    /*     * ** In�cio Propriedades do WebService ************ */

    /** @var string */
    public $codigo = null;

    /** @var string */
    public $rede = null;

    /**
     * Exemplo: DD/MM/YYYY
     * @var string
     * */
    public $dataVitrine = null;

    /** @var string */
    public $nome = null;

    /** @var int */
    public $bloqueioAtraso = null;

    /** @var int */
    public $compraUnica = null;

    /** @var int */
    public $status = 1;

    /** @var int */
    public $vm = null;

    /*     * ** Fim Propriedades do WebService ************ */

    /**
     * @var App_Model_Entity_Rede
     */
    protected $objRede = null;

    /**
     * @var App_Model_Entity_Galeria_Arquivo
     */
    protected $objImagem = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Produto
     */
    protected $objProdutos = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objRede', 'objImagem', 'objProdutos'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Linhas::getInstance());
    }

    public function save()
    {
        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'lin_idLinha' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 6)
            ),
            'lin_idRede' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 6)
            ),
            'lin_idImagem' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'lin_dataVitrine' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'lin_nome' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 45)
            ),
            'lin_bloqueioAtraso' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'lin_compraUnica' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'lin_status' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        //persiste os dados no banco
        try {
            parent::save();
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Define o codigo identificador da linha
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setCodigo($value)
    {
        $this->lin_idLinha = (string) $value;
        return $this;
    }

    /**
     * Recupera o c�digo identificador da linha
     *
     * @return string
     */
    public function getCodigo()
    {
        return (string) $this->lin_idLinha;
    }

    /**
     * Define a imagem a qual a linha pertence
     *
     * @param App_Model_Entity_Galeria_Arquivo $value
     * @return App_Model_Entity_Linha
     */
    public function setImagem(App_Model_Entity_Galeria_Arquivo $value = null)
    {
        $this->objImagem = $value;
        $this->lin_idImagem = $value != null ? $value->getCodigo() : '';
        return $this;
    }

    /**
     * Recupera a imagem a qual a linha pertence
     *
     * @return App_Model_Entity_Galeria_Arquivo
     */
    public function getImagem()
    {
        if (null == $this->objImagem && $this->getCodigo()) {
            $this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
        }
        return $this->objImagem;
    }


    public function setCodigoRede($value){
        $this->lin_idRede = (string) $value;
        return $this;
    }

    public function getCodigoRede() {
        return (string) $this->lin_idRede;
    }

    /**
     * Define a data de vitrine da linha
     *
     * @param string $value
     * @return App_Model_Entity_Rede_Linha
     */
    public function setDataVitrine($value)
    {
        $this->lin_dataVitrine = (string) $value;
        return $this;
    }

    /**
     * Recupera a data de vitrine da linha
     *
     * @return string
     */
    public function getDataVitrine()
    {
        return (string) $this->lin_dataVitrine;
    }

    /**
     * Define a rede a qual a linha pertence
     *
     * @param App_Model_Entity_Rede $value
     * @return App_Model_Entity_Linha
     */
    public function setRede(App_Model_Entity_Rede $value)
    {
        $this->objRede = $value;
        $this->lin_idRede = $value->getCodigo();
        return $this;
    }

    /**
     * Recupera a rede a qual a linha pertence
     *
     * @return App_Model_Entity_Linha
     */
    public function getRede()
    {
        if (null == $this->objRede && $this->getCodigo()) {
            $this->objRede = $this->findParentRow(App_Model_DAO_Redes::getInstance(), 'Rede');
        }
        return $this->objRede;
    }

    /**
     * Define o nome da linha
     *
     * @param string $value
     * @return App_Model_Entity_Rede_Linha
     */
    public function setNome($value)
    {
        $this->lin_nome = (string) $value;
        return $this;
    }

    /**
     * Recupera o nome da linha
     *
     * @return string
     */
    public function getNome()
    {
        return (string) $this->lin_nome;
    }

    /**
     * Recupera o bloqueio de atraso da linha
     *
     * @return int
     */
    public function getBloqueioAtraso()
    {
        return (int) $this->lin_bloqueioAtraso;
    }

    /**
     * Define o bloqueio de atraso da linha
     *
     * @param int $value
     * @return App_Model_Entity_Rede_Linha
     */
    public function setBloqueioAtraso($value)
    {
        $this->lin_bloqueioAtraso = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompraUnica()
    {
        return (int) $this->lin_compraUnica;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Rede_Linha
     */
    public function setCompraUnica($value)
    {
        $this->lin_compraUnica = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return (int) $this->lin_status;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Rede_Linha
     */
    public function setStatus($value = 1)
    {
        $this->lin_status = (int) $value;
        return $this;
    }

    public function getProdutos($loja = false, $forcar = false)
    {
        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        if ($this->objProdutos == null || $forcar) {
            $select = $daoProdutos->select()->from($daoProdutos)
                ->where('prod_idLinha = ?', $this->getCodigo())
                ->where('prod_status = ?', 1)
                ->order('prod_sequenciaItens');

            if ($loja) {
                $endereco = $loja->getEnderecos()->find('loja_end_padrao', 1);
                $UFLoja = $endereco->getEndereco()->getUf();

                $daoBloqueios = App_Model_DAO_Produtos_Bloqueios::getInstance();
                $selectBloqueio = $daoBloqueios->select()
                    ->from($daoBloqueios, 'count(1)')
                    ->where('NOW() BETWEEN prod_bloq_dataInicial AND prod_bloq_dataFinal OR (ISNULL(prod_bloq_dataInicial) AND ISNULL(prod_bloq_dataFinal))')
                    ->where('prod_bloq_uf = ? OR ISNULL(prod_bloq_uf)', $UFLoja)
                    ->where('prod_bloq_idGrupoLoja = ? OR ISNULL(prod_bloq_idGrupoLoja)', $loja->getGrupo()->getCodigo())
                    ->where('prod_bloq_idLoja = ? OR ISNULL(prod_bloq_idLoja)', $loja->getCodigo())
                    ->where('prod_bloq_idProduto = prod_idProduto');

                $select->columns(array(
                    '*',
                    'bloqueado' => "({$selectBloqueio})"
                ));
            }
            $this->objProdutos = $daoProdutos->fetchAll($select);
        }
        return $this->objProdutos;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Rede_Linha
     */
    public function setVm($value)
    {
        $this->lin_vm = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getVm()
    {
        return (int) $this->lin_vm;
    }

}
