<?php

/**
 * Defini��o do objeto Carrinho
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Carrinho extends App_Model_Entity_Abstract
{

    /**
     * @var App_Model_Entity_Linha
     */
    protected $objLinha = null;

    /**
     * @var App_Model_Entity_Sistema_Usuario
     */
    protected $objUsuarioAntilhas = null;

    /**
     * @var App_Model_Entity_Sistema_Usuario
     */
    protected $objUsuario = null;

    /**
     * @var App_Model_Entity_Loja
     */
    protected $objLoja = null;

    /**
     * @var App_Model_Entity_Pedido
     */
    protected $objPedido = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Carrinho_Item
     */
    protected $objItems = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objLoja', 'objUsuarioAntilhas', 'objUsuario', 'objItems', 'objLinha', 'objPedido'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Carrinho::getInstance());
    }

    public function save()
    {
        $this->car_grade = (int) $this->car_grade;

        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'car_idCarrinho' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'car_idLinha' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'car_idUsuario' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'car_idUsuarioAntilhas' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'car_idLoja' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 10)
            ),
            'car_idPedido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'car_grade' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'car_seuNumero' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        // persiste os dados no banco
        try {
            $this->getTable()->getAdapter()->beginTransaction();
            $trans = true;
        } catch (Exception $e) {
            $trans = false;
        }

        try {
            if (!$this->getCodigo())
                $this->car_dataCadastro = date('Y-m-d');
            parent::save();

            // Itens
            foreach ($this->getItens() as $item) {
                $item->setCarrinho($this);
                $item->save();
            }

            if ($trans)
                $this->getTable()->getAdapter()->commit();
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            if ($trans)
                $this->getTable()->getAdapter()->rollBack();
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return integer
     */
    public function getCodigo()
    {
        return (int) $this->car_idCarrinho;
    }

    /**
     * Recupera a data de cadastro
     *
     * @return string
     */
    public function getDataCadastro()
    {
        return (int) $this->car_dataCadastro;
    }

    /**
     * Define a linha
     *
     * @param App_Model_Entity_Linha $value
     * @return App_Model_Entity_Carrinho
     */
    public function setLinha(App_Model_Entity_Linha $value = null)
    {
        $this->objLinha = $value;
        $this->car_idLinha = $value ? (string) $value->getCodigo() : null;
        return $this;
    }

    /**
     * Recupera a linha
     *
     * @return App_Model_Entity_Carrinho
     */
    public function getLinha()
    {
        if (null == $this->objLinha) {
            $this->objLinha = $this->findParentRow(App_Model_DAO_Linhas::getInstance(), 'Linha');
        }
        return $this->objLinha;
    }

    /**
     * Define o usu�rio
     *
     * @param App_Model_Entity_Sistema_Usuario $value
     * @return App_Model_Entity_Carrinho
     */
    public function setUsuario(App_Model_Entity_Sistema_Usuario $value)
    {
        $this->objUsuario = $value;
        $this->car_idUsuario = (int) $value->getCodigo();
        return $this;
    }

    /**
     * Recupera o usu�rio
     *
     * @return App_Model_Entity_Carrinho
     */
    public function getUsuario()
    {
        if (null == $this->objUsuario) {
            $this->objUsuario = $this->findParentRow(App_Model_DAO_Sistema_Usuarios::getInstance(), 'Usuario');
        }
        return $this->objUsuario;
    }

    /**
     * Define o usu�rio antilhas
     *
     * @param App_Model_Entity_Sistema_Usuario $value
     * @return App_Model_Entity_Carrinho
     */
    public function setUsuarioAntilhas(App_Model_Entity_Sistema_Usuario $value = null)
    {
        $this->objUsuario = $value;
        $this->car_idUsuarioAntilhas = $value != null ? $value->getCodigo() : null;
        return $this;
    }

    /**
     * Recupera o usu�rio antilhas
     *
     * @return App_Model_Entity_Carrinho
     */
    public function getUsuarioAntilhas()
    {
        if (null == $this->objUsuarioAntilhas) {
            $this->objUsuarioAntilhas = $this->findParentRow(App_Model_DAO_Sistema_Usuarios::getInstance(), 'UsuarioAntilhas');
        }
        return $this->objUsuarioAntilhas;
    }

    /**
     * Define a loja
     *
     * @param App_Model_Entity_Loja $value
     * @return App_Model_Entity_Carrinho
     */
    public function setLoja(App_Model_Entity_Loja $value)
    {
        $this->objLoja = $value;
        $this->car_idLoja = (string) $value->getCodigo();
        return $this;
    }

    /**
     * Recupera a loja
     *
     * @return App_Model_Entity_Loja
     */
    public function getLoja()
    {
        if (null == $this->objLoja) {
            $this->objLoja = $this->findParentRow(App_Model_DAO_Lojas::getInstance(), 'Loja');
        }
        return $this->objLoja;
    }

    /**
     * Define o pedido relacionado
     *
     * @param App_Model_Entity_Pedido $value
     * @return App_Model_Entity_Carrinho
     */
    public function setPedido(App_Model_Entity_Pedido $value = null)
    {
        $this->objPedido = $value;
        $this->car_idPedido = $value != null ? (int) $value->getCodigo() : null;
        return $this;
    }

    /**
     * Recupera o pedido
     *
     * @return App_Model_Entity_Carrinho
     */
    public function getPedido()
    {
        if (null == $this->objPedido) {
            $this->objPedido = $this->findParentRow(App_Model_DAO_Pedidos::getInstance(), 'Pedido');
        }
        return $this->objPedido;
    }

    /**
     * Define o status
     *
     * @param int $value
     * @return App_Model_Entity_Carrinho
     */
    public function setStatus($value)
    {
        $this->car_status = (int) $value;
        return $this;
    }

    /**
     * Recupera o status
     *
     * @return App_Model_Entity_Carrinho
     */
    public function getStatus()
    {
        return (int) $this->car_status;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Carrinho
     */
    public function setGrade($value)
    {
        $this->car_grade = (int) $value;
        return $this;
    }

    /**
     * Recupera o status
     *
     * @return App_Model_Entity_Carrinho
     */
    public function getGrade()
    {
        return (int) $this->car_grade;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Carrinho
     */
    public function setAviso1Dia($value)
    {
        $this->car_aviso1Dia = (int) $value;
        return $this;
    }

    /**
     * Recupera o aviso de 1 dia
     *
     * @return App_Model_Entity_Carrinho
     */
    public function getAviso1Dia()
    {
        return (int) $this->car_aviso1Dia;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Carrinho
     */
    public function setAviso7Dias($value)
    {
        $this->car_aviso7Dias = (int) $value;
        return $this;
    }

    /**
     * Recupera o aviso de 7 dias
     *
     * @return App_Model_Entity_Carrinho
     */
    public function getAviso7Dias()
    {
        return (int) $this->car_aviso7Dias;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Carrinho
     */
    public function setSeuNumero($value)
    {
        $this->car_seuNumero = (string) $value;
        return $this;
    }

    /**
     * @return App_Model_Entity_Carrinho
     */
    public function getSeuNumero()
    {
        return (string) $this->car_seuNumero;
    }

    /**
     * Recupera os items do pedido
     *
     * @return App_Model_Entity_Pedido_Items
     */
    public function getItens()
    {
        if (null == $this->objItems) {
            if ($this->getCodigo()) {
                $this->objItems = $this->findDependentRowset(App_Model_DAO_Carrinho_Itens::getInstance(), 'Carrinho');
                foreach ($this->objItems as $item) {
                    $item->setCarrinho($this);
                }
                $this->objItems->rewind();
            } else {
                $this->objItems = App_Model_DAO_Carrinho_Itens::getInstance()->createRowset();
            }
        }
        return $this->objItems;
    }

    public function setPagamento($value)
    {

    }

    public function getQtde()
    {
        $qtde = 0;
        foreach ($this->getItens() as $item)
            $qtde += $item->getQtde();
        return $qtde;
    }

    public function getValor($imposto = true)
    {
        $valorTotal = 0;
        foreach ($this->getItens() as $item) {
            if ($imposto) {
                $valorTotal += App_Funcoes_Calculo::calculo($item->getQtde() * $item->getPreco(), $item->getProduto()->getCcf(), $this->getLoja()->getIcms(), $item->getProduto()->getAliquotaIpi());
            } else {
                $valorTotal += $item->getQtde() * $item->getPreco();
            }
        }

        return $valorTotal;
    }

    public function getPeso()
    {
        $valor = 0;
        foreach ($this->getItens() as $item)
            $valor += $item->getProduto()->getPesoPacotes();
        return $valor;
    }

}
