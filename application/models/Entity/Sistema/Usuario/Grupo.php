<?php
class App_Model_Entity_Sistema_Usuario_Grupo
{
	protected $id = null;
	
	protected $nome = null;
	
	protected $tipo = null;
	
	public function setCodigo($value) {
		$this->id = $value;
		return $this;
	}
	
	public function setNome($value) {
		$this->nome = $value;
		return $this;
	}
	
	public function setTipo($value) {
		$this->tipo = $value;
		return $this;
	}
	
	public function getCodigo() {
		return $this->id;
	}
	
	public function getNome() {
		return $this->nome;
	}
	
	public function getTipo() {
		return $this->tipo;
	}
}