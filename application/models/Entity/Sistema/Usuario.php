<?php

/**
 * Defini��o do objeto Usu�rio
 *
 * @category 	Model
 * @package 	Model_Entity
 * @subpackage 	Sistema
 */
class App_Model_Entity_Sistema_Usuario extends App_Model_Entity_Abstract
{

    const PodeCriarPedidosSemAprovacao = 126;
    const PodeCriarPedidosComAprovacao = 137;
    const PodeAprovarPedidosSemQuantidade = 127;
    const PodeAprovarPedidosComQuantidade = 128;
    const PodeAprovarPedidosIntermediario = 155;

    /**
     * @var App_Model_Entity_Sistema_Perfil
     */
    protected $objPerfil = null;

    /**
     * @var App_Model_Entity_GrupoRede
     */
    protected $objGrupoRede = null;

    /**
     * @var App_Model_Entity_Rede
     */
    protected $objRede = null;

    /**
     * @var App_Model_Entity_GrupoLoja
     */
    protected $objGrupoLoja = null;

    /**
     * @var App_Model_Entity_Loja
     */
    protected $objLoja = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Sistema_Acao
     */
    protected $objPermissoes = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objPerfil', 'objGrupoRede', 'objRede', 'objGrupoLoja', 'objLoja'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Sistema_Usuarios::getInstance());
    }

    public function save()
    {
        $this->usr_status = (int) $this->usr_status;

        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'usr_idUsuario' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'usr_idPerfil' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'usr_idGrupoRede' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'usr_idRede' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'usr_idGrupoLoja' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'usr_idLoja' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'usr_login' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new App_Validate_Usuario_Login($this),
                new Zend_Validate_StringLength(1, 15)
            ),
            'usr_senha' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 32)
            ),
            'usr_email' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 60),
                new Zend_Validate_EmailAddress(),
            ),
            'usr_dataAtualizacao' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'usr_status' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'usr_avisoOuvidoria' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            )
        );

//verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        try {
            parent::save();

            $daoPermissoes = App_Model_DAO_Sistema_Permissoes::getInstance();
            $this->getPermissoes(); //for�a o carregamento das permiss�es
            $daoPermissoes->delete($daoPermissoes->getAdapter()->quoteInto('perm_idUsuario = ?', $this->getCodigo()));
            foreach ($this->getPermissoes() as $acao) {
                $daoPermissoes->insert(array(
                    'perm_idUsuario' => $this->getCodigo(),
                    'perm_idAcao' => $acao->getCodigo()
                ));
            }
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            if ($trans)
                $this->getTable()->getAdapter()->rollBack();
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Recupera o c�digo identificador do usu�rio
     *
     * @return integer
     */
    public function getCodigo()
    {
        return (int) $this->usr_idUsuario;
    }

    /**
     * Define o perfil a qual o usu�rio pertence
     *
     * @params App_Model_Entity_Sistema_Perfil $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setPerfil(App_Model_Entity_Sistema_Perfil $value)
    {
        $this->objPerfil = $value;
        $this->usr_idPerfil = $value->getCodigo();
        return $this;
    }

    /**
     * Recupera o perfil a qual o usu�rio pertence
     *
     * @return App_Model_Entity_Sistema_Perfil
     */
    public function getPerfil()
    {
        if (null === $this->objPerfil && $this->getCodigo()) {
            $this->objPerfil = $this->findParentRow(App_Model_DAO_Sistema_Perfis::getInstance(), 'Perfil');
        }
        return $this->objPerfil;
    }

    /**
     * @params App_Model_Entity_GrupoRede $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setGrupoRede(App_Model_Entity_GrupoRede $value = null)
    {
        $this->objGrupoRede = $value;
        $this->usr_idGrupoRede = $value != null ? $value->getCodigo() : null;
        return $this;
    }

    /**
     * @return App_Model_Entity_GrupoRede
     */
    public function getGrupoRede()
    {
        if (null === $this->objGrupoRede && $this->getCodigo()) {
            $this->objGrupoRede = $this->findParentRow(App_Model_DAO_GrupoRedes::getInstance(), 'GrupoRede');
        }
        return $this->objGrupoRede;
    }

    /**
     * @params App_Model_Entity_Rede $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setRede(App_Model_Entity_Rede $value = null)
    {
        $this->objRede = $value;
        $this->usr_idRede = $value != null ? $value->getCodigo() : null;
        return $this;
    }

    /**
     * @return App_Model_Entity_Rede
     */
    public function getRede()
    {
        if (null === $this->objRede && $this->getCodigo()) {
            $this->objRede = $this->findParentRow(App_Model_DAO_Redes::getInstance(), 'Rede');
        }
        return $this->objRede;
    }

    /**
     * @params App_Model_Entity_GrupoLoja $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setGrupoLoja(App_Model_Entity_GrupoLoja $value = null)
    {
        $this->objGrupoLoja = $value;
        $this->usr_idGrupoLoja = $value != null ? $value->getCodigo() : null;
        return $this;
    }

    /**
     * @return App_Model_Entity_GrupoLoja
     */
    public function getGrupoLoja()
    {
        if (null === $this->objGrupoLoja && $this->getCodigo()) {
            $this->objGrupoLoja = $this->findParentRow(App_Model_DAO_GrupoLojas::getInstance(), 'GrupoLoja');
        }
        return $this->objGrupoLoja;
    }

    /**
     * @params App_Model_Entity_Loja $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setLoja(App_Model_Entity_Loja $value = null)
    {
        $this->objLoja = $value;
        $this->usr_idLoja = $value != null ? $value->getCodigo() : null;
        return $this;
    }

    /**
     * @return App_Model_Entity_Loja
     */
    public function getLoja()
    {
        if (null === $this->objLoja && $this->getCodigo()) {
            $this->objLoja = $this->findParentRow(App_Model_DAO_Lojas::getInstance(), 'Loja');
        }
        return $this->objLoja;
    }

    /**
     * Define o login de acesso do usu�rio
     *
     * @param string $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setLogin($value)
    {
        $this->usr_login = (string) $value;
        return $this;
    }

    /**
     * Recupera o login de acesso do usu�rio
     *
     * @return string
     */
    public function getLogin()
    {
        return (string) $this->usr_login;
    }

    /**
     * Define a senha de acesso do usu�rio
     *
     * @param string $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setSenha($value)
    {
        $this->usr_senha = (string) $value;
        return $this;
    }

    /**
     * Recupera a senha de acesso do usu�rio
     *
     * @return string
     */
    public function getSenha()
    {
        return (string) $this->usr_senha;
    }

    /**
     * Define o nome do usu�rio
     *
     * @param string $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setNome($value)
    {
        $this->usr_nome = (string) $value;
        return $this;
    }

    /**
     * Recupera o nome do usu�rio
     *
     * @return string
     */
    public function getNome()
    {
        return (string) $this->usr_nome;
    }

    /**
     * Define o e-mail do usu�rio
     *
     * @param string $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setEmail($value)
    {
        $this->usr_email = (string) $value;
        return $this;
    }

    /**
     * Recupera o e-mail do usu�rio
     *
     * @return string
     */
    public function getEmail()
    {
        return (string) $this->usr_email;
    }

    /**
     * Define data de atualiza��o de cadastro
     *
     * @param string $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setDataAtualizacao($value)
    {
        $this->usr_dataAtualizacao = $value;
        return $this;
    }

    /**
     * Recupera data de atualiza��o de cadastro
     *
     * @return string
     */
    public function getDataAtualizacao()
    {
        return $this->usr_dataAtualizacao;
    }

    /**
     * Define se o usu�rio est� ativo ou n�o
     *
     * @param boolean $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setStatus($value)
    {
        $this->usr_status = (bool) $value;
    }

    /**
     * Recupera se o usu�rio est� ativo ou n�o
     * @return boolean
     */
    public function getStatus()
    {
        return (bool) $this->usr_status;
    }

    /**
     * @param boolean $value
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function setAvisoOuvidoria($value)
    {
        $this->usr_avisoOuvidoria = $value;
    }

    /**
     * @return boolean
     */
    public function getAvisoOuvidoria()
    {
        return $this->usr_avisoOuvidoria;
    }

    /**
     * Recupera as permiss�es do usu�rio
     *
     * @return App_Model_Collection of App_Model_Entity_Sistema_Acao
     */
    public function getPermissoes()
    {
        if (null === $this->objPermissoes) {
            if ($this->getCodigo()) {
                $this->objPermissoes = $this->findManyToManyRowset(
                    App_Model_DAO_Sistema_Acoes::getInstance(), App_Model_DAO_Sistema_Permissoes::getInstance(), 'Usuario', 'Acao'
                );
            } else {
                $this->objPermissoes = App_Model_DAO_Sistema_Acoes::getInstance()->createRowset();
            }
        }
        return $this->objPermissoes;
    }

    /**
     * Verifica se o usu�rio pode criar pedidos sem aprova��o
     *
     * @return Bool
     */
    public function podeCriarPedidosSemAprovacao()
    {
        $retorno = false;
        if ($this->getGrupo()->getTipo() == 'LO' || $this->getGrupo()->getTipo() == 'GL') {
            $listaPermissoes = $this->getPermissoes();
            foreach ($listaPermissoes as $permissao) {
                if ($permissao->getCodigo() == App_Model_Entity_Sistema_Usuario::PodeCriarPedidosSemAprovacao) {
                    $retorno = true;
                    break;
                }
            }
        }
        return $retorno;
    }

    /**
     * Verifica se o usu�rio pode criar pedidos com aprova��o
     *
     * @return Bool
     */
    public function podeCriarPedidosComAprovacao()
    {
        $retorno = false;
        if ($this->getGrupo()->getTipo() == 'LO' || $this->getGrupo()->getTipo() == 'GL') {
            $listaPermissoes = $this->getPermissoes();

            foreach ($listaPermissoes as $permissao) {
                if ($permissao->getCodigo() == App_Model_Entity_Sistema_Usuario::PodeCriarPedidosComAprovacao) {
                    $retorno = true;
                    break;
                }
            }
        }

        return $retorno;
    }

    /**
     * Verifica se o usu�rio pode aprovar pedidos sem alterar quantidade
     *
     * @return Bool
     */
    public function podeAprovarPedidosSemQuantidade()
    {
        $retorno = false;
        if ($this->getGrupo()->getTipo() == 'LO' || $this->getGrupo()->getTipo() == 'GL') {
            $listaPermissoes = $this->getPermissoes();

            foreach ($listaPermissoes as $permissao) {
                if ($permissao->getCodigo() == App_Model_Entity_Sistema_Usuario::PodeAprovarPedidosSemQuantidade) {
                    $retorno = true;
                    break;
                }
            }
        }

        return $retorno;
    }

    /**
     * Verifica se o usu�rio pode aprovar pedidos e alterar as quantidades
     *
     * @return Bool
     */
    public function podeAprovarPedidosComQuantidade()
    {
        $retorno = false;
        if ($this->getGrupo()->getTipo() == 'LO' || $this->getGrupo()->getTipo() == 'GL') {
            $listaPermissoes = $this->getPermissoes();

            foreach ($listaPermissoes as $permissao) {
                if ($permissao->getCodigo() == App_Model_Entity_Sistema_Usuario::PodeAprovarPedidosComQuantidade) {
                    $retorno = true;
                    break;
                }
            }
        }

        return $retorno;
    }

    /**
     * Verifica se o usu�rio pode aprovar pedidos intermedi�rio e alterar as quantidades
     *
     * @return Bool
     */
    public function podeAprovarPedidosIntermediario()
    {
        $retorno = false;
        if ($this->getGrupo()->getTipo() == 'LO' || $this->getGrupo()->getTipo() == 'GL') {
            $listaPermissoes = $this->getPermissoes();

            foreach ($listaPermissoes as $permissao) {
                if ($permissao->getCodigo() == App_Model_Entity_Sistema_Usuario::PodeAprovarPedidosIntermediario) {
                    $retorno = true;
                    break;
                }
            }
        }

        return $retorno;
    }

    /**
     * Verifica se o usu�rio pode visualizar relat�rios gerenciais
     *
     * @return Bool
     */
    public function podeVisualizarRelatorios()
    {
        $retorno = false;
        $listaPermissoes = $this->getPermissoes();
        foreach ($listaPermissoes as $permissao) {
            if ($permissao->getCodigo() == App_Model_Entity_Sistema_Usuario::PodeVisualizarRelatorios) {
                $retorno = true;
                break;
            }
        }
        return $retorno;
    }

    /**
     * Verifica se o usu�rio tem permiss�o para a a��o
     *
     * @param App_Model_Entity_Sistema_Acao $acao
     * @return boolean
     */
    public function hasPermission(App_Model_Entity_Sistema_Acao $acao)
    {
        $retorno = false;
        foreach ($this->getPermissoes() as $perm) {
            if ($perm->getCodigo() === $acao->getCodigo()) {
                $retorno = true;
                break;
            }
        }
        return $retorno;
    }

    /**
     * Retorna os dados do grupo ao qual pertence o usu�rio
     * @return App_Model_Entity_Sistema_Usuario_Grupo
     */
    public function getGrupo()
    {
        $grupo = new App_Model_Entity_Sistema_Usuario_Grupo();
        $grupo->setCodigo($this->getCodigo())
            ->setNome($this->getNome())
            ->setTipo('AN');

        if ($this->getGrupoRede()) {
            $grupo = new App_Model_Entity_Sistema_Usuario_Grupo();
            $grupo->setCodigo($this->getGrupoRede()->getCodigo())
                ->setNome($this->getGrupoRede()->getNome())
                ->setTipo('GR');
        }

        if ($this->getRede()) {
            $grupo = new App_Model_Entity_Sistema_Usuario_Grupo();
            $grupo->setCodigo($this->getRede()->getCodigo())
                ->setNome($this->getRede()->getNome())
                ->setTipo('RE');
        }

        if ($this->getGrupoLoja()) {
            $grupo = new App_Model_Entity_Sistema_Usuario_Grupo();
            $grupo->setCodigo($this->getGrupoLoja()->getCodigo())
                ->setNome($this->getGrupoLoja()->getNome())
                ->setTipo('GL');
        }

        if ($this->getLoja()) {
            $grupo = new App_Model_Entity_Sistema_Usuario_Grupo();
            $grupo->setCodigo($this->getLoja()->getCodigo())
                ->setNome($this->getLoja()->getNomeCompleto())
                ->setTipo('LO');
        }

        return $grupo;
    }

}
