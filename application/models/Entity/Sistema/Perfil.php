<?php

/**
 * Defini��o do objeto Perfil
 *
 * @category 	Model
 * @package 	Model_Entity
 * @subpackage 	Sistema
 * @author 		Eduardo Schmidt <eduschmidt10@hotmail.com>
 */
class App_Model_Entity_Sistema_Perfil extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Collection of App_Model_Entity_Sistema_Usuario
	 */
	protected $objUsuarios = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Sistema_Modulos
	 */
	protected $objModulos = null;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objUsuarios', 'objModulos'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Sistema_Perfis::getInstance());
	}

	public function save()
	{
		$this->getTable()->getAdapter()->beginTransaction();
		try {
			parent::save();
			$this->getTable()->getAdapter()->commit();
		} catch (Exception $e) {
			$this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Recupera o c�digo identificador do perfil
	 * 
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->per_idPerfil;
	}

	/**
	 * Define o nome do perfil
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Sistema_Perfil
	 */
	public function setNome($value)
	{
		$this->per_nome = (string) $value;
		return $this;
	}

	/**
	 * Recupera o nome do perfil
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->per_nome;
	}

	/**
	 * Recupera os usu�rios pertencentes ao perfil
	 * 
	 * @return App_Model_Collection of App_Model_Entity_Sistema_Usuario
	 */
	public function getUsuarios()
	{
		if (null === $this->objUsuarios) {
			if ($this->getCodigo()) {
				$this->objUsuarios = $this->findDependentRowset(App_Model_DAO_Sistema_Usuarios::getInstance(), 'Perfil');
				foreach ($this->objUsuarios as $usuario) {
					$usuario->setPerfil($this);
				}
				$this->objUsuarios->rewind();
			} else {
				$this->objUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance()->createRowset();
			}
		}
		return $this->objUsuarios;
	}
	
	/**
	 * Recupera os m�dulo do perfil
	 * 
	 * @return App_Model_Collection of App_Model_Entity_Sistema_Modulo
	 */
	public function getModulos()
	{
		if (null === $this->objModulos) {
			if ($this->getCodigo()) {
				$this->objModulos = $this->findManyToManyRowset(
					App_Model_DAO_Sistema_Modulos::getInstance(),
					App_Model_DAO_Sistema_Modulos_Perfis::getInstance(),
					'Perfil',
					'Modulo'
				);
			} else {
				$this->objModulos = App_Model_DAO_Sistema_Modulos::getInstance()->createRowset();
			}
		}
		return $this->objModulos;
	}
}