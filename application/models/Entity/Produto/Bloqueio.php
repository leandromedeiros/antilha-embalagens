<?php

class App_Model_Entity_Produto_Bloqueio extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Produto
	 */
	protected $objProduto = null;
	
	/**
	 * @var App_Model_Entity_GrupoLoja
	 */
	protected $objGrupoLoja = null;
	
	/**
	 * @var App_Model_Entity_Loja
	 */
	protected $objLoja = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objProduto', 'objGrupoLoja', 'objLoja'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Produtos_Bloqueios::getInstance());
	}

	public function save()
	{
		
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'prod_bloq_idBloqueio' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),			
			'prod_bloq_idProduto' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());
		
		try {
			parent::save();
			
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * Recupera o c�digo identificador
	 * 
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->prod_bloq_idBloqueio;
	}

	/**
	 * @params App_Model_Entity_Produto $value
	 * @return App_Model_Entity_Produto_Bloqueio
	 */
	public function setProduto(App_Model_Entity_Produto $value = null)
	{
		$this->objProduto = $value;
		$this->prod_bloq_idProduto = $value != null ? $value->getCodigo() : null;
		return $this;
	}

	/**
	 * @return App_Model_Entity_Produto
	 */
	public function getProduto()
	{
		if (null === $this->objProduto && $this->getCodigo()) {
			$this->objProduto = $this->findParentRow(App_Model_DAO_GrupoRedes::getInstance(), 'Produto');
		}
		return $this->objProduto;
	}
	
	/**
	 * @params App_Model_Entity_GrupoLoja $value
	 * @return App_Model_Entity_Produto_Bloqueio
	 */
	public function setGrupoLoja(App_Model_Entity_GrupoLoja $value = null)
	{
		$this->objGrupoLoja = $value;
		$this->usr_idGrupoLoja = $value != null ? $value->getCodigo() : null;
		return $this;
	}

	/**
	 * @return App_Model_Entity_GrupoLoja
	 */
	public function getGrupoLoja()
	{
		if (null === $this->objGrupoLoja && $this->getCodigo()) {
			$this->objGrupoLoja = $this->findParentRow(App_Model_DAO_GrupoLojas::getInstance(), 'GrupoLoja');
		}
		return $this->objGrupoLoja;
	}
	
	/**
	 * @params App_Model_Entity_Loja $value
	 * @return App_Model_Entity_Produto_Bloqueio
	 */
	public function setLoja(App_Model_Entity_Loja $value = null)
	{
		$this->objLoja = $value;
		$this->usr_idLoja = $value != null ? $value->getCodigo() : null;
		return $this;
	}

	/**
	 * @return App_Model_Entity_Loja
	 */
	public function getLoja()
	{
		if (null === $this->objLoja && $this->getCodigo()) {
			$this->objLoja = $this->findParentRow(App_Model_DAO_Lojas::getInstance(), 'Loja');
		}
		return $this->objLoja;
	}

	/**
	 * Define a data inicial do bloqueio
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Produto_Bloqueio
	 */
	public function setDataInicial($value)
	{
		$this->prod_bloq_dataInicial = (string) $value;
		return $this;
	}

	/**
	 * Recupera a data inicial do bloqueio
	 * 
	 * @return string
	 */
	public function getDataInicial()
	{
		return (string) $this->prod_bloq_dataInicial;
	}

	/**
	 * Define a data final do bloqueio
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Produto_Bloqueio
	 */
	public function setDataFinal($value)
	{
		$this->prod_bloq_dataFinal = (string) $value;
		return $this;
	}

	/**
	 * Recupera a data final do bloqueio
	 * 
	 * @return string
	 */
	public function getDataFinal()
	{
		return (string) $this->prod_bloq_dataFinal;
	}

	/**
	 * Define o estado do bloqueio
	 * 
	 * @param string $value
	 * @return App_Model_Entity_Produto_Bloqueio
	 */
	public function setUF($value)
	{
		$this->prod_bloq_uf = (string) $value;
		return $this;
	}

	/**
	 * Recupera o estado do bloqueio
	 * 
	 * @return string
	 */
	public function getUF()
	{
		return (string) $this->prod_bloq_uf;
	}
	
}