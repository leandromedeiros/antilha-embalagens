<?php

/**
 * Defini��o do objeto Pedido
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Pedido extends App_Model_Entity_Abstract
{
    /*     * ** In�cio Propriedades do WebService ************ */

    /** @var int */
    public $codigo;

    /** @var string */
    public $loja;

    /** @var int */
    public $usuario;

    /** @var string */
    public $dataCadastro;

    /** @var string */
    public $dataFaturamento;

    /** @var string */
    public $previsaoEntrega;

    /** @var int */
    public $prazoEntrega;

    /** @var string */
    public $numeroSAP;

    /** @var int */
    public $numeroNF;

    /** @var int */
    public $condicaoPagamento;

    /** @var int */
    public $diasPagamento;

    /** @var int */
    public $transportadora;

    /** @var float */
    public $totalItens;

    /** @var float */
    public $totalPedido;

    /** @var float */
    public $totalLiquido;

    /** @var float */
    public $totalFaturado;

    /** @var float */
    public $juros;

    /** @var float */
    public $totalJuros;

    /** @var float */
    public $icms;

    /** @var string */
    public $tipoFrete;

    /** @var float */
    public $valorFrete;

    /** @var int */
    public $origem;

    /** @var string */
    public $tipoPreco;

    /** @var float */
    public $totalICMS;

    /** @var float */
    public $totalIPI;

    /** @var float */
    public $totalPagamento;

    /** @var float */
    public $totalPeso;

    /** @var float */
    public $totalLiquidoFaturado;

    /** @var string */
    public $observacoes;

    /** @var string */
    public $prioridade;

    /** @var int */
    public $flagImpressao;

    /** @var string */
    public $matriz;

    /** @var string */
    public $faturista;

    /** @var float */
    public $recebedor;

    /** @var string */
    public $programacao;

    /** @var string */
    public $linha;

    /** @var string */
    public $seuNumero;

    /** @var int */
    public $prefixo;

    /** @var string */
    public $checouDuplicata;

    /** @var float */
    public $jurosEspecial;

    /** @var string */
    public $condicaoPagamentoEspecial;

    /** @var float */
    public $diasEspecial;

    /** @var string */
    public $dataHoraAcred;

    /** @var string */
    public $usuarioAcred;

    /** @var string */
    public $gerado;

    /** @var string */
    public $dataColeta;

    /** @var float */
    public $codEmpre;

    /** @var int */
    public $grade;

    /** @var App_Model_Entity_Pedido_Item[] */
    public $itens = null;

    /** @var App_Model_Entity_Pedido_Log[] */
    public $logs = null;

    /** @var App_Model_Entity_Pedido_Nota[] */
    public $notas = null;

    /** @var App_Model_Entity_Pedido_Status[] */
    public $status = null;

    /*     * ** Fim Propriedades do WebService ************ */

    /**
     * @var App_Model_Entity_Sistema_Usuario
     */
    protected $objUsuario = null;

    /**
     * @var App_Model_Entity_Sistema_Usuario
     */
    protected $objUsuarioAntilhas = null;

    /**
     * @var App_Model_Entity_Loja
     */
    protected $objLoja = null;

    /**
     * @var App_Model_Entity_Transportadora
     */
    protected $objTransp = null;

    /**
     * @var App_Model_Entity_Pagamento
     */
    protected $objPagamento = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Pedido_Item
     */
    protected $objItens = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Pedido_Status
     */
    protected $objStatus = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Pedido_Log
     */
    protected $objLogs = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Pedido_Notas
     */
    protected $objNotas = null;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objUsuario', 'objItens', 'objStatus', 'objLogs', 'objNotas', 'objTransp', 'objUsuarioAntilhas', 'objLoja', 'objPagamento'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Pedidos::getInstance());
    }

    public function save($transaction = true)
    {
        $this->ped_grade = (int) $this->ped_grade;

        $filters = array('*' => new Zend_Filter_StringTrim());

        $validators = array(
            'ped_idPedido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_idLoja' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'ped_idUsuario' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'ped_idUsuarioAntilhas' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_dataCadastro' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'ped_dataFaturamento' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_previsaoEntrega' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_prazoEntrega' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_numeroSAP' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 10)
            ),
            'ped_numeroNF' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_condicaoPagamento' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_diasPagamento' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_transportadora' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalItens' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalPedido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalLiquido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalFaturado' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_juros' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalJuros' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_icms' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_tipoFrete' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_valorFrete' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_origem' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_tipoPreco' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 2)
            ),
            'ped_totalICMS' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalIPI' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalPagamento' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalPeso' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalLiquidoFaturado' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_observacoes' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 255)
            ),
            'ped_prioridade' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_flagImpressao' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_matriz' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_faturista' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_recebedor' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_programacao' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 10)
            ),
            'ped_linha' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_seuNumero' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 20)
            ),
            'ped_prefixo' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_checouDuplicata' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_jurosEspecial' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_condicaoPagamentoEspecial' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_diasEspecial' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_dataHoraAcred' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 21)
            ),
            'ped_usuarioAcred' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 15)
            ),
            'ped_gerado' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 13)
            ),
            'ped_dataColeta' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_codEmpre' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_grade' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        // persiste os dados no banco
        if ($transaction) {
            try {
                $this->getTable()->getAdapter()->beginTransaction();
                $trans = true;
            } catch (Exception $e) {
                $trans = false;
            }
        }

        //persiste os dados no banco
        try {
            parent::save();

            // Itens
            $daoItens = App_Model_DAO_Pedidos_Itens::getInstance();
            $this->getItens(); //for�a o carregamento
            $daoItens->delete($daoItens->getAdapter()->quoteInto('ped_item_idPedido = ?', $this->getCodigo()));
            foreach ($this->getItens() as $item) {
                $item->setPedido($this);
                $item->save();
            }

            // Status
            foreach ($this->getStatus() as $status) {
                $status->setPedido($this);
                $status->save($transaction);
            }

            // Logs
            foreach ($this->getLogs() as $log) {
                $log->setPedido($this);
                $log->save();
            }

            if ($transaction && $trans)
                $this->getTable()->getAdapter()->commit();
        } catch (App_Validate_Exception $e) {
            if ($transaction && $trans)
                $this->getTable()->getAdapter()->rollBack();
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            if ($transaction && $trans)
                $this->getTable()->getAdapter()->rollBack();
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }
	
	/*
		Separando modelo para salvamento de dados
	*/
    public function saveService($transaction = true)
    {
        $this->ped_grade = (int) $this->ped_grade;

        $filters = array('*' => new Zend_Filter_StringTrim());

        $validators = array(
            'ped_idPedido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_idLoja' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'ped_idUsuario' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'ped_idUsuarioAntilhas' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_dataCadastro' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'ped_dataFaturamento' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_previsaoEntrega' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_prazoEntrega' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_numeroSAP' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 10)
            ),
            'ped_numeroNF' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_condicaoPagamento' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_diasPagamento' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_transportadora' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalItens' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalPedido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalLiquido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalFaturado' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_juros' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalJuros' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_icms' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_tipoFrete' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_valorFrete' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_origem' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_tipoPreco' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 2)
            ),
            'ped_totalICMS' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalIPI' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalPagamento' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalPeso' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_totalLiquidoFaturado' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_observacoes' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 255)
            ),
            'ped_prioridade' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_flagImpressao' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_matriz' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_faturista' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_recebedor' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_programacao' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 10)
            ),
            'ped_linha' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_seuNumero' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 20)
            ),
            'ped_prefixo' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_checouDuplicata' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_jurosEspecial' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_condicaoPagamentoEspecial' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1)
            ),
            'ped_diasEspecial' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_dataHoraAcred' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 21)
            ),
            'ped_usuarioAcred' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 15)
            ),
            'ped_gerado' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 13)
            ),
            'ped_dataColeta' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_codEmpre' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            ),
            'ped_grade' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        // persiste os dados no banco
        if ($transaction) {
            try {
                $this->getTable()->getAdapter()->beginTransaction();
                $trans = true;
            } catch (Exception $e) {
                $trans = false;
            }
        }

        //persiste os dados no banco
        try {
            parent::save();

            // Itens
            $daoItens = App_Model_DAO_Pedidos_Itens::getInstance();
            $this->getItens(); //for�a o carregamento
            $daoItens->delete($daoItens->getAdapter()->quoteInto('ped_item_idPedido = ?', $this->getCodigo()));
			
			$objIten = $daoItens->createRow();
            foreach ($this->getItens() as $item) {
				$objIten->setPedido($this);
				$objIten->setPrefixo($item->prefixo);
				$objIten->setQtdFaturado($item->qtdFaturado);
				$objIten->setSaldoWcs($item->saldoWcs);
				$objIten->setSaldoProduto($item->saldoProduto);
				$objIten->setTotalPeso($item->totalPeso);
				$objIten->setSequencialItens($item->sequencialItens);
				$objIten->setStatus($item->status);
				$objIten->setUnidade($item->unidade);
				$objIten->setICMS($item->icms);
				$objIten->setIPI($item->ipi);
				$objIten->setValorTotal($item->valorTotal);
				$objIten->setValorUnitario($item->valorUnitario);
				$objIten->setQtdTotal($item->qtdTotal);
				$objIten->setNumeroItem($item->numeroItem);
				
				$daoProdutos = App_Model_DAO_Produtos::getInstance();
				$objProduto = $daoProdutos->find($item->produto)->current();
				$objIten->setProduto($objProduto);
				
                //$item->setPedido($this);
                $objIten->save();
            }

            // Status
            foreach ($this->getStatus() as $status) {
                $status->setPedido($this);
                $status->save($transaction);
            }

            // Logs
            foreach ($this->getLogs() as $log) {
                $log->setPedido($this);
                $log->save();
            }

            if ($transaction && $trans)
                $this->getTable()->getAdapter()->commit();
        } catch (App_Validate_Exception $e) {
            if ($transaction && $trans)
                $this->getTable()->getAdapter()->rollBack();
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            if ($transaction && $trans)
                $this->getTable()->getAdapter()->rollBack();
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Define o c�digo identificador do pedido
     *
     * @param int $value
     * @return App_Model_Entity_Pedido
     */
    public function setCodigo($value)
    {
        $this->ped_idPedido = (int) $value;
        return $this;
    }

    /**
     * Recupera o c�digo identificador do pedido
     *
     * @return int
     */
    public function getCodigo()
    {
        return (int) $this->ped_idPedido;
    }

    /**
     * @param App_Model_Entity_Sistema_Usuario $value
     * @return App_Model_Entity_Pedido
     */
    public function setUsuarioAntilhas(App_Model_Entity_Sistema_Usuario $value = null)
    {
        $this->objUsuarioAntilhas = $value;
        $this->ped_idUsuarioAntilhas = ($value ? $value->getCodigo() : null);
        return $this;
    }

    /**
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function getUsuarioAntilhas()
    {
        if (null == $this->objUsuarioAntilhas && $this->getCodigo()) {
            $this->objUsuarioAntilhas = $this->findParentRow(App_Model_DAO_Sistema_Usuarios::getInstance(), 'UsuarioAntilhas');
        }
        return $this->objUsuarioAntilhas;
    }

    /**
     * @param App_Model_Entity_Loja $value
     * @return App_Model_Entity_Pedido
     */
    public function setLoja(App_Model_Entity_Loja $value = null)
    {
        $this->objLoja = $value;
        $this->ped_idLoja = ($value ? $value->getCodigo() : null);
        return $this;
    }

    /**
     * @return App_Model_Entity_Loja
     */
    public function getLoja()
    {
        if (null == $this->objLoja && $this->getCodigo()) {
            $this->objLoja = $this->findParentRow(App_Model_DAO_Lojas::getInstance(), 'Loja');
        }
        return $this->objLoja;
    }

    /**
     * @param App_Model_Entity_Sistema_Usuario $value
     * @return App_Model_Entity_Pedido
     */
    public function setUsuario(App_Model_Entity_Sistema_Usuario $value)
    {
        $this->objUsuario = $value;
        $this->ped_idUsuario = $value->getCodigo();
        return $this;
    }

    /**
     * @return App_Model_Entity_Sistema_Usuario
     */
    public function getUsuario()
    {
        if (null == $this->objUsuario && $this->getCodigo()) {
            $this->objUsuario = $this->findParentRow(App_Model_DAO_Sistema_Usuarios::getInstance(), 'Usuario');
        }
        return $this->objUsuario;
    }

    /**
     * @param App_Model_Entity_Transportadora $value
     * @return App_Model_Entity_Pedido
     */
    public function setTransportadora(App_Model_Entity_Transportadora $value = null)
    {
        $this->objTransp = $value;
        $this->ped_transportadora = $value != null ? $value->getCodigo() : null;
        return $this;
    }

    /**
     * @return App_Model_Entity_Transportadora
     */
    public function getTransportadora()
    {
        if (null == $this->objTransp && $this->getCodigo()) {
            $this->objTransp = $this->findParentRow(App_Model_DAO_Transportadoras::getInstance(), 'Transportadora');
        }
        return $this->objTransp;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setDataCadastro($value)
    {
        $this->ped_dataCadastro = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataCadastro()
    {
        return (string) $this->ped_dataCadastro;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setDataFaturamento($value)
    {
        $this->ped_dataFaturamento = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataFaturamento()
    {
        return (string) $this->ped_dataFaturamento;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setPrevisaoEntrega($value)
    {
        $this->ped_previsaoEntrega = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrevisaoEntrega()
    {
        return (string) $this->ped_previsaoEntrega;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido
     */
    public function setPrazoEntrega($value)
    {
        $this->ped_prazoEntrega = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrazoEntrega()
    {
        return (int) $this->ped_prazoEntrega;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setNumeroSAP($value)
    {
        $this->ped_numeroSAP = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroSAP()
    {
        return (string) $this->ped_numeroSAP;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido
     */
    public function setNumeroNF($value)
    {
        $this->ped_numeroNF = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumeroNF()
    {
        return (int) $this->ped_numeroNF;
    }

    /**
     * @param App_Model_Entity_Pagamento $value
     * @return App_Model_Entity_Pedido
     */
    public function setCondicaoPagamento(App_Model_Entity_Pagamento $value = null)
    {
        $this->objPagamento = $value;
        $this->ped_condicaoPagamento = $value != null ? $value->getCodigo() : null;
        return $this;
    }

    /**
     * @return App_Model_Entity_Pagamento
     */
    public function getCondicaoPagamento()
    {
        if (null == $this->objPagamento && $this->getCodigo()) {
            $this->objPagamento = $this->findParentRow(App_Model_DAO_Pagamentos::getInstance(), 'Pagamento');
        }
        return $this->objPagamento;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido
     */
    public function setDiasPagamento($value)
    {
        $this->ped_diasPagamento = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiasPagamento()
    {
        return (int) $this->ped_diasPagamento;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalItens($value)
    {
        $this->ped_totalItens = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalItens()
    {
        return (float) $this->ped_totalItens;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalPedido($value)
    {
        $this->ped_totalPedido = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPedido()
    {
        return (float) $this->ped_totalPedido;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalLiquido($value)
    {
        $this->ped_totalLiquido = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalLiquido()
    {
        return (float) $this->ped_totalLiquido;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalFaturado($value)
    {
        $this->ped_totalFaturado = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalFaturado()
    {
        return (float) $this->ped_totalFaturado;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setJuros($value)
    {
        $this->ped_juros = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getJuros()
    {
        return (float) $this->ped_juros;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalJuros($value)
    {
        $this->ped_totalJuros = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalJuros()
    {
        return (float) $this->ped_totalJuros;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setICMS($value)
    {
        $this->ped_icms = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getICMS()
    {
        return (float) $this->ped_icms;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setTipoFrete($value)
    {
        $this->ped_tipoFrete = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoFrete()
    {
        return (string) $this->ped_tipoFrete;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setValorFrete($value)
    {
        $this->ped_valorFrete = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getValorFrete()
    {
        return (float) $this->ped_valorFrete;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido
     */
    public function setOrigem($value)
    {
        $this->ped_origem = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrigem()
    {
        return (int) $this->ped_origem;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setTipoPreco($value)
    {
        $this->ped_tipoPreco = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoPreco()
    {
        return (string) $this->ped_tipoPreco;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalICMS($value)
    {
        $this->ped_totalICMS = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalICMS()
    {
        return (float) $this->ped_totalICMS;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalIPI($value)
    {
        $this->ped_totalIPI = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalIPI()
    {
        return (float) $this->ped_totalIPI;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalPagamento($value)
    {
        $this->ped_totalPagamento = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPagamento()
    {
        return (float) $this->ped_totalPagamento;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalPeso($value)
    {
        $this->ped_totalPeso = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPeso()
    {
        return (float) $this->ped_totalPeso;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setTotalLiquidoFaturado($value)
    {
        $this->ped_totalLiquidoFaturado = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalLiquidoFaturado()
    {
        return (float) $this->ped_totalLiquidoFaturado;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setObservacoes($value)
    {
        $this->ped_observacoes = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getObservacoes()
    {
        return (string) $this->ped_observacoes;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setPrioridade($value)
    {
        $this->ped_prioridade = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrioridade()
    {
        return (string) $this->ped_prioridade;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido
     */
    public function setFlagImpressao($value)
    {
        $this->ped_flagImpressao = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getFlagImpressao()
    {
        return (int) $this->ped_flagImpressao;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setMatriz($value)
    {
        $this->ped_matriz = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getMatriz()
    {
        return (string) $this->ped_matriz;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setFaturista($value)
    {
        $this->ped_faturista = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getFaturista()
    {
        return (string) $this->ped_faturista;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setRecebedor($value)
    {
        $this->ped_recebedor = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getRecebedor()
    {
        return (float) $this->ped_recebedor;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setProgramacao($value)
    {
        $this->ped_programacao = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getProgramacao()
    {
        return (string) $this->ped_programacao;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setLinha($value)
    {
        $this->ped_linha = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinha()
    {
        return (string) $this->ped_linha;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setSeuNumero($value)
    {
        $this->ped_seuNumero = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeuNumero()
    {
        return (string) $this->ped_seuNumero;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido
     */
    public function setPrefixo($value)
    {
        $this->ped_prefixo = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrefixo()
    {
        return (int) $this->ped_prefixo;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setChecouDuplicata($value)
    {
        $this->ped_checouDuplicata = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getChecouDuplicata()
    {
        return (string) $this->ped_checouDuplicata;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setJurosEspecial($value)
    {
        $this->ped_jurosEspecial = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getJurosEspecial()
    {
        return (float) $this->ped_jurosEspecial;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setCondicaoPagamentoEspecial($value)
    {
        $this->ped_condicaoPagamentoEspecial = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getCondicaoPagamentoEspecial()
    {
        return (string) $this->ped_condicaoPagamentoEspecial;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setDiasEspecial($value)
    {
        $this->ped_diasEspecial = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiasEspecial()
    {
        return (float) $this->ped_diasEspecial;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setDataHoraAcred($value)
    {
        $this->ped_dataHoraAcred = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataHoraAcred()
    {
        return (string) $this->ped_dataHoraAcred;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setUsuarioAcred($value)
    {
        $this->ped_usuarioAcred = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsuarioAcred()
    {
        return (string) $this->ped_usuarioAcred;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setGerado($value)
    {
        $this->ped_gerado = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getGerado()
    {
        return (string) $this->ped_gerado;
    }

    /**
     * @param string $value
     * @return App_Model_Entity_Pedido
     */
    public function setDataColeta($value)
    {
        $this->ped_dataColeta = (string) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataColeta()
    {
        return (string) $this->ped_dataColeta;
    }

    /**
     * @param float $value
     * @return App_Model_Entity_Pedido
     */
    public function setCodEmpre($value)
    {
        $this->ped_codEmpre = (float) $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getCodEmpre()
    {
        return (float) $this->ped_codEmpre;
    }

    /**
     * @param int $value
     * @return App_Model_Entity_Pedido
     */
    public function setGrade($value)
    {
        $this->ped_grade = (int) $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getGrade()
    {
        return (int) $this->ped_grade;
    }

	/**
     * @param App_Model_Entity_Pedido_Item $value
     * @return App_Model_Entity_Pedido
     */
    public function setItens(App_Model_Entity_Pedido_Item $value = null)
    {
        $this->objItens = $value;
        return $this;
    }
    /**
     * @return App_Model_Collection of App_Model_Entity_Pedido_Item
     */
    public function getItens()
    {
        if (null == $this->objItens) {
            if ($this->getCodigo()) {
                $this->objItens = $this->findDependentRowset(App_Model_DAO_Pedidos_Itens::getInstance(), 'Pedido', $this->select()->order('ped_item_numeroItem ASC'));
                foreach ($this->objItens as $item) {
                    $item->setPedido($this);
                }
                $this->objItens->rewind();
            } else {
                $this->objItens = App_Model_DAO_Pedidos_Itens::getInstance()->createRowset();
            }
        }
        return $this->objItens;
    }

    /**
     * @return App_Model_Collection of App_Model_Entity_Pedido_Status
     */
    public function getStatus()
    {
        if (null == $this->objStatus) {
            if ($this->getCodigo()) {
                $this->objStatus = $this->findDependentRowset(App_Model_DAO_Pedidos_Status::getInstance(), 'Pedido');
                foreach ($this->objStatus as $item) {
                    $item->setPedido($this);
                }
                $this->objStatus->rewind();
            } else {
                $this->objStatus = App_Model_DAO_Pedidos_Status::getInstance()->createRowset();
            }
        }
        return $this->objStatus;
    }

    /**
     * @return App_Model_Collection of App_Model_Entity_Pedido_Log
     */
    public function getLogs()
    {
        if (null == $this->objLogs) {
            if ($this->getCodigo()) {
                $this->objLogs = $this->findDependentRowset(App_Model_DAO_Pedidos_Logs::getInstance(), 'Pedido');
                foreach ($this->objLogs as $item) {
                    $item->setPedido($this);
                }
                $this->objLogs->rewind();
            } else {
                $this->objLogs = App_Model_DAO_Pedidos_Logs::getInstance()->createRowset();
            }
        }
        return $this->objLogs;
    }

    /**
     * @return App_Model_Collection of App_Model_Entity_Pedido_Nota
     */
    public function getNotas()
    {
        if (null == $this->objNotas) {
            if ($this->getCodigo()) {
                $this->objNotas = $this->findDependentRowset(App_Model_DAO_Pedidos_Notas::getInstance(), 'Pedido');
                foreach ($this->objNotas as $item) {
                    $item->setPedido($this);
                }
                $this->objNotas->rewind();
            } else {
                $this->objNotas = App_Model_DAO_Pedidos_Notas::getInstance()->createRowset();
            }
        }
        return $this->objNotas;
    }

    public function getUltimoStatus()
    {
        return ($this->getStatus()->count() ? $this->getStatus()->seek($this->getStatus()->count() - 1)->current() : null);
    }

    public function getStatusExibicao()
    {
        $ultimoStatus = $this->getUltimoStatus();
        $daoStatus = App_Model_DAO_Status::getInstance();
        $daoStatusRelacionados = App_Model_DAO_Status_Relacionados::getInstance();

        return $daoStatus->getAdapter()->fetchRow(
                $daoStatus->getAdapter()->select()
                    ->from($daoStatus->info('name'))
                    ->joinInner($daoStatusRelacionados->info('name'), 'sta_status_idStatus = sta_idStatus')
                    ->where('sta_status_idStatusRelacionado = ?', $ultimoStatus->getValor())
                    ->where('sta_status = ?', 1)
        );
    }

    /**
     * @param App_Model_Entity_Sistema_Usuario $usuario
     */
    public function podeAprovar()
    {
        $retorno = false;
        $usuario = App_Plugin_Login::getInstance()->getIdentity();
        if ($this->getLoja()->getTemAprovadorIntermediario() || $this->getLoja()->getGrupo()->getTemAprovadorIntermediario()) {
            // Caso a loja tem aprovador intermedi�rio
            if ($this->getUltimoStatus()->getValor() == 0 && ($usuario->podeAprovarPedidosComQuantidade() || $usuario->podeAprovarPedidosSemQuantidade())) {
                $retorno = true;
            }
            if ($this->getUltimoStatus()->getValor() == 12 && $usuario->podeAprovarPedidosIntermediario()) {
                $retorno = true;
            }
        } else {
            // Caso a loja n�o tem aprovador intermedi�rio
            if ($this->getUltimoStatus()->getValor() == 0 && ($usuario->podeAprovarPedidosComQuantidade() || $usuario->podeAprovarPedidosSemQuantidade())) {
                $retorno = true;
            }
        }
        return $retorno;
    }

}
