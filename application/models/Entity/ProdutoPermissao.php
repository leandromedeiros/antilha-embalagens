<?php

/**
 * Defini��o do objeto GrupoRede
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_ProdutoPermissao extends App_Model_Entity_Abstract
{
	/**** In�cio Propriedades do WebService *************/
	
	/** @var string */
    public $codigo = null;
    
    /** @var string */
    public $codigoProduto = null;
    
    /** @var string */
    public $codigoGrupo = null;
    
    /** @var string */
    public $codigoRede = null;
    
    /** @var string */
	public $codigoLinha = null;
	
	public function __sleep()
	{
		$fields = null; //$fields = array_merge(parent::__sleep(), array());
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_ProdutoPermissao::getInstance());
	}

	public function save()
	{
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'per_IdProdutoPermissao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
            'per_idProduto' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 18)
            ),		
            'per_IdGrupo' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 50)
            ),		
            'per_IdRede' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 50)
            ),		
            'per_IdLinha' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 50)
            ),		
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			//throw new Zend_Db_Table_Row_Exception('erro');
			parent::save();		
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * Define o c�digo do grupo de rede
	 * 
	 * @param int $value
	 * @return App_Model_Entity_ProdutoPermissao
	 */
	public function setCodigo($value)
	{
		$this->per_IdProdutoPermissao = (string) $value;
		return $this;
	}

	/**
	 * Recupera o c�digo identificador do grupo de rede
	 * 
	 * @return string
	 */
	public function getCodigo()
	{
		return (string) $this->per_IdProdutoPermissao;
    }
    
	public function setCodigoGrupo($value)
	{
		$this->per_IdGrupo = (string) $value;
		return $this;
	}

	public function getCodigoGrupo()
	{
		return (string) $this->per_IdGrupo;
    }
    

    public function setCodigoRede($value)
	{
		$this->per_IdRede = (string) $value;
		return $this;
	}

	public function getCodigoRede()
	{
		return (string) $this->per_IdRede;
    }
    
    public function setCodigoLinha($value)
	{
		$this->per_IdLinha = (string) $value;
		return $this;
	}

	public function getCodigoLinha()
	{
		return (string) $this->per_IdLinha;
    }
    
    public function setCodigoProduto($value)
	{
		$this->per_IdProduto = (string) $value;
		return $this;
	}

	public function getCodigoProduto()
	{
		return (string) $this->per_IdProduto;
	}
}