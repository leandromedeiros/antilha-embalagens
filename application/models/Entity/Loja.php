<?php

/**
 * Defini��o do objeto Loja
 *
 * @category 	Model
 * @package 	Model_Entity
 */
class App_Model_Entity_Loja extends App_Model_Entity_Abstract
{
    /*     * ** In�cio Propriedades do WebService ************ */

    /** @var string */
    public $codigo = null;

    /** @var string */
    public $grupo = null;

    /** @var string */
    public $codigoLojaCliente = null;

    /** @var string */
    public $cnpj = null;

    /** @var string */
    public $email = null;

    /** @var float */
    public $icms = null;

    /** @var string */
    public $ie = null;

    /** @var string */
    public $tipo = null;

    /** @var string */
    public $nomeCompleto = null;

    /** @var string */
    public $nomeReduzido = null;

    /** @var string */
    public $transportadora = null;

    /** @var string */
    public $condicaoFrete = null;

    /** @var string */
    public $tipoBloqueio = null;

    /** @var string */
    public $classificacao = null;

    /** @var App_Model_Entity_Loja_Endereco[] */
    public $enderecos = null;

    /** @var App_Model_Entity_Loja_Contato[] */
    public $contatos = null;

    /*     * ** Fim Propriedades do WebService ************ */

    /**
     * @var App_Model_Entity_GrupoLoja
     * */
    protected $objGrupo = null;

    /**
     * @var App_Model_Entity_TipoLoja
     * */
    protected $objTipo = null;

    /**
     * @var App_Model_Collection of App_Model_Entity_Loja_Endereco
     * */
    protected $objEnderecos;

    /**
     * @var App_Model_Collection of App_Model_Entity_Loja_Contato
     * */
    protected $objContatos;

    /**
     * @var App_Model_Collection of App_Model_Entity_Loja_Usuario
     * */
    protected $objUsuarios;

    /**
     * @var App_Model_Entity_Loja_Endereco
     * */
    protected $objEnderecoPadrao;

    public function __sleep()
    {
        $fields = array_merge(parent::__sleep(), array('objGrupo', 'objTipo', 'objEnderecos', 'objContatos', 'objUsuarios', 'objEnderecoPadrao'));
        return $fields;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        $this->setTable(App_Model_DAO_Lojas::getInstance());
    }

    public function save()
    {
        $filters = array(
            '*' => new Zend_Filter_StringTrim()
        );

        $validators = array(
            'loja_idLoja' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 10)
            ),
            'loja_idGrupo' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 6)
            ),
            'loja_idTipo' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 10)
            ),
            'loja_codigoLojaCliente' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 20)
            ),
            'loja_cnpj' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new App_Validate_CpfCnpj()
            ),
            'loja_email' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 80),
                new Zend_Validate_EmailAddress()
            ),
            'loja_icms' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false
            ),
            'loja_ie' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true,
                new Zend_Validate_StringLength(1, 20)
            ),
            'loja_nomeCompleto' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 40)
            ),
            'loja_nomeReduzido' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 25)
            ),
            'loja_transportadora' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 10)
            ),
            'loja_tipoBloqueio' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 2)
            ),
            'loja_classificacao' => array(
                Zend_Filter_Input::ALLOW_EMPTY => false,
                new Zend_Validate_StringLength(1, 1)
            ),
            'loja_temAprovadorIntermediario' => array(
                Zend_Filter_Input::ALLOW_EMPTY => true
            )
        );

        //verifica a consist�ncia dos dados
        $this->validate($filters, $validators, $this->toArray());

        //persiste os dados no banco
        try {
            $this->getTable()->getAdapter()->beginTransaction();
            $trans = true;
        } catch (Exception $e) {
            $trans = false;
        }
        try {
            parent::save();

            // Endere�os
            $daoEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance();
            $this->getEnderecos(); //for�a o carregamento dos endere�os
            $daoEnderecos->delete($daoEnderecos->getAdapter()->quoteInto('loja_end_idLoja = ?', $this->getCodigo()));
            foreach ($this->getEnderecos() as $endereco) {
                $daoEnderecos->insert(array(
                    'loja_end_idLoja' => $this->getCodigo(),
                    'loja_end_tipo' => $endereco->getTipo(),
                    'loja_end_cep' => $endereco->getEndereco()->getCep(),
                    'loja_end_logradouro' => $endereco->getEndereco()->getLogradouro(),
                    'loja_end_numero' => $endereco->getEndereco()->getNumero(),
                    'loja_end_complemento' => $endereco->getEndereco()->getComplemento(),
                    'loja_end_bairro' => $endereco->getEndereco()->getBairro(),
                    'loja_end_cidade' => $endereco->getEndereco()->getCidade(),
                    'loja_end_uf' => $endereco->getEndereco()->getUf(),
                    'loja_end_prazo' => $endereco->getPrazo(),
                    'loja_end_padrao' => $endereco->getPadrao()
                ));
            }
            unset($daoEnderecos);

            // Contatos
            $daoContatos = App_Model_DAO_Lojas_Contatos::getInstance();
            $this->getContatos(); //for�a o carregamento dos contatos
            $daoContatos->delete($daoContatos->getAdapter()->quoteInto('loja_con_idLoja = ?', $this->getCodigo()));
            foreach ($this->getContatos() as $contato) {
                $daoContatos->insert(array(
                    'loja_con_idLoja' => $this->getCodigo(),
                    'loja_con_tipo' => $contato->getTipo(),
                    'loja_con_objetivo' => $contato->getObjetivo(),
                    'loja_con_valor' => $contato->getValor()
                ));
            }
            unset($daoContatos);

            if ($trans)
                $this->getTable()->getAdapter()->commit();
        } catch (App_Validate_Exception $e) {
            throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
        } catch (Exception $e) {
            if ($trans)
                $this->getTable()->getAdapter()->rollBack();
            throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Define o codigo identificador da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setCodigo($value)
    {
        $this->loja_idLoja = (string) $value;
        return $this;
    }

    /**
     * Recupera o c�digo identificador da loja
     *
     * @return string
     */
    public function getCodigo()
    {
        return (string) $this->loja_idLoja;
    }

    /**
     * Define o codigo da loja do cliente
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setCodigoLojaCliente($value)
    {
        $this->loja_codigoLojaCliente = (string) $value;
        return $this;
    }

    /**
     * Recupera o codigo da loja do cliente
     *
     * @return string
     */
    public function getCodigoLojaCliente()
    {
        return (string) $this->loja_codigoLojaCliente;
    }

    /**
     * Define o nome completo da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setNomeCompleto($value)
    {
        $this->loja_nomeCompleto = (string) $value;
        return $this;
    }

    /**
     * Recupera o nome completo da loja
     *
     * @return string
     */
    public function getNomeCompleto()
    {
        return (string) $this->loja_nomeCompleto;
    }

    /**
     * Define o nome reduzido da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setNomeReduzido($value)
    {
        $this->loja_nomeReduzido = (string) $value;
        return $this;
    }

    /**
     * Recupera o nome reduzido da loja
     *
     * @return string
     */
    public function getNomeReduzido()
    {
        return (string) $this->loja_nomeReduzido;
    }

    /**
     * Recupera o cnpj da loja
     *
     * @return string
     */
    public function getCnpj()
    {
        return (string) $this->loja_cnpj;
    }

    /**
     * Define o cnpj da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setCnpj($value)
    {
        $this->loja_cnpj = (string) $value;
        return $this;
    }

    /**
     * Recupera o email da loja
     *
     * @return string
     */
    public function getEmail()
    {
        return (string) $this->loja_email;
    }

    /**
     * Define o email da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setEmail($value)
    {
        $this->loja_email = (string) $value;
        return $this;
    }

    /**
     * Recupera o icms da loja
     *
     * @return string
     */
    public function getIcms()
    {
        return (float) $this->loja_icms;
    }

    /**
     * Define o icms da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setIcms($value)
    {
        $this->loja_icms = (float) $value;
        return $this;
    }

    /**
     * Recupera a inscri��o estadual da loja
     *
     * @return string
     */
    public function getIe()
    {
        return (string) $this->loja_ie;
    }

    /**
     * Define a inscri��o estadual da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setIe($value)
    {
        $this->loja_ie = (string) $value;
        return $this;
    }

    /**
     * Define o grupo a qual a loja pertence
     *
     * @param App_Model_Entity_GrupoLoja $value
     * @return App_Model_Entity_Loja
     */
    public function setGrupo(App_Model_Entity_GrupoLoja $value)
    {
        $this->objGrupo = $value;
        $this->loja_idGrupo = $value->getCodigo();
        return $this;
    }

    /**
     * Recupera o grupo a qual a loja pertence
     *
     * @return App_Model_Entity_GrupoLoja
     */
    public function getGrupo()
    {
        if (null == $this->objGrupo && $this->getCodigo()) {
            $this->objGrupo = $this->findParentRow(App_Model_DAO_GrupoLojas::getInstance(), 'Grupo');
        }
        return $this->objGrupo;
    }

    /**
     * Define o tipo a qual a loja pertence
     *
     * @param App_Model_Entity_TipoLoja $value
     * @return App_Model_Entity_Loja
     */
    public function setTipo(App_Model_Entity_TipoLoja $value)
    {
        $this->objTipo = $value;
        $this->loja_idTipo = $value->getCodigo();
        return $this;
    }

    /**
     * Recupera o tipo a qual a loja pertence
     *
     * @return App_Model_Entity_TipoLoja
     */
    public function getTipo()
    {
        if (null == $this->objTipo && $this->getCodigo()) {
            $this->objTipo = $this->findParentRow(App_Model_DAO_TiposLoja::getInstance(), 'Tipo');
        }
        return $this->objTipo;
    }

    /**
     * Define o transportadora da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setTransportadora($value)
    {
        $this->loja_transportadora = (string) $value;
        return $this;
    }

    /**
     * Recupera o transportadora da loja
     *
     * @return string
     */
    public function getTransportadora($nome = false)
    {
        if ($nome) {
            $daoTransportadoras = App_Model_DAO_Transportadoras::getInstance();
            $objTransportadora = $daoTransportadoras->find($this->loja_transportadora)->current();
            if ($objTransportadora)
                return $objTransportadora->getNome();
        }

        return (string) $this->loja_transportadora;
    }

    /**
     * Define a condi��o do frete
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setCondicaoFrete($value)
    {
        $this->loja_condicaoFrete = (string) $value;
        return $this;
    }

    /**
     * Recupera a condi��o do frete
     *
     * @return string
     */
    public function getCondicaoFrete()
    {
        return (string) $this->loja_condicaoFrete;
    }

    /**
     * Define o tipo de bloqueio da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setTipoBloqueio($value)
    {
        $this->loja_tipoBloqueio = (string) $value;
        return $this;
    }

    /**
     * Recupera o tipo de bloqueio da loja
     *
     * @return string
     */
    public function getTipoBloqueio()
    {
        return (string) $this->loja_tipoBloqueio;
    }

    /**
     * Define a classifica��o da loja
     *
     * @param string $value
     * @return App_Model_Entity_Loja
     */
    public function setClassificacao($value)
    {
        $this->loja_classificacao = (string) $value;
        return $this;
    }

    /**
     * Recupera a classifica��o da loja
     *
     * @return string
     */
    public function getClassificacao()
    {
        return (string) $this->loja_classificacao;
    }

    /**
     * @param bool $value
     * @return App_Model_Entity_GrupoLoja
     */
    public function setTemAprovadorIntermediario($value)
    {
        $this->loja_temAprovadorIntermediario = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function getTemAprovadorIntermediario()
    {
        return (bool) $this->loja_temAprovadorIntermediario;
    }

    /**
     * Recupera os dados do endere�o da loja
     *
     * @return App_Model_Collection of App_Model_Entity_Loja_Endereco
     */
    public function getEnderecos()
    {
        if (null == $this->objEnderecos) {
            if ($this->getCodigo()) {
                $this->objEnderecos = $this->findDependentRowset(App_Model_DAO_Lojas_Enderecos::getInstance(), 'Loja');
                foreach ($this->objEnderecos as $endereco) {
                    $endereco->setLoja($this);
                }
                $this->objEnderecos->rewind();
            } else {
                $this->objEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance()->createRowset();
            }
        }
        return $this->objEnderecos;
    }

    /**
     * Recupera os dados dos contatos da loja
     *
     * @return App_Model_Collection of App_Model_Entity_Loja_Contato
     */
    public function getContatos()
    {
        if (null == $this->objContatos) {
            if ($this->getCodigo()) {
                $this->objContatos = $this->findDependentRowset(App_Model_DAO_Lojas_Contatos::getInstance(), 'Loja');
                foreach ($this->objContatos as $contato) {
                    $contato->setLoja($this);
                }
                $this->objContatos->rewind();
            } else {
                $this->objContatos = App_Model_DAO_Lojas_Contatos::getInstance()->createRowset();
            }
        }
        return $this->objContatos;
    }

    /**
     * Retorna todas os usu�rios desta loja
     *
     * @return App_Model_Collection of App_Model_Entity_Sistema_Usuario
     */
    public function getUsuarios()
    {
        if (null == $this->objUsuarios) {
            if ($this->getCodigo()) {
                $this->objUsuarios = $this->findDependentRowset(App_Model_DAO_Sistema_Usuarios::getInstance(), 'Loja');
                foreach ($this->objUsuarios as $usuario) {
                    $usuario->setLoja($this);
                }
                $this->objUsuarios->rewind();
            } else {
                $this->objUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance()->createRowset();
            }
        }
        return $this->objUsuarios;
    }

    /**
     * Endere�o padr�o
     *
     * @return App_Model_Entity_Loja_Endereco
     */
    public function getEnderecoPadrao()
    {
        $daoEnderecos = App_Model_DAO_Lojas_Enderecos::getInstance();
        $endereco = $daoEnderecos->fetchRow(
            $daoEnderecos->select()->from($daoEnderecos)->where('loja_end_idLoja = ?', $this->getCodigo())->where('loja_end_padrao = ?', 1)
        );
        return $endereco;
    }

    public function getCarrinho(App_Model_Entity_Sistema_Usuario $usuario)
    {

        $daoCarrinho = App_Model_DAO_Carrinho::getInstance();

        $select = $daoCarrinho->select()->from($daoCarrinho)
            ->where('car_idLoja = ?', $this->getCodigo())
            ->where('ISNULL(car_idPedido)')
            ->where('car_idUsuario = ?', $usuario->getCodigo());

        return $daoCarrinho->fetchRow($select);
    }

    /**
     * Lista os produtos do carrinho
     * @param App_Model_Entity_Linha $linha || false
     * @param App_Model_Entity_Sistema_Usuario $usuario
     * @param App_Model_Entity_Produto $produto
     * @return array
     */
    public function getProdutosCarrinho(App_Model_Entity_Linha $linha = null, App_Model_Entity_Sistema_Usuario $usuario, App_Model_Entity_Produto $produto)
    {

        $daoProdutos = App_Model_DAO_Produtos::getInstance();
        $daoPrecos = App_Model_DAO_Precos::getInstance();
        $daoCarrinho = App_Model_DAO_Carrinho::getInstance();
        $daoCarrinhoItens = App_Model_DAO_Carrinho_Itens::getInstance();

        $selectCar = $daoCarrinho->getAdapter()->select()->from($daoCarrinho->info('name'), 'car_idCarrinho')
            ->where('car_idLoja = ?', $this->getCodigo())
            ->where('ISNULL(car_idPedido)')
            ->where('car_idUsuario = ?', $usuario->getCodigo());

        if ($linha)
            $selectCar->where('car_idLinha = ?', $linha->getCodigo());

        $select = $daoProdutos->getAdapter()->select()
            ->from($daoProdutos->info('name'))
            ->joinLeft($daoPrecos->info('name'), 'prec_idProduto = prod_idProduto')
            ->joinLeft($daoCarrinhoItens->info('name'), "prod_idProduto = car_item_idProduto AND car_item_idCarrinho = ({$selectCar})")
            ->where('prod_status = ?', 1)
            ->where('prec_idTipo = ?', $this->getTipo()->getCodigo())
            ->where('prod_idProduto = ?', $produto->getCodigo());

        if ($linha)
            $select->where('prod_idLinha = ?', $linha->getCodigo());

        return $daoProdutos->getAdapter()->fetchRow($select);
    }

}
