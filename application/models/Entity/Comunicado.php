<?php

class App_Model_Entity_Comunicado extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Galeria_Arquivo
	 */
	protected $objImagem = null;
	
	/**
	 * @var App_Model_Entity_Galeria_Arquivo
	 */
	protected $objMiniatura = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Comunicado_Permissao
	 */
	protected $objPermissoes = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Sistema_Usuario
	 */
	protected $objLeituras = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Comunicado_Estado
	 */
	protected $objEstados = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objImagem', 'objPermissoes', 'objEstados', 'objLeituras', 'objMiniatura'));
		return $fields;
	}
	
	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Comunicados::getInstance());
	}

	public function save()
	{
		$this->com_status = (int) $this->com_status;
		if(!$this->getCodigo()) $this->com_dataCadastro = date('Y-m-d H:i:s');
		
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'com_idComunicado' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'com_idImagem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'com_idMiniatura' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'com_dataCadastro' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'com_dataInicial' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'com_dataFinal' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false				
			),
			'com_titulo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true,
				new Zend_Validate_StringLength(1, 80)
			),
			'com_descricao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'com_leituraObrigatoria' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'com_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)					
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());
		
		// persiste os dados no banco
        try {
            $this->getTable()->getAdapter()->beginTransaction();
            $trans = true;
        } catch (Exception $e) {
            $trans = false;
        }

		//persiste os dados no banco
		try {
			parent::save();
			
			// Permiss�es
			$daoPermissoes = App_Model_DAO_Comunicados_Permissoes::getInstance();
			$this->getPermissoes(); //for�a o carregamento das permiss�es
			$daoPermissoes->delete($daoPermissoes->getAdapter()->quoteInto('com_perm_idComunicado = ?', $this->getCodigo()));
			foreach ($this->getPermissoes() as $permissao) {
				$daoPermissoes->insert(array(
					'com_perm_idComunicado' => $this->getCodigo(),
					'com_perm_idGrupoRede' => $permissao->getGrupoRede() != null ? $permissao->getGrupoRede()->getCodigo() : null,
					'com_perm_idRede' => $permissao->getRede() != null ? $permissao->getRede()->getCodigo() : null,
					'com_perm_idGrupoLoja' => $permissao->getGrupoLoja() != null ? $permissao->getGrupoLoja()->getCodigo() : null,
					'com_perm_idLoja' => $permissao->getLoja() != null ? $permissao->getLoja()->getCodigo() : null,
					'com_perm_idLinha' => $permissao->getLinha() != null ? $permissao->getLinha()->getCodigo() : null
				));
			}
			unset($daoPermissoes);
			
			// Estados
			$daoEstados = App_Model_DAO_Comunicados_Estados::getInstance();
			$this->getEstados(); //for�a o carregamento das permiss�es
			$daoEstados->delete($daoEstados->getAdapter()->quoteInto('com_est_idComunicado = ?', $this->getCodigo()));
			foreach ($this->getEstados() as $estado) {
				$daoEstados->insert(array(
					'com_est_idComunicado' => $this->getCodigo(),
					'com_est_uf' => $estado->getUf()
				));
			}
			unset($daoEstados);
			
			// Leituras
			$daoLeituras = App_Model_DAO_Comunicados_Leituras::getInstance();
			$this->getLeituras(); //for�a o carregamento das permiss�es
			$daoLeituras->delete($daoLeituras->getAdapter()->quoteInto('com_leit_idComunicado = ?', $this->getCodigo()));
			foreach ($this->getLeituras() as $leitura) {
				$daoLeituras->insert(array(
					'com_leit_idComunicado' => $this->getCodigo(),
					'com_leit_idUsuario' => $leitura->getCodigo(),
					'com_leit_data' => date('Y-m-d H:i:s')
				));
			}
			unset($daoEstados);

			if ($trans) $this->getTable()->getAdapter()->commit();

		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->com_idComunicado;
	}
	
	/**
	 * @return date
	 */
	public function getDataCadastro()
	{
		return (string) $this->com_dataCadastro;
	}

	/**
	 * @param App_Model_Entity_Galeria_Arquivo $value
	 * @return App_Model_Entity_Comunicado
	 */
	public function setImagem(App_Model_Entity_Galeria_Arquivo $value)
	{
		$this->objImagem = $value;
		$this->com_idImagem = $value->getCodigo();
		return $this;
	}

	/**
	 * @return App_Model_Entity_Galeria_Arquivo
	 */
	public function getImagem()
	{
		if (null == $this->objImagem && $this->getCodigo()) {
			$this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
		}
		return $this->objImagem;
	}
	
	/**
	 * @param App_Model_Entity_Galeria_Arquivo $value
	 * @return App_Model_Entity_Comunicado
	 */
	public function setMiniatura(App_Model_Entity_Galeria_Arquivo $value = null)
	{
		$this->objMiniatura = $value;
		$this->com_idMiniatura = (null == $value ? null : $value->getCodigo());
		return $this;
	}

	/**
	 * @return App_Model_Entity_Galeria_Arquivo
	 */
	public function getMiniatura()
	{
		if (null == $this->objMiniatura && $this->getCodigo()) {
			$this->objMiniatura = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Miniatura');
		}
		return $this->objMiniatura;
	}
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Comunicado
	 */
	public function setDataInicial($value)
	{
		$this->com_dataInicial = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDataInicial()
	{
		return (string) $this->com_dataInicial;
	}	
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Comunicado
	 */
	public function setDataFinal($value)
	{
		$this->com_dataFinal = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDataFinal()
	{
		return (string) $this->com_dataFinal;
	}	
	
	/**
	 * @param string $value
	 * @return App_Model_Entity_Comunicado
	 */
	public function setTitulo($value)
	{
		$this->com_titulo = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitulo()
	{
		return (string) $this->com_titulo;
	}

	/**
	 * @param string $value
	 * @return App_Model_Entity_Comunicado
	 */
	public function setDescricao($value)
	{
		$this->com_descricao = (string) $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescricao()
	{
		return (string) $this->com_descricao;
	}
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Comunicado
	 */
	public function setLeituraObrigatoria($value)
	{
		$this->com_leituraObrigatoria = (int) $value;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getLeituraObrigatoria()
	{
		return (int) $this->com_leituraObrigatoria;
	}	
	
	/**
	 * @param int $value
	 * @return App_Model_Entity_Comunicado
	 */
	public function setStatus($value)
	{
		$this->com_status = (bool) $value;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return (bool) $this->com_status;
	}

	public function getLeituras()
	{
		if (null == $this->objLeituras) {
			if ($this->getCodigo()) {
				$this->objLeituras = $this->findManyToManyRowset(
					App_Model_DAO_Sistema_Usuarios::getInstance(),
					App_Model_DAO_Comunicados_Leituras::getInstance(),
					'Comunicado',
					'Usuario'
				);
			} else {
				$this->objLeituras = App_Model_DAO_Sistema_Usuarios::getInstance()->createRowset();
			}
		}
		return $this->objLeituras;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Comunicado_Permissao
	 */
	public function getPermissoes()
	{
		if (null == $this->objPermissoes) {
			if ($this->getCodigo()) {
				$this->objPermissoes = $this->findDependentRowset(App_Model_DAO_Comunicados_Permissoes::getInstance(), 'Comunicado');
				foreach ($this->objPermissoes as $permissao) {
					$permissao->setComunicado($this);
				}
				$this->objPermissoes->rewind();
			} else {
				$this->objPermissoes = App_Model_DAO_Comunicados_Permissoes::getInstance()->createRowset();
			}
		}
		return $this->objPermissoes;
	}
	
	/**
	 * @return App_Model_Collection of App_Model_Entity_Comunicado_Estado
	 */
	public function getEstados()
	{
		if (null == $this->objEstados) {
			if ($this->getCodigo()) {
				$this->objEstados = $this->findDependentRowset(App_Model_DAO_Comunicados_Estados::getInstance(), 'Comunicado');
				foreach ($this->objEstados as $estado) {
					$estado->setComunicado($this);
				}
				$this->objEstados->rewind();
			} else {
				$this->objEstados = App_Model_DAO_Comunicados_Estados::getInstance()->createRowset();
			}
		}
		return $this->objEstados;
	}
}