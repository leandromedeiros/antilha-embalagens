<?php

class App_Model_Response extends App_Model_Listener
{
	/** @var string */
	public $message = null;
	
	/** @var boolean */
	public $success = false;
	
	/** @var array */
	public $logs = array();
}