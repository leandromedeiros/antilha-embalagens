<?php

class App_Model_DAO_GrupoRedes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'grp_redes';
	protected $_primary = 'grp_rede_idGrupo';
	protected $_rowClass = 'App_Model_Entity_GrupoRede';

	protected $_dependentTables = array(
		'App_Model_DAO_Redes'
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_GrupoRedes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}