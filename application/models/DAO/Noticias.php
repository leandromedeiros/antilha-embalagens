<?php

class App_Model_DAO_Noticias extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'noticias';
	protected $_primary = 'not_idNoticia';
	protected $_rowClass = 'App_Model_Entity_Noticia';
	
	protected $_dependentTables = array(
		'App_Model_DAO_Noticias_Estados',
		'App_Model_DAO_Noticias_Permissoes'
	);
	
	protected $_referenceMap = array(
		'Imagem' => array(
			self::COLUMNS => 'not_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Noticias
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}