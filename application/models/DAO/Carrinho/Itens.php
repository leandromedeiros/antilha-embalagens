<?php

class App_Model_DAO_Carrinho_Itens extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'carrinho_itens';
	protected $_primary = array('car_item_idCarrinho', 'car_item_idProduto');
	protected $_sequence = false;
	protected $_rowClass = 'App_Model_Entity_Carrinho_Item';
	
	protected $_referenceMap = array(
		'Carrinho' => array(
			self::COLUMNS => 'car_item_idCarrinho',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Carrinho',
			self::REF_COLUMNS => 'car_idCarrinho'
		),
		'Produto' => array(
			self::COLUMNS => 'car_item_idProduto',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos',
			self::REF_COLUMNS => 'prod_idProduto'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Carrinho_Itens
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}