<?php

class App_Model_DAO_Carrinho extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'carrinho';
	protected $_primary = 'car_idCarrinho';
	protected $_rowClass = 'App_Model_Entity_Carrinho';
	
	protected $_referenceMap = array(		
		'Loja' => array(
			self::COLUMNS => 'car_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		),
		'Produto' => array(
			self::COLUMNS => 'car_idProduto',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos',
			self::REF_COLUMNS => 'prod_idProduto'
		),
		'Linha' => array(
			self::COLUMNS => 'car_idLinha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		),
		'UsuarioAntilhas' => array(
			self::COLUMNS => 'car_idUsuarioAntilhas',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		),
		'Usuario' => array(
			self::COLUMNS => 'car_idUsuario',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		),
		'Pedido' => array(
			self::COLUMNS => 'car_idPedido',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos',
			self::REF_COLUMNS => 'ped_idPedido'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Carrinho
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	/**
	 * Dispara o e-mail de aviso de pedido em aberto
	 *
	 * @return boolean
	 */
	public function emailAviso($carrinho, $dias) {
		try {
			//Envia o email
			$template = new Zend_View();
			$template->setBasePath(Zend_Registry::get('config')->paths->site->root);
			
			$template->carrinho = $carrinho;
			$template->dias = $dias;					
			
			$mailTransport = new App_Model_Email();
			$mail = new Zend_Mail('ISO-8859-1');
			$mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
			$mail->addTo($carrinho->getUsuario()->getEmail(), $carrinho->getUsuario()->getNome());			
			
			$mail->setSubject(Zend_Registry::get('config')->project . ' - Pedido n�o conclu�do');
			$mail->setBodyHtml($template->render('emails/aviso-carrinho.phtml'));
			$mail->send($mailTransport->getFormaEnvio());			
		} catch (Exception $e) {}
		
		return true;
	}
}