<?php

class App_Model_DAO_Downloads_Permissoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'downloads_permissoes';
	protected $_primary = array(
		'down_perm_idDownload', 
		'down_perm_idGrupoRede', 
		'down_perm_idRede', 
		'down_perm_idGrupoLoja', 
		'down_perm_idLoja', 
		'down_perm_idLinha'
	);
	protected $_rowClass = 'App_Model_Entity_Download_Permissao';
	
	protected $_referenceMap = array(
		'Download' => array(
			self::COLUMNS => 'down_perm_idDownload',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Downloads',
			self::REF_COLUMNS => 'down_idDownload'
		),
		'GrupoRede' => array(
			self::COLUMNS => 'down_perm_idGrupoRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoRedes',
			self::REF_COLUMNS => 'grp_rede_idGrupo'
		),
		'Rede' => array(
			self::COLUMNS => 'down_perm_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		),
		'GrupoLoja' => array(
			self::COLUMNS => 'down_perm_idGrupoLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoLojas',
			self::REF_COLUMNS => 'grp_loja_idGrupo'
		),
		'Loja' => array(
			self::COLUMNS => 'down_perm_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		),
		'Linha' => array(
			self::COLUMNS => 'down_perm_idLinha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Downloads_Permissoes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}