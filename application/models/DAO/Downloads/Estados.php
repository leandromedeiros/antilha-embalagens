<?php

class App_Model_DAO_Downloads_Estados extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'downloads_estados';
	protected $_primary = 'down_est_idDownload';
	protected $_rowClass = 'App_Model_Entity_Download_Estado';
	
	protected $_referenceMap = array(
		'Download' => array(
			self::COLUMNS => 'down_est_idDownload',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Downloads',
			self::REF_COLUMNS => 'down_idDownload'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Downloads_Estados
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}