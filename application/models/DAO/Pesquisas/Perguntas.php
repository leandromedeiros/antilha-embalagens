<?php

class App_Model_DAO_Pesquisas_Perguntas extends App_Model_DAO_Abstract
{

    protected static $instance = null;
    protected $_name = 'pesquisas_perguntas';
    protected $_primary = 'pes_perg_idPergunta';
    protected $_rowClass = 'App_Model_Entity_Pesquisa_Pergunta';
    protected $_dependentTables = array(
        'App_Model_DAO_Pesquisas_Alternativas'
    );
    protected $_referenceMap = array(
        'Pesquisa' => array(
            self::COLUMNS => 'pes_perg_idPesquisa',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Pesquisas',
            self::REF_COLUMNS => 'pes_idPesquisa'
        ),
        'Imagem' => array(
            self::COLUMNS => 'pes_perg_idImagem',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
            self::REF_COLUMNS => 'gal_arq_idArquivo'
        )
    );

    /**
     * Implementa��o do m�todo Singleton para obter a instancia da classe
     *
     * @return App_Model_DAO_Pesquisas_Perguntas
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}
