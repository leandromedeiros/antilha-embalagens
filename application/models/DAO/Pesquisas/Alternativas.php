<?php

class App_Model_DAO_Pesquisas_Alternativas extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pesquisas_alternativas';
	protected $_primary = 'pes_alt_idAlternativa';
	protected $_rowClass = 'App_Model_Entity_Pesquisa_Alternativa';
	
	protected $_dependentTables = array(
		'App_Model_DAO_Pesquisas_Respostas'
	);
	
	protected $_referenceMap = array(
		'Pergunta' => array(
			self::COLUMNS => 'pes_alt_idPergunta',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pesquisas_Perguntas',
			self::REF_COLUMNS => 'pes_perg_idPergunta'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Pesquisas_Alternativas
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}