<?php

class App_Model_DAO_Pesquisas_Permissoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pesquisas_permissoes';
	protected $_primary = array(
		'pes_perm_idPesquisa', 
		'pes_perm_idGrupoRede', 
		'pes_perm_idRede', 
		'pes_perm_idGrupoLoja', 
		'pes_perm_idLoja', 
		'pes_perm_idLinha'
	);
	protected $_rowClass = 'App_Model_Entity_Pesquisa_Permissao';
	
	protected $_referenceMap = array(
		'Pesquisa' => array(
			self::COLUMNS => 'pes_perm_idPesquisa',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pesquisas',
			self::REF_COLUMNS => 'pes_idPesquisa'
		),
		'GrupoRede' => array(
			self::COLUMNS => 'pes_perm_idGrupoRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoRedes',
			self::REF_COLUMNS => 'grp_rede_idGrupo'
		),
		'Rede' => array(
			self::COLUMNS => 'pes_perm_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		),
		'GrupoLoja' => array(
			self::COLUMNS => 'pes_perm_idGrupoLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoLojas',
			self::REF_COLUMNS => 'grp_loja_idGrupo'
		),
		'Loja' => array(
			self::COLUMNS => 'pes_perm_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		),
		'Linha' => array(
			self::COLUMNS => 'pes_perm_idLinha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pesquisas_Permissoes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}