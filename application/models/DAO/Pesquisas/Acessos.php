<?php

class App_Model_DAO_Pesquisas_Acessos extends App_Model_DAO_Abstract
{
    protected static $instance = null;

    protected $_name = 'pesquisas_acessos';
    
    protected $_primary = array('pes_ace_idPesquisa', 'pes_ace_idUsuario');

    protected $_referenceMap = array(
        'Pesquisa' => array(
            self::COLUMNS => 'pes_ace_idPesquisa',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Pesquisas',
            self::REF_COLUMNS => 'pes_idPesquisa'
        ),
        'Usuario' => array(
            self::COLUMNS => 'pes_ace_idUsuario',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
            self::REF_COLUMNS => 'usr_idUsuario'
        )
    );

    /**
     * Implementa��o do m�todo Singleton para obter a instancia da classe
     * @return App_Model_DAO_Pesquisas_Acessos
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}