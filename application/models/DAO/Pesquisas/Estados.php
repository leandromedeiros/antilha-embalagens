<?php

class App_Model_DAO_Pesquisas_Estados extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pesquisas_estados';
	protected $_primary = 'pes_est_idPesquisa';
	protected $_rowClass = 'App_Model_Entity_Pesquisa_Estado';
	
	protected $_referenceMap = array(
		'Pesquisa' => array(
			self::COLUMNS => 'pes_est_idPesquisa',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pesquisas',
			self::REF_COLUMNS => 'pes_idPesquisa'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pesquisas_Estados
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}