<?php

class App_Model_DAO_Pesquisas_Respostas extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pesquisas_respostas';
	protected $_primary = array('pes_resp_idUsuario', 'pes_resp_idPergunta');
	protected $_rowClass = 'App_Model_Entity_Pesquisa_Resposta';
	protected $_sequence = false;
	
	protected $_referenceMap = array(
		'Alternativa' => array(
			self::COLUMNS => 'pes_resp_idAlternativa',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pesquisas_Alternativas',
			self::REF_COLUMNS => 'pes_alt_idAlternativa'
		),
		'Usuario' => array(
			self::COLUMNS => 'pes_resp_idUsuario',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Pesquisas_Respostas
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}