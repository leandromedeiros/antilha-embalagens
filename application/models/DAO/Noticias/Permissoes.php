<?php

class App_Model_DAO_Noticias_Permissoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'noticias_permissoes';
	protected $_primary = array(
		'not_perm_idNoticia', 
		'not_perm_idGrupoRede', 
		'not_perm_idRede', 
		'not_perm_idGrupoLoja', 
		'not_perm_idLoja', 
		'not_perm_idLinha'
	);
	protected $_rowClass = 'App_Model_Entity_Noticia_Permissao';
	
	protected $_referenceMap = array(
		'Noticia' => array(
			self::COLUMNS => 'not_perm_idNoticia',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Noticias',
			self::REF_COLUMNS => 'not_idNoticia'
		),
		'GrupoRede' => array(
			self::COLUMNS => 'not_perm_idGrupoRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoRedes',
			self::REF_COLUMNS => 'grp_rede_idGrupo'
		),
		'Rede' => array(
			self::COLUMNS => 'not_perm_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		),
		'GrupoLoja' => array(
			self::COLUMNS => 'not_perm_idGrupoLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoLojas',
			self::REF_COLUMNS => 'grp_loja_idGrupo'
		),
		'Loja' => array(
			self::COLUMNS => 'not_perm_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		),
		'Linha' => array(
			self::COLUMNS => 'not_perm_idLinha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Noticias_Permissoes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}