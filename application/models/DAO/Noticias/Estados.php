<?php

class App_Model_DAO_Noticias_Estados extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'noticias_estados';
	protected $_primary = 'not_est_idNoticia';
	protected $_rowClass = 'App_Model_Entity_Noticia_Estado';
	
	protected $_referenceMap = array(
		'Noticia' => array(
			self::COLUMNS => 'not_est_idNoticia',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Noticias',
			self::REF_COLUMNS => 'not_idNoticia'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Noticias_Estados
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}