<?php

class App_Model_DAO_Produtos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'produtos';
	protected $_primary = 'prod_idProduto';
	protected $_rowClass = 'App_Model_Entity_Produto';
	
	protected $_referenceMap = array(
		'Linha' => array(
			self::COLUMNS => 'prod_idLinha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		),
		'Imagem' => array(
			self::COLUMNS => 'prod_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Produtos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}