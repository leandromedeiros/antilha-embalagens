<?php

class App_Model_DAO_Sistema_Modulos_Perfis extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'sis_modulos_perfis';
	protected $_primary = array('mod_per_idModulo', 'mod_per_idPerfil');
	protected $_sequence = false;

	protected $_referenceMap = array(
		'Modulo' => array(
			self::COLUMNS => 'mod_per_idModulo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Modulos',
			self::REF_COLUMNS => 'mod_idModulo'
		),
		'Perfil' => array(
			self::COLUMNS => 'mod_per_idPerfil',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Perfis',
			self::REF_COLUMNS => 'per_idPerfil'
		),
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Sistema_Modulos_Perfis
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}