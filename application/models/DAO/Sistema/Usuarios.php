<?php

class App_Model_DAO_Sistema_Usuarios extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'sis_usuarios';
	protected $_primary = 'usr_idUsuario';
	protected $_rowClass = 'App_Model_Entity_Sistema_Usuario';

	protected $_dependentTables = array(
		'App_Model_DAO_Galerias_Arquivos'
	);

	protected $_referenceMap = array(
		'Perfil' => array(
			self::COLUMNS => 'usr_idPerfil',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Perfis',
			self::REF_COLUMNS => 'per_idPerfil'
		),
		'GrupoRede' => array(
			self::COLUMNS => 'usr_idGrupoRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoRedes',
			self::REF_COLUMNS => 'grp_rede_idGrupo'
		),
		'Rede' => array(
			self::COLUMNS => 'usr_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		),
		'GrupoLoja' => array(
			self::COLUMNS => 'usr_idGrupoLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoLojas',
			self::REF_COLUMNS => 'grp_loja_idGrupo'
		),
		'Loja' => array(
			self::COLUMNS => 'usr_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Sistema_Usuarios
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}