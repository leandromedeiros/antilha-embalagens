<?php

class App_Model_DAO_Sistema_Permissoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'sis_permissoes';
	protected $_primary = array('perm_idUsuario', 'perm_idAcao');
	protected $_sequence = false;

	protected $_referenceMap = array(
		'Usuario' => array(
			self::COLUMNS => 'perm_idUsuario',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		),
		'Acao' => array(
			self::COLUMNS => 'perm_idAcao',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Acoes',
			self::REF_COLUMNS => 'mod_acao_idAcao'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Sistema_Permissoes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}