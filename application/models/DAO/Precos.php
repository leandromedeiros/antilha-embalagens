<?php

class App_Model_DAO_Precos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'precos';
	protected $_primary = array('prec_idProduto', 'prec_idTipo');
	protected $_rowClass = 'App_Model_Entity_Preco';
	
	protected $_referenceMap = array(
		'Produto' => array(
			self::COLUMNS => 'prec_idProduto',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos',
			self::REF_COLUMNS => 'prod_idProduto'
		),
		'Tipo' => array(
			self::COLUMNS => 'prec_idTipo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_TiposLoja',
			self::REF_COLUMNS => 'tipo_loja_idTipo'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Precos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}