<?php

class App_Model_DAO_Faqs extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'faqs';
	protected $_primary = 'faq_idFaq';
	protected $_rowClass = 'App_Model_Entity_Faq';
	
	protected $_referenceMap = array(
		'Topico' => array(
			self::COLUMNS => 'faq_idTopico',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Topicos',
			self::REF_COLUMNS => 'top_idTopico'
		)
	);
	
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}