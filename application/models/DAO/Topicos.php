<?php

class App_Model_DAO_Topicos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'topicos';
	protected $_primary = 'top_idTopico';
	protected $_rowClass = 'App_Model_Entity_Topico';
	
	protected $_dependentTables = array(
		'App_Model_DAO_Faqs'
	);
	
	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Topicos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}