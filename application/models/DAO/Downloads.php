<?php

class App_Model_DAO_Downloads extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'downloads';
	protected $_primary = 'down_idDownload';
	protected $_rowClass = 'App_Model_Entity_Download';

	protected $_dependentTables = array(
		'App_Model_DAO_Downloads_Estados',
		'App_Model_DAO_Downloads_Permissoes'
	);
	
	protected $_referenceMap = array(
		'Imagem' => array(
			self::COLUMNS => 'down_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Downloads
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}