<?php

class App_Model_DAO_Linhas extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'linhas';
	protected $_primary = 'lin_idLinha';
	protected $_rowClass = 'App_Model_Entity_Linha';
	
	protected $_referenceMap = array(
		'Rede' => array(
			self::COLUMNS => 'lin_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		),
		'Imagem' => array(
			self::COLUMNS => 'lin_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		),		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Linhas
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}