<?php

class App_Model_DAO_Comunicados_Estados extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'comunicados_estados';
	protected $_primary = 'com_est_idComunicado';
	protected $_rowClass = 'App_Model_Entity_Comunicado_Estado';
	
	protected $_referenceMap = array(
		'Comunicado' => array(
			self::COLUMNS => 'com_est_idComunicado',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Comunicados',
			self::REF_COLUMNS => 'com_idComunicado'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Comunicados_Estados
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}