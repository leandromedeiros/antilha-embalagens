<?php

class App_Model_DAO_Comunicados_Permissoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'comunicados_permissoes';
	protected $_primary = array(
		'com_perm_idComunicado', 
		'com_perm_idGrupoRede', 
		'com_perm_idRede', 
		'com_perm_idGrupoLoja', 
		'com_perm_idLoja', 
		'com_perm_idLinha'
	);
	protected $_rowClass = 'App_Model_Entity_Comunicado_Permissao';
	
	protected $_referenceMap = array(
		'Comunicado' => array(
			self::COLUMNS => 'com_perm_idComunicado',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Comunicados',
			self::REF_COLUMNS => 'com_idComunicado'
		),
		'GrupoRede' => array(
			self::COLUMNS => 'com_perm_idGrupoRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoRedes',
			self::REF_COLUMNS => 'grp_rede_idGrupo'
		),
		'Rede' => array(
			self::COLUMNS => 'com_perm_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		),
		'GrupoLoja' => array(
			self::COLUMNS => 'com_perm_idGrupoLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoLojas',
			self::REF_COLUMNS => 'grp_loja_idGrupo'
		),
		'Loja' => array(
			self::COLUMNS => 'com_perm_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		),
		'Linha' => array(
			self::COLUMNS => 'com_perm_idLinha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Comunicados_Permissoes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}