<?php

class App_Model_DAO_Comunicados_Leituras extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'comunicados_leituras';
	protected $_primary = array('com_leit_idComunicado', 'com_leit_idUsuario');
	protected $_sequence = false;
		
	protected $_referenceMap = array(
		'Comunicado' => array(
			self::COLUMNS => 'com_leit_idComunicado',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Comunicados',
			self::REF_COLUMNS => 'com_idComunicado'
		),
		'Usuario' => array(
			self::COLUMNS => 'com_leit_idUsuario',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		)				
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Comunicados_Leituras
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}