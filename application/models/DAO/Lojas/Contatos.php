<?php

class App_Model_DAO_Lojas_Contatos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'lojas_contatos';
	protected $_primary = 'loja_con_idContato';
	protected $_rowClass = 'App_Model_Entity_Loja_Contato';
	
	protected $_referenceMap = array(
		'Loja' => array(
			self::COLUMNS => 'loja_con_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Lojas_Contatos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}