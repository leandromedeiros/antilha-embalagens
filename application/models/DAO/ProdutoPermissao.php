<?php

class App_Model_DAO_ProdutoPermissao extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'produtos_permissao';
	protected $_primary = 'per_IdProduto';
	protected $_sequence = true;
	protected $_rowClass = 'App_Model_Entity_ProdutoPermissao';

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_ProdutoPermissao
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}