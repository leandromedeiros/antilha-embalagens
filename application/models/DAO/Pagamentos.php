<?php

class App_Model_DAO_Pagamentos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pagamentos';
	protected $_primary = 'pag_idPagamento';
	protected $_rowClass = 'App_Model_Entity_Pagamento';
	
	protected $_referenceMap = array(
		'Linha' => array(
			self::COLUMNS => 'pag_linha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		),
		'Rede' => array(
			self::COLUMNS => 'pag_rede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pagamentos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}