<?php

class App_Model_DAO_Comunicados extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'comunicados';
	protected $_primary = 'com_idComunicado';
	protected $_rowClass = 'App_Model_Entity_Comunicado';
	
	protected $_dependentTables = array(
		'App_Model_DAO_Comunicados_Estados',
		'App_Model_DAO_Comunicados_Permissoes',
		'App_Model_DAO_Comunicados_Leituras'
	);
	
	protected $_referenceMap = array(
		'Imagem' => array(
			self::COLUMNS => 'com_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		),
		'Miniatura' => array(
			self::COLUMNS => 'com_idMiniatura',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Comunicados
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}