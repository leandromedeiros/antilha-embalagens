<?php

class App_Model_DAO_Regioes extends App_Model_DAO_Abstract
{

    protected static $instance = null;
    protected $_name = 'regioes';
    protected $_primary = 'reg_idRegiao';
    protected $_rowClass = 'App_Model_Entity_Regiao';

    /**
     * Implementa��o do m�todo Singleton para obter a instancia da classe
     *
     * @return App_Model_DAO_Regioes
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}
