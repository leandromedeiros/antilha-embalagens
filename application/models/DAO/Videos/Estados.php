<?php

class App_Model_DAO_Videos_Estados extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'videos_estados';
	protected $_primary = 'vid_est_idVideo';
	protected $_rowClass = 'App_Model_Entity_Video_Estado';
	
	protected $_referenceMap = array(
		'Video' => array(
			self::COLUMNS => 'vid_est_idVideo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Videos',
			self::REF_COLUMNS => 'vid_idVideo'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Videos_Estados
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}