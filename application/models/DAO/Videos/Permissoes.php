<?php

class App_Model_DAO_Videos_Permissoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'videos_permissoes';
	protected $_primary = array(
		'vid_perm_idVideo', 
		'vid_perm_idGrupoRede', 
		'vid_perm_idRede', 
		'vid_perm_idGrupoLoja', 
		'vid_perm_idLoja', 
		'vid_perm_idLinha'
	);
	protected $_rowClass = 'App_Model_Entity_Video_Permissao';
	
	protected $_referenceMap = array(
		'Video' => array(
			self::COLUMNS => 'vid_perm_idVideo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Videos',
			self::REF_COLUMNS => 'vid_idVideo'
		),
		'GrupoRede' => array(
			self::COLUMNS => 'vid_perm_idGrupoRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoRedes',
			self::REF_COLUMNS => 'grp_rede_idGrupo'
		),
		'Rede' => array(
			self::COLUMNS => 'vid_perm_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		),
		'GrupoLoja' => array(
			self::COLUMNS => 'vid_perm_idGrupoLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoLojas',
			self::REF_COLUMNS => 'grp_loja_idGrupo'
		),
		'Loja' => array(
			self::COLUMNS => 'vid_perm_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		),
		'Linha' => array(
			self::COLUMNS => 'vid_perm_idLinha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Videos_Permissoes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}