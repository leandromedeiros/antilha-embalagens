<?php

class App_Model_DAO_Status extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'status';
	protected $_primary = 'sta_idStatus';
	protected $_rowClass = 'App_Model_Entity_Status';

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Status
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}