<?php

class App_Model_DAO_Pedidos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos';
	protected $_primary = array('ped_idPedido');
	protected $_rowClass = 'App_Model_Entity_Pedido';
	
	protected $_referenceMap = array(
		'Usuario' => array(
			self::COLUMNS => 'ped_idUsuario',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		),
		'Transportadora' => array(
			self::COLUMNS => 'ped_transportadora',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Transportadoras',
			self::REF_COLUMNS => 'trans_codigo'
		),
		'UsuarioAntilhas' => array(
			self::COLUMNS => 'ped_idUsuarioAntilhas',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		),
		'Loja' => array(
			self::COLUMNS => 'ped_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		),
		'Pagamento' => array(
			self::COLUMNS => 'ped_condicaoPagamento',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pagamentos',
			self::REF_COLUMNS => 'pag_idPagamento'
		)
					
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pedidos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}