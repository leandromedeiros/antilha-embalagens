<?php

class App_Model_DAO_Banners extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'banners';
	protected $_primary = 'ban_idBanner';
	protected $_rowClass = 'App_Model_Entity_Banner';

	protected $_dependentTables = array(
		'App_Model_DAO_Banners_Estados',
		'App_Model_DAO_Banners_Permissoes'
	);
	
	protected $_referenceMap = array(
		'Imagem' => array(
			self::COLUMNS => 'ban_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Banners
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}