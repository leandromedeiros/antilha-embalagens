<?php

class App_Model_DAO_Banners_Estados extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'banners_estados';
	protected $_primary = 'ban_est_idBanner';
	protected $_rowClass = 'App_Model_Entity_Banner_Estado';
	
	protected $_referenceMap = array(
		'Banner' => array(
			self::COLUMNS => 'ban_est_idBanner',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Banners',
			self::REF_COLUMNS => 'ban_idBanner'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Banners_Estados
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}