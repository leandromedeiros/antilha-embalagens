<?php

class App_Model_DAO_Banners_Permissoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'banners_permissoes';
	protected $_primary = array(
		'ban_perm_idBanner', 
		'ban_perm_idGrupoRede', 
		'ban_perm_idRede', 
		'ban_perm_idGrupoLoja', 
		'ban_perm_idLoja', 
		'ban_perm_idLinha'
	);
	protected $_rowClass = 'App_Model_Entity_Banner_Permissao';
	
	protected $_referenceMap = array(
		'Banner' => array(
			self::COLUMNS => 'ban_perm_idBanner',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Banners',
			self::REF_COLUMNS => 'ban_idBanner'
		),
		'GrupoRede' => array(
			self::COLUMNS => 'ban_perm_idGrupoRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoRedes',
			self::REF_COLUMNS => 'grp_rede_idGrupo'
		),
		'Rede' => array(
			self::COLUMNS => 'ban_perm_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		),
		'GrupoLoja' => array(
			self::COLUMNS => 'ban_perm_idGrupoLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoLojas',
			self::REF_COLUMNS => 'grp_loja_idGrupo'
		),
		'Loja' => array(
			self::COLUMNS => 'ban_perm_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		),
		'Linha' => array(
			self::COLUMNS => 'ban_perm_idLinha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Banners_Permissoes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}