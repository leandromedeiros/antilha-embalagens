<?php

class App_Model_DAO_Status_Relacionados extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'status_relacionados';
	protected $_primary = 'sta_status_idStatus';
	protected $_rowClass = 'App_Model_Entity_Status_Relacionado';
	
	protected $_referenceMap = array(
		'Status' => array(
			self::COLUMNS => 'sta_status_idStatus',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Status',
			self::REF_COLUMNS => 'sta_idStatus'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Status_Relacionados
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}