<?php

class App_Model_DAO_SugestoesReclamacoes_Status extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'sugestoes_reclamacoes_status';
	protected $_primary = 'sug_rec_status_idStatus';
	protected $_rowClass = 'App_Model_Entity_SugestaoReclamacao_Status';
	
	protected $_referenceMap = array(
		'SugestaoReclamacao' => array(
			self::COLUMNS => 'sug_rec_status_idSugestaoReclamacao',
			self::REF_TABLE_CLASS => 'App_Model_DAO_SugestoesReclamacoes',
			self::REF_COLUMNS => 'sug_rec_idSugestaoReclamacao'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Usuarios_Lancamentos_Status
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}