<?php

class App_Model_DAO_SugestoesReclamacoes_Assuntos_Motivos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'reclamacoes_assuntos_motivos';
	protected $_primary = 'rec_ass_mot_idMotivo';
	protected $_rowClass = 'App_Model_Entity_SugestaoReclamacao_Assunto_Motivo';
	
	protected $_referenceMap = array(
		'Assunto' => array(
			self::COLUMNS => 'rec_ass_mot_idAssunto',
			self::REF_TABLE_CLASS => 'App_Model_DAO_SugestoesReclamacoes_Assuntos',
			self::REF_COLUMNS => 'rec_ass_idAssunto'
		),
    );
	
	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_SugestoesReclamacoes_Assunto
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}