<?php

class App_Model_DAO_SugestoesReclamacoes_Assuntos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'reclamacoes_assuntos';
	protected $_primary = 'rec_ass_idAssunto';
	protected $_rowClass = 'App_Model_Entity_SugestaoReclamacao_Assunto';
	
	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_SugestoesReclamacoes_Assunto
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}