<?php

class App_Model_DAO_TiposLoja extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'tipos_lojas';
	protected $_primary = 'tipo_loja_idTipo';
	protected $_rowClass = 'App_Model_Entity_TipoLoja';
	
	protected $_dependentTables = array(
		'App_Model_DAO_Lojas'
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_TiposLoja
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}