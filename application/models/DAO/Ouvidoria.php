<?php

class App_Model_DAO_Ouvidoria extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'ouvidoria';
	protected $_primary = 'ouv_idContato';
	protected $_rowClass = 'App_Model_Entity_Ouvidoria';
	
	protected $_referenceMap = array(
		'Usuario' => array(
			self::COLUMNS => 'ouv_idUsuario',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Ouvidoria
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}