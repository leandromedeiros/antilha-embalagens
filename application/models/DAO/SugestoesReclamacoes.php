<?php

class App_Model_DAO_SugestoesReclamacoes extends App_Model_DAO_Abstract
{
	public static $instance = null;
    protected $_name = 'sugestoes_reclamacoes';
    protected $_primary = 'sug_rec_idSugestaoReclamacao';
    protected $_rowClass = 'App_Model_Entity_SugestaoReclamacao';

    protected $_referenceMap = array(
        'Usuario' => array(
            self::COLUMNS => 'sug_rec_idUsuario',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
            self::REF_COLUMNS => 'usr_idUsuario'
        ),
		'Assunto' => array(
			self::COLUMNS => 'sug_rec_idAssunto',
			self::REF_TABLE_CLASS => 'App_Model_DAO_SugestoesReclamacoes_Assuntos',
			self::REF_COLUMNS => 'rec_ass_idAssunto'
		),
		'Motivo' => array(
			self::COLUMNS => 'sug_rec_idMotivo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_SugestoesReclamacoes_Assuntos_Motivos',
			self::REF_COLUMNS => 'rec_ass_mot_idMotivo'
		),
		'Pedido' => array(
			self::COLUMNS => 'sug_rec_idPedido',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos',
			self::REF_COLUMNS => 'ped_idPedido'
		)
    );

    /**
     * Implementa��o do m�todo Singleton para obter a instancia da classe
     *
     * @return App_Model_DAO_Reclamacoes
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}