<?php

class App_Model_DAO_Transportadoras extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'transportadoras';
	protected $_primary = 'trans_codigo';
	protected $_rowClass = 'App_Model_Entity_Transportadora';

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Transportadoras
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}