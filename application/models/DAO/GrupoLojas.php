<?php

class App_Model_DAO_GrupoLojas extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'grp_lojas';
	protected $_primary = 'grp_loja_idGrupo';
	protected $_rowClass = 'App_Model_Entity_GrupoLoja';

	protected $_dependentTables = array(
		'App_Model_DAO_Lojas'
	);
	
	protected $_referenceMap = array(
		'Rede' => array(
			self::COLUMNS => 'grp_loja_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Redes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}