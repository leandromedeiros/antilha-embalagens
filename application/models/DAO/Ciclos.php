<?php

class App_Model_DAO_Ciclos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'ciclos';
	protected $_primary = 'ci_idCiclo';
	protected $_rowClass = 'App_Model_Entity_Ciclos';
	
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}