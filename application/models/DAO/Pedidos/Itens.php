<?php

class App_Model_DAO_Pedidos_Itens extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos_itens';
	protected $_primary = 'ped_item_idItem';
	protected $_rowClass = 'App_Model_Entity_Pedido_Item';
	
	protected $_referenceMap = array(
		'Pedido' => array(
			self::COLUMNS => 'ped_item_idPedido',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos',
			self::REF_COLUMNS => 'ped_idPedido'
		),
		'Produto' => array(
			self::COLUMNS => 'ped_item_produto',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos',
			self::REF_COLUMNS => 'prod_idProduto'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pedidos_Itens
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}