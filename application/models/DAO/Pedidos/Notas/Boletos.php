<?php

class App_Model_DAO_Pedidos_Notas_Boletos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos_notas_boletos';
	protected $_primary = 'ped_nota_bol_idBoleto';
	protected $_rowClass = 'App_Model_Entity_Pedido_Nota_Boleto';
	protected $_sequence = false;
	
	protected $_referenceMap = array(
		'Nota' => array(
			self::COLUMNS => 'ped_nota_bol_notaFiscal',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos_Notas',
			self::REF_COLUMNS => 'ped_nota_idNotaFiscal'
		)	
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pedidos_Notas_Boletos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}