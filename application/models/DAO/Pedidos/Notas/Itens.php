<?php

class App_Model_DAO_Pedidos_Notas_Itens extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos_notas_itens';
	protected $_primary = 'ped_nota_item_idItem';
	protected $_rowClass = 'App_Model_Entity_Pedido_Nota_Item';
	
	protected $_referenceMap = array(
		'Nota' => array(
			self::COLUMNS => 'ped_nota_item_idNota',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos_Notas',
			self::REF_COLUMNS => 'ped_nota_idNotaFiscal'
		)	
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pedidos_Notas_Itens
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}