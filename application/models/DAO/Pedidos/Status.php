<?php

class App_Model_DAO_Pedidos_Status extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos_status';
	protected $_primary = 'ped_status_idStatus';
	protected $_rowClass = 'App_Model_Entity_Pedido_Status';
	
	protected $_referenceMap = array(
		'Pedido' => array(
			self::COLUMNS => 'ped_status_idPedido',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos',
			self::REF_COLUMNS => 'ped_idPedido'
		)		
	);
	
	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pedidos_Status
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	/**
	 * Dispara o e-mail de aviso de mudan�a de status
	 *
	 * @return boolean
	 */
	public function enviaEmail($status) {
		try {
			$statusEmail = array('1', '3', '4', '5', '6', '8', '9', '18');
			if(in_array($status->getValor(), $statusEmail)) {
				$template = new Zend_View();
				$template->setBasePath(Zend_Registry::get('config')->paths->site->root);
				
				$template->pedido = $status->getPedido();	
				//se for template de faturamento parcial
				$daoNotas = App_Model_DAO_Pedidos_Notas::getInstance();
				if($status->getValor() == '5') {
					$daoPedidos = App_Model_DAO_Pedidos::getInstance();
					$daoNotasItens = App_Model_DAO_Pedidos_Notas_Itens::getInstance();
					$daoProdutos = App_Model_DAO_Produtos::getInstance();
					
					$selectNotas = $daoNotas->select()->from($daoNotas->info('name'), 'MAX(ped_nota_idNotaFiscal)')->where('ped_nota_idPedido = ped_idPedido');
					$select = $daoNotasItens->getAdapter()->select()
						->from($daoPedidos->info('name'), null)
						->joinInner($daoNotas->info('name'), "ped_nota_idPedido = ped_idPedido AND ped_nota_idNotaFiscal = ({$selectNotas})", null)
						->joinInner($daoNotasItens->info('name'), 'ped_nota_item_idNota = ped_nota_idNotaFiscal')
						->joinInner($daoProdutos->info('name'), 'prod_idProduto = ped_nota_item_produto', 'prod_descricao')
						->where('ped_idPedido = ?', $status->getPedido()->getCodigo());

					$produtosFaturados = $daoNotasItens->getAdapter()->fetchAll($select);
					
					$todosProdutos = $status->getPedido()->getItens();
					$produtosNaoFaturados = array();
					foreach($todosProdutos as $item) {
						$produtosNaoFaturados[$item->getProduto()->getCodigo()] = $item; 
						foreach($produtosFaturados as $itemFaturado) {
							if($item->getProduto()->getCodigo() == $itemFaturado['ped_nota_item_produto']) {
								unset($produtosNaoFaturados[$item->getProduto()->getCodigo()]);
							}
						}
						
					}
					$template->produtosFaturados = $produtosFaturados;
					$template->produtosNaoFaturados = $produtosNaoFaturados;
				}
				
				$template->nota = $daoNotas->fetchRow($daoNotas->select()->from($daoNotas)
					->where('ped_nota_idPedido = ?', $status->getPedido()->getCodigo()));
				
				$mailTransport = new App_Model_Email();
				$mail = new Zend_Mail('ISO-8859-1');
				$mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
				$mail->addTo($status->getPedido()->getUsuario()->getEmail(), $status->getPedido()->getUsuario()->getNome());			
				$mail->setSubject(Zend_Registry::get('config')->project . ' - ' . App_Funcoes_Rotulos::$assuntosEmailsPedidos[$status->getValor()]);
				$mail->setBodyHtml($template->render("emails/pedidos/{$status->getValor()}.phtml"));
				$mail->send($mailTransport->getFormaEnvio());	
				
				$status->setEnvioPendente(0);
				$status->save();
			}
		} catch (Exception $e) {
			//die($e->getMessage());
		}
		
		return true;
	}
}