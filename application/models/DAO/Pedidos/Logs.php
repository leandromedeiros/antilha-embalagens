<?php

class App_Model_DAO_Pedidos_Logs extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos_logs';
	protected $_primary = array('ped_log_idPedido', 'ped_log_idUsuario');
	protected $_rowClass = 'App_Model_Entity_Pedido_Log';
	protected $_sequence = false;
	
	protected $_referenceMap = array(
		'Pedido' => array(
			self::COLUMNS => 'ped_log_idPedido',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos',
			self::REF_COLUMNS => 'ped_idPedido'
		),
		'Usuario' => array(
			self::COLUMNS => 'ped_log_idUsuario',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pedidos_Logs
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}