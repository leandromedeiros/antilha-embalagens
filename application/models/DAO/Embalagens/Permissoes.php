<?php

class App_Model_DAO_Embalagens_Permissoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'embalagens_permissoes';
	protected $_primary = array(
		'emb_perm_idEmbalagem', 
		'emb_perm_idGrupoRede', 
		'emb_perm_idRede', 
		'emb_perm_idGrupoLoja', 
		'emb_perm_idLoja', 
		'emb_perm_idLinha'
	);
	protected $_rowClass = 'App_Model_Entity_Embalagem_Permissao';
	
	protected $_referenceMap = array(
		'Embalagem' => array(
			self::COLUMNS => 'emb_perm_idEmbalagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Embalagens',
			self::REF_COLUMNS => 'emb_idEmbalagem'
		),
		'GrupoRede' => array(
			self::COLUMNS => 'emb_perm_idGrupoRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoRedes',
			self::REF_COLUMNS => 'grp_rede_idGrupo'
		),
		'Rede' => array(
			self::COLUMNS => 'emb_perm_idRede',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Redes',
			self::REF_COLUMNS => 'rede_idRede'
		),
		'GrupoLoja' => array(
			self::COLUMNS => 'emb_perm_idGrupoLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoLojas',
			self::REF_COLUMNS => 'grp_loja_idGrupo'
		),
		'Loja' => array(
			self::COLUMNS => 'emb_perm_idLoja',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Lojas',
			self::REF_COLUMNS => 'loja_idLoja'
		),
		'Linha' => array(
			self::COLUMNS => 'emb_perm_idLinha',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Linhas',
			self::REF_COLUMNS => 'lin_idLinha'
		)			
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Embalagens_Permissoes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}