<?php

class App_Model_DAO_Embalagens_Estados extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'embalagens_estados';
	protected $_primary = 'emb_est_idEmbalagem';
	protected $_rowClass = 'App_Model_Entity_Embalagem_Estado';
	
	protected $_referenceMap = array(
		'Embalagem' => array(
			self::COLUMNS => 'emb_est_idEmbalagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Embalagens',
			self::REF_COLUMNS => 'emb_idEmbalagem'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Embalagens_Estados
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}