<?php

class App_Model_DAO_Pesquisas extends App_Model_DAO_Abstract
{

    protected static $instance = null;
    protected $_name = 'pesquisas';
    protected $_primary = 'pes_idPesquisa';
    protected $_rowClass = 'App_Model_Entity_Pesquisa';
    protected $_dependentTables = array(
        'App_Model_DAO_Pesquisas_Perguntas',
        'App_Model_DAO_Pesquisas_Estados',
        'App_Model_DAO_Pesquisas_Permissoes'
    );
    protected $_referenceMap = array(
        'Imagem' => array(
            self::COLUMNS => 'pes_idImagem',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
            self::REF_COLUMNS => 'gal_arq_idArquivo'
        )
    );

    /**
     * Implementa��o do m�todo Singleton para obter a instancia da classe
     *
     * @return App_Model_DAO_Pesquisas
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}
