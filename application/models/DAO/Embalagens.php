<?php

class App_Model_DAO_Embalagens extends App_Model_DAO_Abstract
{

    protected static $instance = null;
    protected $_name = 'embalagens';
    protected $_primary = 'emb_idEmbalagem';
    protected $_rowClass = 'App_Model_Entity_Embalagem';
    protected $_dependentTables = array(
        'App_Model_DAO_Embalagens_Estados',
        'App_Model_DAO_Embalagens_Permissoes'
    );
    protected $_referenceMap = array(
        'Imagem' => array(
            self::COLUMNS => 'emb_idImagem',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
            self::REF_COLUMNS => 'gal_arq_idArquivo'
        ),
        'ImagemDestaque' => array(
            self::COLUMNS => 'emb_idImagemDestaque',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
            self::REF_COLUMNS => 'gal_arq_idArquivo'
        ),
        'Miniatura' => array(
            self::COLUMNS => 'emb_idMiniatura',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
            self::REF_COLUMNS => 'gal_arq_idArquivo'
        ),
        'Manual' => array(
            self::COLUMNS => 'emb_idManual',
            self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
            self::REF_COLUMNS => 'gal_arq_idArquivo'
        )
    );

    /**
     * Implementa��o do m�todo Singleton para obter a instancia da classe
     *
     * @return App_Model_DAO_Embalagens
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}
