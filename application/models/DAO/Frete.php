<?php

class App_Model_DAO_Frete extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'frete';
	protected $_primary = 'frete_codigo';
	protected $_rowClass = 'App_Model_Entity_Frete';
	
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}