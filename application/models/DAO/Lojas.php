<?php

class App_Model_DAO_Lojas extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'lojas';
	protected $_primary = 'loja_idLoja';
	protected $_rowClass = 'App_Model_Entity_Loja';
	
	protected $_dependentTables = array(
		'App_Model_DAO_Lojas_Enderecos',
		'App_Model_DAO_Lojas_Contatos'
	);
	
	protected $_referenceMap = array(
		'Grupo' => array(
			self::COLUMNS => 'loja_idGrupo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoLojas',
			self::REF_COLUMNS => 'grp_loja_idGrupo'
		),
		'Tipo' => array(
			self::COLUMNS => 'loja_idTipo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_TiposLoja',
			self::REF_COLUMNS => 'tipo_loja_idTipo'
		)		
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Lojas
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}