<?php

class App_Model_DAO_Videos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'videos';
	protected $_primary = 'vid_idVideo';
	protected $_rowClass = 'App_Model_Entity_Video';
	
	protected $_dependentTables = array(
		'App_Model_DAO_Videos_Estados',
		'App_Model_DAO_Videos_Permissoes'
	);
	
	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Videos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}