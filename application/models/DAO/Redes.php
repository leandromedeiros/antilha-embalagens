<?php

class App_Model_DAO_Redes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'redes';
	protected $_primary = 'rede_idRede';
	protected $_rowClass = 'App_Model_Entity_Rede';

	protected $_dependentTables = array(
		'App_Model_DAO_GrupoLojas'
	);
	
	protected $_referenceMap = array(
		'Grupo' => array(
			self::COLUMNS => 'rede_idGrupo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_GrupoRedes',
			self::REF_COLUMNS => 'grp_rede_idGrupo'
		),
		'Imagem' => array(
			self::COLUMNS => 'rede_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Redes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}