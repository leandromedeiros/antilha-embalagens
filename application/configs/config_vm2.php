<?php
$BASE_HREF = 'http://www.homolog.antilhaspedidos.com.br';

return array(
    'project' => 'Antilhas',
    'webservice' =>  $BASE_HREF . '/webservice/soap?wsdl',
    'mail' => array(
        'pendencias' => 'rodrigo.lacerda@vm2.com.br'
    ),
    'paths' => array(
        'admin' => array(
            'root' => APPLICATION_ROOT .'/modules/admin/views',
            'file' => APPLICATION .'/public_html/admin',
            'base' => $BASE_HREF. '/admin'
        ),
        'site' => array(
            'root' => APPLICATION_ROOT .'/modules/site/views',
            'file' => APPLICATION .'/public_html',
            'base' => $BASE_HREF
        )
    ),
    'avisoproduto' => array(
        'email' => 'gabriel.goncalves@antilhas.com.br',
        'nome' => 'Gabriel Gon�alves'
    ),
    'documentos' => array(
        'notasFiscais' => APPLICATION_ROOT . '/documentos/notas-fiscais/',
        'xmls' => APPLICATION_ROOT . '/documentos/xmls/',
        'relatorios' => APPLICATION_ROOT . '/documentos/relatorios-gerenciais/',
    )
);