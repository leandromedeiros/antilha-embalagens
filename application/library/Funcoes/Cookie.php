<?php

class App_Funcoes_Cookie
{
	public static function getCookie($nome)
	{		
		$retorno = '';
		$cookieData = Zend_Controller_Request_Http::getCookie($nome);
		if($cookieData != null) {
			$retorno = Zend_Json::decode($cookieData);
			App_Funcoes_UTF8::encode($retorno);
		}
		return $retorno;
	}
	
	public static function setCookie($name, $value = array())
	{		
		$historico = App_Funcoes_Cookie::getCookie($name);
		if($historico != null) {				
			array_push($historico, $value);
		} else {
			$historico = array();
			$historico[] = $value;
		}			
		App_Funcoes_UTF8::encode($historico);
		$historico = Zend_Json::encode($historico);	
		setcookie($name, $historico, time() + 3600, '/', Zend_Registry::get('config')->paths->base);
	}
	
}