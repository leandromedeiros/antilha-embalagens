<?php

class App_Funcoes_Rotulos
{

	public static $canal = array('0' => 'Portal de Pedidos', '1' => 'E-mail', '2' => 'Telefone', '3' => 'Outros'  );
    public static $status = array('1' => 'Ativo', '0' => 'Inativo');
    public static $statusPed = array('B' => 'Aguardando Pagamento');
    public static $formaEnvio = array('SMTP' => 'SMTP', 'MAIL' => 'PHP MAIL');
    public static $statusSimNao = array('1' => 'Sim', '0' => 'N&atilde;o');
    public static $tipoPergunta = array('M' => 'M&uacute;ltipla Escolha', 'D' => 'Descri&ccedil;&atilde;o');
    public static $tipoAlternativa = array('N' => 'Normal', 'O' => 'Outro');
    public static $statusReclamacao = array(
        '0' => 'N&atilde;o Gostei',
        '1' => 'Gostei',
        '2' => 'Sem opini&atilde;o'
    );
    public static $permissoesManuais = array(
        App_Model_Entity_Sistema_Usuario::PodeCriarPedidosComAprovacao => 'Fazer pedidos com aprova&ccedil;&atilde;o',
        App_Model_Entity_Sistema_Usuario::PodeCriarPedidosSemAprovacao => 'Autonomia para fazer pedido',
        App_Model_Entity_Sistema_Usuario::PodeAprovarPedidosSemQuantidade => 'Aprovar pedidos',
        App_Model_Entity_Sistema_Usuario::PodeAprovarPedidosComQuantidade => 'Aprovar pedidos e alterar quantidade',
        App_Model_Entity_Sistema_Usuario::PodeAprovarPedidosIntermediario => 'Aprovador Duplo', //
    );
    public static $statusPedidosResumo = array(
        '0' => 'Sem status',
        '1' => 'Pedido recebido em processamento',
        '2' => 'Pedido separado e faturado',
        '3' => 'Pedido em transporte',
        '4' => 'Pedido entregue'
    );
    public static $pedidosXresumos = array(
        '0' => '0',
        '1' => '1',
        '2' => '1',
        '3' => '0',
        '4' => '2',
        '5' => '2',
        '6' => '3',
        '8' => '3',
        '9' => '4',
        '10' => '0',
        '11' => '0'
    );
    public static $assuntosEmailsPedidos = array(
        '1' => 'Pedido recebido em processamento',
        '3' => 'Pedido em an&aacute;lise de cr&eacute;dito',
        '4' => 'Pedido faturado',
        '5' => 'Pedido faturado parcialmente',
        '6' => 'Pedido em transporte',
        '8' => 'Pedido bloqueado pela Sefaz',
        '9' => 'Pedido entregue',
        '18' => 'Rejeitado por cr&eacute;dito'
    );
    public static $statusPedidos = array(
        '0' => 'Aguardando Aprova&ccedil;&atilde;o',
        '1' => 'Em Processamento', // Sap n&atilde;o processou no nosso sistema ira puxar somente esse status do webservice
        '2' => 'Aprovado', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o nosso processamento
        '3' => 'Em an&aacute;lise de cr&eacute;dito', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o nosso processamento se o cliente tiver problemas de credito
        '4' => 'Faturado', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o faturamento
        '5' => 'Faturado Parcialmente', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o faturamento parcial
        '6' => 'Em Transporte', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o embarque transportadora
        '8' => 'Bloqueado Sefaz', // Processado pelo SAP: vamos mandar esse status se o pedido for parado na fiscaliza&ccedil;&atilde;o
        '9' => 'Entregue', // Processado pelo SAP: vamos mandar esse status ap&oacute;s a entrega
        '10' => 'Rejeitado por cr&eacute;dito',
        '11' => 'Reprovado',
        '12' => 'Aguardando Aprova&ccedil;&atilde;o Intermedi&aacute;ria' // Aprova&ccedil;&atilde;o Intermedi&aacute;ria: caso a loja tenha um aprovador intermedi&aacute;rio
    );
	
	public static $statusPedidosAbertos = array(
        '0' => 'Aguardando Aprova&ccedil;&atilde;o',
        '1' => 'Em Processamento', // Sap n&atilde;o processou no nosso sistema ira puxar somente esse status do webservice
        '2' => 'Aprovado', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o nosso processamento
        '3' => 'Em an&aacute;lise de cr&eacute;dito', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o nosso processamento se o cliente tiver problemas de credito
        '4' => 'Faturado', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o faturamento
        '5' => 'Faturado Parcialmente', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o faturamento parcial
        '6' => 'Em Transporte', // Processado pelo SAP: vamos mandar esse status ap&oacute;s o embarque transportadora
    );
	
	public static $statusPedidosFechados = array(
		'8' => 'Bloqueado Sefaz', // Processado pelo SAP: vamos mandar esse status se o pedido for parado na fiscaliza&ccedil;&atilde;o
		'9' => 'Entregue', // Processado pelo SAP: vamos mandar esse status ap&oacute;s a entrega
        '10' => 'Rejeitado por cr&eacute;dito',
        '11' => 'Reprovado',
        '12' => 'Aguardando Aprova&ccedil;&atilde;o Intermedi&aacute;ria' // Aprova&ccedil;&atilde;o Intermedi&aacute;ria: caso a loja tenha um aprovador intermedi&aacute;rio
	);
	
    public static $statusSugestoesReclamacoes = array(
        '0' => 'Em aberto',
        '1' => 'Em andamento',
        '2' => 'Finalizado'
    );
    public static $grupos = array(
        'GR' => 'Grupos de Rede',
        'RE' => 'Redes',
        'GL' => 'Grupos de Loja',
        'LO' => 'Lojas',
        'LI' => 'Linhas'
    );
    public static $assuntoReclamacao = array(
        '0' => 'Assunto 1',
        '1' => 'Assunto 2',
        '2' => 'Assunto 3'
    );
    public static $UF = array(
        'AC' => 'Acre',
        'AL' => 'Alagoas',
        'AM' => 'Amazonas',
        'AP' => 'Amap&aacute;',
        'BA' => 'Bahia',
        'CE' => 'Cear&aacute;',
        'DF' => 'Distrito Federal',
        'ES' => 'Esp&iacute;rito Santo',
        'GO' => 'Goi&aacute;s',
        'MA' => 'Maranh&atilde;o',
        'MG' => 'Minas Gerais',
        'MS' => 'Mato Grosso do Sul',
        'MT' => 'Mato Grosso',
        'PA' => 'Par&aacute;',
        'PB' => 'Para&iacute;ba',
        'PE' => 'Pernambuco',
        'PI' => 'Piau&iacute;',
        'PR' => 'Paran&aacute;',
        'RJ' => 'Rio de Janeiro',
        'RN' => 'Rio Grande do Norte',
        'RO' => 'Rond&ocirc;nia',
        'RR' => 'Roraima',
        'RS' => 'Rio Grande do Sul',
        'SC' => 'Santa Catarina',
        'SE' => 'Sergipe',
        'SP' => 'S&atilde;o Paulo',
        'TO' => 'Tocantins'
    );
    public static $regioes = array(
        'CO' => 'CENTRO-OESTE',
        'NT' => 'NORTE',
        'ND' => 'NORDESTE',
        'SL' => 'SUL',
        'SD' => 'SUDESTE'
    );
    public static $meses = array(
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Mar&ccedil;o',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro'
    );
    public static $tipoSugestaoReclamacao = array(
        '1' => 'Sugest&atilde;o',
        '2' => 'Reclama&ccedil;&atilde;o'
    );

}
