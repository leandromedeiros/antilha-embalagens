<?php

class App_Funcoes_Ordenar {
	
	static function ordenar($itens) {
		$count = 0;
		while($count < $itens->count()-1) {
			if($itens->offsetGet($count)->getProduto()->getSequenciaItens() > $itens->offsetGet($count+1)->getProduto()->getSequenciaItens()) {
				$temp = $itens->offsetGet($count);
				$itens->offsetSet($count, $itens->offsetGet($count+1));
				$itens->offsetSet($count+1, $temp);
				return App_Funcoes_Ordenar::ordenar($itens);
			}
			$count++;
		}
		return $itens;
	}
}