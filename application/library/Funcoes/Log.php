<?php

class App_Funcoes_Log
{
	/**
	 * @param string $tipo
	 * @param Exception $e
	 * @param mixed $sessao
	 */
	public static function email($tipo, $e, $sessao = null)
	{
		try {
			// Envia o email
			$template = new Zend_View();
			$template->setBasePath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'views');
		
			$template->erros = $e;
			$template->tipo = $tipo;
			$template->sessao = $sessao;
		
			$mailTransport = new App_Model_Email();
			$mail = new Zend_Mail('ISO-8859-1');
			$mail->setFrom(Zend_Registry::get('configInfo')->emailRemetente, Zend_Registry::get('config')->project);
			foreach (Zend_Registry::get('config')->log as $email => $nome) {
				$mail->addTo($email, $nome);
			}
		
			$mail->setSubject(Zend_Registry::get('config')->project . ' - Log de Erro');
			$mail->setBodyHtml($template->render('emails/log-de-erro.phtml'));
			$mail->send($mailTransport->getFormaEnvio());
		} catch (Exception $e) {}
	}
	
}