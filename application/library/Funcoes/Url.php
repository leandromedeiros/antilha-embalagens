<?php

class App_Funcoes_Url
{

    /**
     * @param int $pagina
     * @param array $params
     * @return string
     */
    public static function link($pagina = null, array $params = null, array $ignore = null)
    {
        $queryParams = array();
        foreach ($_GET as $k => $v) {
            if ((!is_array($params) || !in_array($k, array_keys($params)))) {
                if ($ignore == null || (is_array($ignore) && !in_array($k, $ignore))) {
                    $queryParams[] = sprintf("%s=%s", htmlentities(urldecode($k)), htmlentities(urldecode($v)));
                }
            }
        }

        if (null != $params && count($params)) {
            foreach ($params as $k => $v) {
                $queryParams[] = sprintf("%s=%s", htmlentities(urldecode($k)), htmlentities(urldecode($v)));
            }
        }

        $queryString = implode('&', $queryParams);
        return "{$pagina}?{$queryString}";
    }

}
