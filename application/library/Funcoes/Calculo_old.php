<?php

class App_Funcoes_Calculo
{

    public static function calculo($valor, $ccf, $icms, $ipi)
    {
        if ($ccf != '') {
            if ($icms) {
                $valor = $valor / $icms;
            }
            if ($ipi > 0) {
                $ipi = (($valor * $ipi) / 100);
                $valor = $valor + $ipi;
            }
        }
        return $valor;
    }

}
