<?php

class App_Funcoes_Date
{
	/**
	 * Conversor o formato da data de dd/mm/aaaa para aaaa/mm/dd e vice-versa
	 *
	 * @param string $date dd/mm/aaaa | aaaa-mm-dd [yy:m:ss]
	 * @return string
	 */
	public static function conversion($date, $boolTime = true)
	{
		if (strlen($date)) {
			$time = null;
			if (strlen($date) > 10) {
				$time = substr($date, 10);
				$date = substr($date, 0, 10);
			}

			$token = strpos($date, '/') ? '/' : '-';
			$tmp = explode($token, $date);
			foreach ($tmp as &$val) {
				$val = str_pad($val, 2, 0, STR_PAD_LEFT);
			}

			
			return $boolTime ? trim(implode(($token == '-' ? '/' : '-'), array_reverse($tmp)) ." {$time}") : trim(implode(($token == '-' ? '/' : '-'), array_reverse($tmp)) );
		}
	}

	/**
	 * Retorna a diferen�a entre datas
	 *
	 * @param date $data1 Menor data no formato americano Y-m-d
	 * @param date $data2 Maior data no formato americano Y-m-d
	 * @param char $invalo Tipo de invalo: m=mes, d=dia, h=hora, n=minuto
	 * @return integer
	 */
	static public function diff($data1, $data2, $invalo)
	{
		$q = 1;
		switch ($invalo) {
			case 'm': $q *= 30;
			case 'd': $q *= 24;
			case 'h': $q *= 60;
			case 'n': $q *= 60;
		}
		return intval((strtotime($data2) - strtotime($data1)) / $q);
	}
	
	/**
	CALCULANDO DIAS NORMAIS
	Abaixo vamos calcular a diferen�a entre duas datas. Fazemos uma revers�o da maior sobre a menor
	para n�o termos um resultado negativo. 
	*/
	static public function CalculaDias($xDataInicial, $xDataFinal){
		$time1 = App_Funcoes_Date::dataToTimestamp($xDataInicial);  
		$time2 = App_Funcoes_Date::dataToTimestamp($xDataFinal);  
	
		$tMaior = $time1 > $time2 ? $time1 : $time2;  
		$tMenor = $time1 < $time2 ? $time1 : $time2;  
	
		$diff = $tMaior-$tMenor;  
		$numDias = $diff/86400; //86400 � o n�mero de segundos que 1 dia possui  
		return $numDias;
	}
	
	//LISTA DE FERIADOS NO ANO
	/*Abaixo criamos um array para registrar todos os feriados existentes durante o ano.*/
	static public function Feriados($ano, $posicao, $apenasFeriados = false){
		$dia = 86400;
		$datas = array();
		$datas['pascoa'] = easter_date($ano);
		$datas['sexta_santa'] = $datas['pascoa'] - (2 * $dia);
		$datas['carnaval'] = $datas['pascoa'] - (47 * $dia);
		$datas['corpus_cristi'] = $datas['pascoa'] + (60 * $dia);
		$feriados = array (
			'01/01',
			date('d/m', $datas['carnaval']),
			date('d/m', $datas['sexta_santa']),
			//date('d/m', $datas['pascoa']),
			'21/04',
			'01/05',
			date('d/m', $datas['corpus_cristi']),
			'07/09', 
			'12/10',
			'02/11',
			'15/11',
			'25/12'
		);
		
		if($apenasFeriados) {
			foreach ($feriados as &$feriado) {
				if(substr($feriado, 0, 1) == '0') $feriado = substr($feriado, 1);
				$feriado = str_replace('/0', '/', $feriado);
			} 
			
			return $feriados;	
		}
		return $feriados[$posicao]."/".$ano;
	}  

	//FORMATA COMO TIMESTAMP
	/*Esta fun��o � bem simples, e foi criada somente para nos ajudar a formatar a data j� em formato  TimeStamp facilitando nossa soma de dias para uma data qualquer.*/
	static public function dataToTimestamp($data){
		$ano = substr($data, 6,4);
		$mes = substr($data, 3,2);
		$dia = substr($data, 0,2);
		return mktime(0, 0, 0, $mes, $dia, $ano);  
	} 
	
	//SOMA 01 DIA  
	static public function Soma1dia($data){  
		$ano = substr($data, 6,4);
		$mes = substr($data, 3,2);
		$dia = substr($data, 0,2);
		return date("d/m/Y", mktime(0, 0, 0, $mes, $dia+1, $ano));
	}
	
	//SUBTRAI 01 DIA  
	static public function Subtrai1dia($data){  
		$ano = substr($data, 6,4);
		$mes = substr($data, 3,2);
		$dia = substr($data, 0,2);
		return date("d/m/Y", mktime(0, 0, 0, $mes, $dia-1, $ano));
	}
	
	static public function SubtraiDiasUteis($xDataInicial, $xSomarDias){
		for($j = 1; $j <= $xSomarDias; $j++) {
			$xDataInicial = App_Funcoes_Date::Subtrai1dia($xDataInicial); //SOMA DIA NORMAL
			//VERIFICANDO SE EH DIA DE TRABALHO
			if(date("w", App_Funcoes_Date::dataToTimestamp($xDataInicial)) == "0") {
				//SE DIA FOR DOMINGO SUBTRAI +2
				$xDataInicial = App_Funcoes_Date::Subtrai1dia($xDataInicial);
				$xDataInicial = App_Funcoes_Date::Subtrai1dia($xDataInicial);
        
				//verifica se o proximo dia eh feriado
				for($i=0; $i <= 11; $i++){ 
					if($xDataInicial == App_Funcoes_Date::Feriados(substr($xDataInicial, 6), $i)) {
						$xDataInicial = App_Funcoes_Date::Subtrai1dia($xDataInicial);	
					}
				}
			} else if(date("w", App_Funcoes_Date::dataToTimestamp($xDataInicial)) == "6") {
				//SE DIA FOR SUBTRAI, SOMA +1
				$xDataInicial = App_Funcoes_Date::Subtrai1dia($xDataInicial);
				
				//verifica se o proximo dia eh feriado
				for($i=0; $i <= 11; $i++) { 
					if($xDataInicial == App_Funcoes_Date::Feriados(substr($xDataInicial, 6), $i)) {
						$xDataInicial = App_Funcoes_Date::Subtrai1dia($xDataInicial);	
					}
				}
        
			} else {
				//senaum vemos se este dia eh FERIADO
				for($i=0; $i <= 11; $i++){ 
					if($xDataInicial == App_Funcoes_Date::Feriados(substr($xDataInicial, 6), $i)) {
						// se o feriado for segunda, subtrai 3 dias
						if(date("w", App_Funcoes_Date::dataToTimestamp($xDataInicial)) == "1") {
							$xDataInicial = App_Funcoes_Date::Subtrai1dia($xDataInicial);
							$xDataInicial = App_Funcoes_Date::Subtrai1dia($xDataInicial);
						}
						
						$xDataInicial = App_Funcoes_Date::Subtrai1dia($xDataInicial);
					}
				}
			}
		}
		
		return $xDataInicial;
	}
	
	static public function SomaDiasUteis($xDataInicial, $xSomarDias){
		for($j = 1; $j <= $xSomarDias; $j++){
			$xDataInicial = App_Funcoes_Date::Soma1dia($xDataInicial); //SOMA DIA NORMAL
			
			//VERIFICANDO SE EH DIA DE TRABALHO
			if(date("w", App_Funcoes_Date::dataToTimestamp($xDataInicial)) == "0"){
				//SE DIA FOR DOMINGO OU FERIADO, SOMA +2
				$xDataInicial = App_Funcoes_Date::Soma1dia($xDataInicial);
        
			} else if(date("w", App_Funcoes_Date::dataToTimestamp($xDataInicial)) == "6") {
				//SE DIA FOR SABADO, SOMA +2
				$xDataInicial = App_Funcoes_Date::Soma1dia($xDataInicial);
				$xDataInicial = App_Funcoes_Date::Soma1dia($xDataInicial);
        
			} else {
				//senaum vemos se este dia eh FERIADO
				for($i=0; $i<=11; $i++){ 
					if($xDataInicial == App_Funcoes_Date::Feriados(substr($xDataInicial, 6), $i)) {
						//se o feriado for sexta, soma 3 dias
						if(date("w", App_Funcoes_Date::dataToTimestamp($xDataInicial)) == "5") {
							$xDataInicial = App_Funcoes_Date::Soma1dia($xDataInicial);
							$xDataInicial = App_Funcoes_Date::Soma1dia($xDataInicial);
						}
						$xDataInicial = App_Funcoes_Date::Soma1dia($xDataInicial);
					}
				}
			}
		}
		return $xDataInicial;
	}
	
	//CALCULA DIAS UTEIS
	/*� nesta fun��o que faremos o calculo. Abaixo podemos ver que faremos o c�lculo normal de dias ($calculoDias), ap�s este c�lculo, faremos a compara��o de dia a dia, verificando se este dia � um s�bado, domingo ou feriado e em qualquer destas condi��es iremos incrementar 1*/
	static public function DiasUteis($yDataInicial, $yDataFinal) {
		$time1 = App_Funcoes_Date::dataToTimestamp($yDataInicial);  
		$time2 = App_Funcoes_Date::dataToTimestamp($yDataFinal); 
		
		//se data inicial maior que data final retorna 0
		if($time1 > $time2) return 0;
		
		$diaFDS = 0; //dias n�o �teis(S�bado=6 Domingo=0)
		$calculoDias = App_Funcoes_Date::CalculaDias($yDataInicial, $yDataFinal); //n�mero de dias entre a data inicial e a final
		$diasUteis = 0;
  
		while($yDataInicial != $yDataFinal) {
			$diaSemana = date("w", App_Funcoes_Date::dataToTimestamp($yDataInicial));
			if($diaSemana==0 || $diaSemana==6) {
				//se SABADO OU DOMINGO, SOMA 01
				$diaFDS++;
			} else {
				//sen�o vemos se este dia � FERIADO
				for($i=0; $i<=11; $i++){
					if($yDataInicial == App_Funcoes_Date::Feriados(date("Y"),$i)){
               			$diaFDS++;  
					}
				}
			}
			$yDataInicial = App_Funcoes_Date::Soma1dia($yDataInicial); //dia + 1
		}
		return $calculoDias - $diaFDS;
	}
}