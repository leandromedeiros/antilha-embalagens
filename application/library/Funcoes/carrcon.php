<?php
/**********************************************
* Classe: function_db
* Empresa: VM2
* Autor: Rodrigo Menezes
* Cria��o: 11/08/2005
* �ltima Atualiza��o: 11/08/2005
* Descric�o: Classe de conex�o com o banco.
**********************************************/

define (__SERVER__,"186.202.123.70");
define (__USER__,"antilhas_homol02");
define (__PASSW__,"saknr@387");
define (__DATABASE__,"antilhas_homol02");


class function_db {

	function connectDb(){
		$this->connection = mysql_connect(__SERVER__, __USER__, __PASSW__);
	  if ($this->connection == false) {
			return false;
		}else {
			mysql_select_db(__DATABASE__);
		}
	}

	function closeDb(){
		mysql_close($this->connection);
	}

	function showRecords($query,$grid = false){

		$this->connectDb();
		if ($this->connection === false) return false;

		$array = array();
		$result = mysql_query($query,$this->connection);

		if ($result === false) return false;

		if (($grid == true) && (mysql_num_rows($result) > 0)){
			while ($row = mysql_fetch_assoc($result)) $array[] = $row;
			$return = $array;
		}
		if ($grid == false && mysql_num_rows($result) > 0)
			$return = mysql_fetch_assoc($result);

		if (mysql_num_rows($result) == 0)
			$return = false;

		mysql_free_result($result);
		$this->closeDb();
		return $return;

	}

	function saveRecords($query){
		$erro = $this->connectDb();
		if ($this->connection == false) return false;
		$result = mysql_query($query,$this->connection) || die("Erro >>>>".mysql_error($this->connection));
		$this->closeDb();
		return $result;
	}

	function saveMultipleRecords($querys,$rt = true){
		$erro = $this->connectDb();
		if ($this->connection == false) return false;
		$result = mysql_query('START TRANSACTION',$this->connection);
		foreach($querys as $key => $query){
			$result = mysql_query($query,$this->connection);
			if ($result == false){
				$result = mysql_query('ROLLBACK',$this->connection);
				$this->closeDb();
				return $query;
			}
		}
		if($rt)
			$return = mysql_fetch_assoc($result);
		else
			$return = true;
		$result = mysql_query('COMMIT',$this->connection);
		$this->closeDb();
		return $return;
	}

	function deleteRecords($query){
		$erro = $this->connectDb();
		if ($this->connection == false) return false;
		$result = mysql_query($query);
		$this->closeDb();
		return $result;
	}

	function searchRecords($table,$field,$value,$order = 'ASC'){
		$pos = strpos($value,'*');
		if(($pos == 0) && ($pos !== false)){
			//echo '1 opcao<br>';
			$query = "SELECT * FROM $table WHERE $field LIKE '%".substr($value,0)."' ORDER by $field $order";
		}elseif($pos == (strlen($value)-1)){
			//echo '2 opcao<br>';
			$query = "SELECT * FROM $table WHERE $field LIKE '".substr($value,0,strlen($value)-1)."%' ORDER by $field $order";
		}elseif($pos === false){
			//echo '3 opcao<br>';
			$query = "SELECT * FROM $table WHERE $field LIKE '%$value%' ORDER by $field $order";
		}else{
			return false;
		}
		//echo $query;exit;
		return $this->showRecords($query,true);
	}
}

class CarrGrav extends function_db{

	function ValidaLoja($loja){
			
    		$sql = "SELECT loja_idLoja , loja_idGrupo from antilhas_lojas where loja_idLoja = '$loja'";
          //  echo $sql; 
			$ret = parent::showRecords($sql);
       		return $ret['loja_idGrupo'];
                 
    	}
    	
	function ValidaMat($mat,$loja,$opcao){
			
    		$sql = "select a.prod_idLinha , b.prec_idtipo , b.prec_preco , c.loja_idTipo  
                    from antilhas_produtos as a 
                    inner JOIN antilhas_precos as b on a.prod_idProduto = b.prec_idProduto
                    inner JOIN antilhas_lojas  as c on c.loja_idTipo    = b.prec_idTipo 
                    where a.prod_idProduto = '$mat' and c.loja_idLoja   = $loja";
           
			$ret = parent::showRecords($sql);
       		if ($opcao == 'P') { 
       			return $ret['prec_preco']; 
       			};
       		
       		if ($opcao == 'L') { 
       			return $ret['prod_idLinha']; 
       			};

    	}
    	
   	
   	function GravaCar($grupo,$linha,$user){
			
			$sql = "delete from antilhas_carrinho where car_idusuario = $user and car_idPedido is null"; 
			//echo $sql; 
			 
			$ret = parent::deleteRecords($sql);
			
			$sql = "insert into antilhas_carrinho ( car_dataCadastro , car_idLinha , car_idusuario , car_idLoja,  car_grade )
					select now() , '$linha' , '$user' , loja_idLoja , 1 
					from antilhas_lojas where loja_idGrupo = $grupo";

	    	$ret = parent::saveRecords($sql);
       		return $ret;

    	}
      
     function GravaCarIte($mat,$loja,$qtde,$pedido,$preco,$linha,$user){
			
    		$sql = "insert into antilhas_carrinho_itens ( car_item_idCarrinho , car_item_idProduto , car_item_qtde,car_item_preco  )
					select car_idCarrinho , '$mat' , '$qtde' , '$preco'
				    from antilhas_carrinho where car_idLoja = '$loja' and car_idLinha = '$linha' and car_idusuario = '$user'";
           
            //echo $sql; 
		    $ret = parent::saveRecords($sql);
       		return $ret;

    	}

}
