<?php

class App_Validate_Loja_Email extends Zend_Validate_Abstract
{
	const NOT_RECOGNIZED = 'notRecognized';

	/**
	 * @var App_Model_Entity_Loja
	 */
	protected $loja;

	protected $_messageTemplates = array(
		self::NOT_RECOGNIZED => "Este e-mail j&aacute; est&aacute; sendo utilizado por outra loja"
	);

	public function __construct(App_Model_Entity_Loja $loja)
	{
		$this->loja = $loja;
	}

	public function isValid($value)
	{
		$daoLojas = App_Model_DAO_Lojas::getInstance();
		$loja = $daoLojas->fetchRow(
			$daoLojas->select()->where('loja_email = ?', $value)
		);
		if (null != $loja) {
			if ($this->loja->getCodigo() == $loja->getCodigo()) {
				return true;
			} else {
				$this->_error(self::NOT_RECOGNIZED);
				return false;
			}
		} else {
			return true;
		}
    }
}