<?php

class App_Validate_Usuario_Login extends Zend_Validate_Abstract
{
	const NOT_RECOGNIZED = 'notRecognized';

	/**
	 * @var App_Model_Entity_Sistema_Usuario
	 */
	protected $usuario;

	protected $_messageTemplates = array(
		self::NOT_RECOGNIZED => "Este login j&aacute; est&aacute; sendo utilizado por outro usu�rio"
	);

	public function __construct(App_Model_Entity_Sistema_Usuario $usuario)
	{
		$this->usuario = $usuario;
	}

	public function isValid($value)
	{
		$daoUsuarios = App_Model_DAO_Sistema_Usuarios::getInstance();
		$usuario = $daoUsuarios->fetchRow(
			$daoUsuarios->select()->where('usr_login = ?', $value)
		);
		if (null != $usuario) {
			if ($this->usuario->getCodigo() == $usuario->getCodigo()) {
				return true;
			} else {
				$this->_error(self::NOT_RECOGNIZED);
				return false;
			}
		} else {
			return true;
		}
    }
}