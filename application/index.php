<?php

if (!defined('__DIR__')) {
   define('__DIR__', dirname(__FILE__));
}

$cookie = session_get_cookie_params();
session_set_cookie_params($cookie['lifetime'], $cookie['path'], $cookie['domain'], $cookie['secure'], true);

header('Content-type: text/html; charset=ISO-8859-1');
date_default_timezone_set('America/Sao_Paulo');
session_save_path(__DIR__ . '/../sessions');

set_include_path(
    implode(PATH_SEPARATOR, array(
    realpath(dirname(__FILE__) . '/library'),
    realpath(dirname(__FILE__) . '/library/Pear'),
    realpath(dirname(__FILE__) . '/library/Braspag'),
    get_include_path(),
    ))
);


defined('APPLICATION') ||
    define('APPLICATION', str_replace('application', '', realpath(dirname(__FILE__))));

defined('APPLICATION_ROOT') ||
    define('APPLICATION_ROOT', realpath(dirname(__FILE__)));

defined('APPLICATION_PATH') ||
    define('APPLICATION_PATH', sprintf('%s%smodules%s%s', APPLICATION_ROOT, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, (getenv('MODULE') ? getenv('MODULE') : 'site')));

defined('APPLICATION_ENV') ||
    define('APPLICATION_ENV', (getenv('ENVIRONMENT') ? getenv('ENVIRONMENT') : 'production'));

/** Zend_Application */
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV, APPLICATION_ROOT . '/configs/application.ini'
);

$application->bootstrap();
$application->run();
